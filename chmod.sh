#!/bin/sh
chmod 777 -R backend/web/uploads
chmod 777 -R backend/web/images
chmod 777 -R frontend/web/images
chmod 777 -R frontend/web/booking/rooms
chmod 777 -R frontend/web/booking/uploads
chmod 777 -R vendor/SORApp/Runtime/compiled
chmod 777 -R vendor/SORApp/Runtime/logs
chmod 777 -R vendor/SORApp/Runtime/sessions
chmod 777 -R frontend/runtime
chmod 777 -R backend/runtime

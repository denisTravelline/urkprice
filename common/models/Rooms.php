<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use himiklab\yii2\search\behaviors\SearchBehavior;
use common\models\RoomsFeatures;

/**
 * This is the model class for table "servio_rooms".
 *
 * @property integer $id
 * @property integer $hotelId
 * @property integer $servioId
 * @property string $name
 * @property integer $beds
 * @property integer $extBeds
 * @property integer $visible
 * @property string $photo
 * @property integer $roomType
 * @property integer $hotelType
 * @property string $slug
 * @property string $city
 * @property string $address
 * @property string $phone
 * @property string $stars
 * @property integer $lang_id
 * @property integer $inRoom
 * @property integer $roomDesc
 * @property integer $costRoom
 */
class Rooms extends \yii\db\ActiveRecord
{
    /**
     *  Rooms statuses
     */
    const ROOMS_STATUS_INVISIBLE = 0;
    const ROOMS_STATUS_VISIBLE   = 1;
    
    /**
     * Room features
     * @var array
     */
    public $roomFeatures;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'servio_rooms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hotelId', 'servioId', 'beds', 'extBeds', 'visible', 'roomType', 'hotelType', 'lang_id', 'stars'], 'integer'],
            [['name', 'photo', 'slug', 'city', 'address', 'phone', 'inRoom', 'roomDesc', 'costRoom'], 'string', 'max' => 128],
            [['roomFeatures'], 'each', 'rule' => ['integer']],
            [['servioId', 'lang_id'], 'unique', 'targetAttribute' => ['servioId', 'lang_id'], 'message' => 'The combination of Offer ID and Lang ID has already been taken.'],
            [['lang_id', 'slug'], 'unique', 'targetAttribute' => ['lang_id', 'slug'], 'message' => 'The combination of Lang ID and Slug has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hotelId' => 'Отель',
            'servioId' => 'Servio ID',
            'name' => 'Название',
            'beds' => 'Beds',
            'extBeds' => 'Ext Beds',
            'visible' => 'Visible',
            'photo' => 'Photo',
            'roomType' => 'Room Type',
            'hotelType' => 'Hotel Type',
            'roomFeatures' => 'Характеристики',
            'lang_id' => 'Lang ID',
            'slug' => 'Slug',
            'city' => 'City',
            'address' => 'Address',
            'phone' => 'Phone',
            'stars' => 'Stars',
            'inRoom' => 'inRoom',
            'roomDesc' => 'roomDesc',
            'costRoom' => 'costRoom'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select([
                        'name',
                        'lang_id'
                    ]);
                    $model->leftJoin('rooms', '{{servio}}.id = {{rooms_descriptions}}.servioID');
                    $model->andWhere(['{{rooms}}.status' => Rooms::ROOM_STATUS_VISIBLE]);
                },
                'searchFields' => function ($model) {
                    /** @var self $model */
                    return [
                        ['name' => 'name', 'value' => $model->name],
                        ['name' => 'model', 'value' => 'rooms', 'type' => SearchBehavior::FIELD_UNINDEXED],
                        ['name' => 'langId', 'value' => $model->lang_id, 'type' => SearchBehavior::FIELD_UNINDEXED],
                    ];
                }
            ],
        ];
    }
    
    /**
     * The hotels features relation
     * @return mixed
     */
    public function getFeatures()
    {
        return $this->hasMany(RoomsFeatures::className(), ['room_id' => 'id']);
    }
}

<?php

namespace common\models;

use common\models\Languages;
use common\models\FeaturesDescriptions;

/**
 * This is the model class for table "sanatoriums_features".
 *
 * @property integer $id
 * @property integer $sanatorium_id
 * @property integer $feature_id
 */
class SanatoriumsFeatures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sanatoriums_features';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sanatorium_id', 'feature_id'], 'integer'],
            [['sanatorium_id', 'feature_id'], 'unique', 'targetAttribute' => ['sanatorium_id', 'feature_id'], 'message' => 'The combination of Sanatorium ID and Feature ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sanatorium_id' => 'Sanatorium ID',
            'feature_id' => 'Feature ID',
        ];
    }
    
    /**
     * The feature description relation
     * @param type $code
     * @return type
     */
    public function getDescr($code = null)
    {
        $langId = Languages::getLangId($code);

        return $this->hasOne(FeaturesDescriptions::className(), ['feature_id' => 'feature_id'])
                ->where('{{features_descriptions}}.lang_id = :lang_id', [':lang_id' => $langId]);
    }
    /**
     * The feature relation
     * @param type $code
     * @return type
     */
    public function getFeature()
    {
        return $this->hasOne(Features::className(), ['id' => 'feature_id']);
    }
}

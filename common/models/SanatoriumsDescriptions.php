<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use himiklab\yii2\search\behaviors\SearchBehavior;
use common\models\Sanatoriums;

/**
 * This is the model class for table "sanatoriums_descriptions".
 *
 * @property integer $id
 * @property integer $sanatorium_id
 * @property integer $lang_id
 * @property string $title
 * @property string $annotation
 * @property string $meta_description
 * @property string $description
 * @property string $description_second
 * @property string $slug
 *
 * @property Sanatoriums $sanatorium
 */
class SanatoriumsDescriptions extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sanatoriums_descriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sanatorium_id', 'lang_id'], 'integer'],
            [['description','description_second'], 'string'],
            [['title', 'annotation', 'meta_description', 'slug'], 'string', 'max' => 255],
            [['sanatorium_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sanatoriums::className(), 'targetAttribute' => ['sanatorium_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sanatorium_id' => 'Sanatorium ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'annotation' => 'Annotation',
            'meta_description' => 'Meta Description',
            'description' => 'Description',
            'description_second' => 'Description Second',
            'slug' => 'Slug',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
            ],
            'search' => [
                'class' => SearchBehavior::className(),
                'searchScope' => function ($model) {
                    /** @var \yii\db\ActiveQuery $model */
                    $model->select([
                        'title',
                        'description',
                        'description_second',
                        'slug',
                        'lang_id'
                    ]);
                    $model->leftJoin('sanatoriums', '{{sanatoriums}}.id = {{sanatoriums_descriptions}}.sanatorium_id');
                    $model->andWhere(['{{sanatoriums}}.status' => Sanatoriums::SANATORIUMS_STATUS_VISIBLE]);
                },
                    'searchFields' => function ($model) {
                    /** @var self $model */
                    return [
                        ['name' => 'title', 'value' => $model->title],
                        ['name' => 'description', 'value' => \strip_tags($model->description)],
                        ['name' => 'description_second', 'value' => \strip_tags($model->description_second)],
                        ['name' => 'url', 'value' => $model->slug, 'type' => SearchBehavior::FIELD_KEYWORD],
                        ['name' => 'model', 'value' => 'offers', 'type' => SearchBehavior::FIELD_UNINDEXED],
                        ['name' => 'langId', 'value' => $model->lang_id, 'type' => SearchBehavior::FIELD_UNINDEXED],
                    ];
                }
                ],
            ];
        }

        /**
         * @return \yii\db\ActiveQuery
         */
        public function getSanatorium()
        {
            return $this->hasOne(Sanatoriums::className(), ['id' => 'sanatorium_id']);
        }

    }
    
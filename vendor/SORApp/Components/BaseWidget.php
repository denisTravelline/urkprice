<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 05.09.14 14:03
     */

    namespace SORApp\Components;

    /**
     * Class BaseWidget
     * @package SORApp\Components
     */
    abstract class BaseWidget extends BaseComponent {

        /**
         * Метод запуска виджета
         *
         * @return string
         */
        abstract public function run();

        /**
         * Применение параметров к виджету
         *
         * @param array $data
         */
        public function __construct($data = null) {

            if (is_array($data)) {
                $this->config->applyConfig($this, $data);
            }
        }

        /**
         * Хелпер для рендеринга шаблонов
         *
         * @param string $template
         * @param array $data
         *
         * @return string
         */
        public function render($template, $data = []) {

            return $this->getComponent('templateManager')->render('_widgets/' . $template, $data);
        }
    }

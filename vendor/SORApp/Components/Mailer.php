<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 18.09.14 12:08
     */

    namespace SORApp\Components;

    class Mailer {

        public $swiftMailerPath;

        public $templateDir = 'Email';

        private $_isSwiftMailerLoaded = false;
        private $_swiftMailer;

        public $transportType = 'php';
        public $transportOptions = [];
        private $_transport;

        public $messageConfig = [];

        public function __construct() {

            /** @var \SORApp\Components\TemplateManager $templateManager */
            $templateManager = App::getInstance()->getComponent('templateManager');

            $templateManager->addPath($templateManager->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $templateManager->getTemplate() .
                                      DIRECTORY_SEPARATOR . $this->templateDir . DIRECTORY_SEPARATOR . App::getInstance()->lang, 'email');

            $templateManager->addPath($templateManager->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $templateManager->getTemplate() .
                                      DIRECTORY_SEPARATOR . $this->templateDir, 'email');

            $templateManager->addPath($templateManager->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $templateManager::DEFAULT_TEMPLATE .
                                      DIRECTORY_SEPARATOR . $this->templateDir . DIRECTORY_SEPARATOR . App::getInstance()->lang, 'email');

            $templateManager->addPath($templateManager->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $templateManager::DEFAULT_TEMPLATE .
                                      DIRECTORY_SEPARATOR . $this->templateDir, 'email');
        }

        /**
         * @param null $subject
         * @param null $body
         * @param string $contentType
         * @param string $charset
         *
         * @return \Swift_Message
         */
        public function createMessage($subject = null, $body = null, $contentType = 'text/html', $charset = 'utf-8') {

            $this->initSwiftMailer();
            $message = \Swift_Message::newInstance($subject, $body, $contentType, $charset);
            foreach ($this->messageConfig as $key => $val) {
                $setter = 'set' . ucfirst($key);
                if (method_exists($message, $setter)) {
                    call_user_func([
                        $message,
                        $setter], $val);
                }
            }

            return $message;
        }

        public function send(\Swift_Mime_Message $message, &$failedRecipients = null) {

            return $this->getSwiftMailer()->send($message, $failedRecipients);
        }

        public function getTransport() {

            if ($this->_transport === null) {
                switch ($this->transportType) {

                    case 'php':
                        $this->_transport = \Swift_MailTransport::newInstance();
                        if ($this->transportOptions !== null) {
                            $this->_transport->setExtraParams($this->transportOptions);
                        }
                        break;

                    case 'sendmail':
                        $this->_transport = \Swift_SendmailTransport::newInstance($this->transportOptions);
                        break;

                    case 'smtp':
                        $this->_transport = \Swift_SmtpTransport::newInstance();
                        foreach ($this->transportOptions as $key => $val) {
                            $setter = 'set' . ucfirst($key);
                            if (is_callable([
                                $this->_transport,
                                $setter])) {
                                call_user_func([
                                    $this->_transport,
                                    $setter], $val);
                            }
                        }
                        break;
                }
            }

            return $this->_transport;
        }

        public function getSwiftMailer() {

            if ($this->_swiftMailer === null) {
                $this->initSwiftMailer();
                $this->_swiftMailer = \Swift_Mailer::newInstance($this->getTransport());
            }

            return $this->_swiftMailer;
        }

        protected function initSwiftMailer() {

            if (!$this->_isSwiftMailerLoaded) {
                require_once rtrim($this->swiftMailerPath, '/') . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'swift_required.php';
                $this->_isSwiftMailerLoaded = true;
            }
        }
    }

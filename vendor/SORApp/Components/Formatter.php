<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 15.09.14 10:33
     */

    namespace SORApp\Components;

    /**
     * Class Formatter
     * @package SORApp\Components
     */
    class Formatter extends BaseComponent {

        public $currencyFormatDefault = '%1$01.2f&nbsp;%2$s';
        public $currencyRules = [];

        /**
         * Выводит отформатиированную стоимость с учетом особенностей валюты
         *
         * @param int|float $value Стоимость
         * @param string $currency Трехбуквенный код валюты. Для разных вариантов возможны суффиксы _short|_long
         * @param string $suffix
         * @param string $format Произвольный формат вывода
         *
         * @return string
         */
        public function price($value, $currency, $suffix = 'long', $format = null) {

            if ($format === null) {
                $format = (isset($this->currencyRules[$currency]) ? $this->currencyRules[$currency] : $this->currencyFormatDefault);
            }

            $currencyTranslate = App::_('currencies', $currency . '_' . $suffix, [$value]);

            return sprintf($format, $value, ($currencyTranslate ? $currencyTranslate : $currency));
        }

        public function base64($str) {

            return base64_encode($str);
        }

        /**
         * A sweet interval formatting, will use the two biggest interval parts.
         * On small intervals, you get minutes and seconds.
         * On big intervals, you get months and days.
         * Only the two biggest parts are used.
         *
         * @param \DateTime $start
         * @param \DateTime|null $end
         * @param \bool|false $exact
         * @return string
         */
        public function formatDateDiff($start, $end = null, $exact = false) {

            if (!($start instanceof \DateTime))
                $start = new \DateTime($start);
            if ($end === null) {
                $end = new \DateTime();
            }
            elseif (!($end instanceof \DateTime))
                $end = new \DateTime($end);

            $format = [];
            $interval = $end->diff($start);
            if ($interval->y !== 0)
                $format[] = "%y " . $this->plural(App::_('global', 'year', [$interval->y]), $interval->y);
            if ($interval->m !== 0)
                $format[] = "%m " . $this->plural(App::_('global', 'month', [$interval->m]), $interval->m);
            if ($interval->d !== 0)
                $format[] = "%d " . $this->plural(App::_('global', 'day', [$interval->d]), $interval->d);
            if ($interval->h !== 0)
                $format[] = "%h " . $this->plural(App::_('global', 'hour', [$interval->h]), $interval->h);
            if ($interval->i !== 0)
                $format[] = "%i " . $this->plural(App::_('global', 'minute', [$interval->i]), $interval->i);
            if ($interval->s !== 0) {
                if (!count($format) && !$exact)
                    return App::_('global', 'lessThanAMinute');
                $format[] = "%s " . $this->plural(App::_('global', 'second', [$interval->s]), $interval->s);
            }

            return $interval->format(implode(', ', $format));
        }

        /**
         * Выбор формы множественного существительного
         *
         * @param string|array $forms
         * @param int|float $n
         *
         * @return string
         * @throws \ErrorException
         */
        public function plural($forms, $n = 1) {

            $s = is_array($forms) ? $forms : explode('|', $forms);

            switch (count($s)) {
                case 1:
                    return $s[0];
                case 2:
                    return $n == 1 ? $s[0] : $s[1];
                case 3:
                    return $n % 10 == 1 && $n % 100 != 11 ? $s[0]
                        : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $s[1] : $s[2]);
                default:
                    App::Log('LANG', 'Incorrect plural forms: ' . implode('|', $s));

                    return '';
            }
        }
    }

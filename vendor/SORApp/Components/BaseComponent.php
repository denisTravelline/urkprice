<?php

    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Components;

    /**
     * Class BaseComponent
     *
     * Данный класс обеспечивает связывание всех компонентов системы с базовым управлюшим объектом @see \SORApp\Components\App
     * Содержит реализацию магических сеттеров и геттеров
     *
     * @package SORApp\Components
     *
     * @property \SORApp\Components\App $owner
     * @property \SORApp\Services\Config\Config $config
     */
    class BaseComponent {

        /**
         * При сериализации сораняем все переменные которые не начинаются с символа подчеркивания
         *
         * @return array
         */
        public function __sleep() {

            $sleepAttr = [];
            $reflect = new \ReflectionClass($this);
            $properties = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED);

            foreach ($properties as $property) {
                if (strpos($property->getName(), '_') !== 0) {
                    $sleepAttr[] = $property->getName();
                }
            }

            return $sleepAttr;
        }

        /**
         * При обращении к несуществующему свойству объекта, пробуем найти метод-геттер
         * @param string $attr
         *
         * @return mixed
         * @throws \ErrorException
         */
        public function __get($attr) {

            $getter = 'get' . ucfirst($attr);
            if (!method_exists($this, $getter) || !is_callable([
                    $this,
                    $getter])
            ) {
                throw new \ErrorException('Undefined attribute "' . $attr . '" in object "' . get_class($this) . '"');
            }

            return call_user_func([
                $this,
                $getter]);
        }

        /**
         * При попытке установить несуществующее свойство, прибуем найти метод-сеттер
         * @param string $attr
         * @param mixed $value
         *
         * @throws \ErrorException
         */
        public function __set($attr, $value) {

            $setter = 'set' . ucfirst($attr);
            if (!method_exists($this, $setter) || !is_callable([
                    $this,
                    $setter])
            ) {
                throw new \ErrorException('Undefined attribute "' . $attr . '" in object "' . get_class($this) . '"');
            }

            call_user_func([
                $this,
                $setter], $value);
        }

        /**
         * Геттер который возвращает текущий инстанс приложения
         * @return App
         */
        public function getOwner() {

            return App::getInstance();
        }

        public function getComponent($componentName) {

            return $this->getOwner()->getComponent($componentName);
        }

        /**
         * @return \SORApp\Services\Config\Config
         */
        public function getConfig() {

            return App::getInstance()->getConfig();
        }
    }

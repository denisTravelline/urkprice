<?php

    namespace SORApp\Components;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Services\Config\Config;

    final class App {

        private static $_instance;
        private static $_localizations = [];

        public $lang;
        public $action;
        public $module;
        public $controller;
        public $serverTimezone;
        public $defaultLanguage;

        public $sessionPrefix = 'servio_';

        protected $_time;
        protected $_config;
        protected $_components = [];

        protected function __construct(Config $config) {

            $this->_config = $config;
            $this->_time = [microtime(true), 0, 0];
            set_exception_handler([$this, 'excHandler']);
        }

        public static function widget($template) {

        }

        public static function _($section, $message, $params = []) {

            $app = self::getInstance();
            $lang = $app->getLang();
            if (!isset(self::$_localizations[$lang]))
                $app->getLocalization($lang);

            if (isset(self::$_localizations[$lang][$section]) && isset(self::$_localizations[$lang][$section][$message])) {

                $format = self::$_localizations[$lang][$section][$message];
                if (strpos($format, '|') !== false) {
                    $format = self::getInstance()->getComponent('formatter')->plural($format, current($params));
                }

                return call_user_func_array('sprintf', array_merge([$format], $params));
            }
            self::Log('LANG', 'Localization not found: ' . $section . '/' . $message . ' (' . $lang . ')');

            return '';
        }

        /**
         * Create application instance
         *
         * @param \SORApp\Services\Config\Config $configs
         *
         * @return App
         */
        public static function getInstance(Config $configs = null) {

            if (!self::$_instance) {
                self::$_instance = new self($configs);
            }

            return self::$_instance;
        }

        public function getLang() {

            if ($this->lang === null) {

                $this->lang = $this->getComponent('router')->getLang();

                if ($this->lang === null) {
                    $this->lang = $this->_config['defaultLanguage'];
                }
            }

            return substr($this->lang, 0, 2);
        }

        /**
         * @param string $componentName
         *
         * @return BaseComponent
         */
        public function getComponent($componentName) {

            if (!isset($this->_components[$componentName])) {
                $this->_components[$componentName] = $this->createComponent($componentName);
            }

            return $this->_components[$componentName];
        }

        /**
         * @throws \ErrorException
         * @param string $componentName
         *
         * @return Object
         */
        public function createComponent() {

            $arguments = func_get_args();
            if (!count($arguments)) {
                throw new \ErrorException('Incorrect usage of the createComponent method');
            }

            $componentName = array_shift($arguments);
            if (!($componentConfig = $this->_config['@components/' . $componentName])) {
                throw new \ErrorException('Component "' . htmlentities($componentName) . '" is not registered');
            }

            if (is_string($componentConfig)) {

                $className = $componentConfig;
            }
            else {

                if (!is_array($componentConfig) || !array_key_exists('className', $componentConfig)) {
                    throw new \ErrorException('Unknown class name for component "' . htmlentities($componentName) . '"');
                }

                $className = $componentConfig['className'];
            }

            if (strpos($className, '::')) {

                if (!is_callable($className)) {
                    throw new \ErrorException('Incorrect static constructor "' . htmlentities($className) . '"');
                }

                $component = call_user_func_array($className, $arguments);
            }
            else {

                if (!class_exists($className)) {
                    throw new \ErrorException('Component class "' . htmlentities($className) . '" not found');
                }

                if (count($arguments)) {

                    $reflectionClass = new \ReflectionClass($className);
                    $component = $reflectionClass->newInstanceArgs($arguments);
                }
                else {
                    $component = new $className;
                }
            }

            if (is_array($componentConfig)) {
                $this->_config->applyConfig($component, array_diff_key($componentConfig, ['className' => null]));
            }

            return $component;
        }

        public function getLocalization($lang = false) {

            if (!$lang)
                $lang = $this->getLang();
            if (!isset(self::$_localizations[$lang])) {

                if (file_exists(SOR_APP_DIR . '/Localizations/' . $lang . '.ini')) {

                    $langFileName = SOR_APP_DIR . '/Localizations/' . $lang . '.ini';
                }
                else {

                    if (!file_exists(SOR_APP_DIR . '/Localizations/' . $this->_config['defaultLanguage'] . '.ini')) {
                        throw new \ErrorException('Cannot find translations');
                    }

                    $langFileName = SOR_APP_DIR . '/Localizations/' . $this->_config['defaultLanguage'] . '.ini';
                }
                self::$_localizations[$lang] = parse_ini_file($langFileName, true);
            }

            return self::$_localizations[$lang];
        }

        public static function Log($prefix, $content, $postfix = '') {

            $app = self::getInstance();
            if (!$app->getConfig()['@global/loggingEnabled'])
                return;

            App::saveLog('app_', $app->getConfig()['@global/loggingDir'], $app->LogRequest() . ' ' . date('Y-m-d H:i:s') . ' ' . $prefix . ' ' .
                                                                          $content, $postfix);
        }

        /**
         * @return Config
         */
        public function getConfig() {

            return $this->_config;
        }

        /**
         * Сохранение отладочной информации в лог-файл
         *
         * @param string $prefix
         * @param string $dir
         * @param string $content
         */
        public static function saveLog($prefix, $dir, $content, $postfix = '') {

            if ((is_dir($dir) && is_writable($dir)) ||
                (strpos($dir, SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime') === 0 && @mkdir($dir, 0777, true) !== false)
            ) {

                $content = trim($content);
                if (empty($content))
                    return;

                $fileName = rtrim($dir, '/') . DIRECTORY_SEPARATOR . 'servio_' . $prefix . date('Ymd') . '.log';
                if (file_exists($fileName)) {
                    $content = PHP_EOL . (strpos($content, PHP_EOL) > 0 ? PHP_EOL : '') . $content . $postfix;
                }

                @file_put_contents($fileName, $content, FILE_APPEND);
            }
        }

        /**
         * @param string $uri
         * @param bool|array $query
         * @param bool|string $lang
         *
         * @return string
         */
        public static function url($uri, $query = false, $lang = false) {

            return self::getInstance()->getComponent('router')->createInternalUrl($uri, $query, $lang);
        }

        public function run() {

            ini_set('default_charset', 'utf-8');

            defined('SOR_APP_DEBUG') || define('SOR_APP_DEBUG', $this->_config->is('debug'));
            error_reporting((SOR_APP_DEBUG ? E_ALL : ~E_ALL));
            ini_set("display_errors", SOR_APP_DEBUG);

            if (empty($this->_config['defaultTimezone'])) {

                $this->serverTimezone = date_default_timezone_get();
                $this->_config['defaultTimezone'] = $this->serverTimezone;
            }
            else {

                date_default_timezone_set($this->_config['defaultTimezone']);
                $this->serverTimezone = date_default_timezone_get();
            }

            if (session_id() != '') {
                session_write_close();
            }

            session_name($this->_config['sessionPrefix'] . 'p_sid');
            session_start();

            $router = $this->getComponent('router');
            $router->prepareRequest();
            $this->lang = mb_strtolower($router->getLang());

            $response = $this->runController($router->getModule(), $router->getController(), $router->getAction());
            $response->send($router->getAccept());

            $this->LogResponse($response);

            return $this;
        }

        public function runController($module, $controller, $action) {

            $this->_time[1] = microtime(true);

            $this->module = ucfirst(mb_strtolower($module));
            $this->controller = ucfirst(mb_strtolower($controller));
            $controllerClassName = '\\SORApp\\Controllers\\' . $this->module . '\\' . $this->controller;

            $this->action = mb_strtolower($action);
            $actionName = $this->action . 'Action';

            if (!class_exists($controllerClassName) || !method_exists($controllerClassName, $actionName)) {
                throw new HttpException(404, 'Page not found (' . $this->module . ', ' . $controllerClassName . ', ' . $actionName . ')');
            }

            $controller = new $controllerClassName($this);
            if (method_exists($controller, 'beforeActions')) {
                call_user_func([$controller,
                                'beforeActions']);
            }

            $response = call_user_func([$controller,
                                        $actionName]);
            if (!($response instanceof Response)) {
                $response = $this->getResponse()->setResponse($response);
            }

            if (method_exists($controller, 'afterActions')) {
                call_user_func([$controller,
                                'afterActions']);
            }

            return $response;
        }

        public static function text_string($text, $maxlen = null) {

            $str = trim(str_replace(["\n",
                                     "\r",
                                     '&nbsp;'], ' ', preg_replace('/\s\s+/', ' ', $text)));
            if (isset($maxlen) && strlen($str) > $maxlen)
                $str = substr($str, 0, $maxlen) . '...';

            return $str;
        }

        public static function html_string($html, $maxlen = null) {

            return self::text_string(strip_tags($html), $maxlen);
        }

        protected $_request_id = 0;

        public function LogRequest() {

            if (!!$this->_request_id)
                return $this->_request_id;

            if (SOR_APP_ENV == 'cli') {

		//echo json_encode($_SERVER, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

                $method = 'CLI';

                $argv = empty($_SERVER['argv']) ? '' : $_SERVER['argv'];
                $target = (empty($_SERVER['COMPUTER']) ? '' : $_SERVER['COMPUTER']);
                $target .= (empty($argv)) ? '' : array_shift($argv);
                $user = empty($_SERVER['USERNAME']) ? '' : $_SERVER['USERNAME'];
                $address = '';
                $browser = isset($_SERVER['OS']) && strncasecmp($_SERVER['OS'], 'Windows', 7) == 0 ? 'Windows' : 'Linux';
                $content = (empty($argv)) ? '' : implode(' ', $argv);
            }
            else {

                $user = session_id();
                $user = $user ? $user : 'no auth';

                // Информация о клиенте "адрес:порт платформа браузер версия"
                $content = (!empty($this->post) && ($b = $this->post)) ? (self::text_string($b, 1024) . '[' . strlen($b) . ']') : '';
                $address = $_SERVER['REMOTE_ADDR'] . ':' . (!$_SERVER['REMOTE_PORT'] ? 'CLI' : $_SERVER['REMOTE_PORT']);
                $browser = $this->getComponent('browser');
                $browser = empty($browser) ? '' : ($browser->getBrowser() . ' ' . $browser->getVersion() . ' ' . $browser->getPlatform());

                // Строка запроса URL
                $method = $_SERVER['REQUEST_METHOD'];
                $request_uri = urldecode($_SERVER['REQUEST_URI']);
                $target = (empty($_SERVER['HTTPS']) ? 'http' : 'https') . '://' . $_SERVER['HTTP_HOST'] . $request_uri;
            }
            $route = !empty($this->module) && !empty($this->module) && !empty($this->module)
                ? $this->module . '::' . $this->controller . '.' . $this->action
                : '---';

            $logstr = 'ЗАПРОС ◙ ► ' . $method . ' ' . $target . ' ► ' . $route . ' ◄ ' . $user . ' ◄ ' . $address . ' ◄ ' . $browser . ' ◄ ' . $content . ' ► ';
            $this->_request_id = substr('00000000' . strtoupper(dechex(crc32($logstr))), -8);

            self::Log('HTTP', $logstr);
            return $this->_request_id;
        }

        public function LogResponse($response) {

            $this->_time[2] = microtime(true);

            self::Log('HTTP', 'ОТВЕТ  ■ ◄ ' . sprintf("%01.3f-%01.3f|", $this->_time[2] - $this->_time[0], $this->_time[2] - $this->_time[1]) .
                              self::html_string(json_encode($response, JSON_UNESCAPED_UNICODE), 1024) . " ◄ ", PHP_EOL);
        }

        public function excHandler(\Exception $exception) {

            $response = $this->getResponse();
            $response->addError($exception);
            $response->send();

            $this->LogResponse($response);
        }

        public function getResponse() {

            return $this->getComponent('response');
        }

        public function getBaseUrl() {

            return $this->getComponent('router')->createInternalUrl('/');
        }

        public function getStaticUrl() {

            return $this->getComponent('router')->getStaticUrl();
        }

    }

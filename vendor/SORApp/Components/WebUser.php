<?php

    namespace SORApp\Components;

    use SORApp\Services\Validator\ValidatorException;

    class WebUser extends BaseComponent {

        const SESSION_UID_KEY = 'admin.uid';

        public $userClass = '\\SORApp\\Models\\User';

        public $accessRoles = [
            'manager' => null,
            'admin' => ['manager'],];

        protected $_current;

        protected $_users = [];

        protected static $_userModel;

        /**
         * @return \SORApp\Components\Model\AuthUserModelInterface
         */
        public function getUserModel() {

            if (!is_a(self::$_userModel, $this->userClass))
                self::$_userModel = new $this->userClass;

            return self::$_userModel;
        }

        /**
         * Возвращает модель текущего пользователя
         *
         * @return bool|null|\SORApp\Components\Model\AuthUserModelInterface
         */
        public function getCurrent() {

            if ($this->_current === null)
                $this->_current = $this->authentication();

            return $this->_current;
        }

        /**
         * Устанавливает пользователя как текущего
         *
         * @param \SORApp\Components\Model\AuthUserModelInterface $user
         */
        public function setCurrent($user) {

            $this->_current = $user;
        }

        /**
         * Аутентификация пользователя
         *
         * @return bool|null|\SORApp\Components\Model\AuthUserModelInterface
         */
        public function authentication() {

            if (!isset($_SESSION[static::SESSION_UID_KEY]))
                return false;

            return $this->getUserModel()->findByPK($_SESSION[static::SESSION_UID_KEY]);
        }

        /**
         * Авторизация пользователя
         *
         * @param string $email
         * @param string $password
         *
         * @return bool
         * @throws \SORApp\Services\Validator\ValidatorException
         */
        public function authorization($email, $password) {

            if (empty($email) || empty($password) || !is_scalar($email) || !is_scalar($password)) {
                throw new ValidatorException('Empty email or password');
            }

            $user = $this->getUserModel()->findByEmail($email);
            if (!$user || is_null($user->getPk()) || !$user->checkPassword($password)) {
                throw new ValidatorException('Incorrect email and/or password');
            }

            if (!$user->isActive()) {
                throw new ValidatorException('Account is disabled');
            }

            $this->setCurrent($user);
            $_SESSION[static::SESSION_UID_KEY] = $user->getPk();

            return true;
        }

        public function logout() {

            $this->setCurrent(null);
            unset($_SESSION[static::SESSION_UID_KEY]);
        }

        public function checkAccess($checkedRole, $user = null) {

            if ($user == null)
                $user = $this->getCurrent();
            $userRoles = $user->getRoles();

            if (!count($userRoles))
                return false;
            if (in_array($checkedRole, $userRoles))
                return true;

            foreach ($userRoles as $role) {
                if (array_key_exists($role, $this->accessRoles) && is_array($this->accessRoles[$role]) &&
                    in_array($checkedRole, $this->accessRoles[$role])
                ) {
                    return true;
                }
            }

            return false;
        }
    }

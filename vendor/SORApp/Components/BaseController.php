<?php

    namespace SORApp\Components;

    /**
     * Description of Controllers
     *
     * @author Sevastianov Andrey
     *
     * @property TemplateManager $templateManager
     * @property Response $response
     */
    class BaseController extends BaseComponent {

        public $get;
        public $post;
        public $request;

        public function __construct() {

            $this->get = filter_input_array(INPUT_GET) or [];
            if (!is_array($this->get)) $this->get = [];

            if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST') {

                $this->post = filter_input_array(INPUT_POST) or [];
                if (!is_array($this->post)) $this->post = [];
            }
            else {

                $this->post = [];
            }
            $this->request = $this->get + $this->post;
        }

        /**
         * Редирект
         *
         * @param string $uri
         * @param bool|array $query
         * @param int $code
         * @param bool $terminate
         */
        public function redirect($uri, $query = false, $code = 302, $terminate = true) {

            $this->response->redirect($this->getComponent('router')->createInternalUrl($uri, $query, $this->getOwner()->getLang()), $code, $terminate);
        }

        /**
         * @return TemplateManager
         */
        public function getTemplateManager() {

            return $this->getComponent('templateManager');
        }

        /**
         * @return Response
         */
        public function getResponse() {

            return $this->getComponent('response');
        }
    }

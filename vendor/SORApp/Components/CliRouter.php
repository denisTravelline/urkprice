<?php

    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 24.10.14 12:06
     */

    namespace SORApp\Components;

    class CliRouter extends WebRouter {

        public function prepareRequest() {

            $argv = isset($_SERVER['argv']) && count($_SERVER['argv']) >= 2 ? $_SERVER['argv'][1] : (isset($_REQUEST['argv']) ? $_REQUEST['argv'] : null);
            if (empty($argv))
                throw new \ErrorException("Command line arguments error. \n\n  Usage: php cli.php [controller]/[action] \n\n  controller:  payments|mailing\n  action: worker");

            header('Content-Type: text/plain; charset=UTF-8');
            $this->routingRequest($argv);
        }

        public function getAccept() {

            return ['text/plain'];
        }
    }
<?php

    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Components;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Exceptions\WithDebugException;

    class Response extends BaseComponent {

        public $status = 200;

        public $errors = [];

        public $response = [];

        public $responseType = 'html';

        public $acceptType = 'text/html';

        public function __construct() {

            $this->acceptType = $this->getComponent('router')->getAccept();
            ob_start();
        }

        public function setResponse(array $response) {

            $this->response = $response;

            return $this;
        }

        public function redirect($url, $code, $terminate) {

            switch ($this->acceptType) {

                case 'application/json':
                case 'text/json':
                    $this->addResponseElement('redirect', $url);
                    if ($terminate) {
                        $this->send();
                        die;
                    }
                    break;

                case 'text/html':
                    header('Location: ' . $url, null, $code);
                    if ($terminate) {
                        ob_end_clean();
                        die;
                    }
            }
        }

        public function addResponseElement($key, $element) {

            $this->response[$key] = $element;

            return $this;
        }

        public function send() {

            if (ob_get_level() > 0)
                ob_end_clean();

            self::sendHttpResponseCode($this->status);

            if (array_key_exists('headers', $this->response) && count($this->response['headers'])) {
                foreach ($this->response['headers'] as $header) {
                    header($header);
                }
            }

            if (isset($this->response['bin'])) {

                echo $this->response['bin'];
                die;
            }
            switch ($this->acceptType) {

                case 'text/json':
                case 'application/json':
                    header("Cache-Control: no-cache, must-revalidate");
                    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                    header('Content-Type: application/json; charset=UTF-8');
                    echo json_encode([
                        'error' => count($this->errors),
                        'errorMessages' => $this->getErrors(),
                        'result' => $this->response], JSON_UNESCAPED_UNICODE);
                    break;

                case 'text/plain':
                    echo implode(PHP_EOL, array_merge($this->getErrors(), $this->response));
                    break;

                case 'text/html':
                default:
                    $this->response['errorMessages'] = $this->getErrors();
                    $templateManager = $this->getComponent('templateManager');
                    if ($this->status == 200) {

                        $template = $this->owner->action . '.twig';
                    }
                    else {

                        $template = 'Site/Errors/' . (file_exists($templateManager->getTemplateDir() . '/Site/Errors/' . $this->status .
                                                                  '.twig') ? $this->status : 500) . '.twig';
                    }
                    $templateManager->display($template, $this->response);
                    break;
            }
        }

        public static function sendHttpResponseCode($code = null) {

            if ($code !== NULL) {

                if (!headers_sent()) {

                    switch ($code) {

                        case 100:
                            $text = 'Continue';
                            break;
                        case 101:
                            $text = 'Switching Protocols';
                            break;
                        case 200:
                            $text = 'OK';
                            break;
                        case 201:
                            $text = 'Created';
                            break;
                        case 202:
                            $text = 'Accepted';
                            break;
                        case 203:
                            $text = 'Non-Authoritative Information';
                            break;
                        case 204:
                            $text = 'No Content';
                            break;
                        case 205:
                            $text = 'Reset Content';
                            break;
                        case 206:
                            $text = 'Partial Content';
                            break;
                        case 300:
                            $text = 'Multiple Choices';
                            break;
                        case 301:
                            $text = 'Moved Permanently';
                            break;
                        case 302:
                            $text = 'Moved Temporarily';
                            break;
                        case 303:
                            $text = 'See Other';
                            break;
                        case 304:
                            $text = 'Not Modified';
                            break;
                        case 305:
                            $text = 'Use Proxy';
                            break;
                        case 400:
                            $text = 'Bad Request';
                            break;
                        case 401:
                            $text = 'Unauthorized';
                            break;
                        case 402:
                            $text = 'Payment Required';
                            break;
                        case 403:
                            $text = 'Forbidden';
                            break;
                        case 404:
                            $text = 'Not Found';
                            break;
                        case 405:
                            $text = 'Method Not Allowed';
                            break;
                        case 406:
                            $text = 'Not Acceptable';
                            break;
                        case 407:
                            $text = 'Proxy Authentication Required';
                            break;
                        case 408:
                            $text = 'Request Time-out';
                            break;
                        case 409:
                            $text = 'Conflict';
                            break;
                        case 410:
                            $text = 'Gone';
                            break;
                        case 411:
                            $text = 'Length Required';
                            break;
                        case 412:
                            $text = 'Precondition Failed';
                            break;
                        case 413:
                            $text = 'Request Entity Too Large';
                            break;
                        case 414:
                            $text = 'Request-URI Too Large';
                            break;
                        case 415:
                            $text = 'Unsupported Media Type';
                            break;
                        case 500:
                            $text = 'Internal Server Error';
                            break;
                        case 501:
                            $text = 'Not Implemented';
                            break;
                        case 502:
                            $text = 'Bad Gateway';
                            break;
                        case 503:
                            $text = 'Service Unavailable';
                            break;
                        case 504:
                            $text = 'Gateway Time-out';
                            break;
                        case 505:
                            $text = 'HTTP Version not supported';
                            break;
                        default:
                            throw new \ErrorException('Unknown http status code "' . htmlentities($code) . '"');
                            break;
                    }
                    $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');
                    header($protocol . ' ' . $code . ' ' . $text);
                    $GLOBALS['http_response_code'] = $code;
                }
            }
            else {

                $code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);
            }

            return $code;
        }

        public function getErrors($trace = false) {

            $errors = [];
            if (count($this->errors)) {

                foreach ($this->errors as $error) {

                    list($errorMessage, $errorTrace) = $error;
                    if (!empty($errorMessage) OR ($trace || SOR_APP_DEBUG) && !empty($errorTrace)) {

                        $errors[] = $errorMessage .
                                    (($trace || SOR_APP_DEBUG) && !empty($errorTrace) ? ($this->acceptType !== 'text/plain' ? '<pre>' .
                                                                                                                              print_r($errorTrace, true) .
                                                                                                                              '</pre>'
                                        : PHP_EOL . PHP_EOL . print_r($errorTrace, true) . PHP_EOL) : '');
                    }
                }
            }

            return $errors;
        }

        /**
         * @deprecated
         */
        public function renderBlock($template, $data, $responseKey = null) {

            return $this->addResponseElement((is_scalar($responseKey) ? $responseKey : $template), $this->getComponent('templateManager')->render($template, $data));
        }

        public function addError(\Exception $exception) {

            $this->status = $exception instanceof HttpException ? $exception->getHttpResponseCode() : 500;

            $this->addErrorMessage($exception->getMessage(), ($exception instanceof WithDebugException ? $exception->getDebugMessage() : '') .
                                                             $exception->getFile() . '(' . $exception->getLine() . '): ' .
                                                             get_class($exception) . ' - ' . $exception->getMessage() . PHP_EOL .
                                                             $exception->getTraceAsString());
        }

        public function addErrorMessage($message, $trace = null) {

            $this->errors[] = [
                $message,
                $trace];
        }

    }

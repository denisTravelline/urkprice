<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 13.11.14 10:35
     */

    namespace SORApp\Components;

    /**
     * Class Pagination
     * @package SORApp\Components
     *
     * @property int $currentPage
     * @property int $itemPerPage
     * @property int $itemCount
     * @property int $offset
     */
    class Pagination extends BaseComponent {

        private $_currentPage = 1;
        private $_itemPerPage = 20;
        private $_itemCount = 0;

        public function __construct(array $config = []) {

            if (count($config)) {
                $this->config->applyConfig($this, $config);
            }
        }

        public function setCurrentPage($value) {

            $value = (int)$value;
            if ($value < 1) {
                throw new \HttpException('Incorrect page number');
            }

            $this->_currentPage = $value;
        }

        public function getCurrentPage() {

            return $this->_currentPage;
        }

        public function setItemPerPage($value) {

            $value = (int)$value;
            if ($value < 1) {
                throw new \HttpException('Incorrect number item per page');
            }

            $this->_itemPerPage = $value;
        }

        public function getItemPerPage() {

            return $this->_itemPerPage;
        }

        public function setItemCount($value) {

            $this->_itemCount = max(0, (int)$value);
        }

        public function getItemCount() {

            return $this->_itemCount;
        }

        public function getOffset() {

            return max(0, (($this->currentPage - 1) * $this->itemPerPage));
        }
    }

<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 */

namespace SORApp\Components\Model;

use SORApp\Components\Response,
    SORApp\Components\BaseComponent,
    SORApp\Services\Validator\ValidatorFactory,
    SORApp\Services\Validator\ValidatorException,
    SORApp\Services\Validator\ArrayValidatorException;

/**
 * Class BaseModel
 * @package SORApp\Components\Model
 *
 * @property \SORApp\Models\Translate $translateModel
 * @property \SORApp\Models\Room $roomModel
 * @property \SORApp\Models\Hotel $hotelModel
 * @property \SORApp\Models\Country $countryModel
 */
class BaseModel extends BaseComponent
{
    /**
     * @var array $_modelsRegister регистр инициализированных моделей
     */
    private static $_modelsRegister = array();

    /**
     * @var \SORApp\Services\Validator\ValidatorFactory $_validatorFactory Фабрика валида
     */
    private static $_validatorFactory;

    /**
     * @var array $_errors Массив с ошибками валидации
     */
    private $_errors = array();

    /**
     * Ленивая загрузка моделей
     * При обращеннии к неизвестному свойству которое оканчивается на Model будет произведен поиск моделей
     * Важно. Таким образом можно использовать только модели из корня неймспейса \SORApp\Components\Model\*
     *
     * @param string $attr
     *
     * @return mixed
     */
    public function __get($attr) {

        if ( ! array_key_exists($attr, self::$_modelsRegister) ) {

            $class = preg_replace('/Model$/', '', $attr, 1);
            $className = '\\SORApp\\Models\\' . ucfirst($class);
            if ($className == $attr || !class_exists($className)) {

                return parent::__get($attr);
            }
            self::$_modelsRegister[$attr] = new $className;
        }
        return self::$_modelsRegister[$attr];
    }

    public function __isset($attr)
    {
        if ( ! array_key_exists($attr, self::$_modelsRegister) ) {
            $class = preg_replace('/Model$/', '', $attr, 1);
            $className = '\\SORApp\\Models\\' . ucfirst($class);
            return $className != $attr && class_exists($className);
        } else {
            return true;
        }
    }

    /**
     * Создает копию текущий модели с указанными аттрибутами
     *
     * @param array|bool|null $attributes
     *
     * @return
     */
    public function createModel($attributes)
    {
        if ( is_array($attributes) AND count($attributes) ) {
            $modelClass = get_class($this);
            $model = new $modelClass;
            $model->autoSetAttributes($attributes);
            return $model;
        } else {
            return null;
        }
    }

    /**
     * Массовое присвоение атрибутов модели
     * Атрибут будет присвоен только если для него есть сеттер или указано правило валидации
     *
     * @param array $attributes
     * @param bool  $onlyWithValidators
     *
     * @return $this
     */
    public function autoSetAttributes($attributes = array(), $onlyWithValidators = true)
    {
        if ( is_array($attributes) AND count($attributes) ) {
            $validateRules = $this->validateRules();

            foreach ( $attributes as $key => $val ) {
                $setter = 'set' . ucfirst($key);
                if ( method_exists($this, $setter) AND is_callable(array($this, $setter)) ) {
                    call_user_func(array($this, $setter), $val);
                } else if ( property_exists($this, $key) AND ( ! $onlyWithValidators OR array_key_exists($key, $validateRules)) ) {
                    $this->{$key} = $val;
                }
            }
        }

        return $this;
    }

    /**
     * Метод заглушка для правил валидации
     * Если модели требуется валидировать аттрибуты, его можно переопределить
     *
     * @return array
     */
    public function validateRules()
    {
        return array();
    }

    /**
     * Запуск валидации аттрибутов модели
     * Возвращает ответ в зависимости от того, есть ли ошибки в модели.
     *
     * Важно! Перед валидацией ошибки не сбрасываются!
     * Если перед сохранением модели из корня неймспейса \SORApp\Components\Model\* нужно произвести валидацию,
     * рекомендуется создать новый инстанс модели.
     *
     * @return bool
     */
    public function validate()
    {
        $rules = $this->validateRules();
        if ( count($rules) ) {
            foreach ( $rules as $attr => $rule ) {
                if ( isset($rule[0]) ) {
                    $errorMessage = array_shift($rule);
                }

                if ( count($rule) ) {
                    foreach ( $rule as $validatorName => $validatorOptions ) {
                        try {
                            $this->$attr = $this->validateValue($this->$attr, $validatorName, $validatorOptions);
                        } catch ( ArrayValidatorException $e ) {
                            $errors = $e->getErrors();
                            if ( count($errors) ) {
                                foreach ( $errors as $error ) {
                                    $this->addError($attr, $error);
                                }
                            }
                        } catch ( ValidatorException $e ) {
                            if ( ! isset($errorMessage) ) {
                                $this->addError($attr, $e->getMessage());
                            } else {
                                $this->addError($attr, $errorMessage);
                            }
                        }
                    }
                }
            }
        }

        if ( ! $this->hasErrors() ) {
            if ( method_exists($this, 'afterValidate') AND is_callable(array($this, 'afterValidate')) ) {
                call_user_func(array($this, 'afterValidate'));
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Валидация значение с помощью валидатора
     * Возвращается значение после применения валидатора
     *
     * @param mixed $value
     * @param string $validatorName
     * @param array $validatorOptions
     *
     * @return mixed
     * @throws \ErrorException
     */
    public function validateValue($value, $validatorName, $validatorOptions = array())
    {
        $vf = $this->getValidatorFactory();
        if ( $validator = $vf->get($validatorName) ) {
            return $validator->test($value, $validatorOptions);
        } else {
            throw new \ErrorException('Undefined validator "' . $validatorName . '"');
        }
    }

    /**
     * Возвращает объект фабрики валидаторов
     *
     * @return ValidatorFactory
     */
    protected function getValidatorFactory()
    {
        if ( self::$_validatorFactory === null ) {
            self::$_validatorFactory = new ValidatorFactory();
        }

        return self::$_validatorFactory;
    }

    /**
     * Добавление сообщения об ошибке
     *
     * @param string $attr
     * @param string $error
     */
    public function addError($attr, $error)
    {
        if ( ! isset($this->_errors[$attr]) ) {
            $this->_errors[$attr] = array();
        }

        $this->_errors[$attr][] = $error;
    }

    public function hasError($attr = null)
    {
        return ( $attr === null ? $this->hasErrors() : isset($this->_errors[$attr]) && count($this->_errors) );
    }

    /**
     * Проверка, есть ли ошибки
     *
     * @return bool
     */
    public function hasErrors()
    {
        return ( count($this->_errors) > 0 );
    }

    /**
     * Возвращает массив с ошибками
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * Очищает все ошибки валидации
     *
     * Важно! Использовать это стоит только когда вы уверены, что все правила валидации указаны в validateRules.
     * Во многих моделях валидация происходит в сеттерах и ошибки не будут найдены заново.
     *
     * @return $this
     */
    public function clearErrors()
    {
        $this->_errors = array();
        return $this;
    }

    /**
     * Добавляет сообщения об ошибках в объект Response
     *
     * @param Response $response
     */
    public function attachResponseError(Response $response)
    {
        if ( count($this->_errors) ) {
            foreach ($this->_errors as $attr => $errors ) {
                if ( count($errors) ) {
                    foreach ( $errors as $error ) {
                        $response->addErrorMessage($error);
                    }
                }
            }
        }
    }
} 

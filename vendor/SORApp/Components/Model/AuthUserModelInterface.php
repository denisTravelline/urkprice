<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 09.09.14 13:43 
 */

namespace SORApp\Components\Model;

/**
 * Interface AuthUserModelInterface
 *
 * @package SORApp\Components\Model
 */
interface AuthUserModelInterface
{
    /**
     * Метод должен возвращать идентификатор текущего пользователя
     *
     * @return int|string|null
     */
    public function getPk();

    /**
     * Метод должен возвращать массив ролей пользователя
     *
     * @return array
     */
    public function getRoles();

    /**
     * Проверка введенного пароля
     *
     * @param string $password
     *
     * @return bool
     */
    public function checkPassword($password);

    /**
     * Установить новый пароль
     *
     * @param string $password
     *
     * @return void
     */
    public function changePassword($password);

    /**
     * Поиск пользователя по id
     *
     * @param int $id
     *
     * @return null|AuthUserModelInterface
     */
    public function findByPK($id);

    /**
     * Поиск пользователя по адресу почты
     *
     * @param string $email
     *
     * @return null|AuthUserModelInterface
     */
    public function findByEmail($email);

    /**
     * Проверка, является ли данный аккаунт активным
     *
     * @return bool
     */
    public function isActive();

    /**
     * Включить аккаунт
     *
     * @return void
     */
    public function activate();

    /**
     * Выключить аккаунт
     *
     * @return void
     */
    public function disable();
} 

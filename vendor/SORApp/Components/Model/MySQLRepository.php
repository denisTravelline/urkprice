<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 */

namespace SORApp\Components\Model;

use SORApp\Components\App;
use SORApp\Components\Exceptions\DatabaseException;
use SORApp\Components\Exceptions\HttpException;
use SORApp\Services\Validator\ValidatorException;

/**
 * Class MySQLRepository
 * @package SORApp\Components\Model
 *
 * @property \DbSimple_Database $db
 */
class MySQLRepository extends BaseModel {

    private static $_dbConnection;

    /**
     * @return \DbSimple_Database
     */
    public function getDb() {

        return static::db();
    }

    /**
     * @return \DbSimple_Database
     */
    public static function db() {

        if (self::$_dbConnection == null) {
            /** @var \SORApp\Components\Database $database */
            $database = App::getInstance()->getComponent('database');
            self::$_dbConnection = $database->getConnection();
        }

        return self::$_dbConnection;
    }

    public function getDbErrorString() {

        $errorData = $this->getDb()->error;
        if (is_array($errorData)) {
            $errorMessage = array();
            foreach ($errorData as $key => $val) {
                $errorMessage[] = ucfirst($key) . ': ' . $val;
            }
            return implode(PHP_EOL, $errorMessage);
        }
        else if (is_string($errorData)) {
            return $errorData;
        }
        else {
            return null;
        }
    }

    /**
     * Добавление локализации для коллекции
     *
     * @param array $collection
     * @param string $parentType
     * @param bool $sortByName
     * @param string|array $lang
     */
    public function attachTranslate(&$collection, $parentType, $sortByName = false, $lang = null) {

        if (is_array($collection) AND count($collection)) {
            $lang = ($lang !== null ? $lang : $this->getOwner()->getLang());
            $translates = $this->translateModel->get($parentType, array_keys($collection), $lang);
            if (is_array($translates) AND count($translates)) {
                foreach ($translates as $parentKey => $translate) {
                    $collection[$parentKey]['translates'] = $translate;
                }

                if ($sortByName && count($collection) > 1) {
                    uasort($collection, array($this, 'sortByTranslateName'));
                }
            }
        }
    }

    /**
     * Добавление лакализации для сгруппированной коллекции
     *
     * @param      $collection
     * @param      $parentType
     * @param bool $sortByName
     * @param string|array $lang
     */
    public function attachTranslateGroup(&$collection, $parentType, $sortByName = false, $lang = null) {

        if (is_array($collection) AND count($collection)) {
            $itemKeys = array();
            foreach ($collection as $group) {
                $itemKeys = array_merge($itemKeys, array_keys($group));
            }
            $itemKeys = array_unique($itemKeys);

            $lang = ($lang !== null ? $lang : $this->getOwner()->getLang());
            $translates = $this->translateModel->get($parentType, $itemKeys, $lang);
            foreach ($collection as $groupKey => $group) {
                foreach ($group as $itemKey => $item) {
                    $collection[$groupKey][$itemKey]['translates'] = (isset($translates[$itemKey]) ? $translates[$itemKey] : array());
                }

                if ($sortByName && count($collection[$groupKey]) > 1) {
                    uasort($collection[$groupKey], array($this, 'sortByTranslateName'));
                }
            }
        }
    }

    /**
     * Служебная функция для сортировку двух коллекций по алфавиту перевода
     *
     * @param array $a
     * @param array $b
     *
     * @return int
     */
    public function sortByTranslateName($a, $b) {

        if (isset($a['translates']) AND isset($b['translates']) AND count($a['translates']) AND count($b['translates'])) {
            $lang = $this->getOwner()->getLang();
            if (!isset($a['translates'][$lang])) {
                $lang = key($a['translates']);
            }
            return strcasecmp($a['translates'][$lang]['name'], $b['translates'][$lang]['name']);
        }
        else {
            return 0;
        }
    }

    /**
     * Нормализация перевода
     *
     * @param array $value
     *
     * @return array
     * @throws \SORApp\Services\Validator\ValidatorException
     */
    public function validateTranslate($value) {

        if (!empty($value)) {
            if (!is_array($value)) {
                throw new ValidatorException('Incorrect translate');
            }

            $availableLanguages = $this->config['availableLanguages'];
            $langStandard = array('name' => '', 'descr' => '', 'sub1' => '', 'sub2' => '');
            foreach ($value as $langCode => &$translate) {
                if (!in_array($langCode, $availableLanguages) OR !is_array($translate)) {
                    throw new ValidatorException('Incorrect translate');
                }

                $translate = array_intersect_key($translate, $langStandard);
                if (!count($translate)) {
                    throw new ValidatorException('Incorrect translate');
                }

                foreach ($translate as $attr) {
                    if (!is_string($attr)) {
                        throw new ValidatorException('Incorrect translate');
                    }
                }

                $translate = array_merge($langStandard, $translate);
            }
        }

        return $value;
    }

    public function selectQuery($tableName, array $where, $limit = null, $offset = null, $orderBy = null, $orderType = null) {

        $orderType = strtoupper($orderType);
        if (!($rows = $this->db->select(
            'SELECT * FROM ' . $tableName . '{ WHERE ?a}{ ORDER BY ?#}{ LIMIT ?d}{ OFFSET ?d}',
            (count($where) ? $where : DBSIMPLE_SKIP),
            ($orderBy !== null
                ? $orderBy . ($orderType == 'DESC' ? ' ' . $orderType : '')
                : DBSIMPLE_SKIP
            ),
            ($limit !== null ? $limit : DBSIMPLE_SKIP),
            ($offset !== null ? $offset : DBSIMPLE_SKIP)
        ))
        ) {
            if ($this->db->errmsg) {
                throw new DatabaseException('Database error', $this->dbErrorString);
            }
        }

        return $rows;
    }

    public function insertQuery($tableName, array $data) {

        if (!($id = $this->db->query('INSERT INTO ' . $tableName . ' (?#) VALUES(?a)', array_keys($data), array_values($data)))) {
            throw new DatabaseException('Database error', $this->dbErrorString);
        }

        return $id;
    }

    public function updateQuery($tableName, array $data, array $where) {

        if (!($update = $this->db->query('UPDATE ' . $tableName . ' SET ?a WHERE ?a', $data, $where)) AND $this->db->errmsg) {
            throw new DatabaseException('Database error', $this->dbErrorString);
        }

        return $update;
    }

    public function deleteQuery($tableName, array $where) {

        if (!$this->db->select('DELETE FROM ' . $tableName . ' WHERE ?a LIMIT 1', $where)) {
            if ($this->db->errmsg) {
                throw new DatabaseException('Database error', $this->dbErrorString);
            }

            return false;
        }

        return true;
    }

    public function selectFilteredPage($tableName, array $filters, array $orders, $offset, $limit, $safeKeys = array()) {

        $safeKeys = array_flip($safeKeys);

        $query = 'SELECT SQL_CALC_FOUND_ROWS * FROM ' . $tableName;

        if ($conditions = $this->prepareFilters($filters, $safeKeys)) {
            $query .= ' WHERE ' . $conditions;
        }

        if ($orderBy = $this->prepareOrders($orders, $safeKeys)) {
            $query .= ' ORDER BY ' . $orderBy;
        }

        $query .= ' LIMIT ' . intval($offset) . ', ' . intval($limit);

        if (!($rows = $this->db->select($query))) {
            if ($this->db->errmsg) {
                throw new DatabaseException('Database error', $this->dbErrorString);
            }
        }

        $total = $this->db->selectCell('SELECT FOUND_ROWS();');

        return array('total' => $total, 'page' => $rows);
    }

    public function prepareOrders($orders, $safeKeys = array()) {

        $orderBy = array();
        foreach ($orders as $attr => $val) {
            if (isset($safeKeys[$attr])) {
                $orderBy[] = $this->db->escape($attr, true) . ' ' . (strtolower($val) === 'desc' ? 'DESC' : 'ASC');
            }
        }

        return implode(', ', $orderBy);
    }

    public function prepareFilters($filters, $safeKeys = null, $defaultType = 'IS') {

        $operator = 'AND';
        $expressions = array();

        if (is_array($filters) AND count($filters)) {
            foreach ($filters as $key => $value) {
                if (!is_numeric($key)) {
                    if ($safeKeys AND isset($safeKeys[$key])) {
                        $expressions[] = $this->prepareExpression($key, $value, $defaultType);
                    }
                }
                else {
                    if (is_string($value)) {
                        $value = strtoupper($value);
                        if (in_array($value, array('AND', 'OR'))) {
                            $expressions = array_diff($expressions, array(''));
                            $expressions = array(implode(' ' . $operator . ' ', $expressions));
                            $operator = $value;
                        }
                        else if (in_array($value, array('LIKE', 'COMPARE', 'IN', 'IS'))) {
                            $defaultType = $value;
                        }
                    }
                    else if (is_array($value)) {
                        $expressions[] = $this->prepareFilters($value, $safeKeys, $defaultType);
                    }
                }
            }
        }

        $expressions = array_diff($expressions, array(''));
        return (count($expressions) > 1
            ? '(' . implode(' ' . $operator . ' ', $expressions) . ')'
            : (count($expressions) === 1 ? current($expressions) : '')
        );
    }


    public function prepareExpression($key, $data, $defaultType = 'is') {

        $key = $this->db->escape($key, true);
        if (!is_array($data)) {
            $data = array('type' => $defaultType, 'value' => $data);
        }
        if (!array_key_exists('type', $data) || !array_key_exists('value', $data)) {
            throw new HttpException(400, 'Bad request: format incorrect for key ' . $key);
        }

        $operator = '=';
        if (isset($data['operator']) AND in_array(strtoupper($data['operator']), array('=', '!=', '>=', '<=', 'IS'))) {
            $operator = $data['operator'];
        }

        $data['type'] = strtolower($data['type']);
        switch ($data['type']) {
            case 'between':
                $expression = $key . ' BETWEEN '
                    . $this->db->escape($data['value']['start']) . ' AND ' . $this->db->escape($data['value']['end']);
                break;

            case 'between_date':
                if (empty($data['format'])) {
                    $data['format'] = '%Y-%m-%d %H:%i:%s';
                    $data['value']['start'] = date('Y-m-d H:i:s', strtotime($data['value']['start']));
                    $data['value']['end'] = date('Y-m-d H:i:s', strtotime($data['value']['end']));
                }
                $expression = "DATE_FORMAT(STR_TO_DATE(" . $key . ", '%d.%m.%Y'), '" . $data['format'] . "')  BETWEEN "
                    . $this->db->escape($data['value']['start'])
                    . " AND " . $this->db->escape($data['value']['end']);
                break;

            case 'like':
                $expression = $key . ' LIKE ' . $this->db->escape('%' . $data['value'] . '%');
                break;

            case 'compare':
                // $this->db->escape() - тут его не должно быть!
                $expression = $key . ' ' . $operator . ' ' . $data['value'];
                break;

            case 'in':
                if (is_string($data['value'])) {
                    $data['value'] = explode(',', $data['value']);
                }
                foreach ($data['value'] as &$inValue) {
                    $inValue = $this->db->escape($inValue);
                }
                $expression = $key . ' IN (' . implode(',', $data['value']) . ')';
                break;

            case 'is':
            default:
                $expression = $key . ' ' . $operator . ' ' . $this->db->escape($data['value']);
                break;
        }

        return $expression;
    }
}

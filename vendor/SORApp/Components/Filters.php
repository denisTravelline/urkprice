<?php

    namespace SORApp\Components;

    /**
     * Модель фильтрует входные данные, для дальнейшего использования
     * Доступа к БД не имеет, все методы статичны
     *
     * @author sorokin_r
     * @see \SORApp\Services\Validator\ValidatorFactory
     * @deprecated
     */
    class Filters {

        /**
         *
         * @param array $data Массив, где проверяем наличие данных
         * @param number $key Ключ массива, который проверяем и откуда берем данные
         * @param string $type Тип данных float | int (по умолчанию)
         * @param number|false $min Минимальное значение или false(по умолчанию) если проверка не требуется
         * @param number|false $max Максимальное значение или false(по умолчанию) если проверка не требуется
         * @param number $default Значение по умолчанию, если возникла ошибка при предыдущих
         *                                      проверках или false(по умолчанию) для вызова исключения
         * @return number                       Сконвертированное в нужный тип значение $data[$key]
         * @throws \Exception
         */
        public static function number($data, $key, $type = 'int', $min = false, $max = false, $default = false) {

            if (!isset($data[$key]) && $default === false) {
                throw new \Exception($key . ' не указано', 10);
            }
            elseif ((!isset($data[$key]) || $data[$key] === '') && $default !== false) {
                $var = $default;
            }
            else {
                if ($type == 'float') {
                    $var = (float)$data[$key];
                }
                else {
                    $var = (int)$data[$key];
                }
            }

            if ($var !== $default && $min !== false && $var < $min) {
                if ($default === false) {
                    throw new \Exception($key . '=' . $var . ' менее ' . $min, 10);
                }
                else {
                    $var = $default;
                }
            }

            if ($var !== $default && $max !== false && $var > $max) {
                if ($default === false) {
                    throw new \Exception($key . '=' . $var . ' ' . $min . ' более ' . $max, 10);
                }
                else {
                    $var = $default;
                }
            }

            return $var;
        }

        /**
         *
         * @param array $data Массив, где проверяем наличие данных
         * @param number $key Ключ массива, который проверяем и откуда берем данные
         * @param int $min Минимальная длина строки или false(по умолчанию), если проверка не требуется
         * @param int $max Максимальная длина строки или false(по умолчанию), если проверка не требуется
         * @param regexp $pattern Паттерн регулярного выражения, для проверки на соответствие
         * @param string $default Строка по умолчанию или false(по умолчанию), если нужно вызвать исключение
         * @return string                           Строка, полученная из $data[$key] или из $default, если оно не равно false и
         *                                          строка не прошла предыдущие проверки
         * @throws \Exception
         */
        public static function string($data, $key, $min = false, $max = false, $pattern = false, $default = false) {

            $error = false;
            if (!isset($data[$key]) && $default === false) {
                throw new \Exception('Не передан параметр ' . $key, 10);
            }
            elseif (!isset($data[$key])) {
                $string = $default;
            }
            else {
                $string = $data[$key];
            }

            if ($string !== $default && (int)$min >= 1 && mb_strlen($string, 'UTF-8') < $min) {
                $string = $default;
                $error = 'Длина строки ' . $key . ' меньше ' . $min;
            }
            if ($string !== $default && (int)$max >= 1 && mb_strlen($string, 'UTF-8') > $max) {
                $string = $default;
                $error = 'Длина строки ' . $key . ' больше ' . $max;
            }
            if ($string !== $default && $pattern !== false && !preg_match($pattern, $string)) {
                $string = $default;
                $error = 'Строка ' . $key . ' не соответствует паттеру';
            }

            if ($string === false) {
                throw new \Exception($key . ' ' . $min . ' ' . $max . ' ' . $pattern . ' ' . $default . ' ' . $error, 10);
            }

            return $string;
        }

        public static function checkbox($data, $key, $default = false) {

            if (isset($data[$key]) && ($data[$key] == 'true' || $data[$key] == '1')) {
                $result = true;
            }
            elseif (!isset($data[$key])) {
                $result = $default;
            }
            else {
                $result = false;
            }

            return $result;
        }
    }

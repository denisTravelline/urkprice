<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 28.11.14 17:28 
 */

namespace SORApp\Components\Exceptions;


class DatabaseException extends WithDebugException
{
    public function __construct($message = "", $dbErrorString = null, $code = 0, \Exception $previous = null)
    {
        $this->debugMessage = $dbErrorString;
        parent::__construct($message, $code, $previous);
    }
} 

<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 28.11.14 17:33 
 */

namespace SORApp\Components\Exceptions;


class WithDebugException extends \Exception
{
    public $debugMessage;

    public function getDebugMessage()
    {
        return $this->debugMessage;
    }

} 

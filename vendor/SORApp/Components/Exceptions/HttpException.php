<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 */

namespace SORApp\Components\Exceptions;


class HttpException extends \Exception
{
    public $httpResponseCode;

    public function __construct($httpResponseCode = 500, $message = "", $code = 0, \Exception $previous = null)
    {
        $this->httpResponseCode = $httpResponseCode;
        parent::__construct('Error ' . $httpResponseCode . ': ' . $message /*. ' [' . var_export($_SERVER , true) . ']'*/, $code, $previous);
    }

    public function getHttpResponseCode()
    {
        return $this->httpResponseCode;
    }
}

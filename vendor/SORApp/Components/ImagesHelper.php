<?php

    namespace SORApp\Components;

    /**
     * Class ImagesHelper
     * @package SORApp\Components
     */
    class ImagesHelper extends BaseComponent {

        public $uploadDir;

        public $sizes = [
            'thumb' => '320x200',
            'middle' => '720x480',];

        public $quality = 80;

        public function getUploadDir($filePath = '') {

            if ($this->uploadDir === null OR !file_exists($this->uploadDir) OR !is_dir($this->uploadDir) OR !is_writable($this->uploadDir)) {
                throw new \ErrorException('Incorrect upload directory');
            }

            $fullPath = dirname(rtrim($this->uploadDir, '\\/') . DIRECTORY_SEPARATOR . trim($filePath, '\\/'));
            if (!file_exists($fullPath) AND !@mkdir($fullPath, 0777, true)) {
                throw new \ErrorException('Cannot create folder in upload directory');
            }
            else {
                if (!is_dir($fullPath) OR !is_writable($fullPath)) {
                    throw new \ErrorException('Incorrect file path');
                }
            }

            return $fullPath;
        }

        public function getSize($prefix) {

            if (!is_array($this->sizes) OR !isset($this->sizes[$prefix])) {
                throw new \ErrorException('Incorrect images size config');
            }

            return $this->sizes[$prefix];
        }

        public function unlinkAll($filePath, $originalPrefix = 'original_') {

            $uploadDir = $this->getUploadDir($filePath);
            $fileName = pathinfo($filePath, PATHINFO_BASENAME);

            foreach ($this->sizes as $prefix => $size) {
                $saveFileName = $uploadDir . DIRECTORY_SEPARATOR . $prefix . '_' . $fileName;
                if (file_exists($saveFileName)) {
                    @unlink($saveFileName);
                }
            }

            if (file_exists($uploadDir . DIRECTORY_SEPARATOR . $originalPrefix . $fileName)) {
                @unlink($uploadDir . DIRECTORY_SEPARATOR . $originalPrefix . $fileName);
            }
        }

        public function saveUploadedFile($fileTemp, $filePath, $originalPrefix = 'original_') {

            $files = [];

            $uploadDir = $this->getUploadDir($filePath);
            $fileName = pathinfo($filePath, PATHINFO_BASENAME);

            $image = file_get_contents($fileTemp);
            $this->recreate($image, $uploadDir . DIRECTORY_SEPARATOR . $originalPrefix . $fileName);
            $files[] = $uploadDir . DIRECTORY_SEPARATOR . $originalPrefix . $fileName;
            foreach ($this->sizes as $prefix => $size) {
                $size = preg_split('/[^0-9]+/', $size);
                if (count($size) !== 2) {
                    throw new \ErrorException('Incorrect images size config');
                }

                $saveFileName = $uploadDir . DIRECTORY_SEPARATOR . $prefix . '_' . $fileName;
                $this->resize($image, $size[0], $size[1], $saveFileName);
                $files[] = $saveFileName;
            }

            return $files;
        }

        /**
         *
         * @param   string $image Строка, содержащая изображение
         * @param   int $destWidth Необходимая ширина
         * @param   int $destHeight Необходимая высота
         * @param   string $filename
         *
         * @return  string              Строка полеченного изображения
         */
        private function resize($image, $destWidth, $destHeight, $filename) {

            $src = @imagecreatefromstring($image);
            if ($src) {
                $srcX = imagesx($src);
                $srcY = imagesy($src);

                $destCoef = $destWidth / $destHeight;
                $srcCoef = $srcX / $srcY;

                if ($destCoef < $srcCoef) {
                    // Обрезаем по ширине
                    $tmpWidth = $srcY * $destCoef;
                    $tmpHeight = $srcY;
                    $tmpX = ($srcX - $tmpWidth) / 2;
                    $tmpY = 0;
                }
                else {
                    // Обрезаем по высоте
                    $tmpWidth = $srcX;
                    $tmpHeight = $srcX / $destCoef;
                    $tmpX = 0;
                    $tmpY = ($srcY - $tmpHeight) / 2;
                }

                $dest = imagecreatetruecolor($destWidth, $destHeight);
                imagecopyresampled($dest, $src, 0, 0, $tmpX, $tmpY, $destWidth, $destHeight, $tmpWidth, $tmpHeight);
                imagejpeg($dest, $filename, $this->quality);
                imagedestroy($src);
                imagedestroy($dest);

                return true;
            }

            return false;
        }

        private function recreate($image, $filename) {

            $src = @imagecreatefromstring($image);
            if ($src) {
                $dest = imagecreatetruecolor(imagesx($src), imagesy($src));
                imagecopy($dest, $src, 0, 0, 0, 0, imagesx($src), imagesy($src));
                imagejpeg($dest, $filename, $this->quality);
                imagedestroy($src);
                imagedestroy($dest);

                return true;
            }

            return false;
        }
    }

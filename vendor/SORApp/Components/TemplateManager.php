<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 07.11.14 11:11
     */

    namespace SORApp\Components;

    /**
     * Class TemplateManager
     * @package SORApp\Components
     *
     * @property \Twig_Environment $twig
     */
    class TemplateManager extends BaseComponent {

        const DEFAULT_TEMPLATE = 'default';

        public $twigAutoloaderFile = 'Services/Twig/Autoloader.php';

        public $cache = false;

        /** @var string $template */
        protected $_template;

        /** @var string $templateDir */
        protected $_templateBaseDir;

        /** @var \Twig_Loader_Filesystem */
        protected $_twigLoader;

        /** @var \Twig_Environment */
        protected $_twig;

        /**
         * Setter template
         * @param $value
         *
         * @throws \ErrorException
         */
        public function setTemplate($value) {

            $value = trim($value, '/');
            if (empty($value)) {
                throw new \ErrorException('Empty template name');
            }

            $this->_template = $value;
        }

        /**
         * Getter template
         * @param string $default
         *
         * @return null|string
         */
        public function getTemplate($default = self::DEFAULT_TEMPLATE) {

            if ($this->_template === null) {
                $this->setTemplate($default);
            }

            return $this->_template;
        }

        /**
         * Setter template dir
         * @param string $value
         *
         * @throws \ErrorException
         */
        public function setTemplateBaseDir($value) {

            if (1) {// ! file_exists($value) ) {
                $value = SOR_APP_DIR . DIRECTORY_SEPARATOR . ltrim($value, '/');
                if (!file_exists($value)) {
                    throw new \ErrorException('Incorrect template path');
                }
            }

            if (!is_dir($value) || !is_readable($value)) {
                throw new \ErrorException('Template dir not readable');
            }

            $this->_templateBaseDir = realpath($value);
        }

        /**
         * Getter template dir
         * @return string
         */
        public function getTemplateBaseDir() {

            if ($this->_templateBaseDir === null) {
                $this->setTemplateBaseDir('Templates');
            }

            return $this->_templateBaseDir;
        }

        public function getTemplateDir() {

            return $this->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $this->getTemplate();
        }

        public function getTwig() {

            if ($this->_twig === null) {
                //            $this->fireEvent('beforeCreateTwig', $this);

                require_once SOR_APP_DIR . DIRECTORY_SEPARATOR . trim($this->twigAutoloaderFile, '/');
                \Twig_Autoloader::register();

                $templatesDirectories = array_unique([
                    $this->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $this->getTemplate(),
                    $this->getTemplateBaseDir() . DIRECTORY_SEPARATOR . $this->getTemplate() . DIRECTORY_SEPARATOR . $this->getOwner()->module .
                    DIRECTORY_SEPARATOR . $this->getOwner()->controller,
                    $this->getTemplateBaseDir() . DIRECTORY_SEPARATOR . self::DEFAULT_TEMPLATE,
                    $this->getTemplateBaseDir() . DIRECTORY_SEPARATOR . self::DEFAULT_TEMPLATE . DIRECTORY_SEPARATOR .
                    $this->getOwner()->module . DIRECTORY_SEPARATOR . $this->getOwner()->controller

                ]);

                foreach ($templatesDirectories as $key => &$dir) {
                    if (!file_exists($dir)) {
                        unset($templatesDirectories[$key]);
                    }
                }

                if (!count($templatesDirectories)) {
                    throw new \ErrorException('Cannot find template path');
                }

                $this->_twigLoader = new \Twig_Loader_Filesystem($templatesDirectories);

                $this->_twig = new \Twig_Environment($this->_twigLoader, [
                    'cache' => $this->cache,
                    'debug' => $this->config->is('debug'),]);

                if ($this->config->is('debug')) {
                    if ($this->cache && file_exists($this->cache)) {
                        $this->_twig->clearCacheFiles();
                    }

                    $this->_twig->addExtension(new \Twig_Extension_Debug());
                }

                //            $this->fireEvent('afterCreateTwig', $this, $this->_twig);
                $this->registerGlobal($this->_twig);
            }

            return $this->_twig;
        }

        /**
         * TODO: убрать из данного класа после реализации событий
         * Регистрируем глобальные переменные и фукнции в шаблонизаторе
         *
         * @param \Twig_Environment $twig
         */
        protected function registerGlobal(\Twig_Environment $twig) {

            /** @var WebRouter $router */
            $router = $this->getComponent('router');

            $twig->addGlobal('lang', $this->getOwner()->getLang());
            $twig->addGlobal('baseUrl', $router->createInternalUrl('/'));
            $twig->addGlobal('staticUrl', $router->getStaticUrl());
            $twig->addGlobal('config', App::getInstance()->getConfig());

            $twig->addFunction(new \Twig_SimpleFunction('_', [
                $this->getOwner(),
                '_']));
            $twig->addFunction(new \Twig_SimpleFunction('widget', [
                $this,
                'renderWidget']));
            $twig->addFunction(new \Twig_SimpleFunction('config', [
                $this,
                'renderWidget']));

            $twig->addFunction(new \Twig_SimpleFunction('url', [
                $router,
                'createInternalUrl']));
            $twig->addFunction(new \Twig_SimpleFunction('isCurrentPage', [
                $router,
                'isCurrentPage']));

            $twig->addFunction(new \Twig_SimpleFunction('formatDateDiff', [
                $this->getComponent('formatter'),
                'formatDateDiff']));

            $twig->addFilter(new \Twig_SimpleFilter('price', [
                $this->getComponent('formatter'),
                'price']));
            $twig->addFilter(new \Twig_SimpleFilter('base64', [
                $this->getComponent('formatter'),
                'base64']));
        }

        public function addGlobal($name, $value) {

            $this->twig->addGlobal($name, $value);

            return $this;
        }

        public function addPath($path, $namespace = \Twig_Loader_Filesystem::MAIN_NAMESPACE) {

            $this->getTwig()->getLoader()->addPath($path, $namespace);
        }

        /**
         * TODO: убрать из данного класа после реализации событий
         * @param string $widgetName
         * @param array $data
         *
         * @return string
         */
        public static function renderWidget($widgetName, $data = null) {

            $widgetClass = '\\SORApp\Widgets\\' . $widgetName;
            if (class_exists($widgetClass)) {
                /** @var BaseWidget $widget */
                $widget = new $widgetClass($data);

                return $widget->run();
            }
            else {
                return '';
            }
        }

        /**
         * @param $viewName
         *
         * @return \Twig_TemplateInterface
         */
        public function loadTemplate($viewName) {

            $viewName = trim($viewName, '/');
            if (!preg_match('#\.twig$#i', $viewName)) {
                $viewName .= '.twig';
            }

            try {
                $template = $this->twig->loadTemplate($viewName);
            }
            catch (\Twig_Error_Loader $e) {
                $template = $this->twig->loadTemplate($this->getOwner()->module . DIRECTORY_SEPARATOR . $this->getOwner()->controller .
                                                      DIRECTORY_SEPARATOR . $viewName);
            }

            return $template;
        }

        public function render($viewName, $context = []) {

            return $this->loadTemplate($viewName)->render($context);
        }

        public function renderBlock($block, $viewName, $context = []) {

            return $this->loadTemplate($viewName)->renderBlock($block, $context);
        }

        public function display($viewName, $context = [], $blocks = []) {

            return $this->loadTemplate($viewName)->display($context, $blocks);
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 18.12.14 13:29
     */

    namespace SORApp\Components;

    use SORApp\Components\Exceptions\WithDebugException;

    class CliResponse extends Response {

        public function __construct() {

            $this->acceptType = 'text/plain';
        }

        public function addError(\Exception $exception) {

            echo '------------------------------------------------' . PHP_EOL;
            echo $this->getTimeString() . ' Error: ' . get_class($exception) . PHP_EOL;
            echo '------------------------------------------------' . PHP_EOL;
            echo $exception->getMessage() . PHP_EOL . PHP_EOL;
            echo ($exception instanceof WithDebugException ? $exception->getDebugMessage() : '') . $exception->getFile() . '(' .
                 $exception->getLine() . '): ' . PHP_EOL;
            echo $exception->getTraceAsString() . PHP_EOL;
            echo '------------------------------------------------' . PHP_EOL;
        }

        public function getTimeString($strFormat = 'Y-m-d H:i:s.u', $uTimeStamp = null) {

            if (is_null($uTimeStamp)) {
                $uTimeStamp = microtime(true);
            }

            // Round the time down to the second
            $dtTimeStamp = floor($uTimeStamp);

            // Determine the millisecond value
            $intMilliseconds = round(($uTimeStamp - $dtTimeStamp) * 1000000);
            // Format the milliseconds as a 6 character string
            $strMilliseconds = str_pad($intMilliseconds, 6, '0', STR_PAD_LEFT);

            // Replace the milliseconds in the date format string
            // Then use the date function to process the rest of the string
            return date(preg_replace('`(?<!\\\\)u`', $strMilliseconds, $strFormat), $dtTimeStamp);
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 12.09.14 12:11
     */

    namespace SORApp\Components;

    class Database {

        public $prefix;
        public $dsn;

        protected $_connection;

        /**
         * @return \DbSimple_Database
         */
        public function getConnection() {

            if ($this->_connection === null) {

                include_once(SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Services' . DIRECTORY_SEPARATOR . 'DbSimple' . DIRECTORY_SEPARATOR .
                             'Generic.php');

                $generic = new \DbSimple_Generic();
                $this->_connection = $generic->connect($this->dsn);

                $log = function ($db, $query, $trace) {

                    \SORApp\Components\App::Log('SQL', $query);
                };
                $this->_connection->setLogger($log);

                if (!empty($this->prefix)) {
                    $this->_connection->setIdentPrefix($this->prefix);
                }
            }

            return $this->_connection;
        }
    }

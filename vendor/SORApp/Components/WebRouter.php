<?php

    namespace SORApp\Components;

    class WebRouter extends BaseComponent {

        public $host;

        public $baseUri;

        public $staticUri;

        public $isLangBeforeBaseUri = true;

        public $acceptTypes = ['text/html'];

        public $modules = [];

        public $defaultModule;

        protected $_scheme;

        protected $_requestUri;

        protected $_realBaseUri;

        protected $_lang;

        protected $_module;

        protected $_controller;

        protected $_action;

        public function prepareRequest() {

            $this->host = rtrim($this->host, '/');
            $this->baseUri = trim($this->baseUri, '/');
            $this->_scheme = (!filter_input(INPUT_SERVER, 'HTTPS') || filter_input(INPUT_SERVER, 'HTTPS') == 'off') ? 'http' : 'https';

            $serverName = filter_input(INPUT_SERVER, 'SERVER_NAME');
            if (empty($serverName)) {
                $serverName = $_SERVER['SERVER_NAME'];
            }
            if (empty($serverName) || strpos($serverName, $this->host) !== 0) {
                throw new \ErrorException('Incorrect application host name (expected: ' . $this->host . ', in reality: ' . $serverName . ')');
            }

            $this->_requestUri = trim(filter_input(INPUT_SERVER, 'REQUEST_URI'), '/');
            if (empty($this->_requestUri)) {
                $this->_requestUri = $_SERVER['REQUEST_URI'];
            }

            // Определяем язык приложения
            $pos = strpos($this->baseUri, '{lang}');
            if (FALSE !== $pos) {

                $matches = [];
                $pattern = '#^' . ($pos <= 0 ? '' : preg_quote(substr($this->baseUri, 0, $pos))) . '([a-zA-Z\-]{2,5})' .
                           preg_quote(substr($this->baseUri, $pos + strlen('{lang}'))) . '#i';

                if (!preg_match_all($pattern, $this->_requestUri, $matches, PREG_SET_ORDER)) {

                    $this->getComponent('response')->redirect($this->_scheme . '://' . $this->host . '/' .
                                                              str_replace('{lang}', $this->config['defaultLanguage'], $this->baseUri), 301, true);
                }
                list($this->_realBaseUri, $this->_lang) = $matches[0];
                $this->_lang = substr($this->_lang, 0, 2);
            }
            else {

                $this->_realBaseUri = $this->baseUri;
                $this->_lang = $this->getOwner()->defaultLanguage;
            }

            // Получаем строчку с адресом
            $applicationRequest = substr($this->_requestUri, empty($this->_realBaseUri) ? 0 : (strpos($this->_requestUri, $this->_realBaseUri) + strlen($this->_realBaseUri)));

            // Обрезаем все что после знака вопроса
            if (($queryStringPos = strpos($applicationRequest, '?')) !== false) {
                $applicationRequest = substr($applicationRequest, 0, $queryStringPos);
            }

            $this->routingRequest($applicationRequest);
        }

        public function routingRequest($applicationRequest) {

            $applicationRequest = (!empty($applicationRequest) ? array_diff(explode('/', trim($applicationRequest, '/')), ['']) : []);

            $this->_module = count($applicationRequest) &&
                             array_key_exists($applicationRequest[0], $this->modules) ? array_shift($applicationRequest) : $this->defaultModule;

            switch (count($applicationRequest)) {

                case 2:
                    list($this->_controller, $this->_action) = $applicationRequest;
                    break;

                case 1:
                    $this->_controller = array_shift($applicationRequest);
                    $this->_action = $this->modules[$this->_module]['defaultAction'];
                    break;

                case 0:
                    $this->_controller = $this->modules[$this->_module]['defaultController'];
                    $this->_action = $this->modules[$this->_module]['defaultAction'];
                    break;

                default:
                    throw new \ErrorException('Page not found');
            }
        }

        public function getLang() {

            return substr(!empty($this->_lang) ? $this->_lang : (!empty($_REQUEST['lang']) ? $_REQUEST['lang'] : $this->config['defaultLanguage']), 0, 2);
        }

        public function getAccept() {

            $headerHttpAccept = filter_input(INPUT_SERVER, 'HTTP_ACCEPT');
            if (empty($headerHttpAccept)) {
                $headerHttpAccept = $_SERVER['HTTP_ACCEPT'];
            }
            if ($headerHttpAccept) {
                $accepts = preg_split('/\s*(?:,*("[^"]+"),*|,*(\'[^\']+\'),*|,+)\s*/', $headerHttpAccept, 0, PREG_SPLIT_NO_EMPTY |
                                                                                                             PREG_SPLIT_DELIM_CAPTURE);
                if (!empty($accepts)) {
                    foreach ($accepts as $accept) {
                        if (in_array($accept, $this->acceptTypes)) {
                            return $accept;
                        }
                    }
                }
            }

            return $this->modules[$this->getModule()]['acceptDefault'];
        }

        public function getModule() {

            return $this->_module ? $this->_module : $this->defaultModule;
        }

        public function createInternalUrl($uri, $query = false, $lang = false) {

            $query = is_array($query) && count($query) ? (strpos($uri, '?') !== false ? '&' : '?') . http_build_query($query, null, '&') : '';
            if (strpos($uri, '://') !== false) {
                return $uri . $query;
            }

            return (empty($this->_scheme) ? '' : $this->_scheme . '://' . $this->host) .
                   //'/' . ltrim($lang === false ? $this->_realBaseUri : str_replace('{lang}', $lang, $this->baseUri), '/') .
                   '/' . ltrim(str_replace('{lang}', $lang === false ? $this->lang : $lang, $this->baseUri), '/') . '/' . ltrim($uri, '/') .
                   $query;
        }

        public function isCurrentPage($uri) { // Проверяет является ли указанный путь текущим

            if (!is_array($uri)) {

                if (!is_string($uri)) {
                    throw new \ErrorException('Incorrect call ' . __FUNCTION__);
                }
                $uri = explode('/', $uri);
            }

            return (count($uri) != 3 || array_shift($uri) == $this->getModule()) &&
                   (count($uri) == 2 && array_shift($uri) == $this->getController()) &&
                   (current($uri) == '*' || current($uri) == $this->getAction());
        }

        public function getController() {

            return $this->_controller;
        }

        public function getAction() {

            return $this->_action;
        }

        public function getStaticUrl() {

            return (empty($this->_scheme) ? '' : $this->_scheme . '://' . $this->host) .
                   preg_replace('#[/]+#', '/', '/' . ltrim($this->staticUri, '/') . '/');
        }
    }

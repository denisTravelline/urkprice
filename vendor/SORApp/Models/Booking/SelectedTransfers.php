<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 06.10.14 13:35
     */

    namespace SORApp\Models\Booking;

    /**
     * Class SelectedTransfers
     * @package SORApp\Models\Booking
     *
     * Данная модель предназначена для хранения выбранных трансферов
     */
    class SelectedTransfers extends AbstractBookingEntity {

        public $transferId;
        public $place;
        public $type;
        public $seats;
        public $cost;
        public $currency;
        public $comment;
    }

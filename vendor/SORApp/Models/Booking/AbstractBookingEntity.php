<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Models\Booking;

    use SORApp\Components\Model\BaseModel;
    use SORApp\Services\Validator\ValidatorException;

    /**
     * Class AbstractBookingEntity
     * @package SORApp\Models\Booking
     *
     * @var \SORApp\Models\Booking\AbstractBookingFactory $factory
     */
    class AbstractBookingEntity extends BaseModel {

        /** @var int $id Первичный ключ сущности */
        public $id;

        /** @var string $hash_key Связь с поиском */
        public $hashKey;

        /** @var int $version Версия структуры */
        public $version;

        /** @var string */
        public $userSessionKey;

        /** @var string */
        public $created_at;

        /** @var \SORApp\Models\Booking\AbstractBookingFactory */
        protected $_factory;

        protected $_relations = [];

        public function __construct() {

            $this->userSessionKey = session_id();
        }

        public function validateRules() {

            return [
                'hashKey' => ['string' => ['requires']],
                'created_at' => [
                    'datetime' => [
                        'allowEmpty',
                        'testFormat' => 'Y-m-d H:i:s']],];
        }

        public function __get($attr) {

            return (strpos($attr, 'related') === 0) ? $this->getRelation(substr($attr, strlen('related'))) : parent::__get($attr);
        }

        /**
         * @param string $factoryName
         *
         * @return \SORApp\Models\Booking\AbstractBookingEntity|array|bool
         */
        public function getRelation($factoryName) {

            if (!isset($this->_relations[$factoryName])) {
                $this->_relations[$factoryName] = false;

                $relationFactory = AbstractBookingFactory::getFactory($factoryName);
                if ($relationFactory) {
                    if ($relationFactory instanceof AbstractBookingCollectionFactory) {
                        $relationFactory->addCollectionRelation($this);
                    }

                    $entity = $relationFactory->loadEntity($this->hashKey);
                    if ($entity instanceof AbstractBookingEntity) {
                        $entity->addRelation($this);
                    }

                    $this->_relations[$factoryName] = $entity;
                }
            }

            return $this->_relations[$factoryName];
        }

        public function addRelation(AbstractBookingEntity $entity) {

            if (empty($this->hashKey) && !empty($entity->hashKey)) {
                $this->hashKey = $entity->hashKey;
            }
            else {
                if (!empty($this->hashKey) && $this->hashKey != $entity->hashKey) {
                    throw new ValidatorException('Incorrect relation');
                }
            }

            $this->_relations[$entity->getFactory()->factoryName] = $entity;
        }

        /**
         * @return AbstractBookingFactory
         */
        public function getFactory() {

            return $this->_factory;
        }

        /**
         * @param AbstractBookingFactory $factory
         */
        public function setFactory(AbstractBookingFactory $factory) {

            $this->_factory = $factory;
        }

        public function toDatabaseArray() {

            $reflect = new \ReflectionClass($this);
            $props = $reflect->getProperties(\ReflectionProperty::IS_PUBLIC);

            $array = [];
            foreach ($props as $prop) {
                $attr = $prop->getName();
                $getter = 'get' . ucfirst($attr);
                $array[$attr] = (method_exists($this, $getter)) ? call_user_func([
                    $this,
                    $getter]) : $prop->getValue($this);
            }

            return $array;
        }

        public function beforeSave() {

            if ($this->isAllowedChange() && $this->validate()) {
                if (empty($this->id) && empty($this->created_at)) {
                    $this->created_at = date('Y-m-d H:i:s');
                }

                return true;
            }

            return false;
        }

        public function isAllowedChange() {

            return true;
        }

        public function afterSave() {
        }

        public function afterCreate() {
        }
    }

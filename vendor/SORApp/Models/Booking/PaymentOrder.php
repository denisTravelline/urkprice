<?php

    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 13.02.15 15:43
     */

    namespace SORApp\Models\Booking;

    use SORApp\Components\App;

    class PaymentOrder extends AbstractBookingEntity {

        const TYPE_FIRST_DAY = 1;

        const TYPE_FULL = 2;

        public $type;

        public $amount;

        public $currency;

        public $account;

        public $description;

        public $providerName;

        public $timeout;

        public $result;

        public $status;

        public function beforeSave() {

            if (!parent::beforeSave()) {

                App::Log('Payment', 'Платёж не будет сохранён в БД (истёк срок годности или нарушена очерёдность действий): ' .
                                    json_encode($this, JSON_UNESCAPED_UNICODE));

                return false;
            }

            return true;
        }

        public function execute() {

            $searchRequest = $this->getRelation('searchRequest');

            $accounts = [];
            $reservation = $this->getRelation('reservation');
            $rooms_count = count($reservation->selectedRooms);
            if ($rooms_count > 1) {

                $accounts = $reservation->customer['accounts'];
                if (count($accounts) != $rooms_count || count($reservation->selectedRooms) != $rooms_count) {
                    return false;
                }
            }

            $services = [];
            foreach ($reservation->selectedRooms as $i => $selected) {

                if (!!$searchRequest->foundRooms && !!$searchRequest->foundRooms['roomsGroups']) {

                    $groups = &$searchRequest->foundRooms['roomsGroups'];
                    if (!!$groups[$i]) {

                        $rooms = &$groups[$i];
                        if (!!$rooms[$selected]) {

                            $room = &$rooms[$selected];
                            if (!!$room['prices']) {

                                $prices = &$room['prices'];
                                if (!!$prices['services']) {

                                    foreach ($prices['services'] as $service) {

                                        if (!!$service['PriceDates']) {

                                            $dates = &$service['PriceDates'];
                                            if (($this->type & 3) == self::TYPE_FIRST_DAY) {
                                                $dates = [$dates[0]];
                                            }
                                            if ($rooms_count > 1) {
                                                foreach ($dates as &$date) {
                                                    $date['GuestAccount'] = $accounts[$i];
                                                }

                                            }

                                        }
                                        array_push($services, $service);
                                    }

                                }

                            }

                        }

                    }

                }

            }
            return $searchRequest->getServioConnection()->setReservationType($this->account, $this->amount, $services, $rooms_count > 1);
        }

    }

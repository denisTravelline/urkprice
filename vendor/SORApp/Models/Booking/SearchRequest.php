<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Models\Booking;

    use SORApp\Components\App;
    use SORApp\Models\Room;
    use SORApp\Services\Validator\ValidatorException;

    /**
     * Class SearchRequest
     * @package SORApp\Models\Booking
     *
     * @property \SORApp\Services\Servio\Servio $servioConnection
     */
    class SearchRequest extends AbstractBookingEntity {

        const PAYMENT_TYPE_CASH = 100;
        const PAYMENT_TYPE_ONLINE = 200;
        const PAYMENT_TYPE_CASHLESS = 300;

        protected $needTransfer = 0; // Temp

        /** @var int $hotelID отеля */
        public $hotelId;

        public $arrival;
        public $departure;

        public $paymentType = self::PAYMENT_TYPE_CASH;
        public $contractId = 0;
        public $isTouristTax;

        public $companyCode = "";//"CA3%XAGQU6";
        public $loyaltyCard = "";//"12345678";

        public $rooms;
        public $foundRooms;

        /** @var array $_hotelInfo */
        protected $_hotelInfo;

        protected $_roomsInfo;

        protected $_companyInfo;
        protected $_loyaltyCardInfo;
        protected $_isCompany;

        public function afterCreate() {

            //if (empty($bk = $this->config->getSection('booking'))) return;
            //if (!isset($this->hotelId)) && null !== ($id = $bk['defaultHotelId'])) $this->hotelId = $id;
            //if (!isset($this->isTouristTax)) && null !== ($tt = $bk['defaultTouristTax'])) $this->isTouristTax = $tt;

            if (!isset($this->hotelId)) $this->hotelId = $this->config["@booking/defaultHotelId"];
            if (!isset($this->isTouristTax)) $this->isTouristTax = $this->config["@booking/defaultTouristTax"];
        }

        public function validateRules() {

            return array_merge(parent::validateRules(), [
                'hashKey' => ['string' => ['allowEmpty']],
                'hotelId' => [
                    App::_('validate', 'ValidateError_HotelID'),
                    'number' => [
                        'requires',
                        'int',
                        'min' => 1]],

                'arrival' => [
                    App::_('validate', 'ValidateError_Arrival'),
                    'datetime' => ['fromFormat' => 'd.m.Y H:i']],
                'departure' => [
                    App::_('validate', 'ValidateError_Departure'),
                    'datetime' => ['fromFormat' => 'd.m.Y H:i']],

                'paymentType' => [
                    App::_('validate', 'ValidateError_PayType'),
                    'number' => [
                        'int',
                        'range' => [
                            'range' => [
                                100,
                                200,
                                300]]]],
                'isTouristTax' => [
                    'boolean' => [
                        'defaultOnEmpty' => false,
                        'convert']],
                'contractId' => [
                    App::_('validate', 'ValidateError_ContractId'),
                    'number' => [
                        'int',
                        'min' => 0]],

                // Safe attributes
                'companyCode' => [
                    '',
                    'string' => ['allowEmpty']],
                'loyaltyCard' => [
                    '',
                    'string' => ['allowEmpty']],

                'rooms' => [
                    App::_('validate', 'ValidateError_Rooms'),
                    'callback' => [
                        $this,
                        'roomValidator']]]);
        }

        public function toDatabaseArray() {

            $array = parent::toDatabaseArray();
            $array['rooms'] = json_encode($this->rooms, JSON_UNESCAPED_UNICODE);
            $array['foundRooms'] = json_encode($this->foundRooms, JSON_UNESCAPED_UNICODE);

            return $array;
        }

        public function __isset($name) {

            return in_array($name, [
                'hotelInfo',
                'roomsInfo',
                'companyInfo',
                'loyaltyCardInfo',
                'isCompany']);
        }

        public function setRooms($roomsData) {

            if (is_string($roomsData)) {
                $roomsData = json_decode($roomsData, true);
            }

            $this->rooms = $roomsData;
        }

        public function roomValidator($roomsData = []) {

            if (!is_array($roomsData)) throw new ValidatorException();

            $rooms = [];
            foreach ($roomsData as $room) {

                if (!isset($room['childrenAges'])) {

                    $room['children'] = !isset($room['children']) ? 0 // throw new ValidatorException();
                        : $this->validateValue($room['children'], 'number', [
                            'defaultOnEmpty' => 0,
                            'int',
                            'min' => 0,
                            'max' => 4]);

                    $room['childrenAges'] = ($room['children'] > 0 ? array_fill(0, $room['children'], 10) : []);
                }
                else {

                    $room['childrenAges'] = array_filter($room['childrenAges'], 'is_numeric');
                    $room['children'] = count($room['childrenAges']);
                }
                if (!isset($room['infants'])) {

                    $i = 0;
                    $ca = $this->config["@booking/childAge"];
                    foreach ($room['childrenAges'] as $age) if ($age < $ca) $i++;

                    $room['infants'] = $i;
                }
                $rooms[] = [
                    'adults' => $this->validateValue($room['adults'], 'number', [
                        'defaultOnEmpty' => 1,
                        'int',
                        'min' => 1,
                        'max' => 4]),
                    'children' => $this->validateValue($room['children'], 'number', [
                        'defaultOnEmpty' => 0,
                        'int',
                        'min' => 0,
                        'max' => 4]),
                    'childrenAges' => $room['childrenAges'],
                    'infants' => $this->validateValue($room['infants'], 'number', [
                        'defaultOnEmpty' => 0,
                        'int',
                        'min' => 0,
                        'max' => 4]),
                    'isExtraBedUsed' => $this->validateValue(isset($room['isExtraBedUsed']) ? $room['isExtraBedUsed'] : false, 'boolean', [
                        'defaultOnEmpty' => false,
                        'convert']),];
            }

            return $rooms;
        }

        public function setFoundRooms($foundRooms) {

            if (!is_array($foundRooms) && !($foundRooms = json_decode($foundRooms, true))) throw new ValidatorException();

            $this->foundRooms = $foundRooms;
        }

        public function getTotalGuests() {

            $guests = 0;
            foreach ($this->rooms as $room) $guests += $room['adults'] + count($room['childrenAges']);

            return $guests;
        }

        public function getHotelInfo() {

            if ($this->_hotelInfo === null) $this->_hotelInfo = $this->hotelModel->getInfo($this->hotelId, $this->getOwner()->getLang());

            return $this->_hotelInfo;
        }

        public function getServioConnection() {

            return $this->hotelModel->getServioConnect($this->hotelId);
        }

        /**
         * Выполняем новый поиск вариантов размещения
         * Принудительно генерируется новый хещ и id
         * @return bool
         */
        public function execute() {

            $this->id = null;
            $this->created_at = date('Y-m-d H:i:s');
            $this->hashKey = $this->generateHashKey();

            $companyInfo = $this->getCompanyInfo();

            $this->foundRooms = $this->getServioConnection()
                ->searchQuery($this->hotelInfo['servioId'], $this->arrival, $this->departure, $this->rooms, $this->getOwner()->lang,
                    ($companyInfo['CompanyID'] > 0 ? $companyInfo['CompanyID'] : 0), ($companyInfo['CompanyCodeID'] > 0 ? $companyInfo['CompanyCodeID'] : 0),
                    $this->contractId, $this->paymentType, $this->needTransfer, $this->isTouristTax, $this->loyaltyCard);
                            
            if ($this->foundRooms && is_array($this->foundRooms['contracts']) && count($this->foundRooms['contracts']) == 1) $this->contractId =
                key($this->foundRooms['contracts']);

            // Сортируем
            if ($this->foundRooms && $this->config->is('@booking/sortingByPrice')) {

                foreach ($this->foundRooms['roomsGroups'] as &$group) {

                    uasort($group, function ($a, $b) {

                        if (array_key_exists('prices', $a) && array_key_exists('prices', $b) && $a['prices']['PriceTotal'] > 0 &&
                            $b['prices']['PriceTotal'] > 0
                        ) {

                            return ($a['prices']['PriceTotal'] > $b['prices']['PriceTotal'] ? 1 : -1);
                        }
                        if (!array_key_exists('prices', $a) && !array_key_exists('prices', $b)) {

                            
                            return 0;
                            
                        }

                        return !array_key_exists('prices', $a) ? 1 : -1;
                    });
                }
            }

            return is_array($this->foundRooms);
        }

        public function getCompanyInfo() {

            $code = !empty($this->companyCode) ? $this->companyCode : $this->config["@booking/defaultCompany"]['Code'];

            //if ($this->_companyInfo === null && !empty($this->companyCode))
            if ($this->_companyInfo === null || (!empty($this->_companyInfo['Code']) && $this->_companyInfo['Code'] != $code)) {

                $this->_companyInfo = $this->servioConnection->getCompanyInfo($code);
            }

            return $this->_companyInfo;
        }

        public function getLoyaltyCardInfo() {

            if ($this->_loyaltyCardInfo === null && !empty($this->loyaltyCard) && ($companyInfo = $this->getCompanyInfo())) {

                $this->_loyaltyCardInfo =
                    $this->servioConnection->getLoyaltyCardNumberInfo($companyInfo['CompanyID'], $this->servioConnection->getLoyaltyCardTypes(),
                        $this->hotelInfo['servioId'], $this->loyaltyCard);
            }

            return $this->_loyaltyCardInfo;
        }

        public function getRoomsInfo() {

            if ($this->_roomsInfo === null) {

                $this->_roomsInfo = $this->roomModel->getActive($this->hotelId, Room::FETCH_ARRAY_SERVIO);
            }

            return $this->_roomsInfo;
        }

        /**
         * Возвращает сгенерированую псевдоуникальнаю последовательность символов на основе функции uniqid
         *
         * @throws \ErrorException
         * @return string
         */
        protected function generateHashKey() {

            for ($i = 0; $i < 50; $i++) {

                $bytes = openssl_random_pseudo_bytes(24);
                $hashKey = substr(base_convert(bin2hex($bytes), 16, 36), 0, 32);

                if ($this->getFactory()->db->selectCell('SELECT id FROM ' . $this->getFactory()->getTableName() . ' WHERE `hashKey` = ? LIMIT 1', $hashKey) ==
                    0
                ) {

                    return $hashKey;
                }
            }
            throw new \ErrorException('Cannot generate unique search hash');
        }

    }

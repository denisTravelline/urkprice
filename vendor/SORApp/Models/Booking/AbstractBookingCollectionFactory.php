<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 05.12.14 16:39
     */

    namespace SORApp\Models\Booking;

    class AbstractBookingCollectionFactory extends AbstractBookingFactory {

        /** @var SelectedTransfers */
        protected $_emptyEntity;

        protected function __construct($factoryName) {

            parent::__construct($factoryName);
            $this->_emptyEntity = parent::createEntity();
        }

        public function addCollectionRelation(AbstractBookingEntity $entity) {

            $this->_emptyEntity->addRelation($entity);
        }

        /**
         * @param string $hashKey
         *
         * @return array|bool
         */
        public function loadEntity($hashKey) {

            if (empty($hashKey)) {
                return false;
            }

            if (!isset($this->_entities[$hashKey])) {
                $rows = $this->selectQuery($this->tableName, ['hashKey' => $hashKey]);

                if (!$rows) {
                    $this->_entities[$hashKey] = false;
                }
                else {
                    $this->_entities[$hashKey] = [];
                    foreach ($rows as $row) {
                        $this->_entities[$hashKey][] = $this->createEntity($row);
                    }
                }
            }

            return $this->_entities[$hashKey];
        }

        public function createEntity(array $data = []) {

            $entity = clone $this->_emptyEntity;
            if (count($data)) {
                $entity->autoSetAttributes($data, false);
            }

            return $entity;
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 18.09.14 14:00
     */

    namespace SORApp\Models\Booking;

    use SORApp\Components\Model\MySQLRepository;

    class EmailDocumentsQueue extends MySQLRepository {

        const STATUS_WAITING = 0;
        const STATUS_PROCESSING = 1;
        const STATUS_DONE = 2;

        const  DEADLOCK_INTERVAL_DEFAULT = '60 MINUTE';

        protected $id;

        public $status = self::STATUS_WAITING;
        public $account;
        public $lang;
        public $recipient;
        public $subject;
        public $message;
        public $hotel_id;

        public $created_at;
        public $updated_at;
        public $deadlock_at;
        public $itemCount = 0;

        protected $_documents = [];

        protected function setId($id) {

            $this->id = intval($id);

            return $this;
        }

        public function setStatus($status) {

            $this->status = $status;

            return $this;
        }

        public function setAccount($account) {

            $this->account = $account;

            return $this;
        }

        public function setLang($lang) {

            $this->lang = $lang;

            return $this;
        }

        public function setRecipient($recipient) {

            $this->recipient = $recipient;

            return $this;
        }

        public function setSubject($subject) {

            $this->subject = $subject;

            return $this;
        }

        public function setMessage($message) {

            $this->message = is_string($message) ? $message : serialize($message);

            return $this;
        }

        public function setHotel_id($hotelID) {

            $Id = intval($hotelID);
            $this->hotel_id = $Id ? $Id : null;

            return $this;
        }

        public function setDocument($id) {

            $Id = intval($id);
            if ($Id && !in_array($Id, $this->_documents)) {
                $this->_documents[] = $Id;
            }

            return $this;
        }

        public function setDocuments($ids) {

            $this->_documents = explode(',', $ids);

            return $this;
        }

        public function getDocuments() {

            return $this->_documents;
        }

        public function save() {

            if (empty($this->recipient) || empty($this->message)) {
                throw new \ErrorException('Empty email');
            }

            if (count($this->_documents)) {

                $status = $this->db->query('INSERT INTO ?_queue_emails_documents (`id`, `status`, `account`, `lang`, `recipient`, `subject`, `message`, `hotel_id`, `documents`, `created_at`)' .
                                           ' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, NOW())' . ' ON DUPLICATE KEY UPDATE' .
                                           '  `lang` = VALUES(`lang`),' . '  `status` = VALUES(`status`),' .
                                           '  `account` = VALUES(`account`),' . '  `subject` = VALUES(`subject`),' .
                                           '  `message` = VALUES(`message`),' . '  `hotel_id` = VALUES(`hotel_id`),' .
                                           '  `recipient` = VALUES(`recipient`),' . '  `documents` = VALUES(`documents`) ,' .
                                           '  `updated_at` = NOW()', $this->id, $this->status, $this->account, $this->lang, $this->recipient, $this->subject, $this->message, $this->hotel_id, implode(',', $this->_documents));

                if ($this->id === null) {
                    $this->id = $status;
                }
            }
        }

        public static function releaseDeadLocks($interval = self::DEADLOCK_INTERVAL_DEFAULT) {

            parent::db()->query("
                UPDATE ?_queue_emails_documents
                 SET `status` = '" . static::STATUS_WAITING . "', `deadlock_at` = NOW()
                 WHERE `status` = '" . static::STATUS_PROCESSING . "' AND `deadlock_at` IS NULL AND `updated_at` < NOW() - INTERVAL " .
                                $interval);
        }

        public static function startProcessingWaiting($limit = false) {

            parent::db()->transaction();

            $documents = parent::db()->select("
                SELECT t.id as ARRAY_KEY, t.*
                 FROM ?_queue_emails_documents t
                 WHERE t.`status` = ? {LIMIT ?d} FOR UPDATE
                ", self::STATUS_WAITING, $limit > 0 ? $limit : DBSIMPLE_SKIP);

            if (is_array($documents) && count($documents)) {

                parent::db()->query("
                    UPDATE ?_queue_emails_documents
                     SET `status` = ?, `updated_at` = NOW()
                     WHERE `id` IN (?a) LIMIT ?d
                    ", self::STATUS_PROCESSING, array_keys($documents), count($documents));

                foreach ($documents as $key => $row) {
                    $model = new self;
                    $row['status'] = self::STATUS_PROCESSING;
                    $model->autoSetAttributes($row, false);
                    $documents[$key] = $model;
                }
            }

            parent::db()->commit();

            return $documents;
        }

        public function getDocumentsList($offset = 0, $limit = 50) {

            $documents = parent::db()->selectPage($this->itemCount, "
                SELECT t.id as ARRAY_KEY, t.*, h.alias, CONCAT('document_',t.hotel_id,'_',t.documents,'.pdf') AS docname
                 FROM ?_queue_emails_documents t
                 LEFT JOIN ?_hotels h on t.hotel_id = h.id
                 ORDER BY `created_at` DESC
                 LIMIT ?d, ?d
                ", $offset, $limit);

            $directory = SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'servio_documents';
            foreach ($documents as &$v) {
                if (file_exists($directory . DIRECTORY_SEPARATOR . $v['docname'])) {
                    $v['link'] = true;
                    continue;
                }
                $v['link'] = false;
            }

            return $documents;
        }
    }

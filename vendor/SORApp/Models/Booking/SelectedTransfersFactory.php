<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 02.12.14 15:44
     */

    namespace SORApp\Models\Booking;

    use SORApp\Components\App;

    class SelectedTransfersFactory extends AbstractBookingCollectionFactory {

        /** @var array список доступных трансферов для текущего контейнера поиска номеров */
        protected $_availableTransfers;

        /**
         * Геттер для списка доступных трансферов для текущего контейнера поиска
         * @throws \ErrorException
         * @return array
         */
        public function getAvailableTransfers() {

            if ($this->_availableTransfers === null) {
                $searchRequest = $this->_emptyEntity->getRelation('SearchRequest');
                if (!$searchRequest) {
                    throw new \ErrorException('Cannot find available transfers without search request');
                }

                $this->_availableTransfers = $this->transferModel->getByHotelId($searchRequest->hotelId);
            }

            return $this->_availableTransfers;
        }

        public function hasTransfers() {

            $availableTransfers = $this->getAvailableTransfers();

            return (is_array($availableTransfers) && count($availableTransfers));
        }

        public function createTransfer($attributes = []) {

            $availableTransfers = $this->getAvailableTransfers();
            if (!isset($attributes['seats']) OR !is_numeric($attributes['seats']) OR !isset($attributes['type']) OR
                !in_array($attributes['type'], [
                    'arrival',
                    'departure']) OR !isset($attributes['place']) OR !is_numeric($attributes['place'])

                OR !isset($attributes['transferId']) OR !is_array($attributes['transferId']) OR
                !isset($attributes['transferId'][$attributes['place']]) OR !is_numeric($attributes['transferId'][$attributes['place']])

                OR !isset($availableTransfers[$attributes['place']]) OR !is_array($availableTransfers[$attributes['place']]) OR
                !isset($availableTransfers[$attributes['place']][$attributes['transferId'][$attributes['place']]])
            ) {
                $this->addError('transfer', App::_('validate', 'ValidateError_TransfersIncorrectFormat'));

                return false;
            }
            else {
                $entity = $this->createEntity();
                $entity->transferId = $attributes['transferId'][$attributes['place']];
                $entity->place = $attributes['place'];
                $entity->type = $attributes['type'];
                $entity->seats = $attributes['seats'];
                $entity->cost = $availableTransfers[$attributes['place']][$attributes['transferId'][$attributes['place']]]['cost_' .
                                                                                                                           $attributes['type']];
                $entity->currency = $availableTransfers[$attributes['place']][$attributes['transferId'][$attributes['place']]]['cost_currency'];
                $entity->comment = substr($attributes['comment'], 0, 100);

                return $entity;
            }
        }

        /**
         * Стандартизированный метод говорящий о том, что данный объект может добавлять сумму к общей стомости заказа
         *
         * @return array
         */
        public function getPriceTotal() {

            $priceTotal = [];
            if (count($this->_selectedTransfers)) {

                $availableTransfers = $this->getAvailableTransfers();
                foreach ($this->_selectedTransfers as $transfer) {

                    $attributes = $availableTransfers[$transfer['place']][$transfer['id'][$transfer['place']]];
                    $number = max(1, ceil($transfer['seats'] / $attributes['seats']));

                    if (!isset($priceTotal[$attributes['cost_currency']]))
                        $priceTotal[$attributes['cost_currency']] = 0;
                    $priceTotal[$attributes['cost_currency']] += $number * $attributes['cost_' . $transfer['type']];
                }
            }

            return $priceTotal;
        }

        /**
         * Метод возвращает список выбранных трансферов в виде отформатированной строки
         * @return string
         */
        public function toString() {

            $transfers = '';

            if (count($this->_selectedTransfers)) {
                $transfers .= 'Transfers:';

                $this->getAvailableTransfers();
                foreach ($this->_selectedTransfers as $transfer) {

                    $attributes = $availableTransfers[$transfer['place']][$transfer['id'][$transfer['place']]];
                    $number = max(1, ceil($transfer['seats'] / $attributes['seats']));

                    $transfers .= PHP_EOL . substr($transfer['type'], 0, 3) . '(' .
                                  $availableTransfers[$transfer['place']]['translates'][$this->getOwner()->getLang()]['name'] . ')' . ',' .
                                  $attributes['translates'][$this->getOwner()->getLang()]['name'] . ' ' . $transfer['seats'] . ' seats' . ',' .
                                  ($number * $attributes['cost_' . $transfer['type']]) . $attributes['cost_currency'] .
                                  (!empty($transfer['comment']) ? PHP_EOL . '(' . $transfer['comment'] . ')' : '');
                }
            }

            return $transfers;
        }
    }

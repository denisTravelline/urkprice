<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 27.11.14 15:14
 */

namespace SORApp\Models\Booking;


use SORApp\Components\Model\MySQLRepository;

/**
 * Class BookingFactory
 * @package SORApp\Models\Booking
 *
 * Объект отвечает за создание и хранение сущностей относящихся к процессу бронирования.
 * Любая сущность поддерживает версионность.
 *
 * @property string $tableName Название таблицы в которой храняться сущности
 * @property array $entityVersionMap Массив правил для выбора версии сущности
 */
class AbstractBookingFactory extends MySQLRepository
{
    public static $mapping = [

        // Сущность сохраняет все запросы пользователя и найденные варианты
        'SearchRequest' => array(
            '_tableName' => '?_booking_search_request',
            '_entityVersionMap' => array(
                '1' => 'SORApp\\Models\\Booking\SearchRequest',
            ),
        ),

        // Трансферы
        'SelectedTransfers' => array(
            '_tableName' => '?_booking_selected_transfers',
            '_entityVersionMap' => array(
                '1' => 'SORApp\\Models\\Booking\SelectedTransfers',
            ),
        ),

        // Сущность созданной бронировки
        'Reservation' => array(
            '_tableName' => '?_booking_reservation',
            '_entityVersionMap' => array(
                '1' => 'SORApp\\Models\\Booking\Reservation',
            ),
        ),

        // Платежи
        'PaymentOrder' => [
            '_tableName' => '?_booking_payment_order',
            '_entityVersionMap' => [
                '1' => 'SORApp\\Models\\Booking\PaymentOrder',
            ]
        ]
    ];

    protected $_factoryName;

    protected $_entities = array();

    /**
     * @var array $_factories Храним экземпляры фабрик
     */
    private static $_factories = array();

    /**
     * Найти фабрику для производства требуемых сущностей
     *
     * @param string $factoryName
     *
     * @return AbstractBookingFactory
     * @throws \ErrorException
     */
    public static function getFactory($factoryName)
    {
        $factoryName = ucfirst($factoryName);

        if ( ! isset(self::$_factories[$factoryName]) ) {
            $className = 'SORApp\\Models\\Booking\\' . $factoryName .'Factory';
            if ( class_exists($className) ) {
                self::$_factories[$factoryName] = new $className($factoryName);
            } else if ( isset(self::$mapping[$factoryName]) ) {
                self::$_factories[$factoryName] = new self($factoryName);
            } else {
                throw new \ErrorException('Factory "'. htmlentities($factoryName) .'" not found');
            }
        }

        return self::$_factories[$factoryName];
    }

    protected function __construct($factoryName)
    {
        $this->_factoryName = $factoryName;
    }

    public function getFactoryName()
    {
        return $this->_factoryName;
    }

    /**
     * Название таблицы
     *
     * @return string
     * @throws \ErrorException
     */
    public function getTableName()
    {
        if ( ! empty($this->_factoryName) AND isset(self::$mapping[$this->_factoryName]) AND isset(self::$mapping[$this->_factoryName]['_tableName']) ) {
            return self::$mapping[$this->_factoryName]['_tableName'];
        } else if ( property_exists($this, '_tableName') AND ! empty($this->_tableName) AND is_string($this->_tableName) ) {
            return $this->_tableName;
        } else {
            throw new \ErrorException('Empty entity table name');
        }
    }

    /**
     * Массив правил для выбора версии сущности
     *
     * @return array
     * @throws \ErrorException
     */
    public function getEntityVersionMap()
    {
        if ( ! empty($this->_factoryName) AND isset(self::$mapping[$this->_factoryName]) AND isset(self::$mapping[$this->_factoryName]['_entityVersionMap']) ) {
            return self::$mapping[$this->_factoryName]['_entityVersionMap'];
        } else if ( property_exists($this, '_entityVersionMap') AND is_array($this->_entityVersionMap) AND count($this->_entityVersionMap) ) {
            return $this->_entityVersionMap;
        }

        throw new \ErrorException('Empty entity version map');
    }

    public function getSafeKeys()
    {
        return array_keys($this->createEntity()->validateRules());
    }

    /**
     * Загружаем сущность. Если нет в базе, создаем новую
     *
     * @param string $hashKey
     * @param array  $default
     *
     * @return array|bool|AbstractBookingEntity
     */
    public function getEntity($hashKey = null, $default = array())
    {
        if ( ! ($entity = $this->loadEntity($hashKey)) ) {
            $default['hashKey'] = $hashKey;
            $entity = $this->createEntity($default);
        }

        return $entity;
    }

    /**
     * Загрузка сущностей из базы данных
     *
     * @param string $hashKey
     *
     * @return array|bool|AbstractBookingEntity
     */
    public function loadEntity($hashKey)
    {
        if ( empty($hashKey) ) {
            return false;
        }

        if ( ! isset($this->_entities[$hashKey]) ) {
            $rows = $this->selectQuery($this->tableName, array('hashKey' => $hashKey));

            if ( ! $rows ) {
                $this->_entities[$hashKey] = false;
            } else {
                $this->_entities[$hashKey] = $this->createEntity(current($rows));
            }
        }

        return $this->_entities[$hashKey];
    }

    /**
     * @param array      $filters
     * @param array      $orders
     * @param int        $offset
     * @param int        $limit
     *
     * @return array
     */
    public function getPage($filters = array(), $orders = array(), $offset = 0, $limit = 10)
    {
        $rows = $this->selectFilteredPage($this->tableName, $filters, $orders, $offset, $limit, $this->safeKeys);
        $page = array();
        if ( is_array($rows['page']) AND count($rows['page']) ) {
            foreach ( $rows['page'] as $row ) {
                $hashKey = $row['hashKey'];
                if ( ! isset($this->_entities[$hashKey]) ) {
                    $this->_entities[$hashKey] = $this->createEntity($row);
                }
                $page[$hashKey] =& $this->_entities[$hashKey];
            }
            $rows['page'] = $page;
        }

        return $rows;
    }

    public function loadByID($id)
    {
        $entity = false;

        $rows = $this->selectQuery($this->tableName, array('id' => $id), 1);
        if ( count($rows) === 1 ) {
            $entity = $this->createEntity(current($rows));
        }

        return $entity;
    }

    /**
     * Сохранение сущности в базу данных
     *
     * @param AbstractBookingEntity $entity
     *
     * @return bool
     */
    public function saveEntity(AbstractBookingEntity $entity)
    {
        if ( $entity->beforeSave() ) {
            $data = $entity->toDatabaseArray();
            if ( ! isset($data['id']) OR ! is_numeric($data['id']) ) {
                $entity->id = $this->insertQuery($this->tableName, $data);
            } else {
                $this->updateQuery($this->tableName, $data, array('id' => $data['id']));
            }
            $entity->afterSave();
            return true;
        }

        return false;
    }

    public function deleteEntity(AbstractBookingEntity $entity)
    {
        return ! empty($entity->id) && $this->deleteQuery($this->tableName, array('id' => $entity->id)) && $entity->id = null;
    }

    /**
     * Создание экземпляра сущности
     *
     * @param array $data
     *
     * @return AbstractBookingEntity
     * @throws \ErrorException
     */
    public function createEntity(array $data = array())
    {
        $entityVersionMap = $this->getEntityVersionMap();

        // Если не указана версия, используем первую из списка
        reset($entityVersionMap);
        $version = ( ! count($data) OR ! isset($data['version']) )
            ? key($entityVersionMap)
            : $data['version'];

        if ( ! isset($entityVersionMap[$version]) || ! class_exists($entityVersionMap[$version]) ) {
            throw new \ErrorException('Incorrect entity version "' . htmlentities($version) . '"');
        }

        /** @var AbstractBookingEntity $entity */
        $entity = new $entityVersionMap[$version];
        $entity->version = $version;
        $entity->factory = $this;

        if ( count($data) ) {
            $entity->autoSetAttributes($data, false);
        }

        $entity->afterCreate();
        return $entity;
    }
}

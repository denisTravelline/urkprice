<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Models\Booking;

    use SORApp\Components\App;
    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Model\MySQLRepository;
    use SORApp\Services\Validator\ValidatorException;

    /**
     * Class BookingContainer
     * @package SORApp\Models\Booking
     *
     * Данный класс реализует коллекцию данных/моделей для которых необходимо хранить состояние между запросами.
     *
     * @property int $hotelID
     * @property array $hotelInfo
     * @property array $payment
     * @property \SORApp\Services\Servio\Servio $servio
     * @property \SORApp\Models\Booking\SearchRequest $searchRequest
     * @property \SORApp\Models\Booking\FoundRooms $foundRooms
     * @property \SORApp\Models\Booking\SelectedTransfers $selectedTransfers
     * @property \SORApp\Models\Booking\Customer $customer
     * @property \SORApp\Models\Booking\Reservation $reservation
     */
    class BookingContainer extends MySQLRepository {

        protected $_hashKey;

        /** @var int $hotelID отеля */
        protected $_hotelID;

        /** @var array $_hotelInfo информация об отеле */
        protected $_hotelInfo;

        /** @var array $_payment информация об онлайн оплате */
        protected $_payment = [];

        /** @var \SORApp\Services\Servio\Servio */
        protected $_servio;

        /** @var SearchRequest $searchRequest Модель для хранения запроса на поиск данных */
        protected $_searchRequest;

        /** @var FoundRooms $foundRooms Модель для хранения информации по найденным номерам */
        protected $_foundRooms;

        /** @var SelectedTransfers Модель с информацие о трансфере */
        protected $_selectedTransfers;

        /** @var Customer $customer Модель с данными клиента */
        protected $_customer;

        /** @var Reservation Модель с информацие о статусе резервировании */
        protected $_reservation;

        public function setPayment(array $payment) {

            if (count($payment)) {
                $this->_payment = $payment;
            }

            return $this;
        }

        public function getPayment() {

            return $this->_payment;
        }

        public function getPriceSummary() {

            static $priceSummary;
            if ($priceSummary === null) {
                $priceSummary = $this->calculatePriceRecursive('getPriceTotal');
            }

            return $priceSummary;
        }

        public function getPriceFirstDay() {

            static $priceFirstDay;
            if ($priceFirstDay === null) {
                $priceFirstDay = $this->calculatePriceRecursive('getPriceFirstDay');
            }

            return $priceFirstDay;
        }

        protected function calculatePriceRecursive($method) {

            $priceSummary = [];
            foreach ($this as $item) {
                if (is_object($item) AND method_exists($item, $method)) {
                    $itemPrices = call_user_func([
                        $item,
                        $method]);
                    if (is_array($itemPrices) AND count($itemPrices)) {
                        foreach ($itemPrices as $currency => $value) {
                            if (!array_key_exists($currency, $priceSummary)) {
                                $priceSummary[$currency] = 0;
                            }
                            $priceSummary[$currency] += $value;
                        }
                    }
                }
            }

            return $priceSummary;
        }

        public function getServicesFirstDay() {

            static $servicesFirstDay;
            if ($servicesFirstDay === null) {
                $servicesFirstDay = $this->callChildren('getServicesFirstDay', true);
            }
        }

        protected function callChildren($method, $mergeResponse = false) {

            $response = [];
            foreach ($this as $item) {
                if (is_object($item) AND method_exists($item, $method)) {
                    $itemResponse = call_user_func([
                        $item,
                        $method]);
                    if ($mergeResponse) {
                        $response = array_merge($response, $itemResponse);
                    }
                    else {
                        $response[get_class($item)] = $itemResponse;
                    }
                }
            }

            return $response;
        }
    }

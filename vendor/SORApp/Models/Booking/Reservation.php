<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 */

namespace SORApp\Models\Booking;


use SORApp\Components\App;
use SORApp\Services\Validator\ValidatorException;

/**
 * Class Reservation
 * @package SORApp\Models\Booking
 *
 * Модель отвечат за резервирование номеров
 */
class Reservation extends AbstractBookingEntity {

    const TIMEOUT_CREATE = 1800;
    const TIMEOUT_MAX = 3600;

    /** @var array Массив выбраных номеров */
    public $selectedRooms;

    /** @var array Пользователь */
    public $customer;

    /** @var string */
    public $clientInfo;

    /** @var float Основная стоимость выбранных номеров */
    public $costServices;

    /** @var float Стоимость дополнительных сервисов */
    public $costAdditional = 0;

    /** @var string Валюта */
    public $currency;

    /** @var int Номер созданой бронировки */
    public $account;

    /** @var string Дата и время создания бронировки */
    public $reservation_at;

    /** @var bool Флаг разрешения сохранять резервирование */
    private $_isAllowedChange;

    /**
     * @inherit
     * @return array
     */
    public function validateRules() {

        return array_merge(
            parent::validateRules(),
            [
                'selectedRooms' => [
                    'array' => ['requires'],
                    'callback' => [$this, 'selectedRoomsValidator'],
                ],
                'customer' => [
                    'array' => [
                        'allowEmpty',
                        'intersect' => 'first_name,last_name,patronymic_name,email,phone,fax,address,country,comment,company,contact_name,contact_info,group_name,accounts',
                        'validate' => [
                            'rules' => [
                                'first_name' => [App::_('validate', 'ValidateError_FirstName'), 'string' => ['requires', 'min' => 1, 'max' => 255]],
                                'last_name' => [App::_('validate', 'ValidateError_LastName'), 'string' => ['requires', 'min' => 1, 'max' => 255]],
                                'patronymic_name' => [App::_('validate', 'ValidateError_PatronymicName'), 'string' => ['allowEmpty', 'max' => 255]],
                                'email' => [App::_('validate', 'ValidateError_Email'), 'string' => ['match' => '/^((([a-zA-Z]|\d|[!#$%&\'*+\-\/=?\^_`{|}~]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])+(\.([a-zA-Z]|\d|[!#$%&\'*+\-\/=?\^_`{|}~]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])|(\\\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-zA-Z]|\d|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])|(([a-zA-Z]|\d|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])([a-zA-Z]|\d|-|\.|_|~|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])*([a-zA-Z]|\d|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])))\.)+(([a-zA-Z]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])|(([a-zA-Z]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])([a-zA-Z]|\d|-|\.|_|~|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])*([a-zA-Z]|[\x{00A0}-\x{D7FF}\x{F900}-\x{FDCF}\x{FDF0}-\x{FFEF}])))\.?$/u']],
                                'phone' => [App::_('validate', 'ValidateError_Phone'), 'string' => ['allowEmpty', 'match' => '#^\+?[0-9\-\s]{5,16}$#']],
                                'fax' => [App::_('validate', 'ValidateError_Phone'), 'string' => ['allowEmpty', 'match' => '#^\+?[0-9\-\s]{5,16}$#']],
                                'address' => [App::_('validate', 'ValidateError_Address'), 'string' => ['allowEmpty', 'min' => 6, 'max' => 255]],
                                'country' => [App::_('validate', 'ValidateError_Country'), 'string' => ['allowEmpty', 'match' => '/^[a-zA-Z]{3}$/']],
                                'comment' => [App::_('validate', 'ValidateError_Comment'), 'string' => ['max' => 500]],
                                'company' => [App::_('validate', 'ValidateError_CompanyName'), 'string' => ['allowEmpty', 'min' => 3, 'max' => 255]],
                                'contact_name' => [App::_('validate', 'ValidateError_ContactName'), 'string' => ['allowEmpty', 'min' => 3, 'max' => 255]],
                                'contact_info' => [App::_('validate', 'ValidateError_Comment'), 'string' => ['max' => 400]],
                                'group_name' => [App::_('validate', 'ValidateError_GroupName'), 'string' => [count($this->selectedRooms) > 1 ? 'requires' : 'allowEmpty', 'min' => 3, 'max' => 255]],
                                'accounts' => [['allowEmpty']]
                            ]
                        ]
                    ]
                ],
                'account' => ['allowEmpty'],
                'reservation_at' => ['datetime' => ['allowEmpty', 'testFormat' => 'Y-m-d H:i:s']],
            ]
        );
    }

    /**
     * Валидация выбраных типов номеров
     * Проверяется соответствие запрошенных и выбраных комнат и количество свободных комнат
     *
     * @param array $rooms
     *
     * @return array
     * @throws \SORApp\Services\Validator\ValidatorException
     */
    public function selectedRoomsValidator(array $rooms) {

        $rooms = filter_var_array($rooms, FILTER_SANITIZE_NUMBER_INT);

        /** @var \SORApp\Models\Booking\SearchRequest $searchRequest */
        $searchRequest = $this->getRelation('SearchRequest');

        if (is_array($searchRequest->foundRooms['roomsGroups']) AND count($searchRequest->foundRooms['roomsGroups'])) {
            if (count($rooms) != count($searchRequest->foundRooms['roomsGroups'])) {
                throw new ValidatorException(App::_('validate', 'ValidateError_SelectRoom_NotRelation'));
            }

            $roomTypesCount = array();
            foreach ($rooms as $roomTypeId) {
                $roomTypesCount[$roomTypeId] = (isset($roomTypesCount[$roomTypeId])
                    ? $roomTypesCount[$roomTypeId] + 1
                    : 1
                );
            }

            foreach ($searchRequest->foundRooms['roomsGroups'] as $roomGroupNumber => $roomGroup) {
                if (!isset($rooms[$roomGroupNumber])
                    OR !isset($roomGroup[$rooms[$roomGroupNumber]])
                    OR $roomGroup[$rooms[$roomGroupNumber]]['prices']['PriceTotal'] <= 0
                ) {
                    throw new ValidatorException(App::_('validate', 'ValidateError_SelectRoom_NotRelation'));
                }
                else if ($roomGroup[$rooms[$roomGroupNumber]]['FreeRoom'] < $roomTypesCount[$rooms[$roomGroupNumber]]) {
                    throw new ValidatorException(App::_('validate', 'ValidateError_SelectRoom_NotFree'));
                }
            }
        }

        return $rooms;
    }

    /**
     * Разрешаем сохранение модели только если не вышли таймауты
     *
     * @return bool
     */
    public function isAllowedChange() {

        if ($this->_isAllowedChange === null) {
            if (
                ((!empty($this->_createdAccount) && $this->account == $this->_createdAccount) || (empty($this->reservation_at) && empty($this->account)))
                && ($searchRequest = $this->getRelation('SearchRequest'))
            ) {
                if ((empty($this->id)
                    ? strtotime($searchRequest->created_at) + self::TIMEOUT_CREATE > time()
                    : strtotime($this->created_at) + self::TIMEOUT_MAX > time()
                )
                ) {
                    $this->_isAllowedChange = true;
                }
                else {
                    $this->addError('id', App::_('validate', 'ValidateError_Timeout_Reservation'));
                    $this->_isAllowedChange = false;
                }
            }
            else {
                $this->_isAllowedChange = false;
            }
        }
        return $this->_isAllowedChange;
    }

    /**
     * Сериализируем выбранные комнаты для сохранения в базу данных
     * @return array
     */
    public function toDatabaseArray() {

        $array = parent::toDatabaseArray();
        $array['selectedRooms'] = json_encode($this->selectedRooms, JSON_UNESCAPED_UNICODE);
        $array['customer'] = json_encode($this->customer, JSON_UNESCAPED_UNICODE);
        return $array;
    }

    public function __isset($name) {

        return in_array($name, ['relatedSearchRequest']);
    }

    /**
     * Сеттер для выбраных комнат
     * Ожидается массив или json сериализированный массив
     * @param array|string $rooms
     */
    public function setSelectedRooms($rooms) {

        if (is_string($rooms)) {
            $rooms = json_decode($rooms, true);
        }
        $this->selectedRooms = $rooms;
    }

    public function setCustomer($customer) {

        if (is_string($customer)) {
            $customer = json_decode($customer, true);
        }
        $this->customer = $customer;
    }

    public function afterCreate() {

        if (empty($this->clientInfo)) {
            $this->setClientInfoDefault();
        }
    }

    public function calculateCostServices() {

        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->getRelation('SearchRequest');
        if (!$searchRequest) {
            throw new \ErrorException('Cannot create reservation without search request');
        }

        $this->costServices = (float)0;
        $this->currency = null;
        if (count($this->selectedRooms)) {

            foreach ($this->selectedRooms as $key => $selectedRoomID) {

                if ($this->currency == null) {
                    $this->currency = $searchRequest->foundRooms['roomsGroups'][$key][$selectedRoomID]['prices']['currency'];
                }
                else if ($searchRequest->foundRooms['roomsGroups'][$key][$selectedRoomID]['prices']['currency'] !== $this->currency) {
                    throw new \ErrorException('Currency should be the same for all prices');
                }

                $this->costServices = bcadd($this->costServices, $searchRequest->foundRooms['roomsGroups'][$key][$selectedRoomID]['prices']['PriceTotal'], 2);
            }

            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Получение суммарной стоимости сервисов по дням
     * @throws \ErrorException
     * @return array
     */
    public function getCostServicesByDays() {

        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->getRelation('SearchRequest');
        if (!$searchRequest) {
            throw new \ErrorException('Cannot calculate cost services without search request');
        }

        $dates = [];
        if (count($this->selectedRooms)) {

            foreach ($this->selectedRooms as $key => $selectedRoomID) {

                if ($this->currency == null) {
                    $this->currency = $searchRequest->foundRooms['roomsGroups'][$key][$selectedRoomID]['prices']['currency'];
                }
                else if ($searchRequest->foundRooms['roomsGroups'][$key][$selectedRoomID]['prices']['currency'] !== $this->currency) {
                    throw new \ErrorException('Currency should be the same for all prices');
                }

                foreach ($searchRequest->foundRooms['roomsGroups'][$key][$selectedRoomID]['prices']['services'] as $service) {

                    if (isset($service['PriceDates']) AND is_array($service['PriceDates']) AND count($service['PriceDates'])) {

                        foreach ($service['PriceDates'] as $priceDate) {

                            $currentDate = date('Y-m-d', strtotime($priceDate['Date']));
                            if (!isset($dates[$currentDate])) {
                                $dates[$currentDate] = $priceDate['Price'];
                            }
                            else {
                                $dates[$currentDate] = bcadd($dates[$currentDate], $priceDate['Price'], 2);
                            }
                        }
                    }
                }
            }
        }

        ksort($dates);
        return $dates;
    }

    public function addCostAdditional($cost, $currency) {

        if ($currency != $this->currency) {
            throw new \ErrorException('Currency should be the same for all prices');
        }

        if (bccomp($cost, 0, 2) <= 0) {
            throw new \ErrorException('Cannot add empty cost');
        }

        $this->costAdditional = bcadd($this->costAdditional, $cost, 2);
    }

    public function subCostAdditional($cost, $currency) {

        if ($currency != $this->currency) {
            throw new \ErrorException('Currency should be the same for all prices');
        }

        if (bccomp($cost, 0, 2) <= 0) {
            throw new \ErrorException('Cannot sub empty cost');
        }

        if (bccomp($this->costAdditional, $cost, 2) < 0) {
            throw new \ErrorException('Incorrect remove additional cost. Reservation is failed');
        }

        $this->costAdditional = bcsub($this->costAdditional, $cost, 2);
    }

    /**
     * Создаем резервирование на основе данных доступных в контейнере поиска
     *
     * @throws \ErrorException
     * @throws \SORApp\Services\Validator\ValidatorException
     * @return bool|int
     */
    public function doReservation() {

        if (!$this->isAllowedChange()) {
            return false;
        }

        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->getRelation('SearchRequest');
        if (!$searchRequest) {
            throw new \ErrorException('Cannot create reservation without search request');
        }

        $arrival = str_replace(' ', 'T', $searchRequest->arrival);
        $departure = str_replace(' ', 'T', $searchRequest->departure);

        // Денормализация иформации для создания брони
        $roomReservations = [];
        if (is_array($searchRequest->foundRooms['roomsGroups']) AND count($searchRequest->foundRooms['roomsGroups'])) {

            $selectedTransfers = $this->getRelation('SelectedTransfers');
            foreach ($searchRequest->foundRooms['roomsGroups'] as $key => $roomGroups) {

                $roomReservations[] = [
                    'DateArrival' => $arrival,
                    'DateDeparture' => $departure,
                    'TimeArrival' => $arrival,
                    'TimeDeparture' => $departure,

                    'Adults' => $searchRequest->rooms[$key]['adults'],
                    'Childs' => count($searchRequest->rooms[$key]['childrenAges']),
                    'ChildAges' => $searchRequest->rooms[$key]['childrenAges'],
                    'IsExtraBedUsed' => $searchRequest->rooms[$key]['isExtraBedUsed'],

                    'RoomTypeID' => (int)$this->selectedRooms[$key],
                    'NeedTransport' => (int)(is_array($selectedTransfers) && count($selectedTransfers)),
                    'IsTouristTax' => $searchRequest->isTouristTax,
                    'PriceListID' => $roomGroups[$this->selectedRooms[$key]]['prices']['priceListID'],
                    'LPAuthCode' => '',
                ];
            }
        }

        // Оправляем регистрационные данные на сервер Servio
        if (count($roomReservations) > 1) {
            $result = $searchRequest->getServioConnection()->addGroupRoomReservation(
                $searchRequest->hotelInfo['servioId'],
                $this->customer['group_name'],
                (is_array($searchRequest->companyInfo) ? $searchRequest->companyInfo['CompanyID'] : 0),
                $this->customer['company'],
                $this->customer['country'],
                $this->countryModel->getNameByIsoCode($this->customer['country']),
                $searchRequest->paymentType,
                $this->getCustomerFullName(),
                $this->customer['email'],
                (!empty($this->customer['phone']) ? 'Main phone: ' . $this->customer['phone'] . PHP_EOL : '') . $this->customer['contact_info'],
                $this->customer['comment'],
                $this->clientInfo,
                $searchRequest->contractId,
                $roomReservations,
                (isset($this->customer['contact_name']) ? $this->customer['contact_name'] : '')
            );
            foreach($result as $account => $guest_accounts) {
                $this->account = $account;
                $this->customer['accounts'] = $guest_accounts;
                break;
            }
        }
        else if (count($roomReservations) == 1) {
            $this->account = $searchRequest->getServioConnection()->addRoomReservation(
                $searchRequest->hotelInfo['servioId'],
                $roomReservations[0]['DateArrival'],
                $roomReservations[0]['DateDeparture'],
                $roomReservations[0]['TimeArrival'],
                $roomReservations[0]['Adults'],
                $roomReservations[0]['Childs'],
                $roomReservations[0]['ChildAges'],
                $roomReservations[0]['IsExtraBedUsed'],
                $this->customer['last_name'],
                $this->customer['first_name'],
                $roomReservations[0]['RoomTypeID'],
                (is_array($searchRequest->companyInfo) ? $searchRequest->companyInfo['CompanyID'] : 0),
                $this->customer['company'],
                $searchRequest->contractId,
                $searchRequest->paymentType,
                $this->customer['country'],
                $this->countryModel->getNameByIsoCode($this->customer['country']),
                $this->customer['address'],
                $this->customer['phone'],
                $this->customer['fax'],
                $this->customer['email'],
                $roomReservations[0]['NeedTransport'],
                $this->customer['comment'],
                $this->clientInfo,
                $roomReservations[0]['IsTouristTax'],
                $roomReservations[0]['PriceListID'],
                (is_array($searchRequest->loyaltyCardInfo) ? $searchRequest->loyaltyCardInfo['LoyaltyCardTypeID'] : 0),
                (is_array($searchRequest->loyaltyCardInfo) ? $searchRequest->loyaltyCardInfo['LoyaltyCardNumber'] : ''),
                (isset($this->customer['contact_name']) ? $this->customer['contact_name'] : ''),
                $roomReservations[0]['TimeDeparture']
            );
        }
        else {
            throw new ValidatorException(App::_('validate', 'ValidateError_Reservation_EmptyRoomReservation'));
        }

        if (!$this->account) {
            throw new ValidatorException(App::_('validate', 'ValidateError_Reservation_CannotCreateReservation'));
        }

        // Принудительно сохраняем состояние
        $this->_isAllowedChange = true;
        $this->reservation_at = date('Y-m-d H:i:s');
        $this->getFactory()->saveEntity($this);

        return $this->account;
    }

    public function getCustomerFullName() {

        return trim(
            preg_replace('/\s+/', ' ',
                $this->customer['last_name']
                . ' ' . $this->customer['first_name']
                . ' ' . $this->customer['patronymic_name']
            )
        );
    }

    private function setClientInfoDefault() {

        $this->clientInfo = 'IP: ' . $_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['REMOTE_PORT'] . PHP_EOL
            . 'User agent: ' . $_SERVER['HTTP_USER_AGENT'] . PHP_EOL
            . 'Request timestamp:' . $_SERVER['REQUEST_TIME'];
    }

    public function setCustomerDefault() {

        /** @var SearchRequest $searchRequest */
        $searchRequest = $this->getRelation('SearchRequest');
        if (!$searchRequest) {
            throw new \ErrorException('Cannot create reservation without search request');
        }

        // Применяем информацию из компании пользователя
        if ($companyInfo = $searchRequest->getCompanyInfo()) {
            if (!empty($companyInfo['CompanyName'])) {
                $this->customer['company'] = $companyInfo['CompanyName'];
            }

            if (!empty($companyInfo['CompanyFax'])) {
                $this->customer['fax'] = $companyInfo['CompanyFax'];
            }

            if (!empty($companyInfo['CompanyEmail'])) {
                $this->customer['email'] = $companyInfo['CompanyEmail'];
            }

            // Применяем ифнормацию из карты лояльности
            if ($loyaltyCardInfo = $searchRequest->getLoyaltyCardInfo()) {
                if (!empty($loyaltyCardInfo['GuestFirstName'])) {
                    $this->customer['first_name'] = $loyaltyCardInfo['GuestFirstName'];
                }

                if (!empty($loyaltyCardInfo['GuestLastName'])) {
                    $this->customer['last_name'] = $loyaltyCardInfo['GuestLastName'];
                }
            }
        }
        else {
            $customer = $this->getFactory()->db->selectCell(
                'SELECT `customer` FROM ' . $this->getFactory()->getTableName() . '
                    WHERE
                        `userSessionKey` = ?
                        AND `version` = ?
                        AND `created_at` > NOW() - INTERVAL 4 DAY
                        AND LENGTH(`customer`) > 10
                    LIMIT 1',
                $this->userSessionKey,
                $this->version
            );

            if (is_string($customer)) {
                $this->setCustomer($customer);
            }
        }
    }
}

<?php
    namespace SORApp\Models;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Model\MySQLRepository;
    use SORApp\Services\Servio;

    /**
     * Class Room
     * @package SORApp\Models
     *
     * @author Sevastianov Andrey
     */
    class Room extends MySQLRepository {

        const FETCH_ARRAY = false;
        const FETCH_ARRAY_PK = 'id';
        const FETCH_ARRAY_SERVIO = 'servioId';
        const FETCH_ARRAY_HOTEL = 'hotelId';

        public $id;
        public $hotelId;
        public $servioId;
        public $name;
        public $beds = 0;
        public $extBeds = 0;
        public $visible = 0;
        public $photo;

        public $translates;

        private $_newPhoto = [];

        public function validateRules() {

            return [
                'id' => [
                    'number' => [
                        'allowEmpty',
                        'int'
                    ]
                ],
                'hotelId' => [
                    'number' => [
                        'requires',
                        'int'
                    ]
                ],
                'servioId' => ['number' => ['int']],
                'name' => [
                    'Incorrect alias',
                    'string' => ['requires']
                ],

                'beds' => [
                    'number' => [
                        'int',
                        'min' => 0,
                        'max' => 20
                    ]
                ],
                'extBeds' => [
                    'number' => [
                        'int',
                        'min' => 0,
                        'max' => 20
                    ]
                ],
                'visible' => ['boolean' => ['convert']],
            ];
        }

        public function getActive($hotelId, $fetch_key = self::FETCH_ARRAY_PK) {

            return $this->getRooms($hotelId, 1, $this->getOwner()->getLang(), $fetch_key);
        }

        public function getRooms($hotelId = null, $active = null, $isoLang = null, $fetch_key = self::FETCH_ARRAY_PK) {

            $response = [];
            $rooms = $this->db->select('SELECT * FROM ?_rooms WHERE 1=1 { AND ?_rooms.hotelId = ? } { AND ?_rooms.visible = ? }', ($hotelId === null
                ? DBSIMPLE_SKIP : $hotelId), ($active === null ? DBSIMPLE_SKIP : $active));

            if (is_array($rooms) AND count($rooms)) {
                $roomsIDs = [];
                foreach ($rooms as $key => $room) {
                    if ($fetch_key AND array_key_exists($fetch_key, $room)) {
                        $key = $room[$fetch_key];
                    }

                    $response[$key] = $room;
                    $roomsIDs[$room['id']] = $key;
                }

                if ($isoLang) {
                    $translates = $this->translateModel->get('rooms', array_keys($roomsIDs), $isoLang);
                    if ($translates AND is_array($translates) AND count($translates)) {
                        foreach ($translates as $roomId => $translate) {
                            $response[$roomsIDs[$roomId]]['translates'] = $translate;
                        }
                    }
                }
            }

            return $response;
        }

        public function loadById($id, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_rooms WHERE id=?', $id);
            $this->prepareLoad($row, $lang);

            return $this;
        }

        public function loadByServioId($servioId, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_rooms WHERE servioId=? LIMIT 1', $servioId);
            $this->prepareLoad($row, $lang);

            return $this;
        }

        public function prepareLoad($row, $lang = null) {

            if (!is_array($row) OR !count($row)) {
                throw new HttpException(404, 'Room type not found');
            }

            $this->autoSetAttributes($row, false);

            if ($lang != null) {
                $translates = $this->translateModel->get('rooms', $row['id'], $lang);
                if (count($translates)) {
                    $this->translates = current($translates);
                }
            }
        }

        public function getAll($hotelId, $lang = false, $visible = false) {

            if ($visible) {
                $visible = " AND ?_rooms.visible=1";
            }
            else {
                $visible = "";
            }
            $translates = new Translate();
            $rooms = $this->db->select("SELECT * FROM ?_rooms WHERE hotelId=? $visible", $hotelId);
            for ($r = 0; $r < count($rooms); $r++) {
                $rooms[$r]['translates'] = $translates->getTranslates($rooms[$r]['id'], 'rooms', $lang);
            }

            return $rooms;
        }

        public function getInfo($roomId, $lang = false) {

            $room = $this->db->selectRow("
                SELECT
                    *
                FROM ?_rooms
                WHERE id = ?
        ", $roomId);

            if ($room !== []) {
                $translate = new Translate();
                $hotel = new Hotel();
                $room['translates'] = $translate->getTranslates($roomId, 'rooms', $lang);
                $room['hotel'] = $hotel->getInfo($room['hotelId']);
            }

            return $room;
        }

        /*
         * TEST FUNCTION (used in Test.php)
         */
        public function exportRoom($roomId) {
            /*
              $room = $this->getInfo($roomId);
              $connect = $room['hotel']['server']['host'] . ':' . $room['hotel']['server']['port'];
              $mServio = new ServioModel($connect);
              $mTranslates = new Translate();
              $langsArray = $mTranslates->getIso();
              $photoName = UPLOADS . DIRECTORY_SEPARATOR . 'rooms' . DIRECTORY_SEPARATOR . 'room' . $room['servioId'] . DIRECTORY_SEPARATOR . 'original_' . $room['photo'] . '.jpg';
              if (file_exists($photoName)) {
                  $image = file_get_contents($photoName);
              }
              else {
                  $image = '';
              }
              $result = $mServio->setRoomType($room, $langsArray, $image);
              return $result;
            */
        }

        public function importRoom($roomId) {
            /*
              $room = $this->getInfo($roomId);
              $connect = $room['hotel']['server']['host'] . ':' . $room['hotel']['server']['port'];
              $roomServioId = $room['servioId'];
              $servio = new ServioModel($connect);
              $translates = new Translate();
              $langsArray = $translates->getIso();
              $roomInfo = $servio->getRoomType($roomServioId, $langsArray);

              if ($roomInfo['tm']) {
                  $photo = $roomInfo['tm'];
              }
              else {
                  $photo = null;
              }
              $data = array('photo' => $photo);
              if ($roomInfo['beds'] !== false) {
                  $data['beds'] = $roomInfo['beds'];
                  $data['extBeds'] = $roomInfo['extBeds'];
              }

              $this->update($roomId, $data);
              foreach ($langsArray as $lang) {
                  $translates->setTranslate($roomId, 'rooms', $lang, $roomInfo['langs'][$lang]['name'], $roomInfo['langs'][$lang]['descr']);
              }

              return $roomInfo;
            */
        }

        //    public function setPhoto($roomId, $photoData) {
        //
        //        $room = $this->getInfo($roomId);
        //        $imageName = 'room'.$room['servioId'];
        //        $mImages = new Images();
        //        $tm = $mImages->updatePhotoRoom($photoData, $imageName);
        //        $data = array('photo'=>$tm);
        //        $result=$this->update($roomId, $data);
        //        return $result;
        //    }

        public function updateRoomsTypes($hotelId) {

            $servio = $this->hotelModel->getServioConnect($hotelId);
            $roomTypes = $servio->getRoomTypesList($this->hotelModel->getServioId($hotelId));
            if (!is_array($roomTypes) OR !count($roomTypes)) {
                throw new HttpException(404, 'Servio module not provide data');
            }

            $currentRooms = $this->getRooms($hotelId, null, null, static::FETCH_ARRAY_SERVIO);
            for ($i = 0; $i < $roomTypes['Count']; $i++) {

                if (isset($currentRooms[$roomTypes['IDs'][$i]])) {

                    $currentRooms[$roomTypes['IDs'][$i]]['name'] = $roomTypes['ClassNames'][$i];
                    $this->db->query("UPDATE ?_rooms SET name=? WHERE id=?", $currentRooms[$roomTypes['IDs'][$i]]['name'], $currentRooms[$roomTypes['IDs'][$i]]['id']);
                }
                else {
                    foreach ($currentRooms as &$room) {

                        if (strtolower($room['name']) == strtolower($roomTypes['ClassNames'][$i])) {

                            $room['servioId'] = $roomTypes['IDs'][$i];
                            $this->db->query("UPDATE ?_rooms SET servioId=? WHERE id=?", $room['servioId'], $room['id']);
                            continue 2;
                        }
                    }

                    $currentRooms[$roomTypes['IDs'][$i]] = [
                        'id' => null,
                        'hotelId' => $hotelId,
                        'servioId' => $roomTypes['IDs'][$i],
                        'name' => $roomTypes['ClassNames'][$i],
                        'visible' => 0,
                    ];

                    $this->db->query("INSERT INTO ?_rooms (?#) VALUES (?a)", array_keys($currentRooms[$roomTypes['IDs'][$i]]), array_values($currentRooms[$roomTypes['IDs'][$i]]));
                    $currentRooms[$roomTypes['IDs'][$i]]['id'] = $this->db->selectCell('SELECT LAST_INSERT_ID()');
                }
            }

            return $currentRooms;

            //        for ($r=0; $r<count($roomTypes); $r++) {
            //            $servioId = $roomTypes[$r]['servioId'];
            //            $roomName = $roomTypes[$r]['name'];
            //            $check = $this->db->selectCell("SELECT id FROM ?_rooms WHERE servioId=? AND hotelId=?", $servioId, $hotelId);
            //            if ($check !== null) {
            //                $this->db->query("UPDATE ?_rooms SET name=? WHERE  servioId=? AND hotelId=?", $roomName, $servioId, $hotelId);
            //            } else {
            //                $data = array(
            //                    'id'=>0,
            //                    'hotelId'   => $hotelId,
            //                    'servioId'  => $servioId,
            //                    'name'      => $roomName
            //                );
            //                $this->db->query("INSERT INTO ?_rooms(?#) VALUES(?a)", array_keys($data), array_values($data));
            //            }
            //        }
            //        return $roomTypes;
        }

        /*
         * TEST FUNCTION (used in Test.php)
         */
        private function getservioroomsArray($hotelId, $enabled = true) {
            /*
              $rooms = $this->getAll($hotelId);
              $servioroomsArray = array();
              foreach ($rooms as $room) {
                  if ($enabled) {
                      if ((boolean)$room['visible']) {
                          $servioroomsArray[] = (int)$room['servioId'];
                      }
                  }
                  else {
                      $servioroomsArray[] = (int)$room['servioId'];
                  }

              }
              return $servioroomsArray;
            */
        }

        /*
         * TEST FUNCTION (used in Test.php)
         */
        public function getFree($hotelId, $arrival, $departure, $guestsArray, $lang, $password = false, $visible = true) {
            /*
              $hotels = new Hotel();
              $servio = $hotels->getServioConnect($hotelId);
              $companyInfo = false;
              if ($password) {
                  $companyInfo = $servio->getCompanyInfo($password);
              }

              if ($companyInfo) {
                  $companyCodeID = $companyInfo['companyCodeID'];
              }
              else {
                  $companyCodeID = 0;
              }

              $serviorooms = $servio->getRooms($hotels->getServioId($hotelId), $arrival, $departure, $arrival, $adults, $childs, $childAges, $isExtraBadUser, $lang, $companyCodeID);
              $localRooms = $this->getAll($hotelId, $lang, $visible);

              // Связываем описание из БД с количеством свободных номеров из ответа Сервио
              for ($r = 0; $r < count($localRooms); $r++) {
                  for ($sr = 0; $sr < count($serviorooms['rooms']); $sr++) {
                      if ((int)$localRooms[$r]['servioId'] == (int)$serviorooms['rooms'][$sr]['servioId']) {
                          $localRooms[$r]['freeRoom'] = (int)$serviorooms['rooms'][$sr]['freeRoom'];
                      }
                  }
              }

              $result = array('freeRooms' => $localRooms, 'contracts' => $serviorooms['contracts'], 'companyInfo' => $companyInfo);
              return $result;
            */
        }

        /*
         * TEST FUNCTION (used in Test.php)
         */
        public function getGuestPrices($hotelId, $arrival, $departure, $guestsArray, $payType, $lang = 'ru', $password = false, $servioContractId = 0, $enabled = true) {
            /*
              $hotels = new HotelsModel();
              $servio = new ServioModel($hotels->getServioConnect($hotelId));

              $servioroomsArray = $this->getservioroomsArray($hotelId, $enabled);

              $companyInfo = false;
              if ($password) {
                  $companyInfo = $servio->getCompanyInfo($password);
              }
              if ($companyInfo) {
                  $servioCompanyId = $companyInfo['servioId'];
              }
              else {
                  $servioCompanyId = 0;
                  $servioContractId = 0;
              }

              $prices = $servio->getPrices($hotels->getServioId($hotelId), $servioCompanyId, $arrival, $departure, $guestsArray, $servioroomsArray, $servioContractId, $payType, $lang);

              return $prices;
            */
        }

        /*
        public function getFreeRoomsPrices($hotelId, $arrival, $departure, $guestsArray, $payType, $lang = DEFAULT_LANG, $companyCode = false, $contractId = 0, $enabled = true) {
            $freeRoomsData = $this->getFree($hotelId, $arrival, $departure, $lang, $companyCode, $enabled);
            $freeRooms = $freeRoomsData['freeRooms'];
            $contracts = $freeRoomsData['contracts'];
            $companyInfo = $freeRoomsData['companyInfo'];
            $prices = $this->getPrices($hotelId, $arrival, $departure, $guestsArray, $payType, $lang, $companyCode, $contractId);
            for ($r=0;$r<count($freeRooms); $r++) {
                $roomServioId = $freeRooms[$r]['servioId'];
                $freeRooms[$r]['guestsArray'] = $prices['room'.$roomServioId]['guestsArray'];
            }
            return array('rooms'=>$freeRooms, 'contracts'=>$contracts, 'companyInfo'=>$companyInfo);
        }
        */

        /*
         * TEST FUNCTION (used in Test.php)
         */
        public function getGuestRoomsPrices($hotelId, $arrival, $departure, $guestsArray, $payType, $lang = 'ru', $companyCode = false, $contractId = 0, $enabled = true) {
            /*
              // var_dump($guestsArray); die;
              $freeRoomsData = $this->getFree($hotelId, $arrival, $departure, $guestsArray, $lang, $companyCode, $enabled);

              $freeRooms = $freeRoomsData['freeRooms'];
              $contracts = $freeRoomsData['contracts'];
              $companyInfo = $freeRoomsData['companyInfo'];
              $mHotel = new Hotel();
              $hotel = $mHotel->getInfo($hotelId);
              $guestPrices = $this->getGuestPrices($hotelId, $arrival, $departure, $guestsArray, $payType, $lang, $companyCode, $contractId);
              for ($g = 0; $g < count($guestPrices); $g++) {
                  for ($r = 0; $r < count($guestPrices[$g]['rooms']); $r++) {
                      $guestPrices[$g]['rooms'][$r]['roomInfo'] = $freeRooms[$r];
                  }
              }
              if (!file_exists(CACHE . '/tmpRooms') || is_file(CACHE . '/tmpRooms')) {
                  mkdir(CACHE . '/tmpRooms');
              }
              $tmpFile = date('Ymd-his') . '-' . rand(65000, 120000);
              $result = array('contracts' => $contracts, 'companyInfo' => $companyInfo, 'requestRooms' => $guestPrices, 'hotelInfo' => $hotel, 'tmp' => $tmpFile);
              file_put_contents(CACHE . '/tmpRooms/' . $tmpFile . '.json', json_encode($result, JSON_UNESCAPED_UNICODE));
              return $result;
            */
        }

        public function setNewPhoto($_file, $varName = 'photo') {

            if (count($_file['error']) AND isset($_file['error'][$varName]) AND $_file['error'][$varName] !== UPLOAD_ERR_NO_FILE) {
                $imageinfo = getimagesize($_file['tmp_name'][$varName]);

                if (!$imageinfo OR $imageinfo['mime'] !== 'image/jpeg') {
                    $this->addError('photo', 'Неверный формат файла. Можно загружать только jpg');

                    return false;
                }

                if (!file_exists($_file['tmp_name'][$varName]) OR !is_readable($_file['tmp_name'][$varName])) {
                    $this->addError('photo', 'Не удалось загрузить файл на сервер');

                    return false;
                }

                $this->_newPhoto = [
                    $_file['tmp_name'][$varName],
                    base_convert(md5_file($_file['tmp_name'][$varName]), 16, 36)
                ];

                return true;
            }

            return null;
        }

        public function setNewPhotoRaw($image) {

            $tempName = tempnam(SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime', 'npr');
            file_put_contents($tempName, $image);
            $this->_newPhoto = [
                $tempName,
                base_convert(md5($image), 16, 36)
            ];
        }

        public function update() {

            if (count($this->_newPhoto) == 2) {

                $directory = 'rooms' . DIRECTORY_SEPARATOR . 'room' . $this->servioId . DIRECTORY_SEPARATOR;
                $savedImages = $this->getComponent('ImagesHelper')
                                    ->saveUploadedFile($this->_newPhoto[0], $directory . $this->_newPhoto[1] . '.jpg');
                if (count($savedImages) && $this->_newPhoto[1] != $this->photo) {

                    $this->getComponent('ImagesHelper')
                         ->unlinkAll($directory . $this->photo . '.jpg');
                    $this->photo = $this->_newPhoto[1];
                }
            }
            $result = $this->db->query('UPDATE ?_rooms SET ?a WHERE id = ? LIMIT 1', [
                'hotelId' => $this->hotelId,
                'servioId' => $this->servioId,
                'name' => $this->name,
                'beds' => $this->beds,
                'extBeds' => $this->extBeds,
                'visible' => $this->visible,
                'photo' => $this->photo
            ], $this->id);
            if (is_int($result))
                return true;

            $this->addError('id', $this->dbErrorString);

            return false;
        }

        public function insert() {

            $result = $this->db->query('INSERT INTO ?_rooms (`hotelId`, `servioId`,`name`,`visible`) VALUES (?, ?, ?, 0)', $this->hotelId, $this->servioId, $this->name);

            if (!is_int($result)) {
                $this->addError('id', $this->dbErrorString);

                return false;
            }
            else {
                $this->id = $result;

                return true;
            }
        }

        public function saveTranslate($translates) {

            return $this->translateModel->set('rooms', $this->id, $translates);
        }

        public function syncRoomTypesServioID($serverId, $hotelID, $roomTypes) {

            $response = [];

            $hide = [];

            $result = $this->db->select('SELECT t.`servioId` as ARRAY_KEY_1, h.`servioId` as ARRAY_KEY_2, t.*, h.servioId as hotelServioId
            FROM ?_rooms t
            INNER JOIN ?_hotels h ON t.hotelId = h.id
            WHERE h.`serverId` = ? AND (h.`servioId` = ? OR t.`servioId` IN (?a))', $serverId, $hotelID, [$roomTypes]);

            foreach ($result as $roomTypeID => $roomTypesData) {
                if (count($roomTypesData) > 1) {
                    $response['toMany'][$roomTypeID] = $roomTypesData;
                }

                if (isset($roomTypes[$roomTypeID])) {
                    if (!isset($roomTypesData[$hotelID])) {
                        $response['otherHotel'][$roomTypeID] = $roomTypesData;
                    }

                    unset($roomTypes[$roomTypeID]);
                }
                else {
                    if ($roomTypesData[key($roomTypesData)]['visible'] !== '0') {
                        $response['notExistsInServio'][$roomTypeID] = $roomTypesData;
                        $hide[] = $roomTypesData[key($roomTypesData)]['id'];
                    }
                }
            }

            if (count($roomTypes)) {
                $response['notExistsInDatabase'] = $roomTypes;
            }

            if (count($hide)) {
                $this->db->query('UPDATE ?_rooms SET `visible` = 0 WHERE `id` IN (?a)', $hide);
            }

            return $response;
        }

        /**
         * Удалить все категории номеров с определенным servioId кроме категорий отеля hotelId
         * @param int $servioId
         * @param int $hotelId
         *
         * @throws \ErrorException
         */
        public function removeButHotel($servioId, $hotelId) {

            $rooms = $this->db->selectCol('SELECT id as ARRAY_KEY, `photo` FROM ?_rooms WHERE `servioId` = ? AND `hotelId` != ?', $servioId, $hotelId);
            if (is_array($rooms) && count($rooms)) {
                $ids = array_keys($rooms);
                $this->db->transaction();
                $this->translateModel->delete($ids, 'rooms');

                $count = $this->db->query('DELETE FROM ?_rooms WHERE id IN (?a) LIMIT ?n', $ids, count($ids));
                if ($count != count($ids)) {
                    $this->db->rollback();
                    throw new \ErrorException('Checksum incorrect');
                }

                $this->db->commit();

                foreach ($rooms as $photo) {
                    if ($photo) {
                        $this->getComponent('ImagesHelper')
                             ->unlinkAll('rooms/room' . $servioId . '/' . $photo . '.jpg');
                    }
                }
            }
        }
    }

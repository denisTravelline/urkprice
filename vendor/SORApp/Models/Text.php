<?php
    namespace SORApp\Models;

    use SORApp\Components\Model\MySQLRepository;
    use SORApp\Components\Exceptions\HttpException;

    /*
        CREATE TABLE `servio_texts` (
            `id` int(11) NOT NULL AUTO_INCREMENT, `alias` varchar(50) DEFAULT '0',
            PRIMARY KEY (`id`), UNIQUE KEY `aliasIdx` (`alias`), KEY `id` (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

        ALTER TABLE `optima`.`servio_translates`
            CHANGE COLUMN `parentType` `parentType` ENUM('cities','hotels','rooms','transfer','transfer_place','country','texts') NOT NULL;

        ALTER TABLE `optima`.`servio_hotels`
            ADD COLUMN `rulesId` INT(11) NULL DEFAULT '0';
    */

    /**
     * Description of Texts
     *
     * @author sorokin_r
     */
    class Text extends MySQLRepository {

        const FETCH_ARRAY_PK = 'id';

        public $id;
        public $alias;
        public $translates;

        public function validateRules() {

            return [
                'id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'alias' => [
                    'Incorrect alias',
                    'string' => [
                        'requires',
                        'match' => '/^[a-zA-Z0-9\-\_]{1,255}$/']],
                'translates' => [
                    'Incorrect translate',
                    'callback' => [
                        $this,
                        'validateTranslate']]];
        }

        public function getAllById($textID) {

            if (is_array($textID) || count($textID)) {

                $texts = $this->db->select('SELECT id AS ARRAY_KEY FROM ?_texts WHERE id IN (?a)', $textID);
                $this->attachTranslate($texts, 'texts', true);

                return $texts;
            }
            return false;
        }

        public function getCollection($lang = null) {

            $result = $this->db->select('SELECT t.id as ARRAY_KEY, t.* from ?_texts t');
            $this->attachTranslate($result, 'texts', true, $lang);

            return $result;
        }

        public function loadById($id, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_texts WHERE id=?', $id);
            if (!is_array($row) || !count($row))
                throw new HttpException(404, 'Text not found');

            $this->autoSetAttributes($row, false);

            if ($lang != null) {

                $translates = $this->translateModel->get('texts', $row['id'], $lang);
                if (count($translates))
                    $this->translates = current($translates);
            }
            return $this;
        }

        public function save() {

            if ($this->validate()) {

                $id = $this->db->query(
                    'INSERT INTO ?_texts (`id`, `alias`)' .
                    ' VALUES (?, ?) ON DUPLICATE KEY' .
                    ' UPDATE `alias` = VALUES(`alias`)',
                    $this->id > 0 ? $this->id : null,
                    $this->alias
                );
                if ($id > 0)
                    $this->id = $id;

                if ($this->id > 0) {

                    $this->translateModel->set('texts', $this->id, $this->translates);
                    return true;
                }
            }
            return false;
        }

        public function deleteById($id) {

            if ($this->hotelModel->isHasByRules($id)) {

                $this->addError('id', 'Этот текст используется в свойствах отелей');
                return false;
            }
            $this->db->query('DELETE FROM ?_texts WHERE `id` = ? LIMIT 1', $id);
            $this->translateModel->delete($id, 'texts');
            return true;
        }

        /**
         * @deprecated
         * @param int $id Идентификатор города в БД
         * @param string|boolean $lang Язык isoLang или все языки, если false
         * @return array                    Массив, где ключи совпадают с полями БД + translate == результату перевода
         */
        public function getInfo($id, $lang = false) {

            $translates = new Translate();

            $text = $this->db->selectRow("SELECT * FROM ?_texts WHERE id=?", $id);
            $text['translates'] = $translates->getTranslates($id, 'texts', $lang);

            return $text;
        }

        /**
         * @deprecated
         * @param string|boolean $lang Отдавать все переводы (true), не брать переводы (false), взять определенный перевод ru|en|etc
         * @return array                    Массив, каждый элемент которого - аналогичен getInfo
         */
        public function getAll($lang = false) {

            //$texts = $this->db->select('SELECT id FROM ?_texts');
            //$this->attachTranslate($texts, 'texts', true);

            $texts = $this->db->select('SELECT t.id as ARRAY_KEY, t.* from ?_texts t');
            $this->attachTranslate($texts, 'texts', true);

            /*$texts = [];
            $textsIds = $this->db->selectCol('SELECT id FROM ?_texts ORDER BY alias');
            foreach ($textsIds as $textId)
                $texts[] = $this->getInfo($textId, $lang);*/

            if (!count($texts)) $texts = false;
            return $texts;
        }

        /**
         * @param $alias
         *
         * @return array|bool|null
         * @deprecated
         */
        public function add($alias) {

            $data = [
                'id' => 0,
                'alias' => $alias
            ];
            $result = $this->db->query("INSERT INTO ?_texts(?#) VALUES(?a)", array_keys($data), array_values($data));

            if ($result < 1)
                $result = false;

            return $result;
        }

        /**
         * @param $id
         * @param $data
         *
         * @return array|null
         * @deprecated
         */
        public function update($id, $data) {

            $result = $this->db->query("UPDATE ?_texts SET ?a WHERE id=?", $data, $id);

            return $result;
        }

    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 05.09.14 17:33
     */

    namespace SORApp\Models;

    use SORApp\Components\Model\AuthUserModelInterface;
    use SORApp\Components\Model\MySQLRepository;

    class User extends MySQLRepository implements AuthUserModelInterface {

        const STATUS_DISABLED = 0;
        const STATUS_ACTIVE = 1;

        public $id;

        public $email;
        public $first_name;
        public $last_name;

        protected $_status;
        protected $password;
        protected $access;

        public function validateRules() {

            return [
                'id' => [],
                'status' => [],
                'email' => [],
                'first_name' => [],
                'last_name' => [],
                'access' => [],];
        }

        public function getPk() {

            return $this->id;
        }

        /**
         * Код текущего статуса пользователя
         *
         * @return int
         */
        public function getStatus() {

            if ($this->_status === null) {
                $this->_status = static::STATUS_ACTIVE;
            }

            return $this->_status;
        }

        /**
         * Установка нового статуса пользователя
         *
         * @param int $status
         *
         * @throws \ErrorException
         */
        public function setStatus($status) {

            $status = intval($status);

            $statuses = $this->getStatuses();
            if (!isset($statuses[$status])) {
                throw new \ErrorException('Incorrect set status "' . htmlentities($status) . '"');
            }
            else {
                $this->_status = $status;
            }
        }

        /**
         * Список доступных статусов с названиями
         *
         * @return array
         */
        public function getStatuses() {

            return [
                static::STATUS_DISABLED => 'выключен',
                static::STATUS_ACTIVE => 'активен',];
        }

        public function isActive() {

            return ($this->getStatus() === static::STATUS_ACTIVE);
        }

        public function activate() {

            $this->setStatus(static::STATUS_ACTIVE);
        }

        public function disable() {

            $this->setStatus(static::STATUS_DISABLED);
        }

        /**
         * Геттер массива с доступами пользователя
         *
         * @return array
         */
        public function getAccess() {

            if ($this->access === null) {
                $this->access = [];
            }

            return $this->access;
        }

        /**
         * TODO: need rename var
         * @return array
         */
        public function getRoles() {

            return $this->getAccess();
        }

        /**
         * Сеттер пароля
         * @param string $password
         */
        protected function setPassword($password) {

            $this->password = $password;
        }

        /**
         * Проверка введенного пароля
         *
         * @param string $password
         *
         * @return bool
         */
        public function checkPassword($password) {

            return (crypt($password, $this->password) == $this->password);
        }

        /**
         * @param string $password
         */
        public function changePassword($password) {

            $this->password = crypt($password, '$2a$13$' . substr(base_convert(sha1(uniqid()), 16, 36), 0, 22));
        }

        /**
         * Установка прав доступа
         *
         * @param string|array $access
         *
         * @throws \ErrorException
         */
        protected function setAccess($access) {

            if (!empty($access)) {
                if (is_string($access)) {
                    $access = json_decode($access);
                }

                if (is_array($access)) {
                    $this->access = $access;
                }
                else {
                    throw new \ErrorException('Incorrect access data');
                }
            }
        }

        /**
         * Геттер возвращающий полное имя пользователя
         *
         * @return string
         */
        public function getFullName() {

            $fullName = [];

            if (!empty($this->first_name)) {
                $fullName[] = $this->first_name;
            }

            if (!empty($this->last_name)) {
                $fullName[] = $this->last_name;
            }

            if (!count($fullName)) {
                $fullName[] = $this->email;
            }

            return implode(' ', $fullName);
        }

        public function checkAccess($action) {

            return in_array($action, $this->getAccess());
        }

        /**
         * Поиск пользователя по id
         *
         * @param int $id
         *
         * @return null|User
         */
        public function findByPK($id) {

            return $this->createModel($this->db->selectRow('SELECT * FROM ?_users WHERE id = ? LIMIT 1', (int)$id));
        }

        /**
         * Поиск пользователя по адресу почты
         *
         * @param string $email
         *
         * @return null|User
         */
        public function findByEmail($email) {

            return $this->createModel($this->db->selectRow('SELECT * FROM ?_users WHERE email = ? LIMIT 1', (string)$email));
        }

        public function getAll() {

            return $this->db->select('SELECT * FROM ?_users');
        }

        private function generate_password($number) {

            $arr = [
                'a',
                'b',
                'c',
                'd',
                'e',
                'f',
                'g',
                'h',
                'i',
                'j',
                'k',
                'l',
                'm',
                'n',
                'o',
                'p',
                'r',
                's',
                't',
                'u',
                'v',
                'x',
                'y',
                'z',
                'A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'R',
                'S',
                'T',
                'U',
                'V',
                'X',
                'Y',
                'Z',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
                '0',
                '.',
                ',',
                '(',
                ')',
                '[',
                ']',
                '!',
                '?',
                '&',
                '^',
                '%',
                '@',
                '*',
                '$',
                '<',
                '>',
                '/',
                '|',
                '+',
                '-',
                '{',
                '}',
                '`',
                '~'];
            // Генерируем пароль
            $pass = "";
            for ($i = 0; $i < $number; $i++) {
                // Вычисляем случайный индекс массива
                $index = rand(0, count($arr) - 1);
                $pass .= $arr[$index];
            }

            return $pass;
        }

        public function save() {

            if (!empty($this->password)) {  //если админ регает с заданным паролем
                $updatePassword = ", `password` = VALUES(`password`)";
                $this->changePassword($this->password);
            }
            elseif (empty($this->password) && !$this->id) // если админ регает нового пользователя без пароля
            {
                $updatePassword = ", `password` = VALUES(`password`)";
                $this->password = $this->generate_password(6);
                $mailer = $this->getComponent('mailer');
                $message = $mailer->createMessage();
                $message->setBody("Вы зарегистрированы под именем $this->first_name $this->last_name.<br/> Для входа на сайт используйте реквизиты:<br/> login: $this->email <br/> password: $this->password");
                $message->setTo($this->email);
                $message->setSubject('Регистрация в системе управления бронированием');
                $mailer->send($message);
                $this->changePassword($this->password);
            }
            else {
                $updatePassword = '';
            }
            if (!empty($this->email)) {
                $updateEmail = ", `email` = VALUES(`email`)";
            }
            else $updateEmail = '';

            $id = $this->db->query('INSERT INTO ?_users (`id`, `status`, `password`, `email`, `first_name`, `last_name`, `access`) VALUES (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `status` = VALUES(`status`), `first_name` = VALUES(`first_name`), `last_name` = VALUES(`last_name`), `access` = VALUES(`access`)' .
                                   $updatePassword . $updateEmail, ($this->id >
                                                                    0 ? $this->id : null), $this->getStatus(), $this->password, $this->email, $this->first_name, $this->last_name, json_encode($this->getAccess(), JSON_UNESCAPED_UNICODE));

            if ($id > 0) {
                $this->id = $id;

                return true;
            }

            return false;
        }

        public function delete($id) {

            $this->db->query('DELETE FROM ?_users WHERE id=?', $id);

            return true;
        }
    }

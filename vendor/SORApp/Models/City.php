<?php
    namespace SORApp\Models;

    use SORApp\Components\Model\MySQLRepository;

    /**
     * Description of Cities
     *
     * @author sorokin_r
     */
    class City extends MySQLRepository {

        const FETCH_ARRAY_PK = 'id';

        public $id;
        public $alias;
        public $translates;

        public function validateRules() {

            return [
                'id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'alias' => [
                    'Incorrect alias',
                    'string' => [
                        'requires',
                        'match' => '/^[a-zA-Z0-9\-\_]{1,255}$/']],
                'translates' => [
                    'Incorrect translate',
                    'callback' => [
                        $this,
                        'validateTranslate']]];
        }

        public function getAllById($cityID) {

            if (is_array($cityID) AND count($cityID)) {
                $cities = $this->db->select('SELECT id AS ARRAY_KEY FROM ?_cities WHERE id IN (?a)', $cityID);
                $this->attachTranslate($cities, 'cities', true);

                return $cities;
            }
            else {
                return false;
            }
        }

        public function getCollection($lang = null) {

            $result = $this->db->select('SELECT t.id as ARRAY_KEY, t.* from ?_cities t');
            $this->attachTranslate($result, 'cities', true, $lang);

            return $result;
        }

        public function loadById($id, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_cities WHERE id=?', $id);
            if (!is_array($row) OR !count($row)) {
                throw new HttpException(404, 'City not found');
            }

            $this->autoSetAttributes($row, false);

            if ($lang != null) {
                $translates = $this->translateModel->get('cities', $row['id'], $lang);
                if (count($translates)) {
                    $this->translates = current($translates);
                }
            }

            return $this;
        }

        public function save() {

            if ($this->validate()) {
                $id = $this->db->query('INSERT INTO ?_cities (`id`, `alias`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `alias` = VALUES(`alias`)', ($this->id >
                                                                                                                                                0 ? $this->id : null), $this->alias);

                if ($id > 0) {
                    $this->id = $id;
                }

                if ($this->id > 0) {
                    $this->translateModel->set('cities', $this->id, $this->translates);

                    return true;
                }
            }

            return false;
        }

        public function deleteById($id) {

            if ($this->hotelModel->isHasByCity($id)) {
                $this->addError('id', 'City has hotels');

                return false;
            }
            else {
                $this->db->query('DELETE FROM ?_cities WHERE `id` = ? LIMIT 1', $id);
                $this->translateModel->delete($id, 'cities');

                return true;
            }
        }

        /**
         * @deprecated
         * @param int $id Идентификатор города в БД
         * @param char|boolean $lang Язык isoLang или все языки, если false
         * @return array                    Массив, где ключи совпадают с полями БД + translate == результату перевода
         */
        public function getInfo($id, $lang = false) {

            $translates = new Translate();

            $city = $this->db->selectRow("SELECT * FROM ?_cities WHERE id=?", $id);
            $city['translates'] = $translates->getTranslates($id, 'cities', $lang);

            return $city;
        }

        /**
         * @deprecated
         * @param char|boolean $lang Отдавать все переводы (true), не брать переводы (false), взять определенный перевод ru|en|etc
         * @return array                    Массив, каждый элемент которого - аналогичен getInfo
         */
        public function getAll($lang = false) {

            $cities = [];
            $citiesIds = $this->db->selectCol('SELECT id FROM ?_cities ORDER BY alias');
            foreach ($citiesIds as $cityId) {
                $cities[] = $this->getInfo($cityId, $lang);
            }

            if (count($cities) == 0) {
                $cities = false;
            }

            return $cities;
        }

        /**
         * @param $alias
         *
         * @return array|bool|null
         * @deprecated
         */
        public function add($alias) {

            $data = [
                'id' => 0,
                'alias' => $alias];
            $result = $this->db->query("INSERT INTO ?_cities(?#) VALUES(?a)", array_keys($data), array_values($data));

            if ($result < 1) {
                $result = false;
            }

            return $result;
        }

        /**
         * @param $id
         * @param $data
         *
         * @return array|null
         * @deprecated
         */
        public function update($id, $data) {

            $result = $this->db->query("UPDATE ?_cities SET ?a WHERE id=?", $data, $id);

            return $result;
        }

    }

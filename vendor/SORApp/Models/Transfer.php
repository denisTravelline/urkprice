<?php
    namespace SORApp\Models;

    use SORApp\Components\Model\MySQLRepository;

    /**
     * Description of Transfers
     *
     * @author sorokin_r
     */
    class Transfer extends MySQLRepository {

        public $id;
        public $cost_arrival;
        public $place_id;
        public $hotel_id;
        public $translates;
        public $cost_departure;
        public $seats;
        public $cost_currency;
        public $visible;

        /**
         * Метод возвращает список трансферов сгруппированный по метоназначению
         *
         * @param int $hotelId
         *
         * @return array|null
         */
        public function validateRules() {

            return [
                'id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'translates' => [
                    'Incorrect translate',
                    'callback' => [
                        $this,
                        'validateTranslate']],
                'cost_arrival' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'cost_departure' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'place_id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'hotel_id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'seats' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'cost_currency' => ['string'],
                'visible' => ['string'],];
        }

        public function getByHotelId($hotelId) {

            $transfers = $this->db->select('SELECT place_id as ARRAY_KEY_1, id AS ARRAY_KEY_2, ?_transfers.* FROM ?_transfers WHERE hotel_id=?', $hotelId);

            $this->attachTranslateGroup($transfers, 'transfer');
            $this->attachTranslate($transfers, 'transfer_place');

            return $transfers;
        }

        public function getTransferPlaces($hotelId, $lang = null) {

            $transfers = $this->db->select('SELECT ?_transfers_places.id as ARRAY_KEY, ?_transfers_places.* FROM ?_transfers_places');
            $this->attachTranslate($transfers, 'transfer_place', true, $lang);

            return $transfers;
        }

        public function getCollection($id, $lang = null) {

            $result = $this->db->select('SELECT t.id as ARRAY_KEY, t.* from ?_transfers t WHERE hotel_id =?', $id);
            $this->attachTranslate($result, 'transfer', true, $lang);

            return $result;
        }

        public function loadChildById($id, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_transfers WHERE id=?', $id);
            if (!is_array($row) OR !count($row)) {
                throw new HttpException(404, 'City not found');
            }

            $this->autoSetAttributes($row, false);

            if ($lang != null) {
                $translates = $this->translateModel->get('transfer', $row['id'], $lang);
                if (count($translates)) {
                    $this->translates = current($translates);
                }
            }

            return $this;
        }

        public function save() {

            if ($this->validate()) {
                $id = $this->db->query('INSERT INTO ?_transfers (`id`, `hotel_id`, `place_id`,`cost_arrival`,`cost_departure`,`seats`,`cost_currency`, `visible`) VALUES (?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE `cost_arrival` = VALUES(`cost_arrival`), `cost_departure` = VALUES(`cost_departure`), `seats` = VALUES(`seats`), `cost_currency` = VALUES(`cost_currency`), `visible` = VALUES(`visible`)', ($this->id >
                                                                                                                                                                                                                                                                                                                                                                                                                       0 ? $this->id : null), $this->hotel_id, $this->place_id, $this->cost_arrival, $this->cost_departure, $this->seats, $this->cost_currency, $this->visible);

                if ($id > 0) {
                    $this->id = $id;
                }

                if ($this->id > 0) {
                    $this->translateModel->set('transfer', $this->id, $this->translates);

                    return true;
                }
            }

            return false;
        }

        public function deleteById($id) {

            $this->db->query('DELETE FROM ?_transfers WHERE `id` = ? LIMIT 1', $id);
            $this->translateModel->delete($id, 'transfer');

            return true;

        }
    }

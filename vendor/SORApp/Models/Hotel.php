<?php

    namespace SORApp\Models;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Model\MySQLRepository;
    use SORApp\Services\Servio\Servio;

    /**
     * Description of Hotels
     *
     * @author sorokin_r
     */
    class Hotel extends MySQLRepository {

        const FETCH_ARRAY = false;
        const FETCH_ARRAY_PK = 'id';
        const FETCH_ARRAY_SERVIO = 'servioId';
        const FETCH_ARRAY_CITY = 'cityId';

        public $id;
        public $cityId;
        public $alias;
        public $serverId;
        public $servioId;
        public $tourTax;
        public $manager_emails;
        public $latitude;
        public $longitude;
        public $visible;

        public $translates;

        protected static $_servio = [];

        public function validateRules() {

            return [
                'id' => ['number' => ['int']],
                'cityId' => [
                    'number' => [
                        'requires',
                        'int']],
                'alias' => [
                    'Incorrect alias',
                    'string' => [
                        'requires',
                        'min' => 1,
                        'max' => 255]],

                'serverId' => [
                    'number' => [
                        'requires',
                        'int']],
                'servioId' => ['number' => ['int']],
                'latitude' => [
                    'string' => [
                        'allowEmpty',
                        'match' => '/^-?[0-9]{1,3}\.[0-9]{1,15}$/']],
                'longitude' => [
                    'string' => [
                        'allowEmpty',
                        'match' => '/^-?[0-9]{1,3}\.[0-9]{1,15}$/']],
                'tourTax' => ['boolean' => ['convert']],

                'manager_emails' => ['string' => ['min' => 0]],

                'visible' => ['boolean' => ['convert']],];
        }

        public function getGroup($fetch_key = self::FETCH_ARRAY_PK, $onlyVisible = true, $serverId = null) {

            $result = $this->db->select('SELECT t.`' . $fetch_key . '` as ARRAY_KEY_1, t.id as ARRAY_KEY_2, t.*' . '  FROM ?_hotels t' .
                                        ' WHERE 1=1 {AND visible=?} {AND serverId=?}', ($onlyVisible ? 1 : DBSIMPLE_SKIP), ($serverId ? (int)$serverId : DBSIMPLE_SKIP));
            $this->attachTranslateGroup($result, 'hotels', true);

            return $result;
        }

        public function loadById($id, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_hotels WHERE id=?', $id);
            if (!is_array($row) OR !count($row)) {
                throw new HttpException(404, 'Hotel not found');
            }

            $this->autoSetAttributes($row);

            if ($lang != null) {
                $translates = $this->translateModel->get('hotels', $row['id'], $lang);
                if (count($translates)) {
                    $this->translates = current($translates);
                }
            }

            return $this;
        }

        public function isHasByCity($cityId) {

            return (bool)$this->db->selectCell('SELECT count(*) FROM ?_hotels WHERE `cityId` = ?', $cityId);
        }

        public function update() {

            $result = $this->db->query('UPDATE ?_hotels SET ?a WHERE id = ? LIMIT 1', [
                'cityId' => $this->cityId,
                'alias' => $this->alias,
                'serverId' => $this->serverId,
                'servioId' => $this->servioId,
                'tourTax' => $this->tourTax,
                'manager_emails' => (is_array($this->manager_emails) ? implode(',', $this->manager_emails) : $this->manager_emails),
                'latitude' => $this->latitude,
                'longitude' => $this->longitude,
                'visible' => $this->visible,], $this->id);

            if (!is_int($result)) {
                $this->addError('id', $this->dbErrorString);

                return false;
            }
            else {
                return true;
            }
        }

        public function insert() {

            $result = $this->db->query('INSERT INTO ?_hotels (`alias`, `cityId`, `serverId`, `servioId`, `visible`) VALUES (?, ?, ?, ?, 0)', $this->alias, $this->cityId, $this->serverId, $this->servioId);

            if (!is_int($result)) {
                $this->addError('id', $this->dbErrorString);

                return false;
            }
            else {
                $this->id = $result;

                return true;
            }
        }

        public function saveTranslate($translates) {

            return $this->translateModel->set('hotels', $this->id, $translates);
        }

        public function getHotelsByCities($visible = false) {

            $mCities = new City();
            $cities = $mCities->getAll();
            $hotels = [];
            for ($c = 0; $c < count($cities); $c++) {
                $hotelsInCity = $this->getAll($visible, $cities[$c]['id']);
                if ($hotelsInCity !== false) {
                    $hotelsInfo = $cities[$c];
                    $hotelsInfo['hotels'] = $hotelsInCity;
                    $hotels[] = $hotelsInfo;
                }
            }

            return $hotels;
        }

        public function getAll($visible = false, $cityId = false) {

            if ($visible && $cityId !== false) {
                $where = " WHERE ?_hotels.visible=1 AND ?_hotels.cityId=$cityId";
            }
            elseif ($visible) {
                $where = " WHERE ?_hotels.visible=1";
            }
            elseif ($cityId !== false) {
                $where = " WHERE ?_hotels.cityId=$cityId";
            }
            else {
                $where = '';
            }

            $mTranslates = new Translate();

            $hotels = $this->db->select('SELECT ?_hotels.*, ?_servers.descr AS serverDescr, ?_cities.alias AS cityAlias' . ' FROM ?_hotels' .
                                        ' JOIN ?_servers ON ?_hotels.serverId = ?_servers.id' .
                                        ' JOIN ?_cities ON ?_hotels.cityId = ?_cities.id' . $where);
            if (count($hotels) == 0)
                return false;

            for ($h = 0; $h < count($hotels); $h++) {

                $hotels[$h]['translates'] = $mTranslates->getTranslates($hotels[$h]['id'], 'hotels');
            }

            return $hotels;
        }

        public function getInfo($hotelId, $lang = false) {

            $hotel = $this->db->selectRow('SELECT ?_hotels.*' . ' FROM ?_hotels' . ' WHERE ?_hotels.id = ?', $hotelId);
            if ($hotel !== []) {
                $mTranslates = new Translate();
                $mServers = new Server();
                $mCities = new City();
                $mTransfers = new TransferPlace();
                $hotel['server'] = $mServers->getInfo($hotel['serverId']);
                $hotel['city'] = $mCities->getInfo($hotel['cityId'], $lang);
                $hotel['translates'] = $mTranslates->getTranslates($hotelId, 'hotels', $lang);
                $hotel['transfers'] = $mTransfers->getByHotelId($hotel['id']);
            }
            else {
                $hotel = false;
            }

            return $hotel;
        }

        /**
         * @param int $hotelId
         *
         * @throws \ErrorException
         * @return Servio
         */
        public function getServioConnect($hotelId) {

            if (!isset(self::$_servio[$hotelId])) {
                $connectionData = $this->db->selectRow('SELECT s.url as url, s.accessKey as accessKey, s.version as version, s.currency as serverCurrency' .
                                                       ' FROM ?_hotels h' . ' LEFT JOIN ?_servers s ON h.serverId = s.id' .
                                                       ' WHERE h.id = ? LIMIT 1', $hotelId);
                if (!$connectionData) {
                    throw new \ErrorException('Cannot find servio server information');
                }
                self::$_servio[$hotelId] = $this->owner->createComponent('servio', $connectionData['url'], $connectionData['accessKey'], $connectionData['version'], $connectionData['serverCurrency']);
            }

            return self::$_servio[$hotelId];
        }

        public function getServioId($hotelId) {

            $hotelInfo = $this->getInfo($hotelId);
            $servioId = $hotelInfo['servioId'];

            return $servioId;
        }

        public function add($cityId, $alias, $serverId, $servioId, $tourTax) {

            $data = [
                'cityId' => $cityId,
                'alias' => $alias,
                'serverId' => $serverId,
                'servioId' => $servioId,
                'tourTax' => $tourTax];
            $result = $this->db->query("INSERT INTO ?_hotels(?#) VALUES(?a)", array_keys($data), array_values($data));

            return $result;
        }

        /**
         * Синхронизация отелей с сервером
         *
         * @param int $serverID
         * @param array $hotelServioIDs
         *
         * @return array
         */
        public function syncServioID($serverID, array $hotelServioIDs) {

            if (count($hotelServioIDs)) {

                $result = $this->db->selectCol("SELECT id as ARRAY_KEY, servioId from ?_hotels where serverId = ?", $serverID);
                $internalNotExist = array_diff($result, $hotelServioIDs);
                $externalNotExist = array_diff($hotelServioIDs, $result);

                if (count($internalNotExist)) {

                    // Выключаем отели которые есть в нашей базе но нет на сервере
                    $this->db->query("UPDATE ?_hotels SET visible = 0, WHERE id IN (?a)", array_keys($internalNotExist));
                }

                return [
                    $externalNotExist,
                    $internalNotExist];
            }

            return [];
        }
    }

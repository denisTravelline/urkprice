<?php
    namespace SORApp\Models;

    use SORApp\Components\Model\MySQLRepository;

    /**
     * Description of Transfers
     *
     * @author sorokin_r
     */
    class TransferPlace extends MySQLRepository {

        public $id;
        public $alias;
        public $translates;

        public function validateRules() {

            return [
                'id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'alias' => [
                    'Incorrect alias',
                    'string' => [
                        'requires',
                        'match' => '/^[a-zA-Z0-9\-\_]{1,255}$/']],
                'translates' => [
                    'Incorrect translate',
                    'callback' => [
                        $this,
                        'validateTranslate']]];
        }

        /**
         * Метод возвращает список трансферов сгруппированный по метоназначению
         *
         * @param int $hotelId
         *
         * @return array|null
         */

        public function getByHotelId($hotelId) {

            $transfers = $this->db->select('SELECT place_id as ARRAY_KEY_1, id AS ARRAY_KEY_2, ?_transfers.* FROM ?_transfers WHERE hotel_id=?', $hotelId);

            $this->attachTranslateGroup($transfers, 'transfer');
            $this->attachTranslate($transfers, 'transfer_place');

            return $transfers;
        }

        public function getCollection($lang = null) {

            $result = $this->db->select('SELECT t.id as ARRAY_KEY, t.* from ?_transfers_places t');
            $this->attachTranslate($result, 'transfer_place', true, $lang);

            return $result;
        }

        public function loadById($id, $lang = null) {

            $row = $this->db->selectRow('SELECT * FROM ?_transfers_places WHERE id=?', $id);
            if (!is_array($row) OR !count($row)) {
                throw new HttpException(404, 'City not found');
            }

            $this->autoSetAttributes($row, false);

            if ($lang != null) {
                $translates = $this->translateModel->get('transfer_place', $row['id'], $lang);
                if (count($translates)) {
                    $this->translates = current($translates);
                }
            }

            return $this;
        }

        public function save() {

            if ($this->validate()) {
                $id = $this->db->query('INSERT INTO ?_transfers_places (`id`, `alias`) VALUES (?, ?) ON DUPLICATE KEY UPDATE `alias` = VALUES(`alias`)', ($this->id >
                                                                                                                                                          0 ? $this->id : null), $this->alias);

                if ($id > 0) {
                    $this->id = $id;
                }

                if ($this->id > 0) {
                    $this->translateModel->set('transfer_place', $this->id, $this->translates);

                    return true;
                }
            }

            return false;
        }

        public function hasTransfers($id) {

            $row = $this->db->selectRow("SELECT * FROM ?_transfers WHERE place_id=?", $id);
            if (count($row)) {
                return true;
            }
            else {
                return false;
            }
        }

        public function deleteById($id) {

            if ($this->hasTransfers($id)) {
                $this->addError('id', 'Place has transfers');

                return false;
            }
            else {
                $this->db->query('DELETE FROM ?_transfers_places WHERE `id` = ? LIMIT 1', $id);
                $this->translateModel->delete($id, 'transfer_place');

                return true;
            }
        }
    }

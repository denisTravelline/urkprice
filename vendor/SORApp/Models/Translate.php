<?php

    namespace SORApp\Models;

    use SORApp\Components\Model\MySQLRepository;

    /**
     * Description of Translates
     *
     * @author sorokin_r
     */
    class Translate extends MySQLRepository {

        public function get($parentType, $parentIds, $isoLang = false) {

            if (!$isoLang) {
                $isoLang = $this->getOwner()->getLang();
            }

            if (is_scalar($isoLang)) {
                $isoLang = [$isoLang];
            }

            if (is_scalar($parentIds)) {
                $parentIds = [$parentIds];
            }

            return $this->db->select('SELECT parentId AS ARRAY_KEY_1, isoLang as ARRAY_KEY_2, `name`, `descr`, `sub1`, `sub2` FROM ?_translates WHERE parentType=? AND parentId IN (?a) AND isoLang IN (?a)', $parentType, $parentIds, $isoLang);
        }

        public function set($parentType, $parentIds, array $translates) {

            if (is_array($translates) AND count($translates)) {
                $parentType = $this->db->escape($parentType);
                $parentIds = $this->db->escape($parentIds);

                $values = [];
                foreach ($translates as $isoLang => $data) {
                    if (preg_match('/[a-zA-Z]{2}/i', $isoLang)) {
                        $values[] = '(' . implode(',', [
                                $parentIds,
                                $parentType,
                                $this->db->escape($isoLang),
                                (isset($data['name']) ? $this->db->escape($data['name']) : 'NULL'),
                                (isset($data['descr']) ? $this->db->escape($data['descr']) : 'NULL'),
                                (isset($data['sub1']) ? $this->db->escape($data['sub1']) : 'NULL'),
                                (isset($data['sub2']) ? $this->db->escape($data['sub2']) : 'NULL'),]) . ')';
                    }
                }

                if (count($values)) {
                    $result = $this->db->query('INSERT INTO ?_translates (`parentId`,`parentType`,`isoLang`,`name`,`descr`,`sub1`,`sub2`) VALUES ' .
                                               implode(',', $values) . ' ON DUPLICATE KEY UPDATE
                    `name` = VALUES(`name`),
                    `descr` = VALUES(`descr`),
                    `sub1` = VALUES(`sub1`),
                    `sub2` = VALUES(`sub2`)');

                    if (!is_int($result)) {
                        return false;
                    }
                }
            }

            return true;
        }

        /**
         * Метод возвращает массив с допустимыми локализациями iso и названия языков
         * @return array
         */
        public function getLangs() {

            $langs = $this->db->select("SELECT * FROM ?_langs");

            return $langs;
        }

        /**
         * Метод возвращает одномерный массив, со списком доступных языков iso
         * @return array
         */
        public function getIso($isoLang = false) {

            if ($isoLang) {
                $langsArray = $this->db->selectCol("SELECT isoLang FROM ?_langs WHERE isoLang=?", $isoLang);
            }
            else {
                $langsArray = $this->db->selectCol("SELECT isoLang FROM ?_langs");
            }

            return $langsArray;
        }

        /**
         * Метод получения локализаций
         * @param int $parentId id в локальной таблице
         * @param string $parentType тип таблицы
         * @param char [2] $isoLang      iso обозначение языка или false для всех языков
         * @return array
         */
        public function getTranslates($parentId, $parentType, $isoLang = false) {

            $isoLangs = $this->getIso($isoLang);

            $translates = [];
            foreach ($isoLangs as $isoLang) {
                $translate = $this->db->selectRow("SELECT name, descr, sub1, sub2 FROM ?_translates WHERE parentId =? AND ?_translates.parentType =? AND isoLang=?", (int)$parentId, $parentType, $isoLang);
                if ($translate === []) {
                    $translates[$isoLang] = false;
                }
                else {
                    $translates[$isoLang] = $translate;
                }
            }

            return $translates;
        }

        public function delete($parentId, $parentType) {

            if (is_array($parentId)) {
                return $this->db->query('DELETE FROM ?_translates WHERE `parentType` = ? AND `parentId` IN (?a) LIMIT ?n', $parentType, $parentId, count($parentId));
            }
            else {
                if (is_numeric($parentId)) {
                    return $this->db->query('DELETE FROM ?_translates WHERE `parentId` = ? AND `parentType` = ? LIMIT 1', (int)$parentId, $parentType);
                }
                else {
                    throw new \ErrorException('Incorrect parent id');
                }
            }
        }

        public function setTranslate($parentId, $parentType, $isoLang, $name, $descr = null, $sub1 = null, $sub2 = null) {

            $check = $this->getTranslates($parentId, $parentType, $isoLang);
            if ($check[$isoLang]) {
                $result = $this->updateTranslate($parentId, $parentType, $isoLang, $name, $descr, $sub1, $sub2);
            }
            else {
                $result = $this->addTranslate($parentId, $parentType, $isoLang, $name, $descr, $sub1, $sub2);
            }

            return $result;
        }

        private function addTranslate($parentId, $parentType, $isoLang, $name, $descr, $sub1, $sub2) {

            $data = [
                'parentId' => (int)$parentId,
                'parentType' => $parentType,
                'isoLang' => $isoLang,
                'name' => $name,
                'descr' => $descr,
                'sub1' => $sub1,
                'sub2' => $sub2];
            $result = $this->db->query("INSERT INTO ?_translates(?#) VALUES(?a)", array_keys($data), array_values($data));

            return $result;
        }

        private function updateTranslate($parentId, $parentType, $isoLang, $name, $descr, $sub1, $sub2) {

            $data = [
                'name' => $name,
                'descr' => $descr,
                'sub1' => $sub1,
                'sub2' => $sub2];

            $result = $this->db->query("UPDATE ?_translates SET ?a WHERE isoLang=? AND parentId=? AND parentType=?", $data, $isoLang, $parentId, $parentType);

            return $result;
        }
    }

<?php
    namespace SORApp\Models;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Model\MySQLRepository;
    use SORApp\Services\Servio\Servio;

    /**
     * Description of Servers
     *
     * @author sorokin_r
     */
    class Server extends MySQLRepository {

        public $id;
        public $url;
        public $version;
        public $accessKey;
        public $currency;
        public $descr;

        public function validateRules() {

            return [
                'id' => [
                    'number' => [
                        'allowEmpty',
                        'int']],
                'url' => [
                    'URL адрес модуля Servio неверный',
                    'string' => ['match' => '@^https?://[^\s/$.?#].[^\s]*$@']],
                'version' => [
                    'Версия имеет неверный формат',
                    'string' => ['match' => '/^\d{1,2}\.\d{1,3}(\.\d{1,5})?$/']],
                'accessKey' => [
                    'Не указан секретный ключ',
                    'string' => ['min' => 20]],
                'currency' => [
                    'Указана неверная валюта',
                    'string' => ['length' => 3]],
                'descr' => [
                    'Описание должно быть не длиннее 100 символов',
                    'string' => [
                        'allowEmpty',
                        'max' => 100]]];
        }

        public function loadById($id) {

            $row = $this->db->selectRow('SELECT * FROM ?_servers WHERE id=?', $id);
            if (!is_array($row) OR !count($row)) {
                throw new HttpException(404, 'Server not found');
            }

            $this->autoSetAttributes($row, false);

            return $this;
        }

        public function getAll() {

            $servers = $this->db->select("SELECT t.id as ARRAY_KEY, t.* FROM ?_servers t");
            if (count($servers) == 0) {
                $servers = false;
            }

            return $servers;
        }

        public function add() {

            if (!$this->validate()) {
                throw new HttpException(400, 'Incorrect input');
            }

            $this->id = $this->db->query("INSERT INTO ?_servers (`url`,`version`,`accessKey`,`currency`,`descr`) VALUES(?, ?, ?, ?, ?)", $this->url, $this->version, $this->accessKey, $this->currency, $this->descr);
            if (!$this->id) {
                $this->addError('id', $this->dbErrorString);
                throw new HttpException(400, 'Incorrect input');
            }
        }

        public function update() {

            if (!$this->validate()) {
                throw new HttpException(400, 'Incorrect input');
            }

            $result = $this->db->query('UPDATE ?_servers SET ?a WHERE id = ? LIMIT 1', [
                'url' => $this->url,
                'version' => $this->version,
                'accessKey' => $this->accessKey,
                'currency' => $this->currency,
                'descr' => $this->descr,], $this->id);

            if (!is_int($result)) {
                $this->addError('id', $this->dbErrorString);
                throw new HttpException(400, 'Incorrect input');
            }
        }

        public function delete($serverId) {

            $result = $this->db->query("DELETE FROM ?_servers WHERE id=?", $serverId);

            return $result;
        }

        public function getInfo($serverId) {

            $server = $this->db->selectRow("SELECT * FROM ?_servers WHERE id=?", $serverId);
            if ($server == []) {
                $server = false;
            }

            return $server;
        }

        /**
         * Метод выгружает доступные отели с сервера
         * Возвращает три массива: (Все ключи массива это айдишники из сервио)
         * existing - отели существующие в базе, содержит в себе 2 массива с отсутствующими и лишними категориями номеров
         * notExistsInServer - массив содержит массив отелей которых нет на сервере
         * notExistsInDatabase - массов содержит массив отелей которых нет в базе. Каждый элемент данного массива содержит категории номеров
         *
         * @throws \ErrorException
         * @return array массив новых отелей и категорий номеров
         */
        public function syncHotelData() {

            $servio = $this->owner->createComponent('servio', $this->url, $this->accessKey, $this->version, $this->currency);
            $servioRoomTypes = $servio->getRoomTypesList(0);

            if (!$servioRoomTypes) {
                throw new \ErrorException('Cannot get room types list');
            }

            // Денормализируем список номеров
            $hotelsList = [];
            foreach ($servioRoomTypes['HotelIDs'] as $number => $hotelID) {
                if (!isset($hotelsList[$hotelID])) {
                    $hotelsList[$hotelID] = [];
                }

                $hotelsList[$hotelID][$servioRoomTypes['IDs'][$number]] = $servioRoomTypes['ClassNames'][$number];
            }

            $response = ['existing' => []];

            // Получаем список новых отелей
            list($response['notExistsInDatabase'], $response['notExistsInServer']) = $this->hotelModel->syncServioID($this->id, array_keys($hotelsList));

            // Инвертируем списки, что бы в качестве ключей были айдишники отелей из сервио
            $response['notExistsInDatabase'] = array_flip($response['notExistsInDatabase']);
            $response['notExistsInServer'] = array_flip($response['notExistsInServer']);

            // Проверяем существование номеров
            foreach ($hotelsList as $hotelID => $roomTypes) {
                if (!isset($response['notExistsInDatabase'][$hotelID])) {
                    $roomSync = $this->roomModel->syncRoomTypesServioID($this->id, $hotelID, $roomTypes);
                    if (count($roomSync)) {
                        $response['existing'][$hotelID] = $roomSync;
                        unset($roomSync);
                    }
                }
                else {
                    $response['notExistsInDatabase'][$hotelID] = $roomTypes;
                }
            }

            ksort($response['notExistsInDatabase']);

            return $response;
        }
    }

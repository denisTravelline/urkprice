<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 24.10.14 12:23
     */

    namespace SORApp\Controllers;

    use SORApp\Components\BaseController;

    class CliController extends BaseController {

        private $_timeStart;

        public function beforeActions() {

            $this->_timeStart = microtime(true);
            $this->println('==============================================');
            $this->println(get_class($this) . '::' . ucfirst($this->owner->action) . 'Action()');
            $this->println('==============================================');
        }

        public function getRuntime() {

            return microtime(true) - $this->_timeStart;
        }

        public function printStatus($status) {

            $this->println('[' . $this->response->getTimeString() . '] ' . sprintf('[%011.5f] ' . $status, $this->runtime));
        }

        public function println($text = '') {

            echo $text . PHP_EOL;
        }
    }

<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Controllers\AdminController;
    use SORApp\Components\Pagination;
    use SORApp\Models\Booking\EmailDocumentsQueue;

    /**
     * Class Orders
     * @package SORApp\Controllers\Admin
     */
    class Documents extends AdminController {

        public $accessFilters = [
            'index,show' => 'admin',];

        public function indexAction() {

            $status = [
                "Ожидание",
                "Обработка",
                "Отправлен"];
            $pagination = new Pagination([
                'itemPerPage' => 20,
                'currentPage' => (isset($_GET['page']) ? max(1, (int)$_GET['page']) : 1)]);
            $documents = new EmailDocumentsQueue();
            $result = $documents->getDocumentsList($pagination->offset, $pagination->itemPerPage);
            $pagination->itemCount = $documents->itemCount;

            return [
                'documents' => $result,
                'status' => $status,
                'pagination' => $pagination];
        }

        public function showAction() {

            $directory = SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'servio_documents';
            if (!$handle = @fopen($directory . DIRECTORY_SEPARATOR . $this->get['name'], 'r')) {
                throw new HttpException(404, 'Файл не найден на сервере');
            }
            while (ob_get_level() > 0) {
                ob_end_clean();
            }
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Disposition: attachment; filename=" . $this->get['name']);
            header("Content-Transfer-Encoding: binary");

            while (($buffer = fgets($handle, 4096)) !== false) {
                echo $buffer;
            }

            fclose($handle);
            die;
        }
    }

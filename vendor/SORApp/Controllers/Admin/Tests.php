<?php
    namespace SORApp\Controllers\Admin;

    /**
     * Description of AdminTests
     *
     * @author sorokin_r
     */
    class Tests extends Controllers {

        public function getRooms() {

            $hotelId = 6;
            $arrival = '25.07.2014T14:00:00';
            $departure = '30.07.2014T09:00:00';
            $lang = DEFAULT_LANG;
            $password = '$T6WCZBT#J';
            $rooms = new Room();
            $freeRooms = $rooms->getFree($hotelId, $arrival, $departure, $lang, $password);

            return $freeRooms;
        }

        public function getPrices() {

            $hotelId = 5;
            $arrival = '25.07.2014T10:00:00';
            $departure = '30.07.2014T09:00:00';
            $lang = DEFAULT_LANG;
            $password = '$T6WCZBT#J';
            $payType = 'cash';
            $guestsArray = [
                [
                    'adt' => 1,
                    'cnn' => 0,
                    'inf' => 0,
                    'extBed' => false],
                [
                    'adt' => 3,
                    'cnn' => 0,
                    'inf' => 0,
                    'extBed' => true]];
            $rooms = new Room();
            $prices = $rooms->getPrices($hotelId, $arrival, $departure, $guestsArray, $payType);

            return $prices;
        }

        public function getFreeRoomsPrices() {

            $hotelId = 2;
            $arrival = '25.07.2014T10:00:00';
            $departure = '30.07.2014T09:00:00';
            $lang = DEFAULT_LANG;
            $password = '$T6WCZBT#J';
            $payType = 'cash';
            $guestsArray = [
                [
                    'adt' => 1,
                    'cnn' => 0,
                    'inf' => 0,
                    'extBed' => false],
                [
                    'adt' => 3,
                    'cnn' => 0,
                    'inf' => 0,
                    'extBed' => true]];
            $rooms = new Room();
            $result = $rooms->getFreeRoomsPrices($hotelId, $arrival, $departure, $guestsArray, $payType, 'ru', $password);

            return $result;
        }

    }


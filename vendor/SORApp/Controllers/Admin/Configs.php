<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Controllers\AdminController;

    /**
     * Description of AdminConfig
     *
     * @author sorokin_r
     */
    class Configs extends AdminController {

        public $accessFilters = [
            'access,add,changeAccess,delete,sendPassword' => 'admin',
            'index,profile,setPassword' => 'manager',];

        public function indexAction() {

            return [];
        }

        public function accessAction() {

            $admins = new AdminsModel();
            $adminsList = $admins->getAll();

            $data = [
                'title' => 'Настройки доступа',
                'admins' => $adminsList];

            return $data;
        }

        public function profileAction() {

            return [];
        }

        public function addAction() {

            $eMail = Filters::string($this->post, 'eMail', 3);
            $fName = Filters::string($this->post, 'fName', 3);
            $lName = Filters::string($this->post, 'lName', 3);

            $mAdmin = new AdminsModel();
            $result = $mAdmin->add($eMail, $fName, $lName);

            return $result;
        }

        public function changeAccessAction() {

            $adminId = Filters::number($this->post, 'adminId', 'int', 1);
            $access = Filters::string($this->post, 'access', 3);
            $mAdmins = new AdminsModel();
            $result = $mAdmins->changeAccess($adminId, $access);

            return $result;
        }

        public function deleteAction() {

            $adminId = Filters::number($this->params, 0, 'int', 1);
            $mAdmins = new AdminsModel();
            $result = $mAdmins->delete($adminId);

            return $result;
        }

        public function sendPasswordAction() {

            $adminId = Filters::number($this->params, 0, 'int', 1);
            $mAdmins = new AdminsModel();
            $result = $mAdmins->sendPassword($adminId);

            return $result;
        }

        public function setPasswordAction() {

            $newPassword = Filters::string($this->post, 'newPassword', 6);
            $chkPassword = Filters::string($this->post, 'chkPassword', 6);
            if ($newPassword != $chkPassword) {
                throw new Exception('Пароли не совпадают', 10);
            }
            $adminId = $admin['id'];
            $mAdmins = new AdminsModel();
            $result = $mAdmins->setPassword($adminId, $newPassword);

            return $result;
        }
    }

<?php
    namespace SORApp\Controllers\Admin;

    /**
     * Description of AdminTranslates
     *
     * @author sorokin_r
     */
    class Translates extends Controllers {

        /**
         * Метоод создает или обновляет новый перевод
         * @return integer
         */
        public function set() {

            AdminsModel::check('Сотрудник');
            $mTranslates = new TranslatesModel();

            $parentId = Filters::number($this->post, 'parentId', 'int', 1);
            $parentType = Filters::string($this->post, 'parentType', 2);
            $lang = Filters::string($this->post, 'lang', 2, 2);
            $name = Filters::string($this->post, 'name', 2);
            $descr = Filters::string($this->post, 'descr', false, false, false, null);
            $sub1 = Filters::string($this->post, 'sub1', false, false, false, null);
            $sub2 = Filters::string($this->post, 'sub2', false, false, false, null);

            $result = $mTranslates->setTranslate($parentId, $parentType, $lang, $name, $descr, $sub1, $sub2);

            return $result;
        }
    }

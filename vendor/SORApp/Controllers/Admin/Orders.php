<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Components\DataProvider;
    use SORApp\Components\GridView;
    use SORApp\Components\Pagination;
    use SORApp\Controllers\AdminController;
    use SORApp\Models\Booking\AbstractBookingFactory;

    /**
     * Class Orders
     * @package SORApp\Controllers\Admin
     */
    class Orders extends AdminController {

        public function indexAction() {

            $pagination = new Pagination([
                'itemPerPage' => 20,
                'currentPage' => (isset($_GET['page']) ? max(1, (int)$_GET['page']) : 1)]);

            $filters = (isset($_GET['filters']) ? array_filter(array_filter($_GET['filters'], 'is_scalar'), 'strlen') : []);

            $filtersNormalized = [];
            $isAll = (isset($filters['all']) && $filters['all'] == 1);
            if (!$isAll) {
                $filtersNormalized['account'] = [
                    'type' => 'compare',
                    'operator' => 'IS',
                    'value' => 'NOT NULL'];
            }

            if (isset($filters['date']) && $start = strtotime($filters['date'])) {
                $filtersNormalized['reservation_at'] = [
                    'type' => 'between_date',
                    'start' => $start,
                    'end' => strtotime('midnight next day', $start)];
            }

            $reservationBookingFactory = AbstractBookingFactory::getFactory('reservation');
            $reservations = $reservationBookingFactory->getPage($filtersNormalized, (isset($_GET['orders']) ? $_GET['orders'] : ['created_at' => 'desc']), $pagination->getOffset(), $pagination->getItemPerPage());

            $pagination->setItemCount($reservations['total']);

            return [
                'pagination' => $pagination,
                'page' => $reservations['page'],
                'filters' => $filters];
        }

        public function showAction() {

        }
    }

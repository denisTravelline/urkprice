<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Controllers\AdminController;
    use SORApp\Models\TransferPlace;
    use SORApp\Models\Transfer;

    /**
     * Description of AdminTransfers
     *
     * @author sorokin_r
     */
    class Transfers extends AdminController {

        public $accessFilters = [
            'index,save,delete' => 'manager',];

        public function indexAction() {

            $transferModel = new TransferPlace();

            return [
                'availableLanguages' => $this->config['availableLanguages'],
                'transfers' => $transferModel->getCollection($this->config['availableLanguages'])];
        }

        public function saveAction() {

            if (isset($this->post['Transfer']) AND
                isset($this->post['Transfer']['id']) AND isset($this->post['Transfer'][$this->post['Transfer']['id']])
            ) {
                $transferModel = new TransferPlace();
                if ($this->post['Transfer']['id'] > 0) {
                    $transferModel->loadById(intval($this->post['Transfer']['id']), $this->config['availableLanguages']);
                }
                $transferModel->autoSetAttributes($this->post['Transfer'][$this->post['Transfer']['id']]);
                if ($transferModel->save()) {
                    $this->redirect('admin/transfers/index');
                }
                else {
                    $transferModel->attachResponseError($this->response);
                }
            }

            return $this->getOwner()->runController($this->getOwner()->module, 'transfers', 'index');
        }

        public function saveTransferAction() {

            if (isset($this->post['Transfer']) AND
                isset($this->post['Transfer']['id']) AND isset($this->post['Transfer'][$this->post['Transfer']['id']])
            ) {
                $transferModel = new Transfer();
                if ($this->post['Transfer']['id'] > 0) {
                    $transferModel->loadChildById(intval($this->post['Transfer']['id']), $this->config['availableLanguages']);
                }
                $transferModel->autoSetAttributes($this->post['Transfer'][$this->post['Transfer']['id']]);
                if ($transferModel->save()) {
                    $this->redirect('admin/hotels/transfers?hotelID=' . $this->post['Transfer'][$this->post['Transfer']['id']]['hotel_id']);
                }
                else {
                    $transferModel->attachResponseError($this->response);
                }
            }

            return $this->getOwner()->runController($this->getOwner()->module, 'hotels', 'index');

        }

        public function deleteAction() {

            $transferModel = new TransferPlace();
            if (isset($this->get['transferID']) && !$transferModel->deleteById(intval($this->get['transferID']))) {
                $transferModel->attachResponseError($this->response);

                return $this->getOwner()->runController($this->getOwner()->module, 'transfers', 'index');
            }

            $this->redirect('admin/transfers/index');
        }

        public function deleteTransferAction() {

            $transferModel = new Transfer();
            if (isset($this->get['transferID']) && !$transferModel->deleteById(intval($this->get['transferID']))) {
                $transferModel->attachResponseError($this->response);

                return $this->getOwner()->runController($this->getOwner()->module, 'transfers', 'index');
            }

            $this->redirect('admin/hotels/transfers/?hotelID=' . $this->get['hotelID']);
        }
    }

    /*


     */

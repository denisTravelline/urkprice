<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Filters;
    use SORApp\Controllers\AdminController;
    use SORApp\Models\Hotel;
    use SORApp\Models\Server;

    /**
     * Description of AdminServers
     *
     * @author sorokin_r
     */
    class Servers extends AdminController {

        public $accessFilters = [
            'index,create,update,delete,sync' => 'admin'];

        /**
         * @return array
         */
        public function indexAction() {

            $serverModel = new Server();

            return ['servers' => $serverModel->getAll()];
        }

        /**
         * Добавление сервера
         * @return array
         */
        public function createAction() {

            $serverModel = new Server();

            if (isset($this->post['Server'])) {
                try {
                    $serverModel->autoSetAttributes($this->post['Server']);
                    $serverModel->add();
                    $this->redirect('admin/servers');
                }
                catch (\Exception $e) {
                    $serverModel->attachResponseError($this->getOwner()->getResponse());
                }
            }

            return ['serverModel' => $serverModel];
        }

        /**
         * Обновление сервера
         * @return array
         */
        public function updateAction() {

            $serverModel = $this->getServerEntity();

            if (isset($this->post['Server'])) {
                try {
                    $serverModel->autoSetAttributes($this->post['Server']);
                    $serverModel->update();
                    $this->redirect('admin/servers');
                }
                catch (\Exception $e) {
                    $serverModel->attachResponseError($this->getOwner()->getResponse());
                }
            }

            return ['serverModel' => $serverModel];
        }

        /**
         * Удаление сервера
         */
        public function deleteAction() {

            $serverModel = new Server();
            if (isset($this->get['serverID'])) {
                $serverModel->delete(Filters::number($this->get, 'serverID', 'int'));
            }

            $this->redirect('admin/servers');
        }

        /**
         * Синхронизация данных с сервером
         * @return array
         */
        public function syncAction() {

            $serverModel = $this->getServerEntity();
            $syncResponse = $serverModel->syncHotelData();

            $hotelModel = new Hotel();
            $hotels = $hotelModel->getGroup(Hotel::FETCH_ARRAY_SERVIO, false, $serverModel->id);

            return [
                'server' => $serverModel,
                'syncResponse' => $syncResponse,
                'hotels' => $hotels,];
        }

        /**
         * Получить сущность запрошенного сервера
         * @return Server
         * @throws \SORApp\Components\Exceptions\HttpException
         */
        protected function getServerEntity() {

            if (isset($this->get['serverID'])) {
                $serverModel = new Server();
                $serverModel->loadById(Filters::number($this->get, 'serverID', 'int'));

                return $serverModel;
            }
            else {
                throw new HttpException(404, 'Server not found');
            }

        }
    }

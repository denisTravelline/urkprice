<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Controllers\AdminController;
    use SORApp\Models\User;
    use SORApp\Components;

    /**
     * Description of AdminUsers
     *
     * @author novoselcev.o
     */
    class Users extends AdminController {

        public $accessFilters = [
            'index,delete' => 'admin',
            'profile' => 'manager',];

        public function indexAction() {

            $userComponent = $this->getComponent('user');
            $userModel = new User();
            $users = $userModel->getAll();
            foreach ($users as &$v) {
                foreach ($v as $key => &$value) {
                    if ($key == 'access') {
                        $v[$key] = json_decode($v[$key]);
                    }
                }
            }

            return [
                'users' => $users,
                'roles' => $userComponent->accessRoles,];
        }

        public function saveAction() {

            if (isset($this->post['User']) AND isset($this->post['User']['id']) AND isset($this->post['User'][$this->post['User']['id']])) {
                $userModel = new User();
                $userComponent = $this->getComponent('user');
                if ($this->post['User']['id'] > 0) {
                    $userModel->findByPK(intval($this->post['User']['id']));
                }
                //var_dump($userModel->autoSetAttributes($this->post['User'][$this->post['User']['id']], false));die;
                //var_dump($this->post);die;
                $userModel->autoSetAttributes($this->post['User'][$this->post['User']['id']], false);
                if ($userModel->save()) {
                    if ($userComponent->checkAccess('admin')) {
                        $this->redirect('admin/users/index');
                    }
                    else {
                        $this->redirect('admin/users/profile');
                    }
                }
                else {
                    $userModel->attachResponseError($this->response);
                }
            }

            return $this->getOwner()->runController($this->getOwner()->module, 'transfers', 'index');
        }

        public function profileAction() {

            $userComponent = $this->getComponent('user');
            $user = $userComponent->getCurrent();

            return [
                'user' => $user,
                'roles' => $userComponent->accessRoles,];
        }

        public function deleteAction() {

            $userModel = new User();
            if (isset($this->get['userID']) && !$userModel->delete(intval($this->get['userID']))) {
                $userModel->attachResponseError($this->response);

                return $this->getOwner()->runController($this->getOwner()->module, 'transfers', 'index');
            }

            $this->redirect('admin/users/index');
        }
    }

    /*


     */

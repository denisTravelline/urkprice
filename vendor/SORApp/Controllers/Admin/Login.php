<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 09.09.14 12:55
     */

    namespace SORApp\Controllers\Admin;

    use SORApp\Controllers\AdminController;
    use SORApp\Services\Validator\ValidatorException;

    class Login extends AdminController {

        /**
         * Убираем проверку прав доступа для данного контроллера
         */
        public function beforeActions() { }

        public function indexAction() {

            if ($this->getCurrentUser()) {
                $this->redirect('admin/main');
            }

            if (isset($this->post['email'])) {
                $this->templateManager->addGlobal('email', $this->post['email']);

                if (isset($this->post['password'])) {
                    try {
                        /** @var \SORApp\Components\WebUser $userComponent */
                        $userComponent = $this->getComponent('user');
                        if ($userComponent->authorization($this->post['email'], $this->post['password'])) {
                            $this->redirect('admin/main');
                        }
                    }
                    catch (ValidatorException $e) {
                        $this->response->addErrorMessage($e->getMessage());
                    }
                }
            }

            return $this->response;
        }

        public function logoutAction() {

            /** @var \SORApp\Components\WebUser $userComponent */
            $userComponent = $this->getComponent('user');
            $userComponent->logout();
            $this->redirect('admin/login');
        }
    }

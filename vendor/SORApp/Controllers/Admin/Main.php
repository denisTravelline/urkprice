<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Controllers\AdminController;

    /**
     * Class Main
     * @package SORApp\Controllers\Admin
     */
    class Main extends AdminController {

        public function indexAction() {

            $this->redirect('admin/orders');
        }
    }

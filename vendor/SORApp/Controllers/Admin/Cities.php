<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Controllers\AdminController;
    use SORApp\Models\City;

    /**
     * Class Cities
     * @package SORApp\Controllers\Admin
     */
    class Cities extends AdminController {

        public $accessFilters = [
            'index,save,delete' => 'manager',];

        public function indexAction() {

            $cityModel = new City();

            return [
                'availableLanguages' => $this->config['availableLanguages'],
                'cities' => $cityModel->getCollection($this->config['availableLanguages'])];
        }

        public function saveAction() {

            if (isset($this->post['City']) AND isset($this->post['City']['id']) AND isset($this->post['City'][$this->post['City']['id']])) {
                $cityModel = new City();
                if ($this->post['City']['id'] > 0) {
                    $cityModel->loadById(intval($this->post['City']['id']), $this->config['availableLanguages']);
                }

                $cityModel->autoSetAttributes($this->post['City'][$this->post['City']['id']]);
                if ($cityModel->save()) {
                    $this->redirect('admin/cities/index');
                }
                else {
                    $cityModel->attachResponseError($this->response);
                }
            }

            return $this->getOwner()->runController($this->getOwner()->module, 'cities', 'index');
        }

        public function deleteAction() {

            $cityModel = new City();
            if (isset($this->get['cityID']) && !$cityModel->deleteById(intval($this->get['cityID']))) {
                $cityModel->attachResponseError($this->response);

                return $this->getOwner()->runController($this->getOwner()->module, 'cities', 'index');
            }

            $this->redirect('admin/cities/index');
        }
    }

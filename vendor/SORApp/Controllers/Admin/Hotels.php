<?php
    namespace SORApp\Controllers\Admin;

    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\Filters;
    use SORApp\Controllers\AdminController;
    use SORApp\Models\City;
    use SORApp\Models\Hotel;
    use SORApp\Models\Room;
    use SORApp\Models\Server;
    use SORApp\Models\Translate;
    use SORApp\Models\Transfer;
    use SORApp\Models\Booking\SelectedTransfers;

    /**
     * Class Hotels
     * @package SORApp\Controllers\Admin
     *
     * @author Sevastoanov Andrey
     */
    class Hotels extends AdminController {

        public $accessFilters = [
            'index,getInfo,coords,visible' => 'manager',
            'config,add' => 'admin',];

        /**
         * Спосок отелей
         *
         * @return array
         */
        public function indexAction() {

            $hotelModel = new Hotel();
            $hotels = $hotelModel->getGroup(Hotel::FETCH_ARRAY_CITY, false);

            $cityModel = new City();
            $cities = $cityModel->getAllById(array_keys($hotels));

            $serverModel = new Server();
            $servers = $serverModel->getAll();

            return [
                'hotels' => $hotels,
                'cities' => $cities,
                'servers' => $servers,];
        }

        /**
         * Подробная информация об отеле
         *
         * @return \SORApp\Components\Response
         * @throws \SORApp\Components\Exceptions\HttpException
         */
        public function showAction() {

            if (!isset($this->get['hotelID'])) {
                throw new HttpException(404, 'Hotel not found');
            }

            $response = $this->getOwner()->getResponse();

            $hotelModel = new Hotel();
            $hotelModel->loadById(Filters::number($this->get, 'hotelID', 'int'), $this->config['availableLanguages']);

            if (isset($this->post['Hotel'])) {
                $hotelModel->autoSetAttributes($this->post['Hotel']);
                $hotelModel->id = $this->get['hotelID'];
                if (!$hotelModel->validate() OR !$hotelModel->update()) {
                    $hotelModel->attachResponseError($response);
                }
                else {
                    if (isset($this->post['Hotel']['translates'])) {
                        $hotelModel->saveTranslate($this->post['Hotel']['translates']);
                    }

                    $this->redirect('admin/hotels/show', ['hotelID' => $this->get['hotelID']]);
                }
            }

            $roomModel = new Room();
            $roomTypes = $roomModel->getRooms($hotelModel->id, null, $this->config['availableLanguages']);

            $response->setResponse([
                'availableLanguages' => $this->config['availableLanguages'],
                'hotel' => $hotelModel,
                'roomTypes' => $roomTypes,]);

            return $response;
        }

        public function transfersAction() {

            if (!isset($this->get['hotelID'])) {
                throw new HttpException(404, 'Hotel not found');
            }

            $hotelModel = new Hotel();
            $hotelModel->loadById(Filters::number($this->get, 'hotelID', 'int'), $this->config['availableLanguages']);

            $transferModel = new Transfer();
            $places = $transferModel->getTransferPlaces($this->config['availableLanguages']);
            $trans = $transferModel->getCollection($this->get['hotelID'], $this->config['availableLanguages']);

            foreach ($trans as $v) {
                $places[$v['place_id']]['transfers'][] = $v;
            }

            return [
                'availableLanguages' => $this->config['availableLanguages'],
                'hotel' => $hotelModel,
                'transfers' => $places,];
        }

        /**
         * Редактирование категории номеров
         *
         * @return \SORApp\Components\Response
         * @throws \SORApp\Components\Exceptions\HttpException
         */
        public function roomAction() {

            if (!isset($this->get['hotelID'])) {
                throw new HttpException(404, 'Hotel not found');
            }

            if (!isset($this->get['roomID'])) {
                throw new HttpException(404, 'Hotel not found');
            }

            $hotelModel = new Hotel();
            $hotelModel->loadById(Filters::number($this->get, 'hotelID', 'int'), $this->getOwner()->lang);

            $roomModel = new Room();
            $roomModel->loadById(Filters::number($this->get, 'roomID', 'int'), $this->config['availableLanguages']);

            $response = $this->getOwner()->getResponse();

            if (isset($this->post['Room'])) {
                $roomModel->autoSetAttributes($this->post['Room']);
                if (isset($_FILES['Room'])) {
                    $roomModel->setNewPhoto($_FILES['Room']);
                }

                $roomModel->id = $this->get['roomID'];
                if (!$roomModel->validate() OR !$roomModel->update()) {
                    $roomModel->attachResponseError($response);
                }
                else {
                    if (isset($this->post['Room']['translates'])) {
                        $roomModel->saveTranslate($this->post['Room']['translates']);
                    }

                    $this->redirect('admin/hotels/show', ['hotelID' => $this->get['hotelID']]);
                }
            }

            $response->setResponse([
                'availableLanguages' => $this->config['availableLanguages'],
                'hotel' => $hotelModel,
                'room' => $roomModel,]);

            return $response;
        }

        public function createAction() {

            if (!isset($this->post['Hotel']) OR !is_array($this->post['Hotel'])) {
                $this->response->addErrorMessage('Incorrect use action "create" in hotels controller');

                return $this->getOwner()->runController($this->getOwner()->module, 'hotels', 'index');
            }

            $hotelModel = new Hotel();
            $hotelModel->autoSetAttributes($this->post['Hotel']);

            if (!$hotelModel->validate() OR !$hotelModel->insert()) {
                $hotelModel->attachResponseError($this->response);

                return $this->getOwner()->runController($this->getOwner()->module, 'hotels', 'index');
            }
            else {
                if (isset($this->post['Room']) AND is_array($this->post['Room']) AND count($this->post['Room'])) {
                    $this->createRooms($hotelModel->id, $this->post['Room']);
                }
            }

            $this->redirect('admin/hotels/show', ['hotelID' => $hotelModel->id]);
        }

        public function syncRoomsAction() {

            if (!isset($this->get['hotelID']) OR !isset($this->post['Room']) OR !is_array($this->post['Room']) OR !count($this->post['Room'])) {
                $this->response->addErrorMessage('Incorrect use action "syncRooms" in hotels controller');

                return $this->getOwner()->runController($this->getOwner()->module, 'hotels', 'index');
            }

            $hotelModel = new Hotel();
            $hotelModel->loadById(intval($this->get['hotelID']));
            foreach ($this->post['Room'] as $action => $roomTypesList) {
                switch ($action) {
                    case 'create':
                        $this->createRooms($hotelModel->id, $roomTypesList);
                        break;

                    case 'toMany':
                        foreach ($roomTypesList as $roomServioID => $data) {
                            $data = explode(':', $data);
                            if (count($data) == 2) {
                                switch ($data[0]) {
                                    case 'removeButHotel':
                                        $roomModel = new Room();
                                        $roomModel->removeButHotel($roomServioID, $data[1]);
                                        break;
                                    case 'changeServioID':
                                        $roomModel = new Room();
                                        $roomModel->loadByServioId($roomServioID);
                                        $roomModel->servioId = $data[1];
                                        if ($roomModel->validate()) {
                                            $roomModel->update();
                                        }
                                        break;
                                }
                            }
                        }
                        break;

                    case 'move':
                        foreach ($roomTypesList as $roomServioID => $newHotelID) {
                            $roomModel = new Room();
                            $roomModel->loadByServioId($roomServioID);
                            $roomModel->hotelId = $newHotelID;
                            if ($roomModel->validate()) {
                                $roomModel->update();
                            }
                        }
                        break;
                    default:
                        $this->response->addErrorMessage('Incorrect action "' . htmlentities($action) . '"');
                        break;
                }
            }

            $this->redirect('admin/hotels/show', ['hotelID' => $hotelModel->id]);
        }

        protected function createRooms($hotelID, array $rooms) {

            $hotelModel = new Hotel();
            $servio = $hotelModel->getServioConnect($hotelID);

            foreach ($rooms as $servioId => $alias) {
                $roomModel = new Room();
                $roomModel->name = $alias;
                $roomModel->servioId = $servioId;
                $roomModel->hotelId = $hotelID;

                if (!$roomModel->validate() OR !$roomModel->insert()) {
                    $roomModel->attachResponseError($this->response);
                }
                else {
                    $servioRoomData = $servio->getRoomType($servioId, $this->config['availableLanguages']);
                    $servioImage = $servio->getImages($servioId);

                    if ($servioRoomData) {
                        if (isset($servioRoomData['MainPlacesCount'])) {
                            $roomModel->beds = max((int)$servioRoomData['MainPlacesCount'], 1);
                        }

                        if (isset($servioRoomData['MaxAdditionalPlacesCount'])) {
                            $roomModel->extBeds = max((int)$servioRoomData['MaxAdditionalPlacesCount'], 0);
                        }

                        if ($servioImage AND $servioImage['Count'] == 1) {
                            $roomModel->setNewPhotoRaw(base64_decode(current($servioImage['Images'])));
                            unset($servioImage);
                        }

                        if (isset($servioRoomData['translates'])) {
                            $roomModel->saveTranslate($servioRoomData['translates']);
                        }

                        $roomModel->update();
                    }
                }
            }
        }
    }

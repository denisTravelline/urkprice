<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 05.09.14 17:13
     */

    namespace SORApp\Controllers;

    use SORApp\Components\BaseController;
    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Components\WebUser;

    /**
     * Class AdminController
     * Базовый класс для контроллеров админки
     *
     * @package SORApp\Controllers
     */
    class AdminController extends BaseController {

        public $accessFilters = [];
        protected $_webUser;

        /**
         * Проверка доступа к запрошенному действию для текущего пользователя
         *
         * @throws \SORApp\Components\Exceptions\HttpException
         */
        public function beforeActions() {

            $user = $this->getCurrentUser();
            if (!$user) {
                $this->redirect('admin/login');
            }

            $this->getTemplateManager()->addGlobal('currentUser', $user);

            if (count($this->accessFilters)) {
                $action = strtolower($this->getOwner()->action);
                foreach ($this->accessFilters as $actions => $accessGroup) {
                    $actions = preg_split('/[,\s]+?/', strtolower($actions));
                    if (in_array($action, $actions) AND !$this->checkAccess($accessGroup)) {
                        throw new HttpException(403, 'Access denied');
                    }
                }
            }
            else {
                if (!$this->checkAccess('admin') AND !$this->checkAccess('manager')) {
                    throw new HttpException(403, 'Access denied');
                }
            }
        }

        /**
         * Хелпер для получения информации о текущем пользователе
         *
         * @return \SORApp\Components\Model\AuthUserModelInterface|bool
         */
        public function getCurrentUser() {

            /** @var \SORApp\Components\WebUser $userComponent */
            $userComponent = $this->getComponent('user');

            return $userComponent->getCurrent();
        }

        /**
         * Хелпер для проверки прав доступа
         * @param string $checkedRole
         * @param null $user
         *
         * @return bool
         */
        public function checkAccess($checkedRole, $user = null) {

            /** @var \SORApp\Components\WebUser $userComponent */
            $userComponent = $this->getComponent('user');

            return $userComponent->checkAccess($checkedRole, $user);
        }
    }

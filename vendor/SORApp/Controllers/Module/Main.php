<?php

    namespace SORApp\Controllers\Module;

    use SORApp\Components\Exceptions;
    use SORApp\Components\BaseController;

    /**
     * @author Alexander Olkhovoy <olkhovoy@gmail.com>
     */
    class Main extends BaseController {

        public function indexAction() {

            return [];
        }
    }

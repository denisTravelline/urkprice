<?php
    namespace SORApp\Controllers\Reg;

    use SORApp\Components\Exceptions, SORApp\Components\BaseController, SORApp\Controllers\BookingController;

    /**
     * Description of Main
     *
     * @author Alexander Olkhovoy <olkhovoy@gmail.com>
     */
    class Main extends BookingController {

        public function indexAction() {

            $this->registerStepUrls(['hashKey' => empty($hashKey) ? null : $hashKey]);

            return [];
        }
    }

<?php

    /**
     * @author Olkhovoy Alexander <olkhovoy@gmail.com>
     */
    namespace SORApp\Controllers\Reg {

        use SORApp\Components\App;
        use SORApp\Components\BaseController;
        use SORApp\Controllers\Reg\Noid;
        use SORApp\Services\Servio\Methods\SetClientDocuments;

        /**
         * Class BookingController
         * @package SORApp\Controllers
         *
         * Базовый контроллер который управляет
         * сценарием поиска и бронирования номеров
         */
        class Reg extends BaseController {

            public function __construct() {

                parent::__construct();
            }

            public function indexAction($guestID = 4541674) {

                isset($_GET['u']) ? $u = $_GET['u'] : $u = '';
                $this->response->addResponseElement('uid', $u);

                $servio = $this->owner->createComponent('servio');

                if (isset($_POST['saveForm'])) {
                    $docs = $servio->getClientDocuments($u);
                    $doctypes = $docs['ClientDocumentTypes'];
                    $clientIDs = $this->GetClientIDs($docs['ClientDocuments'],$docs['Adults']);
                    $array = $this->SaveClientDocs($clientIDs,$doctypes);
                    foreach($array as $cdocs){
                        $result = $servio->setClientDocuments($cdocs);
                        //$result = true;
                    }

                    $result
                        ? $this->redirect(App::getInstance()->url('reg', [
                        'u' => $u,
                        'saved' => 'true'
                    ]))
                        : $this->redirect(App::getInstance()->url('reg', [
                        'u' => $u,
                        'saved' => 'false'
                    ]));
                    //;
                }

                if (!empty($this->get['saved'])) {
                    $this->get['saved'] !=
                    'true' ? $this->response->addResponseElement('error', 'Произошла ошибка при сохранении') : $this->response->addResponseElement('success', 'Введенная информация сохранена');
                }

                if ($u != '') {
                    $docs = $servio->getClientDocuments($u);
                    //var_dump($docs);
                    if (!$docs) {
                        $this->response->addResponseElement('Error', 'Гость с таким кодом не найден');

                        return $this->response;
                    }
                    else {
                        $this->response->addResponseElement('docTypes', $docs['ClientDocumentTypes']);

                        $DocFields = $this->DocFields($docs['ClientDocumentTypes'], $this->CDFieldsArray($docs['ClientDocumentFields']));
                        $this->response->addResponseElement('DocFields', $DocFields);
                        $this->response->addResponseElement('AddedArr', empty($docs['ClientDocuments']) ? false : $docs['ClientDocuments']);
                        $this->response->addResponseElement('GuestID', empty($docs['GuestID']) ? false : $docs['GuestID']);
                        $this->response->addResponseElement('ClientID', empty($docs['ClientID']) ? false : $docs['ClientID']);
                        $this->response->addResponseElement('reservInfo', $this->ReservationInfo($docs));

                        return $this->response;
                    }
                }

                return $this->response;
            }

            private function ReservationInfo($docs) {

                $reservInfo = $docs;
                unset($reservInfo['ClientDocumentFields']);
                unset($reservInfo['ClientDocumentTypes']);
                unset($reservInfo['ClientDocuments']);
                unset($reservInfo['GuestName']);

                preg_match('/\((.+)\)/', $reservInfo['DateDeparture'], $m);
                $depdate = gmdate('d.m.Y', time($m[1]));

                $reservInfo['DateDeparture'] = $depdate;
                $reservInfo['DateArrival'] = date('d.m.Y', strtotime($reservInfo['DateArrival']));

                return $reservInfo;
            }

            private function CDFieldsArray($cdf = "") {

                if ($cdf == "") {
                    return false;
                }

                $showFields = [
                    'BlackList' => false,
                    'Vip' => false,
                    'Comment' => false,
                    'DocumentPhotoURL' => false,
                    'AdultOrChild' => false,
                    'CarNumber' => false
                ];

                $ret = [];
                foreach ($cdf as $item) {
                    $code = $item['ClientDocumentFieldCode'];
                    if (!isset($showFields[$code])) {
                        $ret[$code] = $item['ClientDocumentTypeName '];
                    }
                }

                return $ret;
            }

            private function DocFields($docs = "", $CDFieldsArray = "") {

                if ($docs == "" || $CDFieldsArray == "") {
                    return false;
                }

                $ret = [];
                foreach ($docs as $item) {

                    $fields = [];
                    foreach ($item['ClientDocumentFields'] as $cdf) {
                        if (array_key_exists($cdf, $CDFieldsArray))
                            $fields[$cdf] = $CDFieldsArray[$cdf];
                    }
                    $ret[$item['ClientDocumentTypeName']] = $fields; //$item['ClientDocumentFields'];
                    unset($fields);
                }

                return $ret;
            }

            public function GetClientIDs($ClientDocuments = null, $cnt = null){
                $arr = array();
                if($ClientDocuments != null) {
                    foreach ($ClientDocuments as $cd) {
                        if (!in_array($cd['ClientID'], $arr)) {
                            $arr[] = $cd['ClientID'];
                        }
                    }
                }else{
                    for($i=1;$i<=$cnt;$i++){
                        $arr[] = $i;
                    }
                }
                return $arr;
            }

            public function SaveClientDocs($clientIDs = null, $doctypes = 0) {

                $servio = $this->owner->createComponent('servio');

                $form = $_REQUEST;

                $docs = [];
                $array = [];

                foreach($clientIDs as $CID){
                    for($i=1;$i<=count($doctypes);$i++){
                        if($_REQUEST['doc-'.$i.'-'.$CID.'-Last_Name'] != ''){


                            $GuestID = $_REQUEST['doc-'.$i.'-'.$CID.'-GuestID'];
                            $ClientID = empty($_REQUEST['doc-'.$i.'-'.$CID.'-ClientID']) ? -1 : $_REQUEST['doc-'.$i.'-'.$CID.'-ClientID'];
                            $ClientDocumentTypeID = $_REQUEST['doc-'.$i.'-'.$CID.'-ClientDocumentTypeID'];
                            unset($form['u']);
                            unset($form['do']);
                            unset($form['saveForm']);
                            unset($form['GuestID']);
                            unset($form['ClientID']);
                            unset($form['ClientDocumentTypeID']);

                            $dateOfIssue = $form['doc-'.$i.'-'.$CID.'-DateOfIssue'];
                            if (!empty($dateOfIssue)) $form['doc-'.$i.'-'.$CID.'-DateOfIssue'] = date("Y-m-d", strtotime($dateOfIssue));

                            $birthDate = $form['doc-'.$i.'-'.$CID.'-Birth_Date'];
                            if (!empty($birthDate)) $form['doc-'.$i.'-'.$CID.'-Birth_Date'] = date("Y-m-d", strtotime($birthDate));

                            $expirationDate = $form['doc-'.$i.'-'.$CID.'-ExpirationDate'];
                            if (!empty($expirationDate)) $form['doc-'.$i.'-'.$CID.'-ExpirationDate'] = date("Y-m-d", strtotime($expirationDate));

                            $userData = [];

                            foreach ($doctypes[$i-1]['ClientDocumentFields'] as $key => $item) {
                                $it = [];
                                //$key = explode('-',$key);
                                $it['ClientDocumentFieldCode'] = $item;
                                $it['ClientDocumentFieldValue'] = $_REQUEST['doc-'.$i.'-'.$CID.'-'.$item];
                                $userData[] = $it;
                                unset($it);
                            }

                            //$docs = [];
                            $docs['ClientID'] = $ClientID;
                            $docs['ClientDocumentTypeID'] = $ClientDocumentTypeID;
                            $docs['ClientDocumentValues'] = $userData;

                            //$array = [];

                            $array[$CID]['GuestID'] = $GuestID;
                            $array[$CID]['ClientDocuments'][] = $docs;


                        }
                    }
                }

//                $GuestID = $_REQUEST['GuestID'];
//                $ClientID = empty($_REQUEST['ClientID']) ? -1 : $_REQUEST['ClientID'];
//                $ClientDocumentTypeID = $_REQUEST['ClientDocumentTypeID'];
//                unset($form['u']);
//                unset($form['do']);
//                unset($form['saveForm']);
//                unset($form['GuestID']);
//                unset($form['ClientID']);
//                unset($form['ClientDocumentTypeID']);
//
//                $dateOfIssue = $form['DateOfIssue'];
//                if (!empty($dateOfIssue)) $form['DateOfIssue'] = date("Y-m-d", strtotime($dateOfIssue));
//
//                $birthDate = $form['Birth_Date'];
//                if (!empty($birthDate)) $form['Birth_Date'] = date("Y-m-d", strtotime($birthDate));
//
//                $expirationDate = $form['ExpirationDate'];
//                if (!empty($expirationDate)) $form['ExpirationDate'] = date("Y-m-d", strtotime($expirationDate));
//
//                $userData = [];
//                foreach ($form as $key => $item) {
//                    $it = [];
//                    $it['ClientDocumentFieldCode'] = $key;
//                    $it['ClientDocumentFieldValue'] = $item;
//                    $userData[] = $it;
//                    unset($it);
//                }
//
//                $docs = [];
//                $docs['ClientID'] = $ClientID;
//                $docs['ClientDocumentTypeID'] = $ClientDocumentTypeID;
//                $docs['ClientDocumentValues'] = $userData;
//
//                $array = [];
//                $array['GuestID'] = $GuestID;
//                $array['ClientDocuments'][] = $docs;

                return $array;

            }

        }

    }

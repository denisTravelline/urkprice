<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 24.10.14 12:19
     */

    namespace SORApp\Controllers\Cli;

    use SORApp\Controllers\CliController;
    use SORApp\Models\Booking\EmailDocumentsQueue;
    use SORApp\Models\Hotel;

    class Mailing extends CliController {

        public $perPage = 10;
        public $releaseDeadLock = EmailDocumentsQueue::DEADLOCK_INTERVAL_DEFAULT;

        public function workerAction() {

            $this->printStatus('Set transaction configurations...');

            // Указываем стратегию чтения для блокировки
            $this->getComponent('database')->getConnection()->query('SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ');

            // Снимаем блокировки с повисших процессов
            EmailDocumentsQueue::releaseDeadLocks($this->releaseDeadLock);

            $hotelModel = new Hotel();

            /** @var \SORApp\Components\Mailer $mailer */
            $mailer = $this->getComponent('mailer');

            do {
                $documents = EmailDocumentsQueue::startProcessingWaiting($this->perPage);

                if (!is_array($documents) OR !count($documents)) {
                    break;
                }

                foreach ($documents as $id => $model) {
                    /** @var \SORApp\Models\Booking\EmailDocumentsQueue $model */

                    $this->printStatus('Create message. Recipient: ' . $model->recipient);

                    $message = $mailer->createMessage();
                    $message->setBody($model->message);
                    $message->setTo($model->recipient);
                    $message->setSubject($model->subject);

                    $servio = $hotelModel->getServioConnect($model->hotel_id);

                    foreach ($model->getDocuments() as $document) {
                        $servioDocument = $servio->getDocument($document, $model->hotel_id);
                        if (!$servioDocument OR !file_exists($servioDocument)) {
                            $model->status = EmailDocumentsQueue::STATUS_WAITING;
                            $model->save();
                            $this->printStatus('Documents not ready...');
                            continue 2;
                        }
                        else {
                            $message->attach(\Swift_Attachment::fromPath($servioDocument));
                        }
                    }

                    $sendStatus = $mailer->send($message);
                    $this->printStatus('Send message: ' . $sendStatus);
                    $model->status = EmailDocumentsQueue::STATUS_DONE;
                    $model->save();
                    $this->printStatus('Done');
                }
            }
            while (is_array($documents) AND count($documents) >= $this->perPage);

            return $this->response;
        }
    }

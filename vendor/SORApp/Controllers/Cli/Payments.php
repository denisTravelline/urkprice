<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 24.10.14 12:19
     */

    namespace SORApp\Controllers\Cli;

    use SORApp\Components\App;
    use SORApp\Controllers\CliController;
    use SORApp\Models\Booking\AbstractBookingFactory;

    class Payments extends CliController {

        /**
         * @return \SORApp\Components\Response
         * @throws \ErrorException
         */
        public function workerAction() {

            /** @var \SORApp\Components\Database $database */
            $database = $this->getComponent('database');

            /** @var \DbSimple_Database $db */
            $db = $database->getConnection();
            $db->query('SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ');

            $factory = AbstractBookingFactory::getFactory('paymentOrder');
            /** @var \SORApp\Models\Booking\PaymentOrder $ok */
            $pay = $factory->createEntity();

            for ($num = 1; true; $num++) {

                $id = 0;
                $ok = false;

                $log = '#' . $num . '. PaymentOrder';

                $db->transaction();
                try {

                    $row = $db->selectRow(
                        'SELECT id, status + 1 AS status, NOW() + INTERVAL (status + 1) * 5 MINUTE AS timeout' .
                        ' FROM ?_booking_payment_order' .
                        ' WHERE status > 0 AND status < 13 AND (timeout IS NULL OR timeout < NOW())' .
                        ' ORDER BY status ASC, timeout ASC' .
                        ' LIMIT 1' .
                        ' FOR UPDATE'); //echo json_encode($row, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

                    if (!empty($row['id'])) {

            			$id = intval($row['id']);
	                    $log .= ' [ID:' . $id . ']';
            			if (!!$id) {

                            $pay = $factory->loadByID($id);
            			    if ($pay) {

	                            $pay->status = $row['status'];
    	                	    $pay->timeout = $row['timeout'];
        	                    $log .= ' [DATA:' . json_encode($pay, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) . ']';

                				$ok = $factory->saveEntity($pay);
                            	$log .= ' [INIT:' . ($ok ? 'OK' : 'ERROR') . ']';
            			    }
			            }
                    }
                }
                catch (\Exception $e) {

                    $log .= ' [INIT:ERROR>' . $e->getMessage() . ']';
                }
                $db->commit();

                if (!$id) {
		            // No more records
                    break;
                }

                if ($ok) {

                    $ok = false;
                    try {

			            // Send SetReservationType to Servio
                        $ok = $pay->execute();
                        $log .= ' [SERVIO:' . ($ok ? 'OK' : 'ERROR') . ']';
                    }
                    catch (\Exception $e) {

                        $log .= ' [SERVIO:ERROR>' . $e->getMessage() . ']';
                    }

                    if ($ok) {

                        $ok = false;
                        try {

			                // Negative status = task complete
                            $pay->status = -abs($pay->status);
                            $ok = $factory->saveEntity($pay);
                            $log .= ' [RESULT:' . ($ok ? 'OK' : 'ERROR') . ']';
                        }
                        catch (\Exception $e) {

                            $log .= ' [RESULT:ERROR>' . $e->getMessage() . ']';
                        }
                    }
                }
                $log .= ' [' . ($ok ? 'SUCCESS' : 'FAILURE') . ']';

                App::Log('CLI', $log);
                $this->printStatus($log . PHP_EOL);
            }

            $this->printStatus('[DONE]' . PHP_EOL);
            return $this->response;
        }
    }

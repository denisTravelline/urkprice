<?php

    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Controllers;

    use SORApp\Components\App;
    use SORApp\Components\BaseController;
    use SORApp\Models\Booking\AbstractBookingEntity;
    use SORApp\Models\Booking\AbstractBookingFactory;

    /**
     * Class BookingController
     * @package SORApp\Controllers
     *
     * Базовый контроллер который управляет сценарием поиска и бронирования номеров
     */
    class BookingController extends BaseController {

        /**
         * @var array $_stepScenario Последовательность шагов.
         * Каждый шаг указывает на котроллер-исполненитель.
         * Опционально можно укзать название действия которое нужно будет запустить в контроллере.
         */
        protected $_stepScenario = [
            1 => 'search',
            //2 => 'transfer',
            2 => 'customer',
            3 => 'confirm',
            4 => 'payment',
            5 => 'status',];

        /**
         * @var int Номер текущего шага
         */
        protected $_currentStep;

        /**
         * @var string Контекс бронирования
         */
        protected $_hashKey;

        /**
         * Инициализация обработки сценария
         *
         * Действия знают о сценарии только то, что он поход на двусвязный список.
         * Т.е. они могут получить ссылку на предудущее, текущее и следующее действие.
         * Для этого во время инициализации мы определяет текуший шаг на основе запущенного контроллера и вызванного действия.
         * Важно! Если шаг определить не удалось, ссылки не будут сгенерированы.
         * @see \SORApp\Controllers\BookingController::$_stepScenario
         */
        public function __construct() {

            parent::__construct();

            $action = strtolower($this->getOwner()->action);
            $controller = strtolower($this->getOwner()->controller);

            $steps = array_map('strtolower', $this->_stepScenario);
            $this->_currentStep = ($step = array_search($controller . '/' . $action, $steps)) ===
                                  FALSE ? array_search($controller, $steps) : $step;
        }

        /**
         * Инверсия зависимости
         *
         * @param AbstractBookingFactory $factory
         * @param string $hashKey
         * @param array $default
         *
         * @return array|bool|AbstractBookingEntity
         */
        public function getEntity(AbstractBookingFactory $factory, $hashKey = null, $default = []) {

            return $factory->getEntity($hashKey ?: $this->getHashKey(), $default);
        }

        public function getHashKey() {

            if ($this->_hashKey === null) {

                if (isset($this->get['hashKey'])) {
                    $this->setHashKey($this->get['hashKey']);
                }
                else if ($this->_currentStep > 1)
                    $this->redirect($this->_stepScenario['1']);
            }

            return $this->_hashKey;
        }

        public function setHashKey($hashKey) {

            $this->_hashKey = $hashKey;
            $this->registerStepUrls(['hashKey' => $hashKey]);
        }

        /**
         * Регистрируем ссылки на текущий и окружающие шаги
         *
         * @param array $query
         */
        public function registerStepUrls($query = []) {

            $this->templateManager->addGlobal('stepNumber', $this->_currentStep);
            $this->templateManager->addGlobal('currentStepUrl', $this->getCurrentStepUrl($query));
            $this->templateManager->addGlobal('prevStepUrl', $this->getPrevStepUrl($query));
            $this->templateManager->addGlobal('nextStepUrl', $this->getNextStepUrl($query));
            $this->templateManager->addGlobal('steps', $this->getSteps($query));
        }

        public function getSteps($query = []) {

            $steps = [];
            foreach ($this->_stepScenario as $step => $path) {
                $session = explode('/', $path);
                $steps[$step] = [
                    'step' => $step,
                    'path' => $path,
                    'name' => end($session),
                    'title' => App::_(end($session), 'step', [$step]),
                    'url' => $this->getStepUrl($step, $query)];
            }

            return $steps;
        }

        /**
         * Редирект в случае пропуска текущего шага
         * В зависимости от направления редирект происходит либо на шаг вперед, либо на шаг назад
         *
         * @param array $query
         */
        public function skipStep($query = []) {

            if (!isset($this->get['refStep']) || intval($this->get['refStep']) < $this->_currentStep) {

                $this->redirect($this->getNextStepUrl($query));
            }
            else {

                $this->redirect($this->getPrevStepUrl($query));
            }
        }

        public function getNextStepUrl($query = []) {

            return $this->getStepUrl($this->_currentStep + 1, $query);
        }

        /**
         * @param int $step
         * @param array $query
         *
         * @return null|string
         */
        protected function getStepUrl($step, $query = []) {

            if (isset($this->_stepScenario[$step])) {

                if (!isset($query['hashKey']))
                    $query['hashKey'] = $this->getHashKey();
                if (!isset($query['refStep']))
                    $query['refStep'] = $this->_currentStep;

                return App::url($this->_stepScenario[$step], $query);
            }

            return null;
        }

        public function getPrevStepUrl($query = []) {

            return $this->getStepUrl($this->_currentStep - 1, $query);
        }

        /**
         * Хелпер для получения адреса текущего шага с учетом контекста
         * @param array $query
         *
         * @return null|string
         */
        public function getCurrentStepUrl($query = []) {

            return $this->getStepUrl($this->_currentStep, $query);
        }
    }

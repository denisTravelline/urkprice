<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 06.10.14 10:35
     */

    namespace SORApp\Controllers\Site;

    use SORApp\Components\App;
    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Controllers\BookingController;
    use SORApp\Models\Booking\AbstractBookingFactory;

    /**
     * Class Transfer
     * @package SORApp\Controllers\Site
     */
    class Transfer extends BookingController {

        /**
         * @return \SORApp\Components\Response
         * @throws \ErrorException
         * @throws \Exception
         */
        public function indexAction() {

            $searchRequestFactory = AbstractBookingFactory::getFactory('SearchRequest');

            /** @var \SORApp\Models\Booking\SearchRequest $searchRequest */
            $searchRequest = $this->getEntity($searchRequestFactory);

            /** @var \SORApp\Models\Booking\SelectedTransfersFactory $selectedTransfersFactory */
            $selectedTransfersFactory = AbstractBookingFactory::getFactory('SelectedTransfers');
            $selectedTransfersFactory->addCollectionRelation($searchRequest);

            if (!$selectedTransfersFactory->hasTransfers())
                $this->skipStep();

            if (isset($this->post['Transfer'])) {

                $selectedTransfersFactory->db->transaction();
                try {

                    if (!($entity = $selectedTransfersFactory->createTransfer($this->post['Transfer']))) {

                        $selectedTransfersFactory->attachResponseError($this->response);
                        throw new HttpException(400);
                    }

                    if (!$selectedTransfersFactory->saveEntity($entity)) {

                        $entity->attachResponseError($this->response);
                        throw new HttpException(400);
                    }

                    $reservation = $entity->getRelation('Reservation');
                    $reservation->addCostAdditional($entity->cost, $entity->currency);
                    $reservation->getFactory()->saveEntity($reservation);

                    $selectedTransfersFactory->db->commit();
                }
                catch (\Exception $e) {

                    $selectedTransfersFactory->db->rollback();
                    throw $e;
                }
            }
            $this->response->addResponseElement('title', App::_('transfer', 'step', [$this->_currentStep]));
            $this->response->addResponseElement('content', $this->templateManager->render('transferForm', [
                'maxSeats' => $searchRequest->getTotalGuests(),
                'selectedTransfersFactory' => $selectedTransfersFactory,
                'selectedTransfers' => $selectedTransfersFactory->loadEntity($searchRequest->hashKey),]));

            return $this->response;
        }

        /**
         *
         */
        public function removeAction() {

            if (isset($this->get['key'])) {

                /** @var \SORApp\Models\Booking\SelectedTransfersFactory $selectedTransfersFactory */
                $selectedTransfersFactory = AbstractBookingFactory::getFactory('SelectedTransfers');

                $selectedTransfersFactory->db->transaction();
                try {

                    $entity = $selectedTransfersFactory->loadByID($this->get['key']);
                    if ($entity) {

                        $reservation = $entity->getRelation('Reservation');
                        $reservation->subCostAdditional($entity->cost, $entity->currency);
                        $reservation->getFactory()->saveEntity($reservation);

                        $selectedTransfersFactory->deleteEntity($entity);
                    }
                    $selectedTransfersFactory->db->commit();
                }
                catch (\Exception $e) {

                    $selectedTransfersFactory->db->rollback();
                    throw $e;
                }
            }
            $this->redirect($this->getCurrentStepUrl());
        }
    }

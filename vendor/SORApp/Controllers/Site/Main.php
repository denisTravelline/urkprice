<?php

    /**
     * Description of Main
     *
     * @author Alexander Olkhovoy <olkhovoy@gmail.com>
     */

    namespace SORApp\Controllers\Site;

    use SORApp\Components\Exceptions;
    use SORApp\Controllers\BookingController;

    class Main extends BookingController {

        public function indexAction() {

            return [];
        }
    }

<?php

namespace SORApp\Controllers\Site;

use SORApp\Components\App;
use SORApp\Components\Exceptions;
use SORApp\Controllers\BookingController;
use SORApp\Models\Booking\AbstractBookingFactory;

/**
 * Class Search
 *
 * @package SORApp\Controllers\Site
 * @see SORApp\Controllers\BookingController
 */
class Hotelrooms extends BookingController {

    /**
     * @return \SORApp\Components\Response
     * @throws \SORApp\Components\Exceptions\HttpException
     */
    public function indexAction()
    {

        $searchRequestFactory = AbstractBookingFactory::getFactory('SearchRequest');

        // Новый поиск номера
        if(isset($this->post['search'])) {

            $searchRequest = isset($this->post['actualize']) ? $this->getEntity($searchRequestFactory) : $searchRequestFactory->createEntity();

            $search          = $this->post['search'];
            $search['rooms'] = array_slice($search['rooms'], 0, $this->post['roomsCount']);
            $searchRequest->autoSetAttributes($search);

            if(!$searchRequest->validate() || !$searchRequest->execute() || !$searchRequestFactory->saveEntity($searchRequest)) {

                $searchRequest->attachResponseError($this->response);
                throw new Exceptions\HttpException(400);
            }
            // Устанавливаем контекст бронирования
            $this->setHashKey($searchRequest->hashKey);
            //$this->redirect($this->getCurrentStepUrl());
        }
        $searchRequest = $this->getEntity($searchRequestFactory);

        $reservationFactory = AbstractBookingFactory::getFactory('Reservation');
        $reservation        = $this->getEntity($reservationFactory);

        $reservation->addRelation($searchRequest);

        // Если выбраны типы номеров
        if($searchRequest && isset($this->post['selectedRoom'])) {

            $reservation->selectedRooms = $this->post['selectedRoom'];
            if(!$reservation->validate() || !$reservation->calculateCostServices() || !$reservationFactory->saveEntity($reservation)) {

                $reservation->attachResponseError($this->response);
                throw new Exceptions\HttpException(400);
            }
            $this->redirect($this->getNextStepUrl());
        }

        if(isset($this->post['reservation'])) {

            $reservation->autoSetAttributes($this->post['reservation']);

            if($reservationFactory->saveEntity($reservation)) {
                $this->redirect($this->getNextStepUrl());
            }

            $reservation->attachResponseError($this->response);
            throw new Exceptions\HttpException(400);
        }

        if($reservation && empty($reservation->customer)) {
            $reservation->setCustomerDefault();
        }

        // Если ничего не найдено
        if(!$searchRequest || !count($searchRequest->foundRooms['roomsGroups'])) {
            throw new Exceptions\HttpException(404, App::_('search', 'Rooms_Not_Found'));
        }

        //App::Log('search', json_encode($searchsearchRequest, JSON_UNESCAPED_UNICODE));
        //$this->response->addResponseElement('title', App::_('search', 'step', [$this->_currentStep]));
//            $this->response->addResponseElement('content', $this->templateManager->render('searchResult', [
//                    'searchRequest' => $searchRequest,
//                    'reservation' => $reservation,
//                    'countries' => (new Country())->getList()]));
        //print_r($searchRequest);
        //print_r($searchRequest->hashKey);
//        print_r($searchRequest->getRoomsInfo());
//        print_r($searchRequest->foundRooms['roomsGroups'][0]);
//        exit;

        $result = [];
        $lang   = filter_input(INPUT_POST, "lang", FILTER_SANITIZE_STRING);
        $rooms  = $searchRequest->foundRooms['roomsGroups'][0];
        
        foreach($searchRequest->getRoomsInfo() as $room) {
            // Name
            if(!empty($room['translates'][$lang]['name'])) {
                $name = $room['translates'][$lang]['name'];
            } else {
                $name = $room['name'];
            }

            // Description
            if(!empty($room['translates'][$lang]['descr'])) {
                $descr = $room['translates'][$lang]['descr'];
            } else {
                $descr = '';
            }

            $result[] = [
                'id' => $room['servioId'],
                'room_id' => $room['id'],
                'name' => $name,
                'beds' => $room['beds'],
                'image' => '/booking/uploads/rooms/room' . $room['servioId']
                . '/thumb_' . $room['photo'] . '.jpg',
                'price' => $rooms[$room['servioId']]['prices']['PriceTotal'],
                'descr' => $descr,
                'hashKey' => $searchRequest->hashKey,
            ];
        }

        // Prepare array for sorting
        foreach($result as $key => $row) {
            $id[$key]      = $row['id'];
            $name[$key]    = $row['name'];
            $beds[$key]    = $row['beds'];
            $image[$key]   = $row['image'];
            $price[$key]   = $row['price'];
            $descr[$key]   = $row['descr'];
            $hashKey[$key] = $row['hashKey'];
        }

        // Sorting array
        switch ($this->post['sort']) {
            
            case 'beds-asc':
                array_multisort($beds, SORT_ASC, $id, SORT_ASC, $result);
                break;
            
            case 'beds-desc':
                array_multisort($beds, SORT_DESC, $id, SORT_ASC, $result);
                break;
            
            case 'price-desc':
                array_multisort($price, SORT_DESC, $id, SORT_ASC, $result);
                break;
                
            case 'price-asc':
            default:
                array_multisort($price, SORT_ASC, $id, SORT_ASC, $result);
                break;
        }

        return $result;
    }

}

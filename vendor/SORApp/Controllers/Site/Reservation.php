<?php

    namespace SORApp\Controllers\Site;

    use SORApp\Components\App;
    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Controllers\BookingController;
    use SORApp\Models\Booking\AbstractBookingFactory;
    use SORApp\Models\Booking\EmailDocumentsQueue;
    use SORApp\Models\Country;

    class Reservation extends BookingController {

        /** @var \SORApp\Models\Booking\AbstractBookingFactory $this ->reservation */
        protected $reservationFactory;

        /** @var \SORApp\Models\Booking\Reservation $this ->reservation */
        protected $reservation;

        public function __construct() {

            parent::__construct();

            $this->reservationFactory = AbstractBookingFactory::getFactory('Reservation');
            $this->reservation = $this->getEntity($this->reservationFactory);
        }

        public function indexAction() {

            if (isset($this->post['reservation'])) {

                $this->reservation->autoSetAttributes($this->post['reservation']);

                if ($this->reservationFactory->saveEntity($this->reservation)) {

                    $this->reservation->attachResponseError($this->response);
                    throw new HttpException(400);
                }
                $this->redirect($this->getNextStepUrl());
            }

            if (empty($this->reservation->customer))
                $this->reservation->setCustomerDefault();

            $this->response->addResponseElement('title', App::_('customer', 'step', [$this->_currentStep]));
            $this->response->addResponseElement('content', $this->templateManager->render('customerForm', [
                'reservation' => $this->reservation,
                'countries' => (new Country())->getList()]));

            return $this->response;
        }

        public function confirmAction() {

            if (empty($this->reservation->customer) || !$this->reservation->validate())
                $this->redirect($this->getPrevStepUrl());
            if (!$this->reservation->isAllowedChange())
                $this->redirect($this->getNextStepUrl());

            if (isset($this->post['paymentType'])) {

                $searchRequestFactory = AbstractBookingFactory::getFactory('SearchRequest');
                $searchRequest = $this->getEntity($searchRequestFactory);

                $searchRequest->paymentType = $this->post['paymentType'];
                if (!$searchRequestFactory->saveEntity($searchRequest)) {

                    // DATABASE ERROR
                    $searchRequest->attachResponseError($this->response);
                    throw new HttpException(400);
                }
            }

            if (isset($this->post['confirm']) && $this->post['confirm'] === '1') {

                if (!$this->reservation->doReservation()) {

                    // SERVIO ERROR
                    $this->reservation->attachResponseError($this->response);
                    throw new HttpException(400);
                }

                // SERVIO SUCCESS
                $this->sendEmails();
                $this->redirect($this->getNextStepUrl());
            }

            // RENDER CONFIRM FORM
            $this->response->addResponseElement('title', App::_('confirm', 'step', [$this->_currentStep]));
            $this->response->addResponseElement('content', $this->templateManager->render('confirmForm', ['reservation' => $this->reservation]));

            return $this->response;
        }

        protected function sendEmails() {

            if (empty($this->reservation->account)) {
                throw new \ErrorException('Cannot send email notification before create reservation');
            }

            $rooms = count($this->reservation->selectedRooms);
            if ($rooms == 0)
                return;

            /** @var \SORApp\Components\Mailer $mailer */
            $mailer = $this->getComponent('mailer');

            /** @var \SORApp\Models\Booking\SearchRequest $searchRequest */
            $searchRequest = $this->reservation->getRelation('SearchRequest');

            // Если бронь на компанию, то получить id додкументов
            $message_template = 'reservation_guest';
            $companyInfo = $searchRequest->getCompanyInfo();
            if (!!$companyInfo && !!$companyInfo['CompanyID']) {

                // Подтверждение бронирования
                try {
                    $confirm_id = $rooms ==
                                  1 ? $searchRequest->servioConnection->getAccountConfirm($this->reservation->account, $this->owner->lang) : $searchRequest->servioConnection->getGroupAccountConfirm($this->reservation->account, $this->owner->lang);
                    // Счёт на оплату
                    try {
                        $bill_id = $rooms ==
                                   1 ? $searchRequest->servioConnection->getAccountBill($this->reservation->account, $this->owner->lang) : $searchRequest->servioConnection->getGroupAccountBill($this->reservation->account, $this->owner->lang);
                    }
                    catch (\Exception $e) {
                        $bill_id = false;
                    }
                }
                catch (\Exception $e) {
                    $confirm_id = false;
                    $bill_id = false;
                }

                // Подготовить письмо, которое будет отправлено когда будут готовы документы
                if ($bill_id || $confirm_id) {

                    $model = new EmailDocumentsQueue();

                    $model->setLang($this->owner->lang);
                    $model->setAccount($this->reservation->account);
                    $model->setSubject($this->templateManager->renderBlock('subject', '@email/reservation_documents', ['reservation' => $this->reservation]));
                    $model->setMessage($this->templateManager->renderBlock('message', '@email/reservation_documents', ['reservation' => $this->reservation]));

                    if ($bill_id)
                        $model->setDocument($bill_id);

                    if ($confirm_id)
                        $model->setDocument($confirm_id);

                    $model->setHotel_id($searchRequest->hotelId);
                    $model->setRecipient($this->reservation->customer['email']);
                    $model->save();

                    unset($model);
                    $message_template = 'reservation_company';
                }
            }

            // Отправка письма c информацией о бронировании
            $message = $mailer->createMessage($this->templateManager->renderBlock('subject', '@email/' .
                                                                                             $message_template, ['reservation' => $this->reservation]), $this->templateManager->renderBlock('message', '@email/' .
                                                                                                                                                                                                       $message_template, ['reservation' => $this->reservation]));

            $message->setTo($this->reservation->customer['email'], $this->reservation->getCustomerFullName());
            if ($searchRequest->hotelInfo['manager_emails']) {
                $bcc = $searchRequest->hotelInfo['manager_emails'];
                $message->setBcc(is_array($bcc) ? $bcc : explode(',', $bcc));
            }

            //$failed = [];
            try {

                $mailer->send($message); //, $failed);
                App::Log('email', 'Message for #' . $this->reservation->account . ' <' . $this->reservation->customer['email'] . '> SENT');
            }
            catch (\Exception $e) {

                App::Log('email', 'Message for #' . $this->reservation->account . ' <' . $this->reservation->customer['email'] . '> FAILURE: ' . $e->getMessage());
            }

            //return true;//!in_array($this->reservation->customer['email'], $failed);
        }

        public function statusAction() {

            if (empty($this->reservation->account))
                $this->redirect($this->getPrevStepUrl());

            $this->response->addResponseElement('title', App::_('status', 'statusTitle'));
            $this->response->addResponseElement('content', App::_('status', 'statusCompleted_title') .
                                                           App::_('status', 'statusCompleted_body', [$this->reservation->account])); // $this->templateManager->render('statusCompleted', ['reservation' => $this->reservation]));

            return $this->response;
        }
    }

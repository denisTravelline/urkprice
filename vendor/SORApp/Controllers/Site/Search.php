<?php

namespace SORApp\Controllers\Site;

use SORApp\Components\App;
use SORApp\Components\Exceptions;
use SORApp\Controllers\BookingController;
use SORApp\Models\Booking\AbstractBookingFactory;
use SORApp\Models\Country;

/**
 * Class Search
 *
 * @package SORApp\Controllers\Site
 * @see SORApp\Controllers\BookingController
 */
class Search extends BookingController {

    /**
     * @return \SORApp\Components\Response
     * @throws \SORApp\Components\Exceptions\HttpException
     */
    public function indexAction()
    {

        $searchRequestFactory = AbstractBookingFactory::getFactory('SearchRequest');

        // Новый поиск номера
        if(isset($this->post['search'])) {

            $searchRequest = isset($this->post['actualize']) ? $this->getEntity($searchRequestFactory) : $searchRequestFactory->createEntity();

            $search          = $this->post['search'];
            $search['rooms'] = array_slice($search['rooms'], 0, $this->post['roomsCount']);
            $searchRequest->autoSetAttributes($search);

            if(!$searchRequest->validate() || !$searchRequest->execute() || !$searchRequestFactory->saveEntity($searchRequest)) {

                $searchRequest->attachResponseError($this->response);
                throw new Exceptions\HttpException(400);
            }
            // Устанавливаем контекст бронирования
            $this->setHashKey($searchRequest->hashKey);
            $this->redirect($this->getCurrentStepUrl());
        }
        $searchRequest = $this->getEntity($searchRequestFactory);

        $reservationFactory = AbstractBookingFactory::getFactory('Reservation');
        $reservation        = $this->getEntity($reservationFactory);

        $reservation->addRelation($searchRequest);


        // Если выбраны типы номеров
        if($searchRequest && isset($this->post['selectedRoom'])) {

            $reservation->selectedRooms = $this->post['selectedRoom'];
            if(!$reservation->validate() || !$reservation->calculateCostServices() || !$reservationFactory->saveEntity($reservation)) {

                $reservation->attachResponseError($this->response);
                throw new Exceptions\HttpException(400);
            }

            $this->redirect($this->getNextStepUrl());
        }

        if(isset($this->post['reservation'])) {

            $reservation->autoSetAttributes($this->post['reservation']);

            if($reservationFactory->saveEntity($reservation)) {
                $this->redirect($this->getNextStepUrl());
            }

            $reservation->attachResponseError($this->response);
            throw new Exceptions\HttpException(400);
        }

        if($reservation && empty($reservation->customer)) {
            $reservation->setCustomerDefault();
        }

        // Если ничего не найдено
        if(!$searchRequest || !count($searchRequest->foundRooms['roomsGroups'])) {
            throw new Exceptions\HttpException(404, App::_('search', 'Rooms_Not_Found'));
        }
        
        // Lang
        if(!empty($this->get['lang'])) {
            $lang = $this->get['lang'];
        } elseif(!empty($this->post['lang'])) {
            $lang = $this->post['lang'];
        } else {
            $lang = 'uk';
        }
        
        // Get templates
        if($curl = curl_init()) {
            $url = 'http://' . $_SERVER['SERVER_NAME']
            . '/site';
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            
            curl_setopt($curl, CURLOPT_URL, $url . '/header/'. $lang);
            $header = curl_exec($curl);
            
            curl_setopt($curl, CURLOPT_URL, $url . '/footer/'. $lang);
            $footer = curl_exec($curl);
            
            curl_close($curl);
        }
        
        $this->response->addResponseElement('header', $header);
        $this->response->addResponseElement('footer', $footer);

        //App::Log('search', json_encode($searchsearchRequest, JSON_UNESCAPED_UNICODE));

        $this->response->addResponseElement('title', App::_('search', 'step', [$this->_currentStep]));
        $this->response->addResponseElement('content', $this->templateManager->render('searchResult', [
                'searchRequest' => $searchRequest,
                'reservation' => $reservation,
                'countries' => (new Country())->getList()]));

        return $this->response;
    }

}

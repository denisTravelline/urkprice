<?php

namespace SORApp\Controllers\Site;

use SORApp\Components\App;
use SORApp\Controllers\BookingController;
use SORApp\Models\Booking\EmailDocumentsQueue;
use SORApp\Components\Exceptions\HttpException;
use SORApp\Models\Booking\AbstractBookingFactory;

class Confirm extends BookingController {

    /** @var \SORApp\Models\Booking\AbstractBookingFactory $this ->reservation */
    protected $reservationFactory;

    /** @var \SORApp\Models\Booking\Reservation $this ->reservation */
    protected $reservation;

    /**
     * @return \SORApp\Components\Response
     * @throws HttpException
     * @throws \ErrorException
     * @throws \SORApp\Services\Validator\ValidatorException
     */
    public function indexAction()
    {

        $this->reservationFactory = AbstractBookingFactory::getFactory('Reservation');
        $this->reservation        = $this->getEntity($this->reservationFactory);

        if(empty($this->reservation->customer) || !$this->reservation->validate())
            $this->redirect($this->getPrevStepUrl());

        if(!$this->reservation->isAllowedChange()) {
            $this->redirect($this->getNextStepUrl());

            //$this->reservation->attachResponseError($this->response);
            //throw new HttpException(400);
        }

        if(isset($this->post['paymentType'])) {

            $searchRequestFactory = AbstractBookingFactory::getFactory('SearchRequest');
            $searchRequest        = $this->getEntity($searchRequestFactory);

            $searchRequest->paymentType = $this->post['paymentType'];
            if(!$searchRequestFactory->saveEntity($searchRequest)) {

                // DATABASE ERROR
                $searchRequest->attachResponseError($this->response);
                throw new HttpException(400);
            }
        }

        if(isset($this->post['confirm']) && $this->post['confirm'] === '1') {

            if(!$this->reservation->doReservation()) {

                // SERVIO ERROR
                $this->reservation->attachResponseError($this->response);
                throw new HttpException(400);
            }

            // SERVIO SUCCESS
            $this->sendEmails();
            $this->redirect($this->getNextStepUrl());
        }

        // RENDER CONFIRM FORM
        $this->response->addResponseElement('title', App::_('confirm', 'step', [$this->_currentStep]));
        $this->response->addResponseElement('content', $this->templateManager->render('confirmForm', ['reservation' => $this->reservation]));

        return $this->response;
    }

    /**
     * @throws \ErrorException
     */
    protected function sendEmails()
    {

        if(empty($this->reservation->account)) {
            throw new \ErrorException('Cannot send email notification before create reservation');
        }

        $rooms = count($this->reservation->selectedRooms);
        if($rooms == 0)
            return;

        /** @var \SORApp\Components\Mailer $mailer */
        $mailer = $this->getComponent('mailer');

        /** @var \SORApp\Models\Booking\SearchRequest $searchRequest */
        $searchRequest = $this->reservation->getRelation('SearchRequest');

        // Если бронь на компанию, то получить id додкументов
        $message_template = 'reservation_guest';
        $companyInfo      = $searchRequest->getCompanyInfo();

        if(!!$companyInfo && !!$companyInfo['CompanyID']) {

            try {

                // Подтверждение бронирования
                $confirm_id = $rooms == 1 ? $searchRequest->servioConnection->getAccountConfirm($this->reservation->account, $this->owner->lang) : $searchRequest->servioConnection->getGroupAccountConfirm($this->reservation->account, $this->owner->lang);

                // Счёт на оплату
                try {
                    $bill_id = $rooms == 1 ? $searchRequest->servioConnection->getAccountBill($this->reservation->account, $this->owner->lang) : $searchRequest->servioConnection->getGroupAccountBill($this->reservation->account, $this->owner->lang);
                } catch (\Exception $e) {
                    $bill_id = false;
                }
            } catch (\Exception $e) {
                $bill_id    = false;
                $confirm_id = false;
            }

            // Подготовить письмо, которое будет отправлено когда будут готовы документы
            if($bill_id || $confirm_id) {

                $model = new EmailDocumentsQueue();

                $model->setLang($this->owner->lang);
                $model->setAccount($this->reservation->account);
                $model->setSubject($this->templateManager->renderBlock('subject', '@email/reservation_documents', ['reservation' => $this->reservation]));
                $model->setMessage($this->templateManager->renderBlock('message', '@email/reservation_documents', ['reservation' => $this->reservation]));

                if($bill_id)
                    $model->setDocument($bill_id);

                if($confirm_id)
                    $model->setDocument($confirm_id);

                $model->setHotel_id($searchRequest->hotelId);
                $model->setRecipient($this->reservation->customer['email']);
                $model->save();

                unset($model);
                $message_template = 'reservation_company';
            }
        }
        // Отправка письма c информацией о бронировании
        $message = $mailer->createMessage(
            $this->templateManager->renderBlock('subject', '@email/' . $message_template, ['reservation' => $this->reservation]), $this->templateManager->renderBlock('message', '@email/' . $message_template, ['reservation' => $this->reservation])
        );


        //$message->setTo($this->reservation->customer['email'], $this->reservation->getCustomerFullName());
        //$message->setTo([$this->reservation->customer['email'] => $this->reservation->getCustomerFullName()]);
        $message->setTo($this->reservation->customer['email']);

        if($searchRequest->hotelInfo['manager_emails']) {
            $bcc = $searchRequest->hotelInfo['manager_emails'];
            $message->setBcc(is_array($bcc) ? $bcc : explode(',', $bcc));
        }

        //$failed = [];
        try {
            $mailer->send($message); //, $failed);
        } catch (\Exception $e) {
            
        }


        //return true;//!in_array($this->reservation->customer['email'], $failed);
    }

}

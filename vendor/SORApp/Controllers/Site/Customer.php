<?php

/**
 * Created by PhpStorm.
 * User: alder_000
 * Date: 2015-08-22
 * Time: 08:30
 */

namespace SORApp\Controllers\Site;

use SORApp\Components\App;
use SORApp\Components\Exceptions\HttpException;
use SORApp\Controllers\BookingController;
use SORApp\Models\Booking\AbstractBookingFactory;
use SORApp\Models\Country;

class Customer extends BookingController {

    /** @var \SORApp\Models\Booking\AbstractBookingFactory $this ->reservation */
    protected $reservationFactory;

    /** @var \SORApp\Models\Booking\Reservation $this ->reservation */
    protected $reservation;

    public function indexAction()
    {

        $this->reservationFactory = AbstractBookingFactory::getFactory('Reservation');
        $this->reservation        = $this->getEntity($this->reservationFactory);

        if(isset($this->post['reservation'])) {

            $this->reservation->autoSetAttributes($this->post['reservation']);
            if(!$this->reservationFactory->saveEntity($this->reservation)) {

                $this->reservation->attachResponseError($this->response);
                throw new HttpException(400);
            }
            $this->redirect($this->getNextStepUrl());
        }
        if(empty($this->reservation->customer))
            $this->reservation->setCustomerDefault();

        // Lang
        if(!empty($this->get['lang'])) {
            $lang = $this->get['lang'];
        } elseif(!empty($this->post['lang'])) {
            $lang = $this->post['lang'];
        } else {
            $lang = 'uk';
        }

        // Get templates
        if($curl = curl_init()) {
            $url = 'http://' . $_SERVER['SERVER_NAME']
                . '/site';
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($curl, CURLOPT_URL, $url . '/header/' . $lang);
            $header = curl_exec($curl);

            curl_setopt($curl, CURLOPT_URL, $url . '/footer/' . $lang);
            $footer = curl_exec($curl);

            curl_close($curl);
        }
        $this->response->addResponseElement('title', App::_('customer', 'step', [$this->_currentStep]));
        $this->response->addResponseElement('header', $header);
        $this->response->addResponseElement('footer', $footer);
        $this->response->addResponseElement('content', $this->templateManager->render('customerForm', [
                'reservation' => $this->reservation,
                'countries' => (new Country())->getList()]));
        return $this->response;
    }

}

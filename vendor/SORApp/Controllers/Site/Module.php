<?php

    /**
     * Description of Main
     *
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Controllers\Site;

    use SORApp\Controllers\BookingController;

    class Module extends BookingController {

        public function indexAction() {

            /* $searchFormSite = new \SORApp\Widgets\SearchFormSite(null);
             echo $searchFormSite->run();
            */ //echo $this->getComponent('templateManager')->render('_widgets/SearchFormSite', []);
            return [];
        }
    }

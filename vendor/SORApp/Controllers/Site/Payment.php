<?php
    /**
     * Created by PhpStorm.
     * User: Andrej
     * Date: 29.09.14
     * Time: 11:03
     */

    namespace SORApp\Controllers\Site;

    use SORApp\Components\App;
    use SORApp\Components\Exceptions\HttpException;
    use SORApp\Controllers\BookingController;
    use SORApp\Models\Booking\AbstractBookingFactory;
    use SORApp\Models\Booking\PaymentOrder;
    use SORApp\Models\Booking\SearchRequest;
    use SORApp\Services\Payment\PaymentException;

    class Payment extends BookingController {

        public function indexAction() {

            $reservationFactory = AbstractBookingFactory::getFactory('Reservation');

            /** @var \SORApp\Models\Booking\Reservation $reservation */
            $reservation = $this->getEntity($reservationFactory);

            if (empty($reservation->account)) {
                $this->redirect($this->getPrevStepUrl());
            }

            /** @var SearchRequest $searchRequest */
            $searchRequest = $reservation->getRelation('SearchRequest');
            if (!$searchRequest OR $searchRequest->paymentType != SearchRequest::PAYMENT_TYPE_ONLINE) {
                $this->skipStep();
            }

            /** @var \SORApp\Services\Payment\PaymentFactory $paymentFactory */
            $paymentFactory = $payment = $this->getComponent('payment');
            $paymentProvider = $paymentFactory->createPayment();

            $paymentOrder = $reservation->getRelation('paymentOrder');
            if ($paymentOrder) {

                if (!empty($this->post)) {

                    throw new HttpException(403, 'Access error');
                }
                else {

                    $this->skipStep();
                }
            }

            $servicesCosts = $reservation->getCostServicesByDays();
            reset($servicesCosts);

            if (!empty($this->post)) {

                try {

                    $response = $paymentProvider->clientPostHandler($this->post);
                    if (!isset($response['currency']) || !isset($response['amount']) || !isset($response['order']) ||
                        empty($response['order']) || $response['order'] != $reservation->account ||
                        $response['currency'] != $reservation->currency
                    ) {
                        throw new HttpException(403, 'Access error');
                    }

                    $paymentOrderFactory = AbstractBookingFactory::getFactory('paymentOrder');
                    if ($response['amount'] == bcadd($reservation->costAdditional, $reservation->costServices, 2)) {

                        $paymentOrder = $paymentOrderFactory->createEntity([
                            'type' => PaymentOrder::TYPE_FULL,
                            'amount' => $response['amount'],
                            'currency' => $reservation->currency,
                            'account' => $reservation->account,
                            'description' => 'Order ' . $reservation->account . ': full payment',
                            'providerName' => $paymentProvider->getProviderName(),
                            'status' => '1',
                            'result' => json_encode($this->post, JSON_UNESCAPED_UNICODE)]);
                    }
                    else {
                        if ($response['amount'] == current($servicesCosts)) {

                            $paymentOrder = $paymentOrderFactory->createEntity([
                                'type' => PaymentOrder::TYPE_FIRST_DAY,
                                'amount' => $response['amount'],
                                'currency' => $reservation->currency,
                                'account' => $reservation->account,
                                'description' => 'Order ' . $reservation->account . ': first day',
                                'providerName' => $paymentProvider->getProviderName(),
                                'status' => '1',
                                'result' => json_encode($this->post, JSON_UNESCAPED_UNICODE)]);
                        }
                        else {
                            throw new HttpException(403, 'Access error');
                        }
                    }

                    $paymentOrder->addRelation($reservation);
                    $paymentOrder->addRelation($searchRequest);
                    $paymentOrder->addRelation($reservation);

                    if ($paymentProvider->sendConfirmRequest($this->post, '')) {
                        if (!$paymentOrderFactory->saveEntity($paymentOrder)) {
                            throw new HttpException(500, 'Error saving payment order');
                        }
                        if (headers_sent()) {
                            die;
                        }
                        $this->redirect($this->getNextStepUrl());
                    }
                }
                catch (PaymentException $e) {
                    if (headers_sent()) {
                        die;
                    }
                    $this->response->addError(new HttpException(200, $e->getMessage(), $e->getCode(), $e));
                }
                catch (HttpException $e) {
                    if (headers_sent()) {
                        die;
                    }
                    $this->response->addError($e);
                }
            }

            $orderFull = $paymentProvider->createOrder(bcadd($reservation->costAdditional, $reservation->costServices, 2), $reservation->currency, $reservation->account, App::_('payment', 'fullAmount') .
                                                                                                                                                                          ' [#' .
                                                                                                                                                                          $reservation->account .
                                                                                                                                                                          ']', $this->getCurrentStepUrl(), $reservation->customer);

            if (current($servicesCosts) != bcadd($reservation->costAdditional, $reservation->costServices, 2)) {

                $orderFirstDay = $paymentProvider->createOrder(current($servicesCosts), $reservation->currency, $reservation->account, App::_('payment', 'firstDayAmount') .
                                                                                                                                       ' [#' .
                                                                                                                                       $reservation->account .
                                                                                                                                       ']', $this->getCurrentStepUrl(), $reservation->customer);
            }

            $this->response->addResponseElement('title', App::_('payment', 'step', [$this->_currentStep]));
            $this->response->addResponseElement('content', $this->templateManager->render('paymentForm', [
                'firstDayButton' => (isset($orderFirstDay) ? $paymentProvider->renderPaymentButton(App::_('payment', 'paymentButton'), $orderFirstDay) : null),
                'firstDayOrder' => (isset($orderFirstDay) ? $orderFirstDay : null),
                'fullButton' => (isset($orderFull) ? $paymentProvider->renderPaymentButton(App::_('payment', 'paymentButton'), $orderFull) : null),
                'fullOrder' => (isset($orderFull) ? $orderFull : null)]));

            return $this->response;
        }
    }

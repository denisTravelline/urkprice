<?php

namespace SORApp\Controllers\Site;

use SORApp\Components\App;
use SORApp\Components\Exceptions\HttpException;
use SORApp\Controllers\BookingController;
use SORApp\Models\Booking\AbstractBookingFactory;
use SORApp\Models\Booking\EmailDocumentsQueue;
use SORApp\Models\Country;

class Status extends BookingController {

    /** @var \SORApp\Models\Booking\AbstractBookingFactory $this ->reservation */
    protected $reservationFactory;

    /** @var \SORApp\Models\Booking\Reservation $this ->reservation */
    protected $reservation;

    public function indexAction()
    {

        $this->reservationFactory = AbstractBookingFactory::getFactory('Reservation');
        $this->reservation        = $this->getEntity($this->reservationFactory);

        if(empty($this->reservation->account))
            $this->redirect($this->getPrevStepUrl());

        // Lang
        if(!empty($this->get['lang'])) {
            $lang = $this->get['lang'];
        } elseif(!empty($this->post['lang'])) {
            $lang = $this->post['lang'];
        } else {
            $lang = 'uk';
        }

        // Get templates
        if($curl = curl_init()) {
            $url = 'http://' . $_SERVER['SERVER_NAME']
                . '/site';
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            curl_setopt($curl, CURLOPT_URL, $url . '/header/' . $lang);
            $header = curl_exec($curl);

            curl_setopt($curl, CURLOPT_URL, $url . '/footer/' . $lang);
            $footer = curl_exec($curl);

            curl_close($curl);
        }

        $this->response->addResponseElement('header', $header);
        $this->response->addResponseElement('footer', $footer);

        $this->response->addResponseElement('title', App::_('status', 'statusTitle'));
        $this->response->addResponseElement('content', App::_('status', 'statusCompleted_title') .
            App::_('status', 'statusCompleted_body', [$this->reservation->account])); // $this->templateManager->render('statusCompleted', ['reservation' => $this->reservation]));

        return $this->response;
    }

}

<?php

    return [

        'router' => [

            'className' => '\\SORApp\\Components\\CliRouter',

            'acceptTypes' => ['text/plain'],

            'modules' => [
                'cli' => [
                    'defaultController' => 'main',
                    'defaultAction' => 'worker',
                    'acceptDefault' => 'text/plain'],],

            'defaultModule' => 'cli',],

        'response' => [

            'className' => '\\SORApp\\Components\\CliResponse',],];

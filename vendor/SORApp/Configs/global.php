<?php

return [

    // Режим отладки
    'debug' => false,

    // Временная зона сервера
    'defaultTimezone' => 'Europe/Kiev',

    // Язык сайта по-умолчанию [string, default='ru']
    'defaultLanguage' => 'ru',

    // Доступные языки перевода
    'availableLanguages' => ['ru','uk','en'], // ,'ua','en'),

    'versions' => [
        'module' => '1.9.0',
        'build' => '2015-02-10',
        'protocol' => ['1.8', '1.9'],
    ],

    // Сохранять все запросы в лог-файл [int|bool, default=false]
    'loggingEnabled' => true,

    // Директория для сохранения лог-файлов отладчика [string, default='App/runtime/logs']
    //'loggingDir' => SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'logs',
    'loggingDir' => '/var/www/ukrtour.pro/frontend/runtime/servio/logs',
];

<?php

return [
    // Системные настройки ядра приложения
    'global' => [
        // Режим отладки
        'debug' => true,

        // Язык сайта по-умолчанию [string, default='ru']
        'defaultLanguage' => 'uk',

        // Параметры формы поиска номеров
        'booking' => [

            'defaultHotelId' => 1,    // выбранный отель по умолчанию
            'defaultTouristTax' => true,  // состояние чекбокса турсбора по умолчанию
            'sortingByPrice' => false, // сортировать ли номера по цене или выдавать как есть
            'childAge' => 5,          // дети до childAge лет проживают бесплатно
            'visible' => [             // настройка отображения отдельных элементов

                'hotelId' => true,    // показывать ли выбор отеля
                'companyCode' => false, // показывать ли поле ввода кода компании
                'loyaltyCard' => false, // показывать ли поле ввода номера карты постоянного клиента
                'touristTax' => true, // показывать ли чекбокс турсбора
            ],

        ],
    ],

    // Настройки бронирования
    'booking' => [
        // Сортировать список доступных номеров по цене
        // (в данном случае недоступные номера будут в самом низу списка)
        'sortingByPrice' => true,
        'defaultCompany' => [
            'ID' => 5,
            'CodeID' => 1,
            'Code' => 'TD3X9A4HPL',
            'Name' => 'ПрАТ «Укрпрофтур»',
            ],        
    ],

    // reCAPTCHA keys
    'captcha' => [
        'publicKey' => '6LfMYBIUAAAAAGEvdkEpm9DTSkAyxv707MQxEgyQ',
        'secretKey' => '6LfMYBIUAAAAAGgcUAkiRrrr0AYLjYkcEaXcVhEQ',
        ],      

    // Компоненты приложения
    'components' => [
        'router' => [
            // Доменное имя сайта, без указания протокла (http/https) [string, default='reikartz.com']
            'host' => 'www.ukrtour.pro',
            // Адрес модуля бронирования относительно корня сайта [string, default='/booking']
            //'baseUri' => '/{lang}/pms19',
			'baseUri' => '/booking',
            // Адрес по которому доступен каталог static для подключения css и js файлов [string, default='/booking']
            'staticUri' => '/booking',
        ],

        'database' => [
            // Строка uri для подключения к базе данных в формате provider://user:password@host:port/database
            // [string, default='mysqli://root:@127.0.0.1:3306/servio']
            //'dsn' => 'mysqli://localhost:3306/',
            'dsn' => 'mysqli://ukrtour:90H2Ec5i5dsdsfsdlsdmf3nrnr,mewnfsd23M@localhost:3306/ukrtour',

            // Префикс таблиц [string, default='servio_']
            'prefix' => 'servio_',
        ],

        // Настройики компонента отправки почты
        'mailer' => [
            // Тип отправки сообщений
            // [string, default='php']
            // - php - отправка в зависимости от настоек веб-сервера
            'transportType' => 'php',

            // - smtp - отправка напрямую через внешний почтовый сервер по протоколу smtp
           /* 'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'mail.ukrtour.pro',
                'username' => 'reservations',
                'password' => 'safdsf423432c3r32',
                'port' => '995',
                'encryption'=> 'tls',
                'timeout'=> 10,
            ),*/

            // Конфигурацию писем
            'messageConfig' => [
                // Отправлитель
                'from' => ['reservations@ukrtour.pro' => 'Ukrtour'],
                // Header Reply-To
                'reply' => null,
                // Header Return-Path
                'returnPath' => null,
            ],
        ],

        'payment' => [
            'paymentProviderDefault' => 'liqpay',

            'paymentProvidersAvailable' => [
                'liqpay' => [

                        'className' => '\\SORApp\\Services\\Payment\\Providers\\LiqpayPaymentProvider',
                    ],
            ],
        ],

        'servio' => [
            // Профилирование запросов [int|bool, default=false]
            'profilingRequests' => true,

            // Сохранять все запросы в лог-файл [int|bool, default=false]
            'loggingRequests' => true,

            // Параметры подключения к серверу по умолчанию
            'defaultServer' => [
                'url' => 'http://212.26.138.138:8000/ReservationService_1_9', // адрес сервера
                'key' => '/ZbRnMLdrnW/7kg/we6tv0yNZWIsz9kShIjfDMm83GSpOEiRsKdiE3mBjCqdreHFuYgx5Y0z0dLB4TfyYJsTU/vNnnwRnY4ahZyuiWSORPFLOeuH2CD5aFoqkDMcWjo28CG0fAyeqpo0EMXEu1DHVjw26wXZXWWhesLbVCpM0JG2yxUXsZLTWhMQUfLAB+cTW8v3oOyFqVTz9SJTCh2cOeNhzGhjiKSIqJEhhdf+Wj2p+ktMHIkOc0BLZwu2ksiXtOnBM6OgLdoaK9F9z27yUxPGTU7Y8r/WaOj6ygUgmTYP9M9CXueUg5QOiRHuo5c1akhRaxEGF3m/XlnyU38eTEyzCDgtAkFw/feMJDclS/ytiVZxz8jKxVRZitvlOBkaZzvXq8MaWbDI+ysnK6K+d/jATiU9nNZ4f7LqcAQS1NyEG0wUg7uR6V+vvGvc2kwAl/V5zoURe43ya31+X66kKtpMG5xTlstE0xM9zGWVT4H0DGWU2Gg/1QzKeRzCmdwAwAmZWaRaoV3Wmff3AWhAEZgDvRQ',                      // ключ клиента
                'version' => '1.9',               // версия сервера
                'currency' => 'UAH'               // валюта сервера
            ]
        ]
    ]
];

/**
 * ВАЖНО!
 *
 * Данный формат конфигурации позволяет задавать любые публичные свойства создаваемых объектов.
 * Перед тем как дописывать сюда что-либо, убедитесь что вы действительно понимает как это работает.
 *
 * По-умолчанию в данный файл вынесены ностройки, которые нужно и можно менять без нарушения фукнциональности.
 * Полный список используемых настроек можно посмотреть в файле global.php и components.php.
 *
 * При изменении настроек, отличных от тех, что вынесены в данный файл, работоспособность модуля не гарантируется!
 */

<?php

return [
    // Системные настройки ядра приложения
    'global' => [
        // Режим отладки
        'debug' => false,

        // Язык сайта по-умолчанию [string, default='ru']
        'defaultLanguage' => 'ru',

        // Параметры формы поиска номеров
        'booking' => [

            'defaultHotelId' => 25,    // выбранный отель по умолчанию
            'defaultTouristTax' => 0,  // состояние чекбокса турсбора по умолчанию
            'sortingByPrice' => false, // сортировать ли номера по цене или выдавать как есть
            'childAge' => 17,          // дети до childAge лет проживают бесплатно
            'visible' => [             // настройка отображения отдельных элементов

                'hotelId' => false,    // показывать ли выбор отеля
                'companyCode' => true, // показывать ли поле ввода кода компании
                'loyaltyCard' => true, // показывать ли поле ввода номера карты постоянного клиента
                'touristTax' => false, // показывать ли чекбокс турсбора
            ],

        ],
    ],

    // Настройки бронирования
    'booking' => [
        // Сортировать список доступных номеров по цене
        // (в данном случае недоступные номера будут в самом низу списка)
        'sortingByPrice' => true,
    ],

    // Компоненты приложения
    'components' => [
        'router' => [
            // Доменное имя сайта, без указания протокла (http/https) [string, default='reikartz.com']
            'host' => 'domain.com',
            // Адрес модуля бронирования относительно корня сайта [string, default='/booking']
            'baseUri' => '/{lang}/booking',
            // Адрес по которому доступен каталог static для подключения css и js файлов [string, default='/booking']
            'staticUri' => '/booking',
        ],

        'database' => [
            // Строка uri для подключения к базе данных в формате provider://user:password@host:port/database
            // [string, default='mysqli://root:@127.0.0.1:3306/servio']
            'dsn' => 'mysqli://user:password@localhost/database',

            // Префикс таблиц [string, default='servio_']
            'prefix' => 'servio_',
        ],

        // Настройики компонента отправки почты
        'mailer' => [
            // Тип отправки сообщений
            // [string, default='php']
            // - php - отправка в зависимости от настоек веб-сервера
            'transportType' => 'php',

            // - smtp - отправка напрямую через внешний почтовый сервер по протоколу smtp
//            'transportType' => 'smtp',
//            'transportOptions' => array(
//                'host' => '',
//                'username' => '',
//                'password' => '',
//                'port' => '587',
//                'encryption'=> 'tls',
//                'timeout'=> 10,
//            ),

            // Конфигурацию писем
            'messageConfig' => [
                // Отправлитель
                'from' => ['no-reply@reikartz.com' => 'Reikartz Hotel Group'],
                // Header Reply-To
                'reply' => null,
                // Header Return-Path
                'returnPath' => null,
            ],
        ],

        'payment' => [
            'paymentProviderDefault' => 'urkeximbank',

            'paymentProvidersAvailable' => [
                'urkeximbank' => [
                    'processingUrl' => 'https://3ds.eximb.com/cgi-bin/cgi_link',
                    'terminal' => '',
                    'makKey' => '',
                    'merchantID' => '',
                    'merchantName' => '',
                    'merchantEmail' => '',
                    'merchantCountry' => '',
                    'merchantGMT' => '',
                ],
            ],
        ],

        'servio' => [
            // Профилирование запросов [int|bool, default=false]
            'profilingRequests' => true,

            // Сохранять все запросы в лог-файл [int|bool, default=false]
            'loggingRequests' => true,

            // Параметры подключения к серверу по умолчанию
            'defaultServer' => [
                'url' => 'http://host:port/path', // адрес сервера
                'key' => '',                      // ключ клиента
                'version' => '1.9',               // версия сервера
                'currency' => 'UAH'               // валюта сервера
            ]
        ]
    ]
];

/**
 * ВАЖНО!
 *
 * Данный формат конфигурации позволяет задавать любые публичные свойства создаваемых объектов.
 * Перед тем как дописывать сюда что-либо, убедитесь что вы действительно понимает как это работает.
 *
 * По-умолчанию в данный файл вынесены ностройки, которые нужно и можно менять без нарушения фукнциональности.
 * Полный список используемых настроек можно посмотреть в файле global.php и components.php.
 *
 * При изменении настроек, отличных от тех, что вынесены в данный файл, работоспособность модуля не гарантируется!
 */

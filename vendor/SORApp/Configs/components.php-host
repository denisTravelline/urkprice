<?php

return [

    'router' => [

        'className' => '\\SORApp\\Components\\WebRouter',

        // Доменное имя сайта, без указания протокла (http/https) [string, default='reikartz.com']
        'host' => 'ukrtour.pro',

        // Адрес модуля бронирования относительно корня сайта [string, default='/booking']
        'baseUri' => '/booking',

        // Адрес по которому доступен каталог static для подключения css и js файлов [string, default='/booking']
        'staticUri' => '/booking',

        // Массив доступных типов ответа сервера [array, default=['text/html', 'application/json']]
        'acceptTypes' => ['text/html', 'application/json'],

        // Массив обработчиков запросов
        'modules' => [
            'api' => ['defaultController' => 'main', 'defaultAction' => 'index', 'acceptDefault' => 'application/json'],
            'cli' => ['defaultController' => 'main', 'defaultAction' => 'index', 'acceptDefault' => 'text/plain'],
            'reg' => ['defaultController' => 'reg', 'defaultAction' => 'index', 'acceptDefault' => 'text/html'],
            'site' => ['defaultController' => 'main', 'defaultAction' => 'index', 'acceptDefault' => 'text/html'],
            'admin' => ['defaultController' => 'main', 'defaultAction' => 'index', 'acceptDefault' => 'text/html'],
            'module' => ['defaultController' => 'main', 'defaultAction' => 'index', 'acceptDefault' => 'text/html'],
        ],

        // Обработчик запросов по-умолчанию [string, default='site']
        'defaultModule' => 'site',
    ],

    'browser' => [
        'className' => '\\SORApp\\Components\\Browser',
    ],

    'response' => [
        'className' => '\\SORApp\\Components\\Response',
    ],

    'templateManager' => [
        'className' => '\\SORApp\\Components\\TemplateManager',

        // Каталог для файлового кеша шаблонизатора Twig
        // Для данного каталога должы быть установлены права на запись для пользователя от имени которого работает веб-сервер
        //'cache' => SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'compiled',
        //'cache' => realpath('../../frontend/runtime/servio/compiled'),
        'cache' => '/var/www/ukrtour.pro/frontend/runtime/servio/compiled',

    ],

    'database' => [

        'className' => '\\SORApp\\Components\\Database',

        // Строка uri для подключения к базе данных формата provider://user:password@host:port/database
        //'dsn' => 'mysqli://user:password@host:port/database',

        // Префикс таблиц  [string, default='servio_']
        'prefix' => 'servio_'
    ],

    'mailer' => [

        'className' => '\\SORApp\\Components\\Mailer',

        'swiftMailerPath' => SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Services' . DIRECTORY_SEPARATOR . 'SwiftMailer',

        'transportType' => 'smtp',
        'transportOptions' => [
            'host' => 'mail.itib.info',
            'username' => '@',
            'password' => '',
            'port' => '465',
            'encryption'=> 'tls',
            'timeout'=> 10,
        ],

        'messageConfig' => [
            'from' => null,
            'reply' => null,
            'returnPath' => null,
        ],
    ],

    'payment' => [

        'className' => '\\SORApp\\Services\\Payment\\PaymentFactory',

        'paymentProviderDefault' => 'urkeximbank',

        'paymentProvidersAvailable' => [

            'platron' => [

                'className' => '\\SORApp\\Services\\Payment\\Providers\\PlatronPaymentProvider',
                'testingMode' => 0,
                'merchantId' => 0,
                'merchantSecret' => '',
            ],

//            'liqpay' => [
//
//                'className' => '\\SORApp\\Services\\Payment\\Providers\\LiqpayPaymentProvider',
//                'testingMode' => 0,
//                'acqId' => 414963,
//                'merchantId' => 'I0110CX2',
//                'merchantReturnURL' => 'http://hotelmir.kiev.ua/%s/booking/?step=%d&hotel=%d',
//                'merchantPassword' => 'h170i0m15ed72i0',
//                'processingVersion' => '1.0.0',
//                'processingUrl' => 'https://ecommerce.liqpay.com/ecommerce/CheckOutPagen?lang=%s',
//            ],

            'urkeximbank' => [

                'className' => '\\SORApp\\Services\\Payment\\Providers\\UkreximbankPaymentProvider',
                'processingUrl' => 'https://3ds.eximb.com/cgi-bin/cgi_link',
                'terminal' => '',
                'makKey' => '',
                'merchantID' => '',
                'merchantName' => '',
                'merchantEmail' => '',
                'merchantCountry' => '',
                'merchantGMT' => '',
            ],
        ],
    ],

    'formatter' => [
        'className' => '\\SORApp\\Components\\Formatter',

        'currencyFormatDefault' => '%1$01.2f %2$s',
        'currencyRules' => [
            'USD' => '%2$s %1$01.2f',
        ]
    ],

    'user' => [

        'className' => '\\SORApp\\Components\\WebUser',

        'userClass' => '\\SORApp\\Models\\User',
        'accessRoles' => [
            'manager' => null,
            'admin' => ['manager'],
        ]
    ],

    'servio' => [

        'className' => '\\SORApp\\Services\\Servio\\Servio::getConnection',

        // Профилирование запросов [int|bool, default=false]
        'profilingRequests' => true,

        // Директория для сохранения лог-файлов профайлера [string, default='App/runtime/logs']
        //'profilingDir' => SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' .DIRECTORY_SEPARATOR . 'logs',
        //'profilingDir' => realpath('../../frontend/runtime/servio/logs'),
        'profilingDir' => '/var/www/ukrtour.pro/frontend/runtime/servio/logs',

        // Сохранять все запросы в лог-файл [int|bool, default=false]
        'loggingRequests' => true,

        // Директория для сохранения лог-файлов отладчика [string, default='App/runtime/logs']
        //'loggingDir' => SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'logs',
        //'loggingDir' => realpath('../../frontend/runtime/servio/logs'),
        'loggingDir' => '/var/www/ukrtour.pro/frontend/runtime/servio/logs',
    ],

    'ImagesHelper' => [

        'className' => '\\SORApp\Components\ImagesHelper',

        //'uploadDir' => realpath(SOR_APP_DIR . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'uploads'),
        //'uploadDir' => realpath('../../frontend/web/booking/uploads'),
        'uploadDir' => '/var/www/ukrtour.pro/frontend/web/booking/uploads',
        'sizes' => [
            'thumb' => '160x120',
            'middle' => '640x480',
        ],
    ],
];

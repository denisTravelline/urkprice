<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 11.12.14 17:42
     */

    defined('SOR_APP_DIR') OR define('SOR_APP_DIR', realpath(dirname(__FILE__)));
    defined('SOR_APP_ENV') OR define('SOR_APP_ENV', 'web');

    //$sessionsDir = SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'sessions';
    //$sessionsDir = realpath('../../frontend/runtime/servio/sessions');
    $sessionsDir = '/var/www/ukrtour.pro/frontend/runtime/servio/sessions';
    
    if ((is_dir($sessionsDir) && is_writable($sessionsDir)) || !!@mkdir($sessionsDir, 0777, true))
        session_save_path($sessionsDir);

    require_once SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Services' . DIRECTORY_SEPARATOR . 'ClassLoader' . DIRECTORY_SEPARATOR .
                 'Psr4ClassLoader.php';
    $classLoader = new \Symfony\Component\ClassLoader\Psr4ClassLoader(); // Register PSR-4 class loader

    $prefixes = include_once(SOR_APP_DIR . DIRECTORY_SEPARATOR . 'autoload.php');
    foreach ($prefixes as $prefix => $baseDir)
        $classLoader->addPrefix($prefix, $baseDir);

    $classLoader->register();

    $config = new \SORApp\Services\Config\Config(SOR_APP_ENV); // Initialize config component
    $config->registerProvider('FileConfigProvider', ['baseDir' => SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Configs']);

    SORApp\Components\App::getInstance($config);


<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 01.12.14 17:02
     */

    namespace SORApp\Services\Validator;

    class ArrayValidator extends AbstractValidator {

        public function intersect($keys) {

            if (is_string($keys)) {
                $keys = preg_split('/,\s*/', $keys);
            }

            $this->_value = array_intersect_key($this->_value, array_flip($keys));

            return true;
        }

        public function validate($rules) {

            $exception = new ArrayValidatorException();

            foreach ($rules as $attr => $rule) {
                if (isset($rule[0])) {
                    $errorMessage = array_shift($rule);
                }

                if (count($rule)) {
                    foreach ($rule as $validatorName => $validatorOptions) {
                        try {
                            if ($validator = $this->_factory->get($validatorName)) {
                                $this->_value[$attr] = $validator->test((isset($this->_value[$attr]) ? $this->_value[$attr] : null), $validatorOptions);
                            }
                            else {
                                throw new \ErrorException('Undefined validator "' . $validatorName . '"');
                            }
                        }
                        catch (ValidatorException $e) {
                            if (!isset($errorMessage)) {
                                $exception->addError($e->getMessage());
                            }
                            else {
                                $exception->addError($errorMessage);
                            }
                        }
                    }
                }
            }

            if ($exception->hasErrors()) {
                throw $exception;
            }

            return true;
        }
    }

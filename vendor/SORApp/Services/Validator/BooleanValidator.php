<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Validator;

    class BooleanValidator extends AbstractValidator {

        public function convert() {

            $this->_value = (empty($this->_value) || $this->_value == '0' || $this->_value === 'false') ? 0 : 1;
        }
    }

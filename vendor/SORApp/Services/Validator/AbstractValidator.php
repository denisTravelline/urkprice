<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Validator;

    class AbstractValidator {

        protected $_defaultOptions = [];
        protected $_value;

        /** @var \SORApp\Services\Validator\ValidatorFactory */
        protected $_factory;

        public function __construct(ValidatorFactory $factory) {

            $this->_factory = $factory;
        }

        public function test($value, $options = []) {

            $this->_value = (is_scalar($value) ? trim($value) : $value);

            $options = array_merge($this->_defaultOptions, $options);
            if (count($options)) {
                foreach ($options as $method => $params) {
                    if (is_numeric($method) AND is_scalar($params)) {
                        $method = $params;
                        $params = [];
                    }
                    else {
                        if (!is_array($params)) {
                            $params = [$params];
                        }
                    }

                    if ($method == 'allowEmpty') {
                        if (empty($this->_value)) {
                            return $this->_value;
                        }
                        else {
                            continue;
                        }
                    }

                    if (!method_exists($this, $method)) {
                        throw new \ErrorException('Incorrect validator option "' . $method . '"');
                    }

                    call_user_func_array([
                        $this,
                        $method], $params);
                }
            }

            return $this->_value;
        }

        public function defaultOnEmpty($value) {

            if (empty($this->_value)) {
                $this->_value = $value;
            }
        }

        public function requires() {

            if (empty($this->_value)) {
                throw new ValidatorException('Value is required');
            }
        }
    }

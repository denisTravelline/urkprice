<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 05.09.14 14:03
     */

    namespace SORApp\Services\Validator;

    /**
     * Class ValidatorFactory
     * @package SORApp\Services\Validator
     */
    class ValidatorFactory {

        private static $_validatorMap;

        /**
         * При необходимости создает и возвращает объект валидатора
         *
         * @param string $validatorName
         *
         * @return AbstractValidator|bool
         */
        public function get($validatorName) {

            if (!isset(self::$_validatorMap[$validatorName])) {
                $validatorClass = '\\SORApp\\Services\\Validator\\' . ucfirst($validatorName) . 'Validator';
                if (!class_exists($validatorClass)) {
                    return false;
                }

                self::$_validatorMap[$validatorName] = new $validatorClass($this);
            }

            return self::$_validatorMap[$validatorName];
        }
    }

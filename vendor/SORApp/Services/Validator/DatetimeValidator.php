<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Validator;

    class DatetimeValidator extends AbstractValidator {

        public function testFormat($format) {

            return (\DateTime::createFromFormat($format, $this->_value));
        }

        public function fromFormat($format) {

            if ($dateTime = \DateTime::createFromFormat($format, $this->_value)) {
                $this->_value = $dateTime->format('Y-m-d H:i:s');

                return true;
            }
            else {
                return false;
            }
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Validator;

    class StringValidator extends AbstractValidator {

        public function min($length) {

            if (mb_strlen($this->_value, 'utf-8') < $length) {
                throw new ValidatorException('Incorrect sting min length');
            }
        }

        public function max($length) {

            if (mb_strlen($this->_value, 'utf-8') > $length) {
                throw new ValidatorException('Incorrect sting max length');
            }
        }

        public function length($length) {

            if (mb_strlen($this->_value, 'utf-8') !== $length) {
                throw new ValidatorException('Incorrect sting length');
            }
        }

        public function match($pattern) {

            if (!preg_match($pattern, $this->_value)) {
                throw new ValidatorException('Incorrect sting format');
            }
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 12.11.14 16:32
     */

    namespace SORApp\Services\Validator;

    class CallbackValidator extends AbstractValidator {

        public function test($value, $options = []) {

            if (!is_callable($options)) {
                throw new \ErrorException('Incorrect callback validator');
            }

            return call_user_func($options, $value);
        }

    }

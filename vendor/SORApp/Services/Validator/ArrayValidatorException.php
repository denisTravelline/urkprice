<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 05.12.14 12:15
     */

    namespace SORApp\Services\Validator;

    class ArrayValidatorException extends ValidatorException {

        public $errors = [];

        public function addError($error) {

            $this->errors[] = $error;
        }

        public function getErrors() {

            return $this->errors;
        }

        public function hasErrors() {

            return count($this->errors) > 0;
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Validator;

    class NumberValidator extends AbstractValidator {

        public function int() {

            $value = (int)$this->_value;
            if ($value == $this->_value) {
                $this->_value = $value;
            }
            else {
                throw new ValidatorException('Incorrect value type');
            }
        }

        public function min($length) {

            if ($this->_value < $length) {
                throw new ValidatorException('Value is too high. Need < ' . $length);
            }
        }

        public function max($length) {

            if ($this->_value > $length) {
                throw new ValidatorException('Value is too low. Need > ' . $length);
            }
        }

        public function range(array $range) {

            return in_array($this->_value, $range);
        }
    }

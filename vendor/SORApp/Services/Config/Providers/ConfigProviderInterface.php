<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 11.12.14 15:25
     */

    namespace SORApp\Services\Config\Providers;

    interface ConfigProviderInterface {

        public function __construct($environment);

        public function load($sectionName);
    }

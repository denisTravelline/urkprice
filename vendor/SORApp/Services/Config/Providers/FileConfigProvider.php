<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 11.12.14 16:35
     */

    namespace SORApp\Services\Config\Providers;

    /**
     * Class FileConfigProvider
     * @package SORApp\Services\Config\Providers
     */
    class FileConfigProvider implements ConfigProviderInterface {

        public $baseDir;

        protected $_environmentDir;
        protected $_environmentConfig;

        public function __construct($environment) {

            $this->_environmentDir = $environment;
        }

        /**
         * @param string $sectionName
         *
         * @return array|bool
         */
        public function load($sectionName) {

            $config = $this->reedFile($sectionName . '.php');
            if ($this->_environmentDir) {
                $config = array_replace_recursive($config, $this->reedFile($this->_environmentDir . DIRECTORY_SEPARATOR . $sectionName .
                                                                           '.php'));
            }

            if ($this->_environmentConfig === null) {
                $this->_environmentConfig = $this->reedFile('env.php');
                if (file_exists($this->baseDir . DIRECTORY_SEPARATOR . $this->_environmentDir)) {
                    $this->_environmentConfig = array_replace_recursive($this->_environmentConfig, $this->reedFile($this->_environmentDir
                                                                                                                   /*$this->_environmentConfig*/ .
                                                                                                                   DIRECTORY_SEPARATOR .
                                                                                                                   'env.php'));
                }
            }

            if (isset($this->_environmentConfig[$sectionName])) {
                $config = array_replace_recursive($config, $this->_environmentConfig[$sectionName]);
            }

            return (count($config) ? $config : false);
        }

        /**
         * @param string $fileName
         *
         * @throws \ErrorException
         * @return array
         */
        protected function reedFile($fileName) {

            $config = [];
            if (file_exists($this->baseDir . DIRECTORY_SEPARATOR . $fileName)) {
                $config = include($this->baseDir . DIRECTORY_SEPARATOR . $fileName);
                if (!is_array($config)) {
                    throw new \ErrorException('Incorrect config file');
                }
            }

            return $config;
        }
    }

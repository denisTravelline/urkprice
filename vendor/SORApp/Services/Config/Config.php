<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 11.12.14 13:32
     */

    namespace SORApp\Services\Config;

    use SORApp\Services\Config\Providers\ConfigProviderInterface;

    class Config implements \ArrayAccess {

        const DEFAULT_SECTION = 'global';

        /** @var array Провайдеры данных конфигурации */
        protected $_providers = [];

        /** @var array Неймспейсы конфигурации */
        protected $_sections = [];

        /** @var string Текущее окружение */
        protected $_environment;

        /**
         * @param string $environment название окружения
         */
        public function __construct($environment) {

            $this->_environment = (string)$environment;
        }

        /**
         * @return string
         */
        public function getEnvironment() {

            return $this->_environment;
        }

        /**
         * Регистрация провайдера конфигурации
         *
         * @param string $providerName
         * @param array $providerOptions
         * @param string $providerClassName
         */
        public function registerProvider($providerName, $providerOptions = [], $providerClassName = null) {

            if ($providerClassName === null) {
                $providerClassName = '\\SORApp\\Services\\Config\\Providers\\' . $providerName;
            }

            $this->_providers[$providerName] = [
                $providerClassName,
                $providerOptions];
        }

        /**
         * Возвращает инициализированный провайдер конфигурации
         *
         * @param string $providerName
         *
         * @return ConfigProviderInterface
         * @throws \ErrorException
         */
        protected function getProvider($providerName) {

            if (!isset($this->_providers[$providerName])) {
                throw new \ErrorException('Provider not registered');
            }

            if (is_array($this->_providers[$providerName])) {
                list($className, $options) = $this->_providers[$providerName];
                if (!class_exists($className)) {
                    //                $providerBaseDir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Providers';
                    //                if ( file_exists($providerBaseDir . DIRECTORY_SEPARATOR . $providerName . '.php') ) {
                    //                    include_once($providerBaseDir . DIRECTORY_SEPARATOR . $providerName . '.php');
                    //                    if ( ! class_exists($this->_providers[$providerName]['className']) ) {
                    //                        throw new \ErrorException('Incorrect class name of config provider "' . htmlentities($providerName) . '"');
                    //                    }
                    //                } else {
                    throw new \ErrorException('Cannot find config provider "' . htmlentities($providerName) . '"');
                    //                }
                }

                $this->_providers[$providerName] = new $className($this->_environment);

                if (!($this->_providers[$providerName] instanceof ConfigProviderInterface)) {
                    throw new \ErrorException('Config provider "' . htmlentities($providerName) .
                                              '" most be instance of ConfigProviderInterface');
                }

                if (is_array($options)) {
                    $this->applyConfig($this->_providers[$providerName], $options);
                }
            }

            return $this->_providers[$providerName];
        }

        /**
         * @param string $sectionName
         * @param array $section
         */
        public function setSection($sectionName, $section) {

            $this->_sections[$sectionName] = $section;
        }

        /**
         * @param string $sectionName
         *
         * @return array
         */
        public function getSection($sectionName) {

            if (!isset($this->_sections[$sectionName])) {
                $this->_sections[$sectionName] = $this->loadSection($sectionName);
            }

            return $this->_sections[$sectionName];
        }

        /**
         * @param string $sectionName
         * @param string|array $providerName
         *
         * @throws \ErrorException
         * @return array
         */
        public function loadSection($sectionName, $providerName = null) {

            if (!$providerName) {
                $providerName = array_keys($this->_providers);
            }

            if (is_string($providerName)) {
                $providerName = [$providerName];
            }

            if (!is_array($providerName) OR !count($providerName)) {
                throw new \ErrorException('Cannot load config section, incorrect provider name');
            }

            $section = [];
            foreach ($providerName as $provider) {
                if (($data = $this->getProvider($provider)->load($sectionName)) !== false) {
                    $section = array_merge($section, $data);
                }
            }

            return $section;
        }

        /**
         * Установить параметр конфигурации
         *
         * @param string $offset
         * @param mixed $value
         */
        public function offsetSet($offset, $value) {

            $configPath = $this->parseKey($offset);
            $section = $this->getSection($configPath['sectionName']);
            $section[$configPath['path']] = $value;
        }

        /**
         * Получить значение конфигурации
         *
         * @param string $offset
         *
         * @return mixed|null
         */
        public function offsetGet($offset) {

            $configPath = $this->parseKey($offset);
            $section = $this->getSection($configPath['sectionName']);

            return (isset($section[$configPath['path']]) ? $section[$configPath['path']] : null);
        }

        /**
         * Существует ли параметр конфигурации
         *
         * @param string $offset
         *
         * @return bool
         */
        public function offsetExists($offset) {

            $configPath = $this->parseKey($offset);
            $section = $this->getSection($configPath['sectionName']);

            return isset($section[$configPath['path']]);
        }

        /**
         * Нельзя удалять значения конфигурации
         *
         * @param mixed $offset
         *
         * @throws \ErrorException
         */
        public function offsetUnset($offset) {

            throw new \ErrorException('Cannot unset config');
        }

        /**
         * Разбор адреса параметра конфигурации
         *
         * @param string $key
         *
         * @return array
         * @throws \ErrorException
         */
        protected function parseKey($key) {

            if (is_string($key) AND preg_match('/(?:[@_](?<sectionName>[^\/]+)\/)?(?<path>.+)/', $key, $matches) AND count($matches['path'])) {
                return [
                    'sectionName' => strtolower(!empty($matches['sectionName']) ? $matches['sectionName'] : self::DEFAULT_SECTION),
                    'path' => $matches['path'],];
            }

            throw new \ErrorException('Empty or incorrect config key');
        }

        /**
         * Хелпер для установки публичных свойств объектов
         *
         * @param Object $object
         * @param array $config
         * @param bool $ignoreMissing
         *
         * @throws \ErrorException
         */
        public function applyConfig($object, array $config, $ignoreMissing = true) {

            if (is_array($config) AND count($config) AND is_object($object)) {
                foreach ($config as $param => $value) {
                    if (property_exists($object, $param) OR method_exists($object, 'set' . ucfirst($param))) {
                        $object->$param = $value;
                    }
                    else {
                        if (!$ignoreMissing) {
                            throw new \ErrorException('Configuration error: cannot find object attribute ' . get_class($object) . '::$' .
                                                      $param);
                        }
                    }
                }
            }
        }

        /**
         * Сравнение параметра конфигурации с определенным значением
         * Если третий параметр bool true будет использовано строгое сравнение
         *
         * @param string $key
         * @param mixed $compare
         * @param bool $strict
         *
         * @return bool
         */
        public function compareWith($key, $compare, $strict = false) {

            return ($strict ? $this->offsetGet($key) === $compare : $this->offsetGet($key) == $compare);
        }

        /**
         * Compare config with boolean true
         *
         * @param      $key
         * @param bool $strict
         *
         * @return bool
         */
        public function is($key, $strict = false) {

            return $this->compareWith($key, true, $strict);
        }
    }

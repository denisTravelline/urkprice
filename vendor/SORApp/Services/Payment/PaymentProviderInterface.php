<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 09.10.14 13:59
     */

    namespace SORApp\Services\Payment;

    interface PaymentProviderInterface {

        /**
         * Устанавливает название текущего провайдера
         *
         * @param string $providerName
         */
        public function setProviderName($providerName);

        /**
         * Возвращает название текущего провайдера оплаты
         *
         * @return string
         */
        public function getProviderName();

        /**
         * Создает модель для платежного ордера
         * Каждый провайдер реализовывает свой формат на основе требований платежной системы
         *
         * @param float $amount
         * @param string $currency
         * @param int $orderID
         * @param string $orderDescription
         * @param string $returnUrl
         *
         * @return mixed
         */
        public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null);

        /**
         * Генерация формы с кнопкой оплаты платежного ордера
         *
         * @param string $label
         * @param array $order
         *
         * @return string
         */
        public function renderPaymentButton($label, $order);

        /**
         * Обработка данных если клиент вернулся с платежной системы
         *
         * @param array $data полный массив $_POST
         *
         * @return array
         */
        public function clientPostHandler(array $data);

        /**
         * Отправка подтверждения банку (если не требуется метод должен возвращать true)
         *
         * @param array $data полный массив $_POST
         * @param string $returnUrl
         *
         * @return bool
         */
        public function sendConfirmRequest(array $data, $returnUrl);
    }

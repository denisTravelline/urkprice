<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 09.10.14 13:56
     */

    namespace SORApp\Services\Payment;

    use SORApp\Components\App;
    use SORApp\Components\BaseComponent;

    class PaymentFactory extends BaseComponent {

        public $paymentProvidersAvailable = [];
        public $paymentProviderDefault;

        /**
         * Создать экземпляр провайдера для проведения оплаты
         * @param string $paymentProvider
         *
         * @return \SORApp\Services\Payment\PaymentProviderInterface
         * @throws \ErrorException
         */
        public function createPayment($paymentProvider = null) {

            if ($paymentProvider === null)
                $paymentProvider = $this->paymentProviderDefault;

            if (!isset($this->paymentProvidersAvailable[$paymentProvider]) || !is_array($this->paymentProvidersAvailable[$paymentProvider]) ||
                !isset($this->paymentProvidersAvailable[$paymentProvider]['className'])
            )
                throw new \ErrorException('Payment provider not available');

            $className = $this->paymentProvidersAvailable[$paymentProvider]['className'];
            $payment = new $className;
            if (!($payment instanceof PaymentProviderInterface))
                throw new \ErrorException('Payment provider has incorrect interface');

            $payment->setProviderName($paymentProvider);

            if (count($this->paymentProvidersAvailable[$paymentProvider]) > 1)
                $this->config->applyConfig($payment, array_diff_key($this->paymentProvidersAvailable[$paymentProvider], ['className' => null]));

            return $payment;
        }
    }

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 09.10.14 14:02
     */

    namespace SORApp\Services\Payment;

    use SORApp\Components\BaseComponent;

    abstract class AbstractPaymentProvider extends BaseComponent implements PaymentProviderInterface {

        public $requestTimeout = 10;

        protected $providerName;

        public function __construct() {

            $this->getComponent('templateManager')->addPath(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Views', 'payment');
        }

        public function setProviderName($providerName) {

            $this->providerName = $providerName;
        }

        public function getProviderName() {

            return $this->providerName;
        }

        abstract public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null);

        public function renderPaymentButton($label, $order) {

            $this->getComponent('templateManager')->render('@payment/default', ['buttonLabel' => $label,
                                                                                'order' => $order,
                                                                                'payment' => $this]);
        }

        protected function sendPostRequest($url, $postData) {

            $ch = curl_init($url);
            curl_setopt_array($ch, [CURLOPT_POST => true,
                                    CURLOPT_POSTFIELDS => $postData,

                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_TIMEOUT => $this->requestTimeout,

                                    // И да, я знаю что это небезопасно
                                    CURLOPT_SSL_VERIFYPEER => false]);

            return ['response' => curl_exec($ch),
                    'errno' => curl_errno($ch),
                    'error' => curl_error($ch),
                    'curlinfo' => curl_getinfo($ch),];
        }
    }

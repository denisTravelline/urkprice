<?php

    /*
     *   Процессинг "Платрон"
     **/

    namespace SORApp\Services\Payment\Providers;

    use SORApp\Components\App;
    use SORApp\Services\Payment\AbstractPaymentProvider;
    use SORApp\Services\Payment\PaymentException;

    class PlatronPaymentProvider extends AbstractPaymentProvider {

        public $paymentUrl = 'https://www.platron.ru/payment.php';

        private $salt;

        private $merchantId;

        private $merchantSecret;

        protected $testingMode;

        public function setMerchantId($id) {

            $this->merchantId = $id;
        }

        public function setMerchantSecret($secret) {

            $this->merchantSecret = $secret;
        }

        public function getTestingMode() {

            return $this->testingMode;
        }

        public function setTestingMode($testing) {

            $this->testingMode = $testing;
        }

        public function renderPaymentButton($label, $order) {

            return $this->getComponent('templateManager')->render('@payment/platron', [
                'order' => $order,
                'payment' => $this,
                'buttonLabel' => $label]);
        }

        // Название статуса положительной транзакции
        public $ACCEPTED_STATUS_NAME = '0';

        public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null) {

            $lang = App::getInstance()->getLang();
            if ($lang !== 'en') {
                $lang = 'ru';
            }

            if ($currency !== 'RUR' && $currency !== 'EUR' && $currency !== 'USD') {
                throw new PaymentException('Invalid currency');
            }

            $url = App::getInstance()->url('payment', ['hashKey' => $_GET['hashKey']]);
            $this->salt = self::salt();

            $order = [
                'pg_testing_mode' => $this->testingMode,
                'pg_merchant_id' => $this->merchantId,
                'pg_order_id' => $orderID,
                'pg_amount' => number_format($amount, 2, '.', ''),
                'pg_currency' => $currency,
                'pg_lifetime' => 60 * 60 * 24,
                'pg_language' => $lang,
                'pg_description' => $orderDescription,
                'pg_result_url' => $url,
                'pg_success_url' => $url,
                'pg_failure_url' => $url,
                'pg_request_method' => 'POST',
                'pg_success_url_method' => 'AUTOGET',
                'pg_failure_url_method' => 'AUTOGET',
                'pg_salt' => $this->salt];

            if (!empty($customer)) {

                $email = preg_match('/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/', $customer['email'], $m) ? $m[0] : false;

                $phone = preg_match_all('/(\d)/', $customer['phone'], $m) ? (count($m[0]) > 9 ? implode($m[0]) : false) : false;
                $order += ($email ? ['pg_user_contact_email' => $email] : []) + ($phone ? [
                        'pg_user_phone' => $phone,
                        'pg_need_phone_notification' => 0] : []);
            }
            $order['pg_sig'] = self::digest($order, $this->merchantSecret, self::script($this->paymentUrl));

            return $order;
        }

        public function clientPostHandler(array $data) {

            if (empty($data['pg_sig'])) {
                App::Log('Platron', 'ERROR, signature missing: ' . json_encode($data, JSON_UNESCAPED_UNICODE));
                throw new PaymentException('Incorrect bank response, signature missing');
            }
            if (!self::verify($data['pg_sig'], $data, $this->merchantSecret)) {
                App::Log('Platron', 'ERROR, signature check failed: ' . json_encode($data, JSON_UNESCAPED_UNICODE));
                throw new PaymentException('Incorrect bank response, signature check failed');
            }

            $result = isset($data['pg_result']) && !!$data['pg_result'];
            App::Log('Platron', 'RESULT: ' . ($result ? 'SUCCESS' : 'FAILURE') . json_encode($data, JSON_UNESCAPED_UNICODE));

            if (!$result) {
                sendConfirmRequest($data, '');
                return [];
            }

            return [
                'order' => (int)$data['pg_order_id'],
                'amount' => $result ? floatval($data['pg_ps_full_amount']) : 0.00,
                'currency' => (string)$data['pg_currency']];
        }

        public function sendConfirmRequest(array $data, $returnUrl) {

            $response = [
                'pg_salt' => $data['pg_salt'],
                'pg_status' => 'ok',
                'pg_description' => 'ok'];
            $response['pg_sig'] = self::digest($response, $this->merchantSecret);

            header('Content-type: text/xml');
            print '<?xml version="1.0" encoding="utf-8"?>';
            ?>
            <response>
                <pg_salt><?php echo $response['pg_salt'] ?></pg_salt>
                <pg_status><?php echo $response['pg_status'] ?></pg_status>
                <pg_description><?php echo $response['pg_description'] ?></pg_description>
                <pg_sig><?php echo $response['pg_sig'] ?></pg_sig>
            </response>
            <?php
            return true;
        }

        private static function flat($params, $parent = '') {

            $flat = [];
            $index = 0;
            foreach ($params as $key => $value) {
                $index++;
                if ('pg_sig' === $key) {
                    continue;
                }
                $name = $parent . $key . sprintf('%03d', $index);
                if (is_array($value)) {
                    $flat = array_merge($flat, self::flat($value, $name));
                    continue;
                }
                $flat[$name] = (string)$value;
            }

            return $flat;
        }

        private static function script($url) {

            $path = parse_url($url, PHP_URL_PATH);
            $len = strlen($path);
            if (!$len || '/' == $path{$len - 1}) {
                return '';
            }

            return basename($path);
        }

        private static function signature($params, $secret, $script) {

            unset($params['pg_sig']);

            ksort($params);

            array_unshift($params, $script);
            array_push($params, $secret);

            return join(';', $params);
        }

        private static function digest($params, $secret, $script = null) {

            if (null === $script) {
                $script = self::script($_SERVER['REQUEST_URI']);
            }

            return md5(self::signature(self::flat($params), $secret, $script));
        }

        public static function verify($digest, $params, $secret, $script = null) {

            return (string)$digest === self::digest($params, $secret, $script);
        }

        private static function salt() {

            return rand(21, 43433);
        }

    }

<?php
    /**
     * @author Alexander Olkhovoy <ao@ze1.org>
     * Created 27.03.15 12:04
     */

    namespace SORApp\Services\Payment\Providers;

    use SORApp\Services\Payment\AbstractPaymentProvider;
    use SORApp\Services\Payment\PaymentException;

    class VirtualPaymentProvider extends AbstractPaymentProvider {

        protected $result = true;

        /**
         * @param mixed $terminal
         */
        public function setResult($result) {

            $this->result = $result;
        }

        public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null) {

            $order = [// Загальна сума покупки у форматі з відокремленням копійок крапкою
                      // (відображається на сторінці «Введення реквізитів картки»)
                      'AMOUNT' => number_format($amount, 2, '.', ''),

                      // Валюта покупки: 3-х буквений код валюти. Має дорівнювати значенню 'UAH'
                      // (відображається на сторінці «Введення реквізитів картки»).
                      'CURRENCY' => $currency,

                      // Цифровий ідентифікаційний номер покупки (відображається на сторінці «Введення реквізитів картки»).
                      // Останні 6 цифр мають бути унікальними в проміжку однієї дати (доби).
                      'ORDER' => sprintf("%06d", $orderID),

                      // Опис покупки (відображається на сторінці «Введення реквізитів картки»).
                      // Кодування Win1251. Не повинно закінчуватись символом "пробіл".
                      'DESC' => $orderDescription,

                      // URL-адреса Торговця на яку буде направлено відповідь з результатами по авторизаційному запиту.
                      // Також на цю адресу перенаправляється Покупець з сайту Системи.
                      'BACKREF' => $returnUrl];

            return $order;
        }

        public function renderPaymentButton($label, $order) {

            return $this->getComponent('templateManager')->render('@payment/virtual', ['buttonLabel' => $label,
                                                                                       'order' => $order,
                                                                                       'payment' => $this]);
        }

        public function clientPostHandler(array $data) {

            if (!$this->result)
                throw new PaymentException('ERROR');

            return ['order' => ltrim($data['ORDER'], '0'),
                    'amount' => floatval($data['AMOUNT']),
                    'currency' => (string)$data['CURRENCY']];
        }

        public function sendConfirmRequest(array $data, $returnUrl) {

            return true;
        }

    }

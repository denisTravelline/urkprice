<?php

    namespace SORApp\Services\Payment\Providers;

    use SORApp\Components\App;
    use SORApp\Services\Payment\AbstractPaymentProvider;
    use SORApp\Services\Payment\PaymentException;
    use SORApp\Controllers\BookingController;
    use SORApp\Models\Booking\AbstractBookingFactory;

	class AssistPaymentProvider extends AbstractPaymentProvider {

		protected $Merchant_ID = 0;
        protected $login = '';
        protected $password = '';
        protected $order = null;
		protected $customer;
        protected $ordernumber;
		protected $reservation;
		protected $reservationFactory;
        protected $processingUrl = 'https://test.paysec.by/orderresult/orderresult.cfm';

		public $paymentUrl = 'https://test.paysec.by/pay/order.cfm';
        public function setOrderNumber($orderID) { $this->ordernumber = $orderID; }
        public function setOrder($order) { $this->order = $order; }
        public function resetOrder() { $this->order = null; }
		public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null) {
            return [
                  'Merchant_ID' 	=> $this->Merchant_ID,
                  'OrderNumber' 	=> $orderID,
                  'OrderAmount'     => number_format($amount, 2, '.', ''),
                  'OrderCurrency'   => $currency,
                  'FirstName'		=> $customer['first_name'],
                  'LastName'		=> $customer['last_name'],
                  'Email'			=> $customer['email'],
                  'MobilePhone'		=> $customer['phone'],
                  'OrderComment' 	=> $orderDescription,
                  'TestMode'		=> 1,
                  'URL_RETURN'      => $returnUrl,
                  'URL_RETURN_OK' 	=> $returnUrl,
                  'URL_RETURN_NO' 	=> $returnUrl,
            ];
        }
        public function renderPaymentButton($label, $order) {
            return $this->getComponent('templateManager')->render('@payment/assist', [
                'order' => $order,
                'payment' => $this,
                'buttonLabel' => $label,
                'error' => ($this->order->operation->responsecode == 'AS000') ? NULL : $this->order,
            ]);
        }

        /**
         * Обработка данных если клиент вернулся с платежной системы
         *
         * @param array $data полный массив $_POST
         *
         * @return array
         */
        public function clientPostHandler(array $data) {
            $this->setOrder($this->getOrderResult());
            return [
                'order' => (int)$this->order->{'ordernumber'},  
                'amount' => floatval($this->order->{'orderamount'}),
                'currency' => (string)$this->order->{'ordercurrency'},
            ];
        }

        /**
         * Зпрос к системе оплаты для полчения детальной информации о результате оплаты (если не требуется метод должен возвращать true)
         *
         * @param int $oredID 
         * @return object
         */
        protected function getOrderResult() {
            $request = [
                'ordernumber'   => $this->ordernumber,
                'Merchant_ID'   => $this->Merchant_ID,
                'login'         => $this->login,
                'password'      => $this->password,
                'format'        => 3 //XML
            ];
            $response = $this->sendPostRequest($this->processingUrl, $request);
            $xml = simplexml_load_string($response['response']);
            $lastOne = count($xml->order)-1;
            return $xml->order[$lastOne];
        }

        /**
         * Отправка подтверждения банку (если не требуется метод должен возвращать true)
         *
         * @param array $data полный массив $_POST
         * @param string $returnUrl
         *
         * @return bool
         */
        public function sendConfirmRequest(array $data, $returnUrl) {
            return $this->order->operation->responsecode == 'AS000';
        }

    }

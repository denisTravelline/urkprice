<?php

    /*
     *   Процессинг "LiqPay"
     **/

    namespace SORApp\Services\Payment\Providers;

    use SORApp\Components\App;
    use SORApp\Services\Payment\AbstractPaymentProvider;
    use SORApp\Services\Payment\PaymentException;

    class LiqpayPaymentProvider extends AbstractPaymentProvider {
        public $paymentUrl = 'https://www.liqpay.com/api/checkout';
        protected $_supportedCurrencies = array('EUR','UAH','USD','RUB','RUR');
        const public_key = 'i88218305863';
        const private_key = 'MT1c0S8MbL9nKMDW0WhLCehC261DhvcHrpfjRubx';
        const MERCHANTID = 'i88218305863';
        const MERRESPURL = 'https://www.liqpay.com/api/checkout';
        const VERSION = '3';
        const URL = 'https://www.liqpay.com/api/checkout';
        const SANDBOX = '0';

//        public function __construct($pParent)
//        {
//            $this->pParent = $pParent;
//        }

        public function Details($arrFieldsData, $step, $lang = 'ru')
        {
            if (count($_POST) && isset($_POST['responsecode']))
                return $this->Validate($step, $lang);
            else return $this->Request($arrFieldsData, $step, $lang);
        }

//        public function GetInfo()
//        {
//            return array(
//                AdkorSystem::ADKORSYSTEM_MODULEINFO_RELATIONMODULES => array(
//                    'localization::index', 	// Модуль локализации
//                )
//            );
//        }

        public function Refund()
        {
        }

        public function Run()
        {
        }

        public function cnb_form($params)
        {

            $language = 'ru';
            if (isset($params['language']) && $params['language'] == 'en') {
                $language = 'en';
            }

            $params    = $this->cnb_params($params);
            $data      = base64_encode( json_encode($params) );
            $signature = $this->cnb_signature($params);

            return sprintf('
            <form method="POST" action="%s" accept-charset="utf-8">
                %s
                %s
                <input type="image" src="//static.liqpay.com/buttons/p1%s.radius.png" name="btn_text" />
            </form>
            ',
                self::URL,
                sprintf('<input type="hidden" name="%s" value="%s" />', 'data', $data),
                sprintf('<input type="hidden" name="%s" value="%s" />', 'signature', $signature),
                $language
            );
        }

        private function cnb_params($params)
        {

            $params['public_key'] = self::public_key;

            if (!isset($params['version'])) {
                throw new InvalidArgumentException('version is null');
            }
            if (!isset($params['amount'])) {
                throw new InvalidArgumentException('amount is null');
            }
            if (!isset($params['currency'])) {
                throw new InvalidArgumentException('currency is null');
            }
            if (!in_array($params['currency'], $this->_supportedCurrencies)) {
                throw new InvalidArgumentException('currency is not supported');
            }
            if ($params['currency'] == 'RUR') {
                $params['currency'] = 'RUB';
            }
            if (!isset($params['description'])) {
                throw new InvalidArgumentException('description is null');
            }

            return $params;
        }

        public function cnb_signature($params)
        {
//            $params      = $this->cnb_params($params);
            $private_key = self::private_key;

            $json      = base64_encode( json_encode($params) );
            $signature = $this->str_to_sign($private_key . $json . $private_key);

            return $signature;
        }

        public function str_to_sign($str)
        {

            $signature = base64_encode(sha1($str,1));

            return $signature;
        }

        private static function Hex2Bin($data)
        {
            $str = '';
            $len = mb_strlen($data);
            for($i = 0; $i < mb_strlen($data); $i += 2)
                $str .= chr(hexdec(substr($data, $i, 2)));

            return $str;
        }

        private function Confirm($arrFieldsData, &$arrErrors, $lang = 'ru')
        {
            //$pLocalizationIndex = $this->pParent->GetModule('localization_index');
            //$pLoc = $pLocalizationIndex->GetLocalization($lang);
            $pProtocolIndex = $this->pParent->GetModule('protocol_index');
            $pProtocolIndex->Init($arrFieldsData['idHotel']);
            $pProtocolIndex->SetReservationType($arrFieldsData['Account'], $arrFieldsData['FirstSum']);
            if ($pProtocolIndex->GetLastError())
            {
                $arrErrors[] = 'Service is down';//$pLoc->service_down;
                $pProtocolIndex->ResetError();
                return false;
            }

            return true;
        }

        public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null) //
        {
            $lang = App::getInstance()->getLang();
            if ($lang == 'ua')
                $retlang = 'uk';
            else $retlang = $lang;

            $arrErrors = array();
            //$pLocalizationIndex = $this->pParent->GetModule('localization_index');
            //$pLoc = $pLocalizationIndex->GetLocalization($lang);
            $description = $orderDescription;
            $purchaseamt = $amount;

            $url = App::getInstance()->url('payment', ['hashKey' => $_GET['hashKey']]);

            $array = [];

            $array['version'] = self::VERSION;
            $array['public_key'] = self::public_key;
            $array['amount'] = $amount;
            $array['currency'] = $currency;
            $array['product_name'] = $_GET['hashKey'];
            $array['product_description'] = $amount;
            $array['info'] = $orderID;
            $array['description'] = $orderDescription.' ('.$customer['first_name'].' '.$customer['last_name'].', '.$customer['phone'].')';
            $array['orderID'] = $orderID;
            $array['server_url'] = $url;
            $array['result_url'] = $url;
            $array['sandbox'] = self::SANDBOX;  // Тестовый режим = 1

            $params    = $this->cnb_params($array);
            $data      = base64_encode( json_encode($params) );
            $signature = $this->cnb_signature($params);

//            $arrRet['nights'] = ''; //(date("j", (strtotime($arrFieldsData['dateOut']) - strtotime($arrFieldsData['dateIn']))) - 1);
//            $arrRet['processingURL'] = self::URL;
//            $arrRet['acqid'] = self::ACQID;
//            $arrRet['merid'] = self::MERCHANTID;
//            $arrRet['merrespurl'] = sprintf(self::MERRESPURL, $retlang);
//            $arrRet['orderdescription'] = $description;
//            $arrRet['orderid'] = $orderID;
//            $arrRet['purchaseamt'] = $purchaseamt;
//            $arrRet['purchasecurrency'] = self::PURCHASECURRENCY;
//            $arrRet['purchasecurrencyexponent'] = self::PURCHASECURRENCYEXPONENT;


            $arrRet['data'] = $data;
            $arrRet['signature'] = $signature;
            $arrRet['pg_amount'] = $amount;
            $arrRet['pg_currency'] = $currency;
            $arrRet['AMOUNT'] = $amount;
            $arrRet['CURRENCY'] = $currency;

            return $arrRet;
        }

        public function renderPaymentButton($label, $order) {

            return $this->getComponent('templateManager')->render('@payment/liqpay', [
                'order' => $order,
                'payment' => $this,
                'buttonLabel' => $label]);
        }

        private function Signature($orderid, $purchaseamt, $description)
        {
            return base64_encode(self::Hex2Bin(sha1(self::PASSWORD
                . self::MERCHANTID
                . self::ACQID
                . $orderid
                . $purchaseamt
                . self::PURCHASECURRENCY
                . $description)));
        }

        private function Validate($step, $lang = 'ru')
        {
            $arrRet = array();
            $arrErrors = array();
            $pReservationIndex = $this->pParent->GetModule('reservation_index');
            $arrFieldsData = $pReservationIndex->unserializeData((int)$_POST['orderid']);
            $sth = $this->pParent->DB()->prepare('INSERT INTO pmsr_processing (`name`, `surname`, `order`, `amount`, `status`, `status_number`, `status_info`, `user_info`, `ts`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)');
            $res = $this->pParent->DB()->execute($sth, array
            (
                'name' => 			$arrFieldsData['name'],
                'surname' => 		$arrFieldsData['surname'],
                'order' => 			$_POST['orderid'],
                'amount' => 		$arrFieldsData['FirstSum'],
                'status' => 		$_POST['responsecode'],
                'status_number' => 	$_POST['reasoncodedesc'],
                'status_info' => 	serialize($_POST),
                'user_info' => 		serialize($arrFieldsData),
                'ts' => 			time()
            ));

            if ($_POST['responsecode'] == 1)
                $this->Confirm($arrFieldsData, $arrErrors, $lang);
            else $arrErrors[] = $_POST['reasoncodedesc'];
            if (count($arrErrors)) $step--;

            return array
            (
                'arrFieldsData' => 	$arrFieldsData,
                'lang' => 			$lang,
                'step' => 			$step,
                'arrRet' => 		$arrRet,
                'arrErrors' => 		$arrErrors
            );
        }

//        public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null)
//        {
//            // TODO: Implement createOrder() method.
//        }


        public function clientPostHandler(array $data)
        {
            $ans = json_encode($data, JSON_UNESCAPED_UNICODE);
            $result = json_decode(base64_decode($data['data']));

            if ($data['signature']=='') {
                App::Log('POST_LIQ', 'ERROR, signature missing: ' . $ans);
                throw new PaymentException('Incorrect bank response, signature missing');
            }
            if (!self::verify($data['signature'], $data['data'])) {
                App::Log('POST_LIQ', 'ERROR, signature check failed: ' . json_encode($data, JSON_UNESCAPED_UNICODE));
                throw new PaymentException('Incorrect bank response, signature check failed');
            }
            if(self::SANDBOX != '1'){
                if ($result->status!='success') {
                    App::Log('POST_LIQ', 'ERROR, Payment status is not success: ' . $ans);
                    throw new PaymentException('Payment status is not success');
                }else{
                    App::Log('POST_LIQ', 'Payment successful: '. $ans);
                }
            }

            App::Log('POST_LIQ', 'info: ' . $result->info);
            App::Log('POST_LIQ', 'amount: ' . $result->amount);
            App::Log('POST_LIQ', 'currency: ' . $result->currency);

            return [
                'order' => $result->info,
                'amount' => $result ? floatval($result->amount) : 0.00,
                'currency' => $result->currency
            ];
        }

        public function verify($sign, $data){

            $signature =  base64_encode( sha1( self::private_key . $data . self::private_key, 1 ) );

            return $sign === $signature;
        }

        /**
         * Отправка подтверждения банку (если не требуется метод должен возвращать true)
         *
         * @param array $data полный массив $_POST
         * @param string $returnUrl
         *
         * @return bool
         */
        public function sendConfirmRequest(array $data, $returnUrl)
        {
            $response = [
                'version' => $data['version'],
                'order_id' => $data['order_id']];

            return $response;
        }
    }

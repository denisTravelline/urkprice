<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 09.10.14 14:21
     */

    namespace SORApp\Services\Payment\Providers;

    use SORApp\Services\Payment\AbstractPaymentProvider;
    use SORApp\Services\Payment\PaymentException;

    class UkreximbankPaymentProvider extends AbstractPaymentProvider {

        public $processingUrl = 'https://3ds.eximb.com/cgi-bin/cgi_link';

        protected $terminal;
        protected $makKey;

        protected $merchantID;
        protected $merchantURL;
        protected $merchantName;
        protected $merchantEmail;
        protected $merchantCountry;
        protected $merchantGMT;

        private $transactionType = 0;

        // Язык интерфейса процессинга
        private $ARR_PROCESSING_LANG = ['ru' => 'RUS',
                                        'en' => 'ENG',
                                        'ua' => 'UKR',
                                        'uk' => 'UKR'];

        private $ARR_MAC_FIELDS = ['order' => ['AMOUNT',
                                               'CURRENCY',
                                               'ORDER',
                                               'DESC',
                                               'MERCH_NAME',
                                               'MERCH_URL',
                                               'MERCHANT',
                                               'TERMINAL',
                                               'EMAIL',
                                               'TRTYPE',
                                               'COUNTRY',
                                               'MERCH_GMT',
                                               'TIMESTAMP',
                                               'NONCE',
                                               'BACKREF',],
                                   'clientResponse' => ['RRN',
                                                        'INT_REF',
                                                        'TERMINAL',
                                                        'TRTYPE',
                                                        'ORDER',
                                                        'AMOUNT',
                                                        'CURRENCY',
                                                        'ACTION',
                                                        'RC',
                                                        'APPROVAL',
                                                        'TIMESTAMP',
                                                        'NONCE'],
                                   'confirm' => ['ORDER',
                                                 'AMOUNT',
                                                 'CURRENCY',
                                                 'RRN',
                                                 'INT_REF',
                                                 'TRTYPE',
                                                 'TERMINAL',
                                                 'TIMESTAMP',
                                                 'NONCE'],];

        /**
         * @param mixed $terminal
         */
        public function setTerminal($terminal) {

            $this->terminal = $terminal;
        }

        /**
         * @param mixed $makKey
         */
        public function setMakKey($makKey) {

            $this->makKey = $makKey;
        }

        /**
         * @param mixed $currency
         */
        public function setCurrency($currency) {

            $this->currency = $currency;
        }

        /**
         * @param mixed $merchantID
         */
        public function setMerchantID($merchantID) {

            $this->merchantID = $merchantID;
        }

        /**
         * @param mixed $merchantName
         */
        public function setMerchantName($merchantName) {

            $this->merchantName = $merchantName;
        }

        /**
         * @param mixed $merchantURL
         */
        public function setMerchantURL($merchantURL) {

            $this->merchantURL = $merchantURL;
        }

        /**
         * @param mixed $merchantEmail
         */
        public function setMerchantEmail($merchantEmail) {

            $this->merchantEmail = $merchantEmail;
        }

        /**
         * @param mixed $merchantCountry
         */
        public function setMerchantCountry($merchantCountry) {

            $this->merchantCountry = $merchantCountry;
        }

        /**
         * @param mixed $merchantGMT
         */
        public function setMerchantGMT($merchantGMT) {

            $this->merchantGMT = $merchantGMT;
        }

        public function createOrder($amount, $currency, $orderID, $orderDescription, $returnUrl, $customer = null) {

            // Данные для процессинга (порядок должен быть обязательно в таком виде)
            $order = [// Має дорівнювати значенню '0' у разі роботи по схемі, яка вимагає 'Sales Completion Request',
                      // або значенню '1' у разі роботи по схемі без 'Sales Completion Request'.
                      'TRTYPE' => $this->transactionType,

                      // Загальна сума покупки у форматі з відокремленням копійок крапкою
                      // (відображається на сторінці «Введення реквізитів картки»)
                      'AMOUNT' => number_format($amount, 2, '.', ''),

                      // Валюта покупки: 3-х буквений код валюти. Має дорівнювати значенню 'UAH'
                      // (відображається на сторінці «Введення реквізитів картки»).
                      'CURRENCY' => $currency,

                      // Цифровий ідентифікаційний номер покупки (відображається на сторінці «Введення реквізитів картки»).
                      // Останні 6 цифр мають бути унікальними в проміжку однієї дати (доби).
                      'ORDER' => sprintf("%06d", $orderID),

                      // Опис покупки (відображається на сторінці «Введення реквізитів картки»).
                      // Кодування Win1251. Не повинно закінчуватись символом "пробіл".
                      'DESC' => $orderDescription,

                      // Метод доставки товару. 'S' – електронна доставка, 'T' – фізична доставка.
                      'DELIVERY' => 'S',

                      // Назва Торговця (відображається на сторінці «Введення реквізитів картки»)
                      'MERCH_NAME' => $this->merchantName,

                      // Адреса Сайту Торговця (відображається на сторінці «Введення реквізитів картки»).
                      'MERCH_URL' => $this->merchantURL,
                      //'www.reikartz.com', // 'http://' . $this->getComponent('router')->host . '/',

                      // Ідентифікатор Торговця присвоєний Банком.
                      'MERCHANT' => $this->merchantID,

                      // Ідентифікатор V-POS-термінала присвоєний Банком
                      'TERMINAL' => $this->terminal,

                      // E-mail Торговця для отримання додаткового повідомлення про результат виконання транзакції
                      'EMAIL' => $this->merchantEmail,

                      // Мова сторінок Системи. Може набувати значення 'UKR', 'RUS', 'ENG'.
                      // Від значення цього поля залежить якою мовою Покупцю будуть показані сторінки Системи.
                      // У разі відсутності поля використовується значення 'UKR'.
                      'LANG' => $this->ARR_PROCESSING_LANG[$this->getOwner()->getLang()],

                      // 2-х буквений код країни магазина Торговця.
                      // Необов'язкове для заповнення поле. Заповнюється, якщо магазин Торговця знаходиться в іншій країні ніж Банк.
                      'COUNTRY' => $this->merchantCountry,

                      // Часова зона Торговця (наприклад -3).
                      // Необов'язкове для заповнення поле. Заповнюється якщо Торговець знаходиться в іншій часовій зоні ніж Банк.
                      'MERCH_GMT' => $this->merchantGMT,

                      // Часовий штамп транзакції у GMT: РРРРММДДГГХХСС.
                      // Якщо часовий штамп транзакції відрізняється від часу Системи більш ніж на 500 секунд, такий запит буде відхилено (з кодом RC = -20).
                      'TIMESTAMP' => gmdate("YmdHis", time()),

                      // Випадкова величина. Має бути заповнена випадковим чином у шістнадцятиричному форматі.
                      'NONCE' => $this->createNonce(),

                      // URL-адреса Торговця на яку буде направлено відповідь з результатами по авторизаційному запиту.
                      // Також на цю адресу перенаправляється Покупець з сайту Системи.
                      'BACKREF' => $returnUrl,];

            $order['P_SIGN'] = static::createMACSign($this->ARR_MAC_FIELDS['order'], $order, $this->makKey);

            return $order;
        }

        public function renderPaymentButton($label, $order) {

            return $this->getComponent('templateManager')->render('@payment/ukreximbank', ['buttonLabel' => $label,
                                                                                           'order' => $order,
                                                                                           'payment' => $this]);
        }

        public function clientPostHandler(array $data) {

            self::verify($data);

            return ['order' => (int)$data['pg_order_id'],
                    'amount' => floatval($data['pg_amount']),
                    'currency' => (string)$data['pg_currency']];
        }

        public function sendConfirmRequest(array $data, $returnUrl) {

            $this->validateClientPostData($data);

            $request = [// Має дорівнювати '21'
                        'TRTYPE' => 21,

                        // N(6..20) Має дорівнювати значенню поля ORDER відповіді на авторизаційний запит.
                        'ORDER' => $data['ORDER'],

                        // N(1..12) Має дорівнювати або бути менше ніж значення поля AMOUNT відповіді на авторизаційний запит.
                        'AMOUNT' => $data['AMOUNT'],

                        //  S(3) Має дорівнювати значенню поля CURRENCY відповіді на авторизаційний запит.
                        'CURRENCY' => $data['CURRENCY'],

                        // N(12) Має дорівнювати значенню поля RRN відповіді на авторизаційний запит.
                        'RRN' => $data['RRN'],

                        // HN(1..32) Має дорівнювати значенню поля INT_REF відповіді на авторизаційний запит.
                        'INT_REF' => $data['INT_REF'],

                        // S(8) Дорівнює значенню поля TERMINAL авторизаційного запиту.
                        'TERMINAL' => $this->terminal,

                        // N(14) Часовий штамп запиту в GMT: РРРРММДДГГХХСС.
                        // Якщо часовий штамп транзакції відрізняється від часу Системи більш ніж на 500 секунд, такий запит буде відхилено (з кодом RC = -20).
                        'TIMESTAMP' => gmdate("YmdHis", time()),

                        // HN(1..64) Випадкова величина.
                        // Має бути заповнена випадковим чином у шістнадцятиричному форматі.
                        'NONCE' => $this->createNonce(),

                        // S(1..80) E-mail Торговця для отримання додаткового повідомлення про результат виконання запиту.
                        'EMAIL' => $this->merchantEmail,

                        // S(1..250) URL-адреса Торговця для відправки результатів запиту.
                        'BACKREF' => $returnUrl,

                        // S(3) Мова сторінок Системи. Може набувати значення 'UKR', 'RUS', 'ENG'.
                        // У разі відсутності поля використовується значення 'UKR'.
                        'LANG' => $this->ARR_PROCESSING_LANG[$this->getOwner()->getLang()],];

            $request['P_SIGN'] = static::createMACSign($this->ARR_MAC_FIELDS['confirm'], $request, $this->makKey);

            $response = $this->sendPostRequest($this->processingUrl, $request);

            return ($response['curlinfo']['http_code'] && $response['curlinfo']['http_code'] < 300);
        }

        protected function validateClientPostData(array &$data) {

            $data = array_intersect_key($data, array_flip(['TERMINAL',
                                                           'TRTYPE',
                                                           'ORDER',
                                                           'DESC S',
                                                           'AMOUNT',
                                                           'CURRENCY',
                                                           'ACTION',
                                                           'RC',
                                                           'EXTCODE',
                                                           'APPROVAL',
                                                           'RRN',
                                                           'INT_REF',
                                                           'CARDBIN',
                                                           'BIN',
                                                           'PAN',
                                                           'CARDCOUNTRY',
                                                           'IP',
                                                           'TIMESTAMP',
                                                           'NONCE',
                                                           'P_SIGN']));

            // Response validation
            if (!isset($data['P_SIGN']) OR
                !self::checkMACSign($data['P_SIGN'], $this->ARR_MAC_FIELDS['clientResponse'], $data, $this->makKey)
            ) {
                throw new PaymentException('Incorrect bank response');
            }
            else if ($data['RC'] !== '00' OR !in_array($data['ACTION'], ['0',
                                                                         '1'])
            ) {
                throw new PaymentException(self::getResponseCodeTranslate($data['RC'], $this->getOwner()->getLang()));
            }
        }

        /**
         * Случайная величина, которая генерируется Программой Торговца.
         * Должна быть заполнена случайным образом 8-32 байтами в шестнадцатеричных формате.
         * @return mixed
         */
        protected function createNonce() {

            $nonce = unpack("H*r", strtoupper(substr(md5(uniqid(30)), 0, 8)));

            return array_shift($nonce);
        }

        protected static function createMACSign(array $fields, array $data, $key, $iconv = true) {

            $strHMAC = '';
            foreach ($fields as $field) {
                if (isset($data[$field])) {
                    $v = ($iconv ? iconv('utf-8', 'cp1251', $data[$field]) : $data[$field]);
                    $strHMAC .= (strlen($v) == 0 ? '-' : strlen($v) . $v);
                }
                else {
                    $strHMAC .= '-';
                }
            }

            return strtoupper(hash_hmac('sha1', $strHMAC, pack("H*", $key)));
        }

        protected static function checkMACSign($sign, array $fields, array $data, $key) {

            return $sign === self::createMACSign($fields, $data, $key, false);
        }

        protected static function getResponseCodeTranslate($rc, $lang = 'en') {

            $errors = ['-1' => ['ua' => 'Обов`язкове поле в запиті незаповнене',
                                'ru' => 'Обязательное поле в запросе незаполнено',
                                'en' => 'Mandatory field is empty',],
                       '-2' => ['ua' => 'Запит не відповідає вимогам специфікації',
                                'ru' => 'Запрос не соответствует требованиям спецификации',
                                'en' => 'Bad CGI request',],
                       '-3' => ['ua' => 'Комунікаційний сервер не відповідає або невірний формат файла відповіді',
                                'ru' => 'Коммуникационный сервер не отвечает или неверный формат отвата',
                                'en' => 'No or Invalid response received',],
                       '-4' => ['ua' => 'Відсутній зв`язок з комунікаційним сервером',
                                'ru' => 'Нет связи с коммуникационным сервером',
                                'en' => 'Server is not responding',],
                       '-5' => ['ua' => 'Зв`язок з комунікаційним сервером не конфігуровано',
                                'ru' => 'Ошибка соединения с коммуникационным сервером',
                                'en' => 'Connect failed',],
                       '-6' => ['ua' => 'Помилка конфігурації',
                                'ru' => 'Ошибка конфигурации',
                                'en' => 'e-Gateway Configuration error',],
                       '-7' => ['ua' => 'Помилкова відповідь від комунікаційного сервера',
                                'ru' => 'Ошибка ответа коммуникационного сервера',
                                'en' => 'Invalid response',],
                       '-8' => ['ua' => 'Помилка в полі "Card number"',
                                'ru' => 'Ошибка в поле "Номер карты"',
                                'en' => 'Error in card number field',],
                       '-9' => ['ua' => 'Помилка в полі "Card expiration date"',
                                'ru' => 'Ошибка в поле "Срок действия"',
                                'en' => 'Error in card expiration date field',],
                       '-10' => ['ua' => 'Помилка в полі "Amount"',
                                 'ru' => 'Ошибка в поле "Стоимость"',
                                 'en' => 'Error in amount field',],
                       '-11' => ['ua' => 'Помилка в полі "Currency"',
                                 'ru' => 'Ошибка в поле "Валюта"',
                                 'en' => 'Error in currency field',],
                       '-12' => ['ua' => 'Помилка в полі "Merchant ID"',
                                 'ru' => 'Неверно указан номер продавца',
                                 'en' => 'Error in merchant terminal field',],
                       '-13' => ['ua' => 'IP-адреса не така як очікується',
                                 'ru' => 'IP-адрес не такой как ожидается',
                                 'en' => 'Unknown referrer',],
                       '-15' => ['ua' => 'Помилка в полі "RRN"',
                                 'ru' => 'Ошибка в поле RRN',
                                 'en' => 'Invalid Retrieval reference number',],
                       '-16' => ['ua' => 'Термінал тимчасово заблоковано, спробуйте ще раз.',
                                 'ru' => 'Терминал временно заблокирован, попробуйте еще раз',
                                 'en' => 'Terminal is locked, please try again',],
                       '-17' => ['ua' => 'Доступ заборонено/Невірний МАС-підпис',
                                 'ru' => 'Доступ запрещен',
                                 'en' => 'Access denied',],
                       '-18' => ['ua' => 'Помилка в CVC2 або в описі CVC2',
                                 'ru' => 'Ошибка в CVC2 или в описании CVC2',
                                 'en' => 'Error in CVC2 or CVC2 Description fields',],
                       '-19' => ['ua' => 'Помилка аутентифікації',
                                 'ru' => 'Ошибка аутентификации',
                                 'en' => 'Authentication failed',],
                       '-20' => ['ua' => 'Перевищено час проведення транзакції',
                                 'ru' => 'Превышено время проведения транзакции',
                                 'en' => 'Expired transaction',],
                       '-21' => ['ua' => 'Дублікат транзакції',
                                 'ru' => 'Дубликат транзакции',
                                 'en' => 'Duplicate transaction',],
                       '-22' => ['ua' => 'Помилка при аутентифікації клієнта',
                                 'ru' => 'Ошибка при атентификации клиента',
                                 'en' => 'Error in the request information required for client identification'],

                       '01' => ['ua' => 'Зверніться до емітента картки',
                                'en' => 'Call your bank',],
                       '02' => ['ua' => 'Зверніться до емітента картки - спеціальні умови',
                                'en' => 'Call your bank',],
                       '03' => ['ua' => 'Відмовити, підприємство не приймає даний вид карт',
                                'en' => 'Invalid merchant',],
                       '04' => ['ua' => 'Відмовити, картка заблокована',
                                'en' => 'Your card is restricted',],
                       '05' => ['ua' => 'Відмовити, операція відхилена',
                                'en' => 'Transaction declined',],
                       '06' => ['ua' => 'Помилка - повторіть запит',
                                'en' => 'Error - retry',],
                       '07' => ['ua' => 'Відмовити, картка заблокована',
                                'en' => 'Your card is disabled',],
                       '08' => ['ua' => 'Необхідна додаткова ідентифікація',
                                'en' => 'Additional identification required',],
                       '09' => ['ua' => 'Запит в процесі обробки',
                                'en' => 'Request in progress',],
                       '10' => ['ua' => 'Підтвердити для часткової суми операції',
                                'en' => 'Partially approved',],
                       '11' => ['ua' => 'Підтвердити для особливо важливої персони (VIP)',
                                'en' => 'Approved (VIP)',],
                       '12' => ['ua' => 'Відмовити, невідомий тип операції',
                                'en' => 'Invalid transaction',],
                       '13' => ['ua' => 'Відмовити, некоректна сума операції',
                                'en' => 'Invalid amount',],
                       '14' => ['ua' => 'Відмовити, картку не знайдено',
                                'en' => 'No such card',],
                       '15' => ['ua' => 'Відмовити, емітент не існує',
                                'en' => 'No such card/issuer',],
                       '16' => ['ua' => 'Підтвердити, поновити третю доріжку картки',
                                'en' => 'Approved, update track 3',],
                       '17' => ['ua' => 'Відмовити, відмова користувача',
                                'en' => 'Customer cancellation',],
                       '18' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'Customer dispute',],
                       '19' => ['ua' => 'Відмовити, повторити операцію',
                                'en' => 'Re-enter transaction',],
                       '20' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'Invalid response',],
                       '21' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'No action taken',],
                       '22' => ['ua' => 'Помилка в роботі системи',
                                'en' => 'Suspected malfunction',],
                       '23' => ['ua' => 'Відмовити, неакцептовані витрати операції',
                                'en' => 'Unacceptable fee',],
                       '24' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'Update not supported',],
                       '25' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'No such record',],
                       '26' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'Duplicate update/replaced',],
                       '27' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'Field update error',],
                       '28' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'File locked out',],
                       '29' => ['ua' => 'Помилка, зв`яжіться з центром обробки',
                                'en' => 'Error, contact acquirer',],
                       '30' => ['ua' => 'Відмовити, помилка в форматі запиту',
                                'en' => 'Format error',],
                       '31' => ['ua' => 'Відмовити, емітент тимчасово відключився',
                                'en' => 'Issuer signed-off',],
                       '32' => ['ua' => 'Часткове закінчення',
                                'en' => 'Completed partially',],
                       '33' => ['ua' => 'Відмовити, термін дії картки вичерпано',
                                'ru' => 'Срок действия карты истек',
                                'en' => 'Expired card',],
                       '34' => ['ua' => 'Відмовити, підозра у шахрайстві',
                                'en' => 'Suspected fraud',],
                       '35' => ['ua' => 'Відмовити, підприємству зв`язатись з емітентом',
                                'en' => 'Acceptor contact acquirer',],
                       '36' => ['ua' => 'Відмовити, картка блокована',
                                'en' => 'Restricted card',],
                       '37' => ['ua' => 'Відмовити, зв`яжіться зі своїм банком',
                                'en' => 'Call your bank',],
                       '38' => ['ua' => 'Відмовити, перевищено кількість спроб вводу ПІН',
                                'en' => 'PIN tries exceeded',],
                       '39' => ['ua' => 'Відмовити, кредитного рахунку немає',
                                'en' => 'No credit account',],
                       '40' => ['ua' => 'Відмовити, функція не підтримується',
                                'en' => 'Function not supported',],
                       '41' => ['ua' => 'Відмовити, картка загублена',
                                'ru' => 'Отказать, карта заявлена потерянной',
                                'en' => 'Lost card',],
                       '42' => ['ua' => 'Відмовити, універсального рахунку немає',
                                'en' => 'No universal account',],
                       '43' => ['ua' => 'Відмовити, картку викрадено',
                                'en' => 'Stolen card',],
                       '44' => ['ua' => 'Відмовити, інвестиційного рахунку немає',
                                'en' => 'No investment account',],
                       '51' => ['ua' => 'Відмовити, недостатньо коштів',
                                'en' => 'Not sufficient funds',],
                       '52' => ['ua' => 'Відмовити, чекового рахунку немає',
                                'en' => 'No chequing account',],
                       '53' => ['ua' => 'Відмовити, ощадного рахунку немає',
                                'en' => 'No savings account',],
                       '54' => ['ua' => 'Відмовити, термін дії картки вичерпано',
                                'ru' => 'Срок действия карты истек',
                                'en' => 'Expired card',],
                       '55' => ['ua' => 'Відмовити, некоректний ПІН',
                                'en' => 'Incorrect PIN',],
                       '56' => ['ua' => 'Відмовити, інформація про картку відсутня',
                                'en' => 'No card record',],
                       '57' => ['ua' => 'Відмовити, операцію не дозволено',
                                'en' => 'Not permitted to client',],
                       '58' => ['ua' => 'Відмовити, невідомий тип картки',
                                'en' => 'Not permitted to merchant',],
                       '59' => ['ua' => 'Відмовити, невірний CVC або термін дії картки',
                                'en' => 'Suspected fraud',],
                       '60' => ['ua' => 'Відмовити, підприємству зв`язатись з центром обробки',
                                'en' => 'Acceptor call acquirer',],
                       '61' => ['ua' => 'Відмовити, перевищено ліміт суми операцій',
                                'ru' => 'Отказано, привышен лимит сумы операции',
                                'en' => 'Exceeds amount limit',],
                       '62' => ['ua' => 'Відмовити, картка блокована',
                                'en' => 'Restricted card',],
                       '63' => ['ua' => 'Помилка, порушення безпеки системи',
                                'en' => 'Security violation',],
                       '64' => ['ua' => 'Відмовити, невірна оригінальна сума операції',
                                'en' => 'Wrong original amount',],
                       '65' => ['ua' => 'Відмовити, перевищено ліміт повторень операції',
                                'en' => 'Exceeds frequency limit',],
                       '66' => ['ua' => 'Відмовити, підприємству зв`язатись з центром обробки',
                                'en' => 'Acceptor call acquirer',],
                       '67' => ['ua' => 'Відмовити, якщо операція в АТМ',
                                'en' => 'Pick up at ATM',],
                       '68' => ['ua' => 'Помилка, немає відповіді у відведений час',
                                'en' => 'Reply received too late',],
                       '75' => ['ua' => 'Відмовити, перевищено кількість спроб вводу ПІН',
                                'en' => 'PIN tries exceeded',],
                       '76' => ['ua' => 'Відмовити, невірний ПІН, перевищено кількість спроб',
                                'en' => 'Wrong PIN,tries exceeded',],
                       '77' => ['ua' => 'Помилка, неприпустимий код відповіді',
                                'en' => 'Wrong Reference No.',],
                       '79' => ['ua' => 'Помилка, вже відреверсовано',
                                'en' => 'Already reversed',],
                       '80' => ['ua' => 'Відмовити, помилка авторизаційної мережі',
                                'en' => 'Network error',],
                       '81' => ['ua' => 'Відмовити, помилка зовнішньої мережі',
                                'en' => 'Foreign network error',],
                       '82' => ['ua' => 'Відмовити, тайм-аут мережі зв`язку / Невірний CVC',
                                'en' => 'Time-out at issuer',],
                       '83' => ['ua' => 'Відмовити, помилка операції',
                                'en' => 'Transaction failed26',],
                       '84' => ['ua' => 'Відмовити, перевищено час преавторизації',
                                'en' => 'Pre-authorization timed out',],
                       '85' => ['ua' => 'Відмовити, необхідна перевірка рахунку',
                                'en' => 'Account verification required',],
                       '86' => ['ua' => 'Відмовити, перевірка ПІН неможлива',
                                'en' => 'Unable to verify PIN',],
                       '88' => ['ua' => 'Відмовити, помилка криптографії',
                                'en' => 'Cryptographic failure',],
                       '89' => ['ua' => 'Відмовити, помилка аутентифікації',
                                'en' => 'Authentication failure',],
                       '90' => ['ua' => 'Відмовити, повторіть через деякий час',
                                'en' => 'Cutoff is in progress',],
                       '91' => ['ua' => 'Відмовити, емітент чи вузол комутації недоступний',
                                'en' => 'Issuer unavailable',],
                       '92' => ['ua' => 'Відмовити, неможлива адресація запиту',
                                'en' => 'Router unavailable',],
                       '93' => ['ua' => 'Відмовити, порушення закону',
                                'en' => 'Violation of law',],
                       '94' => ['ua' => 'Відмовити, повторний запит',
                                'en' => 'Duplicate transmission',],
                       '95' => ['ua' => 'Відмовити, помилка узгодження',
                                'en' => 'Reconcile error',],
                       '96' => ['ua' => 'Відмовити, помилка в роботі системи',
                                'en' => 'System malfunction',],];
            if (isset($errors[$rc])) {
                return (isset($errors[$rc][$lang]) ? $errors[$rc][$lang] : $errors[$rc]['en']);
            }
            else {
                return 'Incorrect response code';
            }
        }
    }

<?php
    namespace SORApp\Services\Servio;

    use SORApp\Components\App;
    use SORApp\Components\BaseComponent;

    /**
     * Servio service provider
     *
     * Allowed methods:
     * @see For detail see "Protocol HMS Reservation Service with PHP-client" document
     *
     * @method bool ping()
     * @see SORApp\Services\Servio\Methods\Ping::run()
     *
     * @method array|bool getCompanyInfo()
     * @see \SORApp\Services\Servio\Methods\GetCompanyInfo::run()
     *
     * @method array|bool getReservationInfo()
     * @see \SORApp\Services\Servio\Methods\GetReservationInfo::run($guestId)
     *
     * @method array|bool getClientDocuments()
     * @see \SORApp\Services\Servio\Methods\getClientDocuments::run()
     *
     * @method array|bool setClientDocuments()
     * @see \SORApp\Services\Servio\Methods\setClientDocuments::run()
     *
     * @method array|bool GetLoyaltyCardNumberInfo()
     * @see \SORApp\Services\Servio\Methods\GetLoyaltyCardNumberInfo::run()
     *
     * @method array|bool getRooms($hotelId, $dateArrival, $dateDeparture, $timeArrival, $adults, $childs, $childAges, $isExtraBedUsed, $isoLanguage, $companyCodeId)
     * @see \SORApp\Services\Servio\Methods\GetRooms::run()
     *
     * @method array|bool getGroupRooms($hotelId, $bookingRequests, $isoLanguage, $companyCodeId)
     * @see \SORApp\Services\Servio\Methods\GetGroupRooms::run()
     *
     * @method array|bool getPrices($hotelID, $companyID, $dateArrival, $dateDeparture, $timeArrival, $adults, $childs, $childAges, $isExtraBedUsed, $isoLanguage, $roomTypeIDs, $contractConditionID, $paidType, $needTransport, $isTouristTax, $loyaltyCode)
     * @see \SORApp\Services\Servio\Methods\GetPrices::run()
     *
     * @method array|bool getRoomTypesList($HotelID)
     * @see \SORApp\Services\Servio\Methods\GetRoomTypesList::run()
     *
     * @method int|bool addRoomReservation($HotelID, $DateArrival, $DateDeparture, $TimeArrival, $Adults, $Childs, $ChildAges, $IsExtraBedUsed, $GuestLastName, $GuestFirstName, $RoomTypeID, $CompanyID, $Company, $ContractConditionID, $PaidType, $Iso3Country, $Country, $Address, $Phone, $Fax, $eMail, $NeedTransport, $Comment, $ClientInfo, $IsTouristTax, $PriceListID, $LoyaltyCardTypeID, $LoyaltyCardNumber, $ContactName, $TimeDeparture)
     * @see \SORApp\Services\Servio\Methods\AddRoomReservation::run()
     *
     * @method int|array|bool addGroupRoomReservation($HotelID, $GroupName, $CompanyID, $Company, $Iso3Country, $Country, $PaidType, $ContactName, $ContactEMail, $ContactInfo, $Comment, $ClientInfo, $ContractConditionID, $RoomReservations)
     * @see \SORApp\Services\Servio\Methods\AddGroupRoomReservation::run()
     *
     * @method int|bool getAccountBill($Account, $IsoLanguage)
     * @see \SORApp\Services\Servio\Methods\GetAccountBill::run()
     *
     * @method int|bool getAccountConfirm($Account, $IsoLanguage)
     * @see \SORApp\Services\Servio\Methods\GetAccountConfirm::run()
     *
     * @method int|bool getGroupAccountBill($Account, $IsoLanguage)
     * @see \SORApp\Services\Servio\Methods\GetGroupAccountBill::run()
     *
     * @method int|bool getGroupAccountConfirm($Account, $IsoLanguage)
     * @see \SORApp\Services\Servio\Methods\GetGroupAccountConfirm::run()
     *
     * @method int|bool setReservationType($Account, $Amount, $Services)
     * @see \SORApp\Services\Servio\Methods\SetReservationType::run()
     *
     * Custom methods:
     *
     * @method bool|array searchQuery($hotelID, $arrival, $departure, $searchRooms, $lang, $companyCodeID, $contractId, $paymentType, $needTransfer, $isTouristTax, $loyaltyCode)
     * @see \SORApp\Services\Servio\Methods\SearchQuery::run()
     *
     * @author Sevastianov Andrey
     */
    class Servio extends BaseComponent {

        const DEFAULT_VERSION = '1.9';
        const DEFAULT_CURRENCY = 'UAH';

        public static $requestCount = 0;
        public static $processId;

        /** @var string protocol version */
        public $version;

        /** @var string */
        public $serverCurrency;

        public $streamTimeOut = 60;
        public $proxy;

        public $defaultServer;

        /**
         * @var array of Servio instances
         */
        protected static $_connections = [];

        /**
         * @var array of ServioMethod instances
         */
        protected static $_methods = [];

        /** @var array */
        protected static $_errors = [];

        /** @var string servio server url */
        protected $_connect;

        /** @var  string */
        protected $_accessKey;

        /** @var bool server is online */
        private $_serverPing;

        private $_curlHandle;

        /** @var bool|int Включить профилирование запросов */
        public $profilingRequests = false;

        /** @var string Куда сохранять информацию профилирования */
        public $profilingDir;

        /** @var array Массов с информацией о профилировании запросов */
        protected static $_profiling = [];

        /** @var bool|int Логирование запросов */
        public $loggingRequests = false;

        /** @var string Куда сохранять логи */
        public $loggingDir;

        /** @var array Логи запросов */
        protected static $_logging = [];

        /**
         * getConnection - get active connection instance to needed servio server and protocol version
         *
         * @param null|string url
         * @param null|string $key
         * @param null|string $version
         * @param null|string $currency
         *
         * @return self
         */
        public static function getConnection($url = null, $key = null, $version = null, $currency = null) {

            $srv = App::getInstance()->getConfig()['@components/servio'];
            $cfg = empty($srv) ? [] : $srv['defaultServer'];

            if (empty($url))
                $url = empty($cfg['url']) ? '' : $cfg['url'];
            if (empty($key))
                $key = empty($cfg['key']) ? '' : $cfg['key'];
            if (empty($ver))
                $version = empty($cfg['version']) ? '' : $cfg['version'];
            if (empty($cur))
                $currency = empty($cfg['currency']) ? '' : $cfg['currency'];

            $connectionName = static::getHashTableKey($url, $key, $version);
            if (empty(self::$_connections[$connectionName])) {
                self::$_connections[$connectionName] = new self($url, $key, $version, $currency);
            }

            return self::$_connections[$connectionName];
        }

        /**
         * getHashTableKey method given key to use in register table
         *
         * @param string $url
         * @param string $key
         * @param null|string $ver
         *
         * @return string
         */
        protected static function getHashTableKey($url, $key, $ver = null) {

            return $url . '|' . md5($key) . '|' . ($ver ? $ver : static::DEFAULT_VERSION);
        }

        public static function getRequestKey() {

            if (self::$processId === null) {
                self::$processId = uniqid(str_replace('.', '_', microtime(true)) . '_');
            }

            return self::$processId . '_' . self::$requestCount;
        }

        /**
         * @param string $url
         * @param string $key
         * @param string $ver
         * @param string $cur
         */
        public function __construct($url, $key, $ver = self::DEFAULT_VERSION, $cur = self::DEFAULT_CURRENCY) {

            $this->_connect = rtrim($url, '/');
            $this->_accessKey = $key;
            $this->version = $ver;
            $this->serverCurrency = $cur;
        }

        /**
         * Magic request servio method
         *
         * @param string $method
         * @param array $arguments
         *
         * @return bool|array
         * @throws \ErrorException
         */
        public function __call($method, $arguments) {

            $method = trim(preg_replace('[^a-zA-Z]', '', $method));

            $methodName = static::getHashTableKey($method, $this->_accessKey, $this->version);
            if (!array_key_exists($methodName, self::$_methods)) {
                $methodClassName = '\\SORApp\\Services\\Servio\\Methods\\' . ucfirst($method);

                if (!class_exists($methodClassName)) {
                    throw new \ErrorException('Undefined method "' . $method . '" in servio protocol version ' . $this->version);
                }

                self::$_methods[$methodName] = new $methodClassName($this);
            }

            if ($method != 'ping') {
                if (is_null($this->_serverPing)) {
                    $this->_serverPing = $this->ping();
                }

                if (!$this->_serverPing) {
                    throw new \ErrorException('Servio server is down');
                }
            }

            return call_user_func_array([
                self::$_methods[$methodName],
                'run'], [$arguments]);
        }

        /**
         * Метод получает на вход название РПЦ метода и массив проверенных и в правльном порядке подготовленных параметров.
         *
         * _TODO: Использовать одно соединение
         * _TODO: Если использовать curl мы сможем делать несколько асинхронных запросов
         *
         * @param string $servioMethod - метод api модуля сервио
         * @param array $servioData - массив с предпроверенными параметрами
         * @param int $timeout - таймаут ожидания ответа, сек
         * @param int $options - параметры упаковки в JSON
         *
         * @return array массив ответа
         */
        public function execute($servioMethod, $servioData, $timeout = -1, $options = -1) {

            self::$requestCount++;
            $timeStart = microtime(true);

            if (version_compare($this->version, '1.9') < 0) {

                $result = $this->requestXMLRpc($servioMethod, $servioData);

                self::$_logging[self::$requestCount] = ' ▪ ' . $servioMethod . PHP_EOL . '   ► ' .
                                                       json_encode($servioData, JSON_UNESCAPED_UNICODE) . PHP_EOL . '   ◄ ' .
                                                       substr(json_encode($result, JSON_UNESCAPED_UNICODE), 0, 4096) . PHP_EOL;
            }
            else {

                if ($timeout == -1)
                    $timeout = $this->streamTimeOut;
                if ($options == -1)
                    $options = JSON_UNESCAPED_UNICODE;
                $result = $this->requestJsonRpc($servioMethod, $servioData, $timeout, $options);

                self::$_profiling[self::$requestCount] = [
                    $servioMethod,
                    $result['curlinfo']['connect_time'],
                    $result['curlinfo']['total_time'],
                    microtime(true) - $timeStart];

                unset($result['curlinfo'], $timeStart);
            }

            return $result;
        }

        /**
         * Отправка запроса к серверу с модулем резервирования версии от 1.9 включительно
         *
         * @param string $servioMethod - метод api модуля сервио
         * @param array $servioData - массив с предпроверенными параметрами
         * @param int $timeout - таймаут ожидания ответа, сек
         * @param int $options - параметры упаковки в JSON
         *
         * @return array|mixed
         */
        protected function requestJsonRpc($servioMethod, $servioData, $timeout, $options) {

            $request = json_encode($servioData, $options);

            if ($this->_curlHandle === null)
                $this->_curlHandle = curl_init();

            curl_setopt_array($this->_curlHandle, [
                    CURLOPT_VERBOSE => TRUE,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_TIMEOUT => $timeout,
                    CURLOPT_POST => TRUE,
                    CURLOPT_POSTFIELDS => $request,
                    CURLOPT_PROXY => !$this->proxy ? FALSE : $this->proxy,
                    CURLOPT_URL => $this->_connect . '/' . $servioMethod,
                    CURLOPT_HTTPHEADER => [
                        'Accept:',
                        'AccessToken: ' . $this->_accessKey,
                        'Content-Type: application/json; charset=utf-8',
                        'Expect: 100-continue',
                        'Connection: Keep-Alive'],]);

            $response = curl_exec($this->_curlHandle);

            if (empty($response)) {
                $response = curl_error($this->_curlHandle);
                $result = [
                    'Result' => 1,
                    'Error' => $response];
            }
            else {
                $httpCode = curl_getinfo($this->_curlHandle, CURLINFO_HTTP_CODE);
                if ($httpCode != 200) {
                    $result = [
                        'Result' => 1,
                        'Error' => 'Request failed, status code: ' . $httpCode . ', response: ' . $response];
                }
                else {
                    if (($result = json_decode($response, true)) === null) {
                        $result = [
                            'Result' => 1,
                            'Error' => 'Incorrect response type: ' . $response];
                    }
                    else {
                        if (!is_array($result)) {
                            $result = ['scalarResponse' => $result];
                        }
                    }
                }
            }

            self::$_logging[self::$requestCount] = ' ▪ ' . $servioMethod . PHP_EOL . '   ► ' . $request . PHP_EOL . '   ◄ ' .
                                                   (isset($httpCode) ? '[' . $httpCode . '] ' : '') . substr($response, 0, 4096) . PHP_EOL;

            $result['jsonRequest'] = $request;
            $result['jsonResponse'] = $response;
            $result['curlinfo'] = curl_getinfo($this->_curlHandle);

            return $result;
        }

        /**
         * Отправка запроса к серверу с модулем резервирования версии до 1.8
         *
         * @param string $servioMethod
         * @param array|mixed $servioData
         *
         * @return array|mixed
         */
        protected function requestXMLRpc($servioMethod, $servioData) {

            $request = xmlrpc_encode_request($servioMethod, array_values($servioData), [
                    'output_type' => 'xml',
                    'encoding' => 'UTF-8',
                    'escaping' => 'markup',
                    'verbosity' => 'no_white_space']);

            $contextOptions = [
                'http' => [
                    'method' => "POST",
                    'header' => [
                        'Content-Type: text/xml',
                        'AccessToken: ' . $this->_accessKey],
                    'content' => $request,
                    'timeout' => $this->streamTimeOut,]];

            if ($this->proxy)
                $contextOptions['http']['proxy'] = $this->proxy;

            $context = stream_context_create($contextOptions);

            $response = @file_get_contents($this->_connect, false, $context);
            $result = xmlrpc_decode($response, 'UTF-8');

            if (!is_array($result)) {
                $result = ['scalarResponse' => $result];
            }

            $result['xmlRequest'] = $request;
            $result['xmlResponse'] = $response;

            return $result;
        }

        public function addError(ServioException $exception, $method = false) {

            array_push(static::$_errors, [
                $method,
                $exception]);
        }

        public function getErrors() {

            return static::$_errors;
        }

        public function getLastError() {

            return static::$_errors[count(static::$_errors) - 1];
        }

        /**
         * Сохраняем отладочную инфомацию и закрываем соединение с сервером
         */
        public function __destruct() {

            if ($this->_curlHandle !== null) {
                curl_close($this->_curlHandle);
            }

            if ($this->profilingRequests && count(self::$_profiling) > intval($this->profilingRequests)) {
                $total = [
                    'ct' => 0,
                    'tt' => 0,
                    'total' => 0];
                $log = [];
                foreach (self::$_profiling as $request) {
                    $log[] = $request[0] . '|ct:' . $request[1] . '|tt:' . $request[2] . '|total:' . $request[3];
                    $total['ct'] += $request[1];
                    $total['tt'] += $request[2];
                    $total['total'] += $request[3];
                }

                $content = date('Y-m-d H:i:s') . ' | ' . filter_input(INPUT_SERVER, 'REMOTE_ADDR') . ' | ' . $this->_connect . PHP_EOL .
                           'Requests: ' . count(self::$_profiling) . ' | Connection time: ' . $total['ct'] . ' | Transfer time: ' .
                           $total['tt'] . ' | Total time:' . $total['total'] . PHP_EOL . ' - ' . implode(PHP_EOL . ' - ', $log);
                App::saveLog('profiling_', $this->profilingDir, $content);
                self::$_profiling = [];
            }

            if ($this->loggingRequests && count(self::$_logging) > intval($this->loggingRequests)) {
                $content = date('Y-m-d H:i:s') . ' | ' . filter_input(INPUT_SERVER, 'REMOTE_ADDR') . ' | ' . $this->_connect . PHP_EOL .
                           implode(PHP_EOL, self::$_logging);
                App::saveLog('logging_', $this->loggingDir, $content);
                self::$_logging = [];
            }
        }
    }

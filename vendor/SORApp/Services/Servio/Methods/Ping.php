<?php

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioMethod;

    /**
     * @author Sevastionov Andrey
     */
    class Ping extends ServioMethod {

        public $versionMap = [
            [
                '>=',
                '1.9',
                'json'],
            [
                '>=',
                '1.7',
                'xml'],];

        public function json() {

            $scalarRequest = (int)time();
            $response = $this->owner->execute('Ping', $scalarRequest);

            return (is_array($response) AND array_key_exists('scalarResponse', $response) AND $response['scalarResponse'] == $scalarRequest);
        }

        public function xml() {

            $scalarRequest = (int)time();
            $response = $this->owner->execute('Ping', [$scalarRequest]);

            return (is_array($response) AND array_key_exists('scalarResponse', $response) AND $response['scalarResponse'] == $scalarRequest);
        }
    }

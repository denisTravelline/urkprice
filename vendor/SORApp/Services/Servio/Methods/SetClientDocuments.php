<?php
    /**
     * Created by PhpStorm.
     * User: vladimir
     * Date: 19.08.15
     * Time: 15:53
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class SetClientDocuments extends ServioMethod {

        public function run($arr = []) {

            try {

                $result = $this->owner->execute('SetClientDocuments', count($arr) ? $arr[0] : []);

                //var_dump($result);

                $this->checkError($result);

                return true;
            }
            catch (ServioException $e) {
                return false;
            }
        }

    }
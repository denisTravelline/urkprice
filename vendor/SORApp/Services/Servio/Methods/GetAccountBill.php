<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 18.09.14 15:44
     */

    namespace SORApp\Services\Servio\Methods;

    class GetAccountBill extends AbstractGetAccountDocument {

        protected function getServioMethod() {

            return 'GetAccountBill';
        }
    }

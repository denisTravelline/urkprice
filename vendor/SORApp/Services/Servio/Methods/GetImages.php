<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 24.11.14 11:47
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetImages extends ServioMethod {

        public $versionMap = [
            [
                '>=',
                '1.9',
                'deprecated'],
            [
                '>=',
                '1.7',
                'version_170'],];

        public function deprecated() {

            return false;
        }

        public function version_170($args = []) {

            try {
                if (!count($args)) {
                    return false;
                }

                $result = $this->owner->execute('GetImages', [
                    'RoomTypeIDs' => (!is_array(current($args)) ? [current($args)] : array_values(current($args))),]);
                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                    'Count',
                    'RoomTypeIDs',
                    'Images',
                    'ImageHashs']));
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }

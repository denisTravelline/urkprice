<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetLoyaltyCardTypes extends ServioMethod {

        public function run($args = []) {

            try {
                $result = $this->owner->execute('GetLoyaltyCardTypes', []);
                $this->checkError($result);

                return (array_key_exists('LoyaltyCardTypes', $result) ? $result['LoyaltyCardTypes'] : false);
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }

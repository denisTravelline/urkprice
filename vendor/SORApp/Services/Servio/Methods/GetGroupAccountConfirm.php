<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 18.09.14 15:45
     */

    namespace SORApp\Services\Servio\Methods;

    class GetGroupAccountConfirm extends AbstractGetAccountDocument {

        protected function getServioMethod() {

            return 'GetGroupAccountConfirm';
        }
    }

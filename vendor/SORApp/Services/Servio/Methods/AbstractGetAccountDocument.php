<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 18.09.14 15:40
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    abstract class AbstractGetAccountDocument extends ServioMethod {

        abstract protected function getServioMethod();

        public function run($args = []) {

            list($Account, $IsoLanguage) = $args;
            try {

                $result = $this->owner->execute($this->getServioMethod(), [
                    'Account' => (int)$Account,
                    'IsoLanguage' => (string)$IsoLanguage,]);

                $this->checkError($result);
                if (!isset($result['DocumentID'])) {
                    throw new ServioException('Incorrect response from Servio HMS');
                }

                return $result['DocumentID'];
            }
            catch (ServioException $e) {

                return false;
            }
        }
    }

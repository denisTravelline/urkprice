<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 21.10.14 16:05
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class SetReservationType extends ServioMethod {

        public $versionMap = [
            [
                '>=',
                '1.9',
                'run19'],
            [
                '>=',
                '1.7',
                'run17']];

        public function run19($args = []) {

            array_push($args, true);

            return $this->exec($args);
        }

        public function run17($args = []) {

            array_push($args, false);

            return $this->exec($args);
        }

        public function exec($args = []) {

            $account = array_shift($args);
            $amount = array_shift($args);
            $services = array_shift($args);
            $group = array_shift($args);
            $ver19 = array_shift($args);
            try {

                $params = [
                    'Account' => (int)$account,
                    'Amount' => (float)$amount];
                if (!!$ver19) {
                    $params['Services'] = $services;
                }

                $result = $this->owner->execute(!$group ? 'SetReservationType' : 'SetGroupReservationType', $params, 300, JSON_OBJECT_AS_ARRAY |
                                                                                                                          JSON_UNESCAPED_UNICODE);

                $this->checkError($result);

                return true;
            }
            catch (ServioException $e) {
                return false;
            }
        }

    }

<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 28.11.14 15:16 
 */

namespace SORApp\Services\Servio\Methods;


use SORApp\Models\Booking\SearchRequest;
use SORApp\Services\Servio\ServioMethod;

class SearchQuery extends ServioMethod
{
    public function run($args = array())
    {
        $request = array();
        list(
            $request['hotelID'],
            $request['arrival'],
            $request['departure'],
            $request['searchRooms'],
            $request['lang'],
            $request['companyID'],
            $request['companyCodeID'],
            $request['contractId'],
            $request['paymentType'],
            $request['needTransfer'],
            $request['isTouristTax']
        ) = $args;

        // Prepare datetime format for servio reservation server
        $dateTime = new \DateTime($request['arrival']);
        $request['arrival'] = $dateTime->format('d.m.Y\TH:i');

        $dateTime = new \DateTime($request['departure']);
        $request['departure'] = $dateTime->format('d.m.Y\TH:i');

        $response = false;

        switch ( count($request['searchRooms']) ) {
            // Поиск одного номера
            case 1:
                $searchRoomItem = current($request['searchRooms']);
                $rooms = $this->owner->getRooms(
                    $request['hotelID'],
                    $request['arrival'],
                    $request['departure'],
                    $request['arrival'],
                    $searchRoomItem['adults'],
                    count($searchRoomItem['childrenAges']),
                    $searchRoomItem['childrenAges'],
                    $searchRoomItem['isExtraBedUsed'],
                    $request['lang'],
                    $request['companyCodeID']
                );

                if ( $rooms !== false AND is_array($rooms) ) {
                    $response = $this->normalizeGroupRooms(array($rooms), $request);
                }
                break;

            // Поиск номеров для групповой бронировки
            default:
                $bookingRequests = array();
                foreach ( $request['searchRooms'] as $searchRoomItem ) {
                    array_push($bookingRequests, array(
                            $request['arrival'],
                            $request['departure'],
                            $request['arrival'],
                            $searchRoomItem['adults'],
                            count($searchRoomItem['childrenAges']),
                            $searchRoomItem['childrenAges'],
                            $searchRoomItem['isExtraBedUsed']
                        ));
                }

                $groupRooms = $this->owner->getGroupRooms($request['hotelID'], $bookingRequests, $request['lang'], $request['companyCodeID']);

                if ( $groupRooms !== false AND is_array($groupRooms)
                    AND array_key_exists('BookingRequestAnswers', $groupRooms)
                    AND is_array($groupRooms['BookingRequestAnswers'])
                    AND count($groupRooms['BookingRequestAnswers'])
                ) {
                    $response = $this->normalizeGroupRooms($groupRooms['BookingRequestAnswers'], $request);
                }

                break;
        }

        return $response;
    }

    /**
     * Нормализация списка групп номеров
     *
     * @param array $bookingRequestAnswer Найденные варианты номеров
     * @param array $request Данные поиска
     *
     * @throws \ErrorException
     * @return array
     */
    protected function normalizeGroupRooms(array $bookingRequestAnswer, $request)
    {
        $response = array(
            'roomsGroups' => array(),
            'contracts' => array(),
        );

        $maxUsed = 0;
        $currentRoom = 0;
        $now = new \DateTime('now');

        foreach ( $bookingRequestAnswer as $roomTypeGroup ) {

            if ( ! array_key_exists($currentRoom, $request['searchRooms']) ) {
                throw new \ErrorException('Incorrect room relation');
            }

            if ( is_array($roomTypeGroup) AND array_key_exists('RoomTypes', $roomTypeGroup) AND count($roomTypeGroup['RoomTypes'])) {

                // Текущая группа номеров
                $group = array();
                foreach ( $roomTypeGroup['RoomTypes'] as $room ) {
                    if ( is_array($room) AND array_key_exists('ID', $room) /* AND array_key_exists($room['ID'], $response->rooms) *//* AND $room['FreeRoom'] > 0 */ ) {
                        $group[$room['ID']] = $room;
                    }
                }

                if ( count($group) ) {
                    // Доступные условия
                    if ( array_key_exists('ContractConditions', $roomTypeGroup) AND count($roomTypeGroup['ContractConditions']) ) {
                        foreach ( $roomTypeGroup['ContractConditions'] as $condition ) {
                            if ( ! array_key_exists($condition['ContractConditionID'], $response['contracts']) ) {
                                $response['contracts'][$condition['ContractConditionID']] = array(
                                    // TODO: Удалить костыль когда пофиксят сервисты название переменной
                                    'ContractConditionName' => ( isset($condition['ContractConditionName ']) ? $condition['ContractConditionName '] : $condition['ContractConditionName']),
                                    'PaidTypes' => $condition['PaidTypes'],
                                    'ExpiredDate' => ( isset($condition['ExpiredDate']) ? $condition['ExpiredDate'] : false),
                                    'ExpiredDays' => ( isset($condition['ExpiredDate']) ? $now->diff(new \DateTime($condition['ExpiredDate']))->days : 999 ),
                                    'used' => 1,
                                );
                            } else {
                                $response['contracts'][$condition['ContractConditionID']]['used']++;
                            }

                            if ( $response['contracts'][$condition['ContractConditionID']]['used'] > $maxUsed ) {
                                $maxUsed = $response['contracts'][$condition['ContractConditionID']]['used'];
                            }
                        }

                    }

                    $response['roomsGroups'][] = $group;
                }
            }

            $currentRoom++;
        }

        if ( count($response['contracts']) ) {
            // Ищем только пересекающиеся контракты для всех групп номеров
            foreach ( $response['contracts'] as $id => $condition ) {
                if ( $condition['used'] < $maxUsed ) {
                    unset($response['contracts'][$id]);
                } else {
                    unset($response['contracts'][$id]['used']);
                }
            }

            // Принудительно устанавливаем первый доступный контракт
            if ( $request['contractId'] <= 0 ) {
                $request['contractId'] = key($response['contracts']);
            }
        } else {
            $response['contracts'][0] = array(
                'ContractConditionName' => 'Общие условия',
                'PaidTypes' => array(
                    SearchRequest::PAYMENT_TYPE_CASH,
                    SearchRequest::PAYMENT_TYPE_ONLINE
                )
            );
        }

        if ( count($response['roomsGroups']) ) {
            $currentRoom = 0;
            foreach ( $response['roomsGroups'] as &$group ) {
                // Получаем цены на номера группы
                $this->getPrices($group, $currentRoom, $request);
                $currentRoom++;
            }
        }


        return $response;
    }


    protected function getPrices(&$group, $currentRoom, $request)
    {
        $roomIDs = array();
        foreach ( $group as $id => $room ) {
            if ( $room['FreeRoom'] > 0 ) {
                array_push($roomIDs, $id);
            }
        }

        if ( ! count($roomIDs) ) {
            return;
        }

        $searchRoomsItem = $request['searchRooms'][$currentRoom];

        $prices = $this->owner->getPrices(
            $request['hotelID'],
            $request['companyID'],
            $request['arrival'],
            $request['departure'],
            $request['arrival'],
            $searchRoomsItem['adults'],
            count($searchRoomsItem['childrenAges']),
            $searchRoomsItem['childrenAges'],
            $searchRoomsItem['isExtraBedUsed'],
            $request['lang'],
            $roomIDs,
            $request['contractId'],
            $request['paymentType'],
            $request['needTransfer'],
            $request['isTouristTax'],
            ''
        );


        if ( $prices AND is_array($prices) AND array_key_exists('PriceLists', $prices) AND is_array($prices['PriceLists']) AND count($prices['PriceLists']) ) {
            foreach ( $prices['PriceLists'] as $priceList ) {
                if ( array_key_exists('RoomTypes', $priceList) AND count($priceList['RoomTypes']) ) {
                    foreach ( $priceList['RoomTypes'] as $roomType ) {
                        if ( array_key_exists('ID', $roomType) AND array_key_exists($roomType['ID'], $group) AND array_key_exists('Services', $roomType) AND count($roomType['Services']) ) {
                            $group[$roomType['ID']]['prices'] = array(
                                'priceListID' => $priceList['PriceListID'],
                                'isSpecRate' => $priceList['IsSpecRate'],
                                'isNonReturnRate' => $priceList['IsNonReturnRate'],
                                'currency' => $this->owner->serverCurrency,
                            );
                            list($group[$roomType['ID']]['prices']['services'], $group[$roomType['ID']]['prices']['PriceTotal'])
                                = $this->preparePriceServices($roomType['Services']);
                        }
                    }
                }
            }
        }
    }


    protected function preparePriceServices($services)
    {
        $prices = array();
        $priceTotal = 0;

        foreach ( $services as $service ) {
            if ( is_array($service['PriceDates']) AND count($service['PriceDates']) ) {
                $currentPrice = null;
                $currentPriceStartDate = null;
                $currentPriceCount = 0;

                $service['PriceTotal'] = 0;
                $service['PriceDatesGroups'] = array();

                foreach ( $service['PriceDates'] as $priceDate ) {
                    if ( $currentPrice == $priceDate['Price'] ) {
                        $currentPriceCount++;
                    } else {
                        $date = new \DateTime($priceDate['Date']);
                        if ( $currentPriceCount > 0 ) {
                            $previous = clone $date;
                            if ( $currentPriceStartDate != $previous->format('Y-m-d') ) {
                                $previous->modify('previous day');
                            }
                            array_push($service['PriceDatesGroups'], array(
                                    $currentPriceStartDate,
                                    $previous->format('Y-m-d'),
                                    $currentPrice,
                                    $currentPriceCount
                                ));
                            $service['PriceTotal'] = bcadd($service['PriceTotal'], bcmul($currentPrice, $currentPriceCount, 2), 2);
                        }

                        $currentPrice = $priceDate['Price'];
                        $currentPriceStartDate = $date->format('Y-m-d');
                        $currentPriceCount = 1;
                    }
                }

                if ( $currentPriceCount > 0 ) {
                    $date = new \DateTime($priceDate['Date']);
                    array_push($service['PriceDatesGroups'], array(
                            $currentPriceStartDate,
                            $date->format('Y-m-d'),
                            $currentPrice,
                            $currentPriceCount
                        ));
                    $service['PriceTotal'] = bcadd($service['PriceTotal'], bcmul($currentPrice, $currentPriceCount, 2), 2);
                }

                $priceTotal = bcadd($priceTotal, $service['PriceTotal'], 2);
            }
            $prices[$service['ServiceID']] = $service;
        }

        return array($prices, $priceTotal);
    }
} 

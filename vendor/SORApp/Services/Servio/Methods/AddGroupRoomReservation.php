<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 17.09.14 15:02
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class AddGroupRoomReservation extends ServioMethod {

        /**
         * @param array $args
         * @return array|bool
         * @throws \ErrorException
         */
        public function run($args = []) {

            list(
                $HotelID, $GroupName,
                $CompanyID, $Company,
                $Iso3Country, $Country,
                $PaidType, $ContactName,
                $ContactEMail, $ContactInfo,
                $Comment, $ClientInfo,
                $ContractConditionID, $RoomReservations) = $args;

            try {

                array_walk($RoomReservations, [
                    $this,
                    'prepareRoomReservation']);

                $result = $this->owner->execute('AddGroupRoomReservation', [
                        'HotelID' => (int)$HotelID,
                        'GroupName' => (string)$GroupName,
                        'CompanyID' => (int)$CompanyID,
                        'Company' => (string)$Company,
                        'Iso3Country' => (string)$Iso3Country,
                        'Country' => (string)$Country,
                        'PaidType' => (int)$PaidType,
                        'ContactName' => (string)$ContactName,
                        'ContactEMail' => (string)$ContactEMail,
                        'ContactInfo' => (string)$ContactInfo,
                        'Comment' => (string)$Comment,
                        'ClientInfo' => (string)$ClientInfo,
                        'ContractConditionID' => (int)$ContractConditionID,
                        'RoomReservations' => (array)$RoomReservations]);

                $this->checkError($result);
                if (!isset($result['Account']) ||
                    !isset($result['RoomReservations'])) {
                    throw new \ErrorException('Incorrect server response');
                }

                // ▪ AddGroupRoomReservation = 200
                //   ► {"HotelID":1,"GroupName":"ГРУП","CompanyID":0,"Company":"ФОП Обрій","Iso3Country":"RUS","Country":"Россия","PaidType":200,"ContactName":"TEST TEST","ContactEMail":"aleksandr.olhovoy@raziogroup.com","ContactInfo":"Main phone: +79164564544\r\n","Comment":"","ClientInfo":"IP: 192.168.119.204:57287\r\nUser agent: Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/41.0.2217.0 Safari\/537.36\r\nRequest timestamp:1427811366","ContractConditionID":0,"RoomReservations":[{"DateArrival":"2015-04-01","DateDeparture":"2015-04-02","TimeArrival":"14:00","TimeDeparture":"2015-04-02T12:00:00","Adults":2,"Childs":0,"ChildAges":[],"IsExtraBedUsed":false,"RoomTypeID":6,"NeedTransport":0,"IsTouristTax":true,"PriceListID":631},{"DateArrival":"2015-04-01","DateDeparture":"2015-04-02","TimeArrival":"14:00","TimeDeparture":"2015-04-02T12:00:00","Adults":2,"Childs":1,"ChildAges":[10],"IsExtraBedUsed":true,"RoomTypeID":6,"NeedTransport":0,"IsTouristTax":true,"PriceListID":631}]}
                //   ◄ {"Error":"","ErrorCode":"","Result":0,"Account":4358690,"RoomReservations":[
                //          {"GuestAccount":4379582,"Adults":2,"ChildAges":[],"Childs":0,"DateArrival":"2015-04-01","DateDeparture":"2015-04-02","IsExtraBedUsed":false,"IsTouristTax":true,"NeedTransport":0,"PriceListID":631,"RoomTypeID":6,"TimeArrival":"14:00"},
                //          {"GuestAccount":4379581,"Adults":2,"ChildAges":[10],"Childs":1,"DateArrival":"2015-04-01","DateDeparture":"2015-04-02","IsExtraBedUsed":true,"IsTouristTax":true,"NeedTransport":0,"PriceListID":631,"RoomTypeID":6,"TimeArrival":"14:00"}]}

                $rooms = [];
                foreach ($result['RoomReservations'] as $room) {
                    array_push($rooms, $room['GuestAccount']);
                }

                return [$result['Account'] => $rooms];
            }
            catch (ServioException $e) {
                return false;
            }
        }

        /**
         * @param $item
         */
        public function prepareRoomReservation(&$item) {

            $item['TimeArrival'] = $this->getServioTime($item['DateArrival']);
            $item['DateArrival'] = $this->getServioDate($item['DateArrival']);
            $item['TimeDeparture'] = $this->getServioTime($item['DateDeparture']);
            $item['DateDeparture'] = $this->getServioDate($item['DateDeparture']);
        }

    }

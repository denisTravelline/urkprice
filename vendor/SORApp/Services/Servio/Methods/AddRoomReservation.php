<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 22.09.14 14:32
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class AddRoomReservation extends ServioMethod {

        /**
         * @param array $args
         * @return bool
         * @throws \ErrorException
         */
        public function run($args = []) {

            list(
                $HotelID, $DateArrival, $DateDeparture,
                $TimeArrival, $Adults, $Childs, $ChildAges,
                $IsExtraBedUsed, $GuestLastName, $GuestFirstName,
                $RoomTypeID, $CompanyID, $Company, $ContractConditionID,
                $PaidType, $Iso3Country, $Country, $Address, $Phone, $Fax,
                $eMail, $NeedTransport, $Comment, $ClientInfo, $IsTouristTax,
                $PriceListID, $LoyaltyCardTypeID, $LoyaltyCardNumber, $ContactName, $TimeDeparture) = $args;

            try {

                $result = $this->owner->execute('AddRoomReservation', [
                    'HotelID' => (int)$HotelID,
                    'DateArrival' => $this->getServioDate($DateArrival),
                    'DateDeparture' => $this->getServioDate($DateDeparture),
                    'TimeArrival' => $this->getServioTime($TimeArrival),
                    'Adults' => (int)$Adults,
                    'Childs' => (int)$Childs,
                    'ChildAges' => (array)$ChildAges,
                    'IsExtraBedUsed' => (bool)$IsExtraBedUsed,
                    'GuestLastName' => (string)$GuestLastName,
                    'GuestFirstName ' => (string)$GuestFirstName,
                    'RoomTypeID' => (int)$RoomTypeID,
                    'CompanyID' => (int)$CompanyID,
                    'Company' => (string)$Company,
                    'ContractConditionID' => (int)$ContractConditionID,
                    'PaidType' => (int)$PaidType,
                    'Iso3Country' => (string)$Iso3Country,
                    'Country' => (string)$Country,
                    'Address' => (string)$Address,
                    'Phone' => (string)$Phone,
                    'Fax' => (string)$Fax,
                    'eMail' => (string)$eMail,
                    'NeedTransport' => (int)$NeedTransport,
                    'Comment' => (string)$Comment,
                    'ClientInfo' => (string)$ClientInfo,
                    'IsTouristTax' => (int)$IsTouristTax,
                    'PriceListID' => (int)$PriceListID,
                    'LoyaltyCardTypeID' => (int)$LoyaltyCardTypeID,
                    'LoyaltyCardNumber' => $LoyaltyCardNumber,
                    // ( ! empty($LoyaltyCardNumber) ? (string) $LoyaltyCardNumber : null ),
                    'ContactName' => (string)$ContactName,
                    'TimeDeparture' => $this->getServioTime($TimeDeparture)
                ]);

                $this->checkError($result);
                if (!isset($result['Account'])) {
                    throw new \ErrorException('Incorrect server response');
                }

                return $result['Account'];
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }

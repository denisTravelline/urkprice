<?php

    /**
     * @author Alexander Olkhovoy <olkhovoy@gmail.com>
     */
    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioMethod;
    use SORApp\Services\Servio\ServioException;

    class GetReservationInfo extends ServioMethod {

        public function run($args = []) {

            list($guestId) = $args;
            try {

                $result = $this->owner->execute('GetReservationInfo', ['GuestID' => (string)$guestId]);

                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                        'Adults',
                        'Childs',
                        'DateArrival',
                        'DateDeparture',
                        'GuestName',
                        'HotelID',
                        'RoomTypeID',
                        'StatusID',
                        'StatusName']));
            }
            catch (ServioException $e) {

                return false;
            }
        }
    }

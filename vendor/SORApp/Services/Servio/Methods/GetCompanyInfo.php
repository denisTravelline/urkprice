<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetCompanyInfo extends ServioMethod {

        public function run($args = []) {

            list($id) = $args;

            try {
                $result = $this->owner->execute('GetCompanyInfo', ['CompanyCode' => (string)$id]);

                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                    'CompanyID',
                    'CompanyCodeID',
                    'CompanyName',
                    'CompanyEmail',
                    'CompanyFax',
                    'CompanyPhoneNumber']));
            }
            catch (ServioException $e) {

                return false;
            }
        }
    }

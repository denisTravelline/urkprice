<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetLoyaltyCardNumberInfo extends ServioMethod {

        public function run($args = []) {

            list($CompanyID, $LoyaltyCardTypeID, $HotelID, $LoyaltyCardNumber) = $args;

            try {
                if (is_array($LoyaltyCardTypeID)) {
                    foreach ($LoyaltyCardTypeID as $cardType) {
                        if (is_array($cardType) AND array_key_exists('LoyaltyCardTypeID', $cardType) AND ($response = $this->run([
                                $CompanyID,
                                $cardType['LoyaltyCardTypeID'],
                                $HotelID,
                                $LoyaltyCardNumber])) !== false
                        ) {
                            return $response;
                        }
                    }

                    throw new ServioException('Undefined loyalty card');
                }
                else {
                    $result = $this->owner->execute('GetLoyaltyCardNumberInfo', [
                        'CompanyID' => (int)$CompanyID,
                        'LoyaltyCardTypeID' => (int)$LoyaltyCardTypeID,
                        'HotelID' => (int)$HotelID,
                        'LoyaltyCardNumber' => (string)$LoyaltyCardNumber]);

                    $this->checkError($result);

                    $response = array_intersect_key($result, array_flip([
                        'ContractConditionID',
                        'СontractConditionName',
                        'PaidTypes',
                        'GuestLastName',
                        'GuestFirstName',
                        'PriceListID',]));
                    $response['LoyaltyCardTypeID'] = (int)$LoyaltyCardTypeID;
                    $response['LoyaltyCardNumber'] = (string)$LoyaltyCardNumber;

                    return $response;
                }
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }

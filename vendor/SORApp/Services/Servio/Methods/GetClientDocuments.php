<?php
    /**
     * Created by PhpStorm.
     * User: vladimir
     * Date: 12.08.15
     * Time: 12:15
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetClientDocuments extends ServioMethod {

        public function run($args = []) {

            //list($guestCode) = $args;

            try {

                $result = $this->owner->execute('GetClientDocuments', [
                        'GuestCode' => (string)array_shift($args)]);

                //var_dump($result);

                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                        'GuestID',
                        'GuestName',
                        'DateArrival',
                        'DateDeparture',
                        'Adults',
                        'Childs',
                        'StatusName',
                        'ClientDocumentFields',
                        'ClientDocumentTypes',
                        'ClientDocuments',]));
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }
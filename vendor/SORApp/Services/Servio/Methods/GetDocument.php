<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 24.10.14 14:40
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetDocument extends ServioMethod {

        public function run($args = []) {

            try {

                $directory = SOR_APP_DIR . DIRECTORY_SEPARATOR . 'Runtime' . DIRECTORY_SEPARATOR . 'servio_documents';
                if ((!file_exists($directory) AND !@mkdir($directory, 0777, true)) OR !is_dir($directory)) {
                    throw new \ErrorException('Incorrect document directory');
                }

                $filename = $directory . DIRECTORY_SEPARATOR . 'document_' . (int)$args[1] . '_' . (int)$args[0] . '.pdf';

                if (!file_exists($filename)) {
                    $result = $this->owner->execute('GetDocument', ['DocumentID' => (int)$args[0]]);
                    $this->checkError($result);
                    if ($result['IsReady']) {
                        file_put_contents($filename, base64_decode($result['DocumentCode']));
                    }
                    else {
                        return false;
                    }
                }

                return $filename;
            }
            catch (ServioException $e) {
                return false;
            }
        }

    }

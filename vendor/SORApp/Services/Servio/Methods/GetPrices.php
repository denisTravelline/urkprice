<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 */

namespace SORApp\Services\Servio\Methods;


use SORApp\Services\Servio\ServioException;
use SORApp\Services\Servio\ServioMethod;

class GetPrices extends ServioMethod
{
    public function run($args = array())
    {
        list(
            $hotelID,
            $companyID,
            $dateArrival,
            $dateDeparture,
            $timeArrival,
            $adults,
            $childs,
            $childAges,
            $isExtraBedUsed,
            $isoLanguage,
            $roomTypeIDs,
            $contractConditionID,
            $paidType,
            $needTransport,
            $isTouristTax,
            $LPAuthCode) = $args;

        try {
            $result = $this->owner->execute(
                'GetPrices',
                array(
                    'HotelID' => (int)$hotelID,
                    'CompanyID' => (int)$companyID,
                    'DateArrival' => $this->getServioDate($dateArrival),
                    'DateDeparture' => $this->getServioDate($dateDeparture),
                    'TimeArrival' => $this->getServioTime($timeArrival),
                    'Adults' => (int)$adults,
                    'Childs' => (int)$childs,
                    'ChildAges' => (array)$childAges,
                    'IsExtraBedUsed' => (bool)$isExtraBedUsed,
                    'IsoLanguage' => (string)$isoLanguage,
                    'RoomTypeIDs' => (array)$roomTypeIDs,
                    'ContractConditionID' => (int)$contractConditionID,
                    'PaidType' => (int)$paidType,
                    'NeedTransport' => (int)$needTransport,
                    'IsTouristTax' => (int)$isTouristTax,
                    'TimeDeparture' => $this->getServioTime($dateDeparture),
                    'LPAuthCode' => (string)$LPAuthCode,
                )
            );

            $this->checkError($result);
            $this->prepareDate($result);

            return $result;
        } catch (ServioException $e) {
            return false;
        }
    }
}

<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetRooms extends ServioMethod {

        public function run($args = []) {

            list($hotelId, $dateArrival, $dateDeparture, $timeArrival, $adults, $childs, $childAges, $isExtraBedUsed, $isoLanguage, $companyCodeId) = $args;

            try {

                $result = $this->owner->execute('GetRooms', [
                    'HotelID' => (int)$hotelId,
                    'DateArrival' => $this->getServioDate($dateArrival),
                    'DateDeparture' => $this->getServioDate($dateDeparture),
                    'TimeArrival' => $this->getServioTime($timeArrival),
                    'Adults' => (int)$adults,
                    'Childs' => (int)$childs,
                    'ChildAges' => (array)$childAges,
                    'IsExtraBedUsed' => (bool)$isExtraBedUsed,
                    'IsoLanguage' => (string)$isoLanguage,
                    'CompanyCodeID' => (int)$companyCodeId,
                    'TimeDeparture' => $this->getServioTime($dateDeparture),]);

                $this->checkError($result);
                $this->prepareDate($result);

                return array_intersect_key($result, array_flip([
                    'RoomTypes',
                    'ContractConditions']));
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }

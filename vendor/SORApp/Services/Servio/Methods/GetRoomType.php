<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 20.11.14 16:06
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetRoomType extends ServioMethod {

        public $versionMap = [
            [
                '>=',
                '1.9',
                'version_190'],
            [
                '>=',
                '1.7',
                'version_170'],];

        public function version_190($args = []) {

            try {
                $result = $this->owner->execute('GetRoomType', [
                    'RoomTypeID' => (int)array_shift($args),]);

                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                    'MainPlacesCount',
                    'MaxAdditionalPlacesCount']));
            }
            catch (ServioException $e) {
                return false;
            }
        }

        public function version_170($args = []) {

            list($RoomTypeID, $Languages) = $args;
            try {
                $result = $this->owner->execute('GetRoomType', [
                    'RoomTypeID' => (int)$RoomTypeID,
                    'Languages' => array_values($Languages),]);

                $this->checkError($result);

                $response = [
                    'MainPlacesCount' => $result['MainPlacesCount'],
                    'MaxAdditionalPlacesCount' => $result['MaxAdditionalPlacesCount'],
                    'ImageHash' => $result['ImageHash'],];

                foreach ($result['Languages'] as $key => $lang) {
                    $response['translates'][$lang] = [
                        'name' => $result['ClassNames'][$key],
                        'descr' => $result['Descriptions'][$key],];
                }

                return $response;
            }
            catch (ServioException $e) {
                return false;
            }
        }
    }

<?php

    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     * Created 11.09.14 10:39
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetRoomTypesList extends ServioMethod {

        public function run($args = []) {

            try {

                $result = $this->owner->execute('GetRoomTypesList', [
                        (int)array_shift($args)]);

                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                    'IDs',
                    'HotelIDs',
                    'ClassNames',
                    'Count']));
            }
            catch (ServioException $e) {

                return false;
            }
        }

    }

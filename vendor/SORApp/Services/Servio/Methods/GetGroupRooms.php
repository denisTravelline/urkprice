<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Servio\Methods;

    use SORApp\Services\Servio\ServioException;
    use SORApp\Services\Servio\ServioMethod;

    class GetGroupRooms extends ServioMethod {

        public $versionMap = [
            [
                '>=',
                '1.9',
                'json'],
            [
                '>=',
                '1.7',
                'xml'],];

        public function json($args = []) {

            list($hotelId, $bookingRequests, $isoLanguage, $companyCodeId) = $args;

            return $this->process([
                'HotelID' => (int)$hotelId,
                'BookingRequests' => $this->prepareBookingRequests($bookingRequests),
                'IsoLanguage' => (string)$isoLanguage,
                'CompanyCodeID' => (int)$companyCodeId]);
        }

        public function xml($args = []) {

            list($hotelId, $bookingRequests, $isoLanguage, $companyCodeId) = $args;

            return $this->process([
                (int)$hotelId,
                $this->prepareBookingRequests($bookingRequests),
                (string)$isoLanguage,
                (int)$companyCodeId]);
        }

        public function process($data) {

            try {
                $result = $this->owner->execute('GetGroupRooms', $data);
                $this->checkError($result);

                return array_intersect_key($result, array_flip([
                    'BookingRequestAnswers',
                    'ContractConditions']));
            }
            catch (ServioException $e) {
                return false;
            }
        }

        protected function prepareBookingRequests($bookingRequests) {

            $prepared = [];
            if (is_array($bookingRequests) AND count($bookingRequests)) {
                foreach ($bookingRequests as $request) {
                    if (count($request) === 7) {
                        array_push($prepared, [
                            'DateArrival' => $this->getServioDate(array_shift($request)),
                            'DateDeparture' => $this->getServioDate(array_shift($request)),
                            'TimeArrival' => $this->getServioTime(array_shift($request)),
                            'Adults' => (int)array_shift($request),
                            'Childs' => (int)array_shift($request),
                            'ChildAges' => (array)array_shift($request),
                            'IsExtraBedUsed' => (bool)array_shift($request),]);
                    }
                }
            }
            else {
                throw new ServioException('BookingRequestIsEmpty', 'Empty group booking request');
            }

            return $prepared;
        }
    }

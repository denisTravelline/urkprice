<?php
    /**
     * @author Sevastianov Andrey <mrpkmail@gmail.com>
     */

    namespace SORApp\Services\Servio;

    /**
     * Class ServioMethod
     * @property array versionMap
     *
     * @package SORApp\Services\Servio
     */
    abstract class ServioMethod {

        /** @var Servio */
        protected $owner;

        private $_method;

        /**
         * @param Servio $owner
         *
         * @throws ServioException
         */
        public function __construct(Servio $owner) {

            $this->owner = $owner;
        }

        /**
         * @return callable
         * @throws ServioException
         */
        public function getMethod() {

            if ($this->_method === null) {
                if (!property_exists($this, 'versionMap')) {
                    $this->_method = 'run';
                }
                else {
                    if (property_exists($this, 'versionMap') AND is_array($this->versionMap) AND count($this->versionMap)) {
                        foreach ($this->versionMap as $version) {
                            if (version_compare($this->owner->version, $version[1], $version[0])) {
                                $this->_method = $version[2];
                                break;
                            }
                        }
                    }
                }

                if (!is_callable([
                    $this,
                    $this->_method])
                ) {
                    throw new ServioException('Need implemented servio method ' . get_called_class() . '::run() or configure versionMap');
                }
            }

            return [
                $this,
                $this->_method];
        }

        /**
         * Запуск выполнения метода для требуемой версии протокола
         *
         * @param array $args
         *
         * @return bool|array|mixed
         */
        public function run($args = []) {

            return call_user_func_array($this->getMethod(), func_get_args());
        }

        /**
         * Функция проверяет полученный результат на наличие ошибок
         *
         * @param array $result
         * @param string $resultName
         * @param string $errorCodeName
         * @param string $errorMessageName
         *
         * @throws ServioException
         */
        public function checkError(array $result, $resultName = 'Result', $errorCodeName = 'ErrorCode', $errorMessageName = 'Error') {

            if (!array_key_exists($resultName, $result) OR $result[$resultName] !== 0) {
                $exception = new ServioException((array_key_exists($errorCodeName, $result) ? $result[$errorCodeName] : 'UnknownError'), (array_key_exists($errorMessageName, $result) ? $result[$errorMessageName] : 'Unknown Error'));

                $this->owner->addError($exception, get_called_class());

                throw $exception;
            }
        }

        public function prepareDate(array &$result) {

            array_walk_recursive($result, function (&$item) {

                if (is_object($item) && property_exists($item, 'xmlrpc_type') && $item->xmlrpc_type == 'datetime') {
                    $item = $item->scalar;
                }
            });
        }

        protected function parseDateTime($dateTime) {

            var_dump(date('Y-m-d', strtotime($dateTime)));
            $matches = false;
            if (preg_match('/([\d]{2})\.([\d]{2})\.([\d]{4})[T|\s]+([\d]{2}):([\d]{2})(:[\d]{2})?/', $dateTime, $matches)) {

            }

            var_dump($dateTime, $matches);
            die;
        }

        /**
         * @param $dateTime
         *
         * @return string
         */
        protected function getServioDate($dateTime) {

            $date = false;
            if ($timestamp = strtotime($dateTime)) {
                $date = date('Y-m-d', $timestamp);
                if (version_compare($this->owner->version, '1.9') < 0) {
                    $date .= 'T00:00:00';
                    xmlrpc_set_type($date, 'datetime');
                }
            }

            return $date;
        }

        /**
         * @param $dateTime
         *
         * @return string
         */
        protected function getServioTime($dateTime) {

            $time = false;
            if ($timestamp = strtotime($dateTime)) {
                $time = date('H:i', $timestamp);
            }

            return $time;
        }

        protected function getServioDatetime($dateTime) {

            $timestamp = strtotime($dateTime);
            if (version_compare($this->owner->version, '1.9') >= 0) {
                $date = date('Y-m-d H:i:s', $timestamp);
            }
            else {
                $date = date('Y-m-d\TH:i:s', $timestamp);
                xmlrpc_set_type($date, 'datetime');
            }

            return $date;
        }
    }

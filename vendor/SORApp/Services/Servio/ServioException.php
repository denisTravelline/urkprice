<?php
    namespace SORApp\Services\Servio;

    class ServioException extends \Exception {

        public $textCode;

        public function __construct($textCode = '', $message = '', $code = 0, \Exception $previous = null) {

            $this->textCode = $textCode;
        }

        public function getTextCode() {

            return $this->textCode;
        }
    }

<?php

namespace SORApp\Widgets;

use SORApp\Components\BaseWidget;

class ShowErrors extends BaseWidget {

    public $view = 'ShowErrors';

    public function run() {

        $errors = $this->getOwner()->getResponse()->getErrors();
        if (count($errors))
            return $this->render($this->view, array('errors' => $errors));

        return '';
    }

}

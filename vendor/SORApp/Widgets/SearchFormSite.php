<?php

/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 05.09.14 14:03
 */

namespace SORApp\Widgets;

use SORApp\Components\BaseWidget,
    SORApp\Models\City,
    SORApp\Models\Hotel,
    SORApp\Models\Booking\AbstractBookingFactory;

class SearchFormSite extends BaseWidget {

    public $hashKey;

    public function run() {

        $hotelModel = new Hotel();
        $hotels = $hotelModel->getGroup(Hotel::FETCH_ARRAY_CITY);
        if (!$hotels) throw new \ErrorException('No hotels configured');

        $cityModel = new City();
        $cities = $cityModel->getAllById(array_keys($hotels));

        if ($this->hashKey === null && isset($_GET['hashKey'])) $this->hashKey = $_GET['hashKey'];

        $searchRequest = AbstractBookingFactory::getFactory('SearchRequest')->getEntity($this->hashKey, [
            'arrival' => date('d.m.Y 14:00', strtotime('+1 day')),
            'departure' => date('d.m.Y 12:00', strtotime('+2 day')),
        ]);

        return $this->render('SearchFormSite', [
            'hotels' => $hotels,
            'cities' => $cities,
            'searchRequest' => $searchRequest
        ]);
    }
}

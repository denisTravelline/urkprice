<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 03.09.15
 * Time: 18:11
 */

namespace SORApp\Widgets;

use SORApp\Components\BaseWidget,
    SORApp\Components\Exceptions\HttpException,
    SORApp\Models\Booking\AbstractBookingFactory,
    SORApp\Models\City,
    SORApp\Models\Hotel;

class SearchRoomForm extends BaseWidget {

    public $hashKey;

    public function run() {

        $params = [];

        $hotelModel = new Hotel();
        $params['hotels'] = $hotelModel->getGroup(Hotel::FETCH_ARRAY_CITY);
        if (!$params['hotels']) throw new HttpException('No hotels configured');

        $cityModel = new City();
        $params['cities'] = $cityModel->getAllById(array_keys($params['hotels']));

        if ($this->hashKey === null AND isset($_GET['hashKey'])) $this->hashKey = $_GET['hashKey'];

        $params['searchRequest'] = AbstractBookingFactory::getFactory('SearchRequest')->getEntity($this->hashKey, [
            'arrival' => date('d.m.Y 14:00', strtotime('+1 day')),
            'departure' => date('d.m.Y 12:00', strtotime('+2 day')),
        ]);

        return $this->render('SearchRoomForm', $params);
    }

}
<?php
/**
 * @author Sevastianov Andrey <mrpkmail@gmail.com>
 * Created 13.11.14 10:50 
 */

namespace SORApp\Widgets;


use SORApp\Components\BaseWidget;
use SORApp\Components\Pagination;

class ShowPagination extends BaseWidget
{
    public $view = 'ShowPagination';

    public $pageAround = 5;
    public $url = '?page={page}';

    public $prevLabel = '&laquo;';
    public $nextLabel = '&raquo;';

    /** @var Pagination $_pagination */
    private $_pagination;

    public function run()
    {
        if ( $this->_pagination instanceof Pagination ) {
            $pages = array();

            if ( $this->prevLabel && $this->_pagination->currentPage > 1 ) {
                $pages[] = array(
                    'url' => str_replace('{page}', $this->_pagination->currentPage - 1, $this->url),
                    'anchor' => $this->prevLabel,
                    'active' => false
                );
            }

            for (
                $i = max($this->_pagination->currentPage - $this->pageAround, 1);
                $i <= min($this->_pagination->currentPage + $this->pageAround, ceil($this->_pagination->itemCount / $this->_pagination->itemPerPage));
                $i ++
            ) {
                $pages[] = array(
                    'url' => str_replace('{page}', $i, $this->url),
                    'anchor' => $i,
                    'active' => ($this->_pagination->currentPage == $i)
                );
            };

            if ( $this->nextLabel && $this->_pagination->currentPage < ceil($this->_pagination->itemCount / $this->_pagination->itemPerPage) ) {
                $pages[] = array(
                    'url' => str_replace('{page}', $this->_pagination->currentPage + 1, $this->url),
                    'anchor' => $this->nextLabel,
                    'active' => false
                );
            }

            return $this->render($this->view, array(
                'pages' => $pages,
                'pagination' => $this->_pagination,
            ));
        } else {
            return '';
        }
    }

    public function setPagination(Pagination $pagination)
    {
        $this->_pagination = $pagination;
    }

} 

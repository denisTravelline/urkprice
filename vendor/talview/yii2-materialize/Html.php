<?php
/**
 * yii2-materialize\Html is the extended yii2 BaseHtml class.
 *
 * @author    Karthik S G<merlano17@gmail.com>
 * @version 0.1
 */
namespace talview\materialize;


use yii\helpers\BaseHtml;

class Html extends BaseHtml
{
    /**
     * @inheritdoc
     */
    public static function checkbox($name, $checked = false, $options = [])
    {
        if (!isset($options['id'])) {
            $options['id'] = Widget::$autoIdPrefix . Widget::$counter++;
        }
        $content = parent::checkbox($name, $checked, array_merge($options, ['label' => null]));
        if (isset($options['label'])) {
            $label = $options['label'];
            $for = (isset($options['id']))?$options['id'] : $options['label'];
            $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
            unset($options['label'], $options['labelOptions']);
            $content .= parent::label($label, $for, $labelOptions);
        } else {
            $label = $name;
            $for = (isset($options['id']))?$options['id'] : $options['label'];
            $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
            $content .= parent::label($label, $for, $labelOptions);
        }
        return $content;
    }

    /**
     * @inheritDoc
     */
    public static function radio($name, $checked = false, $options = [])
    {
        if (!isset($options['id'])) {
            $options['id'] = Widget::$autoIdPrefix . Widget::$counter++;
        }
        $content = parent::radio($name, $checked, array_merge($options, ['label' => null]));
        if (isset($options['label'])) {
            $label = $options['label'];
            $for = (isset($options['id']))?$options['id'] : $options['label'];
            $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
            unset($options['label'], $options['labelOptions']);
            $content .= parent::label($label, $for, $labelOptions);
        } else {
            $label = $name;
            $for = (isset($options['id']))?$options['id'] : $options['label'];
            $labelOptions = isset($options['labelOptions']) ? $options['labelOptions'] : [];
            $content .= parent::label($label, $for, $labelOptions);
        }
        return $content;
    }


}

<?php
/**
 * @package   yii2-materialize
 * @author    Karthik S G<merlano17@gmail.com>
 * @copyright Copyright &copy; Karthik S G
 * @version   0.1
 */

namespace talview\materialize;

use yii\web\AssetBundle;

class MaterializeCustomAsset extends AssetBundle
{
    public $sourcePath = '@vendor/talview/yii2-materialize/';

    public $css = ['assets/css/custom.css'
    ];
}
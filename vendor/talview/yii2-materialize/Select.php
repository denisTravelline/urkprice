<?php
/**
 * @author Karthik S G<karthik.sg@talview.com>
 */

namespace talview\materialize;


use yii\widgets\InputWidget;

class Select extends InputWidget
{
    use BaseWidgetTrait;

    public $data = [];

    public function run()
    {
        $this->registerPlugin('material_select');
        if ($this->hasModel()) {
            return Html::activeDropDownList($this->model, $this->attribute, $this->data, $this->options);
        } else {
            return Html::dropDownList($this->name, $this->value, $this->data, $this->options);
        }
    }
}
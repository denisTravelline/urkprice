<?php

namespace talview\materialize;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
/**
 *** Example ***
 *echo Tab::Widget(['items' => [
 *                        [
 *                            'options' => ['id' => 't'],
 *                            'label' => 'test',
 *                            'content' => 'testing',
 *                            'itemOptions' => [
 *                                            'class'=>'tab col s3',
 *                            ],
 *                        ],
 *                        [
 *                            'options' => ['id' => 't2'],
 *                            'label' => 'test1',
 *                            'content' => 'tab',
 *                            'itemOptions' => [
 *                                            'class'=>'tab col s3',
 *                            ],
 *                        ],
 *                    ]
 *                ]);
 **/

class Tab extends Widget
{
    public $encodeLabel = false;
    /**
     * @var string default css class for button
     */
    public $defaultClass = 'tabs';

    public $containerOptions = ['class' => 'row'];

    public $itemsContainerOptions = ['class' => 'tabs'];

    public $items = [];

    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();
        $this->clientOptions = false;
        if ($this->defaultClass)
            Html::addCssClass($this->options, $this->defaultClass);
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerPlugin('tabs');
        echo Html::beginTag('div', $this->containerOptions);
        echo parent::run();
        echo $this->renderItems($this->items);
        echo Html::endTag('div');
        //return Html::tag($this->tagName, $this->encodeLabel ? Html::encode($this->label) : $this->label, $this->options);
    }

    protected function renderItems($items)
    {
        $lines = [];
        $ret = Html::beginTag('div',['class' => 'col s12']);
        $lines = $this->renderHeader($items);
        $ret .= Html::tag('ul', implode("\n", $lines), ['class' => 'tabs']);
        $ret .= Html::endTag('div');
        $ret .= $this->renderBody($items);
        return $ret;
    }

    protected function renderHeader($items)
    {
        foreach ($items as $i => $item) {
            if (!array_key_exists('label', $item)) {
                throw new InvalidConfigException("The 'label' option is required.");
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabel;
            $label = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $itemOptions = ArrayHelper::getValue($item, 'itemOptions', []);
            $itemOptions['class'] = (isset($itemOptions['class']))?'tab '.$itemOptions['class']:'tab col s3';
            $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
            $url = array_key_exists('url', $item) ? $item['url'] : null;
            $href = (isset($item['options']['id'])?$item['options']['id']:$item['id']);
            $content = Html::a($label, '#'.$href);
            $lines[] = Html::tag('li', $content, $itemOptions);
        }
        return $lines;
    }

    protected function renderBody($items)
    {
        $ret = '';
        foreach ($items as $i => $item) {
            $item['options']['class'] = (isset($item['options']['class']))?$item['options']['class'].' col s12':'col s12';
            $ret .= Html::tag('div', $item['content'], $item['options']);
        }
        return $ret;
    }
}
<?php
/**
 * @author Karthik S G <karthik.sg@talview.com>
 */

namespace talview\materialize;


use yii\helpers\Json;

trait BaseWidgetTrait
{
    /**
     * @var array the HTML attributes for the widget container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    /**
     * @var array the options for the underlying Materialize JS plugin.
     * Please refer to the corresponding Materialize plugin Web page for possible options.
     * For example, [this page](http://materializecss.com/modals.html) shows
     * how to use the "Modal" plugin and the supported options (e.g. "remote").
     */
    public $clientOptions = [];
    /**
     * @var array the event handlers for the underlying Materialize JS plugin.
     * Please refer to the corresponding Materialize plugin Web page for possible events.
     * For example, [this page](http://materializecss.com/modals.html) shows
     * how to use the "Modal" plugin and the supported events (e.g. "shown").
     */
    public $clientEvents = [];

    /**
     * @var bool its the options for enabling/disabling the custom css
     * default FALSE
     */
    public $customAsset = false;

    /**
     * @var bool its the options for enabling/disabling the materialize assets
     * default FALSE
     */
    public $materializeAsset = false;


    /**
     * Initializes the widget.
     * This method will register the materialize asset bundle. If you override this method,
     * make sure you call the parent implementation first.
     */
    public function init()
    {
        parent::init();
        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }
    }

    /**
     * Registers a specific Materialize plugin and the related events
     * @param string $name the name of the Materialize plugin
     */
    protected function registerMaterializePlugin($name)
    {
        $view = $this->getView();
        if ($this->materializeAsset) {
            MaterializeAsset::register($view);
        }
        if ($this->customAsset) {
            MaterializeCustomAsset::register($view);
        }

        $id = $this->options['id'];

        if ($this->clientOptions !== false && is_array($this->clientOptions)) {
            $options = empty($this->clientOptions) ? '' : Json::htmlEncode($this->clientOptions);
            $js = "Materialize.$name.apply(null, $options);";
            $view->registerJs($js);
        }

        $this->registerClientEvents();
    }

    /**
     * Registers a specific Materialize plugin to jquery and the related events
     * @param string $name the name of the Materialize plugin
     */
    protected function registerPlugin($name)
    {
        $view = $this->getView();

        if ($this->materializeAsset) {
            MaterializeAsset::register($view);
        }
        if ($this->customAsset) {
            MaterializeCustomAsset::register($view);
        }

        $id = $this->options['id'];

        if ($this->clientOptions !== false) {
            $options = empty($this->clientOptions) ? '' : Json::encode($this->clientOptions);
            $js = "jQuery('#$id').$name($options);";
            $view->registerJs($js);
        }

        $this->registerClientEvents();
    }

    /**
     * Registers JS event handlers that are listed in [[clientEvents]].
     * @since 0.1
     */
    protected function registerClientEvents()
    {
        if (!empty($this->clientEvents)) {
            $id = $this->options['id'];
            $js = [];
            foreach ($this->clientEvents as $event => $handler) {
                $js[] = "jQuery('#$id').on('$event', $handler);";
            }
            $this->getView()->registerJs(implode("\n", $js));
        }
    }
}
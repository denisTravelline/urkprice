<?php
namespace talview\materialize;

use Yii;
use yii\helpers\Json;

/**
 * yii2-materialize\Widget is the base class for all materialize widgets.
 *
 * @author    Karthik S G<merlano17@gmail.com>
 * @version 0.1
 */
class Widget extends \yii\base\Widget
{
    use BaseWidgetTrait;
}

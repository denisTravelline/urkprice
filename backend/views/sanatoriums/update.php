<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sanatoriums */

// title
if(!empty($model->descr->title)) {
    $title = $model->descr->title;
} else {
    $title = $model->id;
}

$this->title = 'Редактирование: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Санатории', 'url' => ['index']];
?>
<div class="sanatoriums-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'langs' => $langs,
        'features' => $features,
        'images' => $images,
    ]) ?>

</div>

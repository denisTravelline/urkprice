<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sanatoriums */

$this->title = 'Добавление санатория';
$this->params['breadcrumbs'][] = ['label' => 'Санатории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sanatoriums-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'langs' => $langs,
        'features' => $features,
    ]) ?>

</div>

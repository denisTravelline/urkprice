<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'pdIcPItSM9L7M9Q_Ab5TNuvB6df1E4oV',
        ],
    ],
    'aliases' => [
	    '@fronturl' => 'http://www.ukrtour.pro/',
    ],
];

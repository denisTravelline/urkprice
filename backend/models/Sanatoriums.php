<?php

namespace backend\models;

use Yii;
use common\models\SanatoriumsDescriptions;
use common\models\Languages;
use common\models\ServioCities;
use yii\helpers\ArrayHelper;
use common\models\SanatoriumsFeatures;

/**
 * This is the model class for table "sanatoriums".
 *
 * @property integer $id
 * @property integer $status
 * @property string $image
 * @property string $phone
 *
 * @property SanatoriumsDescriptions[] $sanatoriumsDescriptions
 */
class Sanatoriums extends \common\models\Sanatoriums {

    /**
     * Save offer descrioptions to db
     * @return boolean
     */
    public function saveDescr()
    {
        $langs = Languages::getLangs();

        // save
        foreach($langs as $code => $lang) {
            // Skip empty description

            if(!empty($this->title[$code])) {

                // find descr
                $model = SanatoriumsDescriptions::findOne([
                        'sanatorium_id' => $this->id,
                        'lang_id' => $lang['id'],
                ]);

                if(empty($model)) {
                    // new entry
                    $model            = new SanatoriumsDescriptions();
                    $model->sanatorium_id = $this->id;
                    $model->lang_id   = $lang['id'];
                }

                // Descr properties
                $model->title            = $this->title[$code];
                $model->annotation       = $this->annotation[$code];
                $model->description      = $this->description[$code];
                $model->description_second = $this->description_second[$code];
                $model->meta_description = $this->meta_description[$code];
                $model->address          = $this->address[$code];

                // Slug
                if(!empty($this->slug[$code])) {
                    $model->detachBehavior('slug');
                    $model->slug = $this->slug[$code];
                }

                if(!$model->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Load sanatorium description to the model
     * @return boolean
     */
    public function sanatoriumDescr()
    {
        $langs = Languages::getLangs();
        foreach($langs as $code => $lang) {
            // Get sanatorium description
            $descr = $this->getDescr($code)->one();

            // Assign properties
            if(!empty($descr)) {
                $this->title[$code]            = $descr->title;
                $this->annotation[$code]       = $descr->annotation;
                $this->description[$code]      = $descr->description;
                $this->description_second[$code]= $descr->description_second;
                $this->meta_description[$code] = $descr->meta_description;
                $this->slug[$code]             = $descr->slug;
                $this->address[$code]          = $descr->address;
            }
        }
        return true;
    }
    
    public static function citiesTitles()
    {
        $cities = [];
        
        // Get all city + translates
        $citiesModel = ServioCities::find()->all();
        
        // Cities array
        foreach($citiesModel as $city) {
            $cities[$city->id] = $city->descr->name;
        }
        
        return $cities;
        
    }

    /**
     * Features available for sanatorium
     * @return array $features The sanatorium features
     */
    public function featuresList()
    {
        $model = Features::find()
            ->select('{{features}}.id as id, {{features_descriptions}}.title as title')
            ->joinWith('descr')
            ->where([
                'status' => Features::FEATURES_STATUS_ACTIVE,
                'type' => Features::FEATURES_TYPE_SANATORIUM
            ])
            ->asArray()
            ->all();

        $features = ArrayHelper::map($model, 'id', 'title');
        return $features;
    }

    /**
     * Save sanatoriums features
     * @return boolean
     */
    public function saveFeatures()
    {
        // Clean features
        if(SanatoriumsFeatures::deleteAll(['sanatorium_id' => $this->id]) !== null) {
            if(!empty($this->sanatoriumFeatures)) {
                // Add features
                foreach($this->sanatoriumFeatures as $feature) {
                    $model             = new SanatoriumsFeatures();
                    $model->sanatorium_id   = $this->id;
                    $model->feature_id = $feature;
                    if(!$model->save()) {
                        Yii::error('Error save sanatoriums feature');
                        return false;
                    }
                }
            }
            return true;
        } else {
            Yii::error('Error clean sanatoriums features');
            return false;
        }
    }

    public function sanatoriumFeatures()
    {
        //$features = [];
        // Get faetures
        $features = SanatoriumsFeatures::find()
            ->where(['sanatorium_id' => $this->id])
            ->asArray()
            ->all();

        return ArrayHelper::map($features, 'feature_id', 'feature_id');
    }
}

<?php

namespace frontend\controllers;

use common\models\Rooms;
use Yii;
use yii\web\NotFoundHttpException;
use common\models\Languages;

class RoomsController extends \yii\web\Controller {

    /**
     *  Room statuses
     */
    const ROOM_STATUS_INVISIBLE = 0;
    const ROOM_STATUS_VISIBLE   = 1;

    /**
     * Body id attribute
     * @var string body ID 
     */
    public $bodyId;
    public $langId;

    public function actionView($slug)
    {
        $this->langId = Languages::getLangId(Yii::$app->language);
        // Set body ID
        $this->bodyId = 'page-room';

        // Get room model
        $room = self::findModelBySlug($slug, $this->langId);

        return $this->render('view', [
                'room' => $room
        ]);
    }

    /**
     * Find model by slug
     * @param string $slug
     * @return mixes
     * @throws NotFoundHttpException
     */
    protected static function findModelBySlug($slug, $langId)
    {
        $model = Rooms::find()
            ->where('slug = :slug', [':slug' => $slug])
            ->andWhere('lang_id = :lang_id', [':lang_id' => $langId])
            ->andWhere(['visible' => self::ROOM_STATUS_VISIBLE])
            ->one();
        if(!empty($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Сторінку не знайдено'));
        }
    }

}

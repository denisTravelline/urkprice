<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use frontend\models\Sanatoriums;
use common\models\Categories;
use frontend\models\Gallery;

class SanatoriumsController extends \yii\web\Controller
{
    /**
     * Body id attribute
     * @var string body ID 
     */
    public $bodyId;

    public function actionView($slug)
    {
        // Set body ID
        $this->bodyId = 'page-hotel';

        // Get offer model
        $sanatorium = self::findModelBySlug($slug);
        if($sanatorium == null) {
            throw new NotFoundHttpException('Page not found');
        }
        
        // Image
        $sanatorium->defaultImage();
        
        // Category
        $category = Categories::findOne(['type' => Categories::CATEGORY_TYPE_SANATORIUM]);
        
        // Banners
        $banners = Gallery::getCarousel('sanatorium', $sanatorium->id);
        
        // Conference rooms
        $confRooms = $sanatorium->getConfRooms();

        return $this->render('view', [
                'sanatorium' => $sanatorium,
                'category' => $category,
                'banners' => $banners,
                'confRooms' => $confRooms
        ]);
    }

    /**
     * Find model by slug
     * @param string $slug
     * @return mixes
     * @throws NotFoundHttpException
     */
    protected static function findModelBySlug($slug)
    {
        $model = Sanatoriums::find()
            ->joinWith('descr')
            ->where('slug = :slug', [':slug' => $slug])
            ->andWhere(['status' => Sanatoriums::SANATORIUMS_STATUS_VISIBLE])
            ->one();
        if(!empty($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Сторінку не знайдено'));
        }
    }

    public function actionRoomsFeatures($hotel)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        // Features
        $features = Rooms::getRoomsFeatures($hotel);
        return [$features];
    }
}

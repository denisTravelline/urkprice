<?php
return [
    'adminEmail' => 'info@ukrtour.pro',
    'reservationEmail' => 'reservations@ukrtour.pro',
    'debugEmail' => 'ingvarr25@gmail.com',
    'phone' => '0 800 211 050',
    'phone_two' => '096 366 27 55',
    'phone_three' => '099 213 02 65',
    'instagram' => 'http://instagram.com/ukrtour_pro/',
    'facebook' => 'https://www.facebook.com/ukrtour.pro',
    'tripadvisor' => 'https://www.tripadvisor.com/',
    'showRooms' => 8, // Rooms quantity
    'roomsMinPrice' => 50, // Rooms min price to show
    'cacheExpiration' => 0, // Seconds 
];

<?php

namespace frontend\models;

use yii\helpers\StringHelper;
use yii\helpers\Url;
use common\models\RoomsFeatures;

/**
 * This is the model class for table "servio_rooms".
 *
 * @property integer $id
 * @property integer $hotelId
 *@property integer $servioId
 * @property string $name
 * @property integer $beds
 * @property integer $extBeds
 * @property integer $visible
 * @property string $photo
 * @property integer roomType
 */
class Rooms extends \common\models\Rooms {

    /**
     * Rooms features
     * @param integer $hotelId The hotel ID
     * @return mixed features
     */
    public static function getRoomsFeatures($hotelId)
    {
        $features = [];
        // Get rooms
        $rooms    = self::find()
            ->where('hotelId = :id', [':id' => $hotelId])
            ->andWhere(['visible' => self::ROOMS_STATUS_VISIBLE])
            ->all();
        if(!empty($rooms)) {
            // Get features
            foreach($rooms as $room) {
                if(!empty($room->features)) {
                    // Get icoms
                    foreach($room->features as $feature) {
                        $features[$feature->room_id][] = $feature->feature->icon;
                    }
                }
            }
            return $features;
        } else {
            return null;
        }
    }

    /**
     * Rooms
     * @param integer $type the room type
     * @param integer $limit
     * @return boolean
     */
    public static function getRooms($limit = false)
    {
        // Get offers model
        $query = self::find()
            ->where([
                'status' => self::ROOM_STATUS_VISIBLE,
            ]);

        // Limit
        if($limit) {
            $query->limit($limit);
        }

        $model = $query->all();

        if(!empty($model)) {
            // Rooms array
            $rooms = [];
            foreach($model as $item) {
                // Title
                $name = '';
                if(isset($item->descr->name)) {
                    $name = StringHelper::truncate($item->descr->name, 70);
                }
                
                // Push to array
                $rooms[] = [
                    'name' => $name,
                ];
            }

            return $rooms;

        } else {
            return false;
        }
    }

}

<?php

/* Site/layout.twig */
class __TwigTemplate_c7ac5dd66a2f95b7a84f3d0a935048638b1601c40ebf2172769e34f71f7b6be1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'lang' => array($this, 'block_lang'),
            'breadcrumbs' => array($this, 'block_breadcrumbs'),
            'erorrs' => array($this, 'block_erorrs'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html ng-app=\"UkrTour\">
    <head>
        <script type=\"text/javascript\" src=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/jquery.js\"></script>
        ";
        // line 5
        $this->loadTemplate("Site/assets.twig", "Site/layout.twig", 5)->display($context);
        // line 6
        echo "        <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/servio.";
        echo twig_escape_filter($this->env, (isset($context["lang"]) ? $context["lang"] : null), "html", null, true);
        echo ".js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/servio.js\"></script>

        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        // line 11
        echo "</title>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" media=\"all\"/>
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/theme/css/materialize.min.css\" media=\"all\"/>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/theme/css/style.css\" media=\"all\"/>

            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <meta name=\"description\" content=\"Модуль онлайн-бронирования Ukr Tour\" />
            <script>
                 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                 ga('create', 'UA-90808900-1', 'auto');
                 ga('send', 'pageview');
            </script>
        </head>

        <body id=\"page-hotel\">
            <div id=\"main-wrapper\">";
        // line 33
        $this->displayBlock('header', $context, $blocks);
        // line 34
        echo "<div id=\"servioBooking\" class=\"container-fluid\">

                        <div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-3\">

                            ";
        // line 38
        $this->displayBlock('lang', $context, $blocks);
        // line 45
        echo "
                        </div>

                        <div class=\"col-xs-12 col-sm-12 col-md-8 col-lg-9\">

                            <!-- BOOKING STEPS -->
                            ";
        // line 51
        $this->displayBlock('breadcrumbs', $context, $blocks);
        // line 61
        echo "
                                                      <p class=\"clearfix\"/>

                                                      <!-- ERROR MESSAGE -->
                                                      ";
        // line 65
        $this->displayBlock('erorrs', $context, $blocks);
        // line 89
        echo "
                                                      <!-- PAGE CONTENT -->
                                                      <div id=\"servioBookingContent\">
                                                          ";
        // line 92
        $this->displayBlock('content', $context, $blocks);
        // line 94
        echo "                                                      </div>

                                                  </div>

                                            </div>
                                        </div>
                                        <footer id=\"footer\">";
        // line 101
        $this->displayBlock('footer', $context, $blocks);
        // line 102
        echo "</footer>
                                            <script src=\"/theme/js/materialize.min.js\"></script>
                                            <script src=\"/theme/js/script.js\"></script>
                                        </body>
                                    </html>
";
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
    }

    // line 33
    public function block_header($context, array $blocks = array())
    {
        echo (isset($context["header"]) ? $context["header"] : null);
    }

    // line 38
    public function block_lang($context, array $blocks = array())
    {
        // line 39
        echo "                                ";
        if ((twig_length_filter($this->env, (isset($context["langs"]) ? $context["langs"] : null)) > 0)) {
            // line 40
            echo "                                    <div class=\"row\">
                                        ";
            // line 41
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["langs"]) ? $context["langs"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["lang"]) {
                echo "<a href=\"/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["lang"], "isoLang", array()), "html", null, true);
                echo "/booking/\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["lang"], "language", array()), "html", null, true);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "                                    </div>
                                ";
        }
        // line 44
        echo "                            ";
    }

    // line 51
    public function block_breadcrumbs($context, array $blocks = array())
    {
        // line 52
        echo "                                <div class=\"breadcrumbs\">
                                    ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["steps"]) ? $context["steps"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["num"] => $context["step"]) {
            // line 54
            echo "                                        <span style=\"z-index: ";
            echo twig_escape_filter($this->env, (100 + (($this->getAttribute((isset($context["steps"]) ? $context["steps"] : null), "length", array()) - $this->getAttribute($context["loop"], "index", array())) * 10)), "html", null, true);
            echo ";\"
                                              class=\"";
            // line 55
            if (($context["num"] == (isset($context["stepNumber"]) ? $context["stepNumber"] : null))) {
                echo "bt2";
            } else {
                if (($context["num"] < (isset($context["stepNumber"]) ? $context["stepNumber"] : null))) {
                    echo "bt3";
                } else {
                    echo "bt1";
                }
            }
            echo "\">
                                            <span";
            // line 56
            if (($context["num"] <= (isset($context["stepNumber"]) ? $context["stepNumber"] : null))) {
                echo " class=\"label-primary\"";
            } else {
                if (($context["num"] < (isset($context["stepNumber"]) ? $context["stepNumber"] : null))) {
                    echo " class=\"label-primary\"";
                }
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["step"], "title", array()), "html", null, true);
            echo "</span>
                                                  </span>
                                                  ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['num'], $context['step'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                                                  </div>
                                                  ";
    }

    // line 65
    public function block_erorrs($context, array $blocks = array())
    {
        // line 66
        echo "                                                          ";
        if ((twig_length_filter($this->env, (isset($context["errorMessages"]) ? $context["errorMessages"] : null)) > 0)) {
            // line 67
            echo "                                                              <div class=\"row panel-group\" role=\"tablist\">
                                                                  <div class=\"panel panel-danger\">
                                                                      <div class=\"panel-heading\" role=\"tab\" id=\"errorListGroupHeading\">
                                                                          <h4 class=\"panel-title\">
                                                                              <a role=\"button\" data-toggle=\"collapse\" href=\"#errorListGroup\" aria-expanded=\"true\" aria-controls=\"errorListGroup\" class=\"servio-error\">
                                                                                  Error
                                                                              </a>
                                                                          </h4>
                                                                      </div>
                                                                      <div id=\"errorListGroup\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"errorListGroupHeading\" aria-expanded=\"true\">
                                                                          <ul class=\"list-group\">
                                                                              ";
            // line 78
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errorMessages"]) ? $context["errorMessages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 79
                echo "                                                                                  <li class=\"servio-error-message list-group-item\">
                                                                                      ";
                // line 80
                echo nl2br(twig_escape_filter($this->env, $context["error"], "html", null, true));
                echo "
                                                                                  </li>
                                                                              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "                                                                          </ul>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          ";
        }
        // line 88
        echo "                                                      ";
    }

    // line 92
    public function block_content($context, array $blocks = array())
    {
        // line 93
        echo "                                                          ";
    }

    // line 101
    public function block_footer($context, array $blocks = array())
    {
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "Site/layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 101,  289 => 93,  286 => 92,  282 => 88,  275 => 83,  266 => 80,  263 => 79,  259 => 78,  246 => 67,  243 => 66,  240 => 65,  235 => 59,  210 => 56,  198 => 55,  193 => 54,  176 => 53,  173 => 52,  170 => 51,  166 => 44,  162 => 42,  149 => 41,  146 => 40,  143 => 39,  140 => 38,  134 => 33,  128 => 10,  119 => 102,  117 => 101,  109 => 94,  107 => 92,  102 => 89,  100 => 65,  94 => 61,  92 => 51,  84 => 45,  82 => 38,  76 => 34,  74 => 33,  51 => 11,  49 => 10,  44 => 7,  37 => 6,  35 => 5,  31 => 4,  26 => 1,);
    }

    public function getSource()
    {
        return "<!DOCTYPE html>
<html ng-app=\"UkrTour\">
    <head>
        <script type=\"text/javascript\" src=\"{{ staticUrl }}static/js/jquery.js\"></script>
        {% include \"Site/assets.twig\" %}
        <script type=\"text/javascript\" src=\"{{ staticUrl }}static/js/servio.{{ lang }}.js\"></script>
        <script type=\"text/javascript\" src=\"{{ staticUrl }}static/js/servio.js\"></script>

        <title>
            {%- block title %}{{ title }}{% endblock -%}
            </title>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" media=\"all\"/>
            <link href=\"https://fonts.googleapis.com/icon?family=Material+Icons\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/theme/css/materialize.min.css\" media=\"all\"/>
            <link rel=\"stylesheet\" type=\"text/css\" href=\"/theme/css/style.css\" media=\"all\"/>

            <meta charset=\"UTF-8\">
            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
            <meta name=\"description\" content=\"Модуль онлайн-бронирования Ukr Tour\" />
            <script>
                 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                 ga('create', 'UA-90808900-1', 'auto');
                 ga('send', 'pageview');
            </script>
        </head>

        <body id=\"page-hotel\">
            <div id=\"main-wrapper\">
                {%- block header %}{{ header|raw }}{% endblock -%}
                    <div id=\"servioBooking\" class=\"container-fluid\">

                        <div class=\"col-xs-12 col-sm-6 col-md-4 col-lg-3\">

                            {% block lang %}
                                {% if langs | length > 0 %}
                                    <div class=\"row\">
                                        {% for lang in langs %}<a href=\"/{{ lang.isoLang }}/booking/\">{{ lang.language }}</a>{% endfor %}
                                    </div>
                                {% endif %}
                            {% endblock %}

                        </div>

                        <div class=\"col-xs-12 col-sm-12 col-md-8 col-lg-9\">

                            <!-- BOOKING STEPS -->
                            {% block breadcrumbs %}
                                <div class=\"breadcrumbs\">
                                    {% for num, step in steps %}
                                        <span style=\"z-index: {{ 100 + (steps.length - loop.index) * 10 }};\"
                                              class=\"{% if num == stepNumber %}bt2{% else %}{% if num < stepNumber %}bt3{% else %}bt1{% endif %}{% endif %}\">
                                            <span {%- if num <= stepNumber %} class=\"label-primary\"{% else %}{% if num < stepNumber %} class=\"label-primary\"{% endif%}{% endif %}{# href=\"{{ step.url }}\"#}>{{ step.title }}</span>
                                                  </span>
                                                  {% endfor %}
                                                  </div>
                                                  {% endblock %}

                                                      <p class=\"clearfix\"/>

                                                      <!-- ERROR MESSAGE -->
                                                      {% block erorrs %}
                                                          {% if errorMessages | length > 0 %}
                                                              <div class=\"row panel-group\" role=\"tablist\">
                                                                  <div class=\"panel panel-danger\">
                                                                      <div class=\"panel-heading\" role=\"tab\" id=\"errorListGroupHeading\">
                                                                          <h4 class=\"panel-title\">
                                                                              <a role=\"button\" data-toggle=\"collapse\" href=\"#errorListGroup\" aria-expanded=\"true\" aria-controls=\"errorListGroup\" class=\"servio-error\">
                                                                                  Error
                                                                              </a>
                                                                          </h4>
                                                                      </div>
                                                                      <div id=\"errorListGroup\" class=\"panel-collapse collapse in\" role=\"tabpanel\" aria-labelledby=\"errorListGroupHeading\" aria-expanded=\"true\">
                                                                          <ul class=\"list-group\">
                                                                              {% for error in errorMessages %}
                                                                                  <li class=\"servio-error-message list-group-item\">
                                                                                      {{ error | nl2br | raw }}
                                                                                  </li>
                                                                              {% endfor %}
                                                                          </ul>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          {% endif %}
                                                      {% endblock %}

                                                      <!-- PAGE CONTENT -->
                                                      <div id=\"servioBookingContent\">
                                                          {% block content %}
                                                          {% endblock %}
                                                      </div>

                                                  </div>

                                            </div>
                                        </div>
                                        <footer id=\"footer\">
                                            {%- block footer %}{{ footer|raw }}{% endblock -%}
                                            </footer>
                                            <script src=\"/theme/js/materialize.min.js\"></script>
                                            <script src=\"/theme/js/script.js\"></script>
                                        </body>
                                    </html>
";
    }
}

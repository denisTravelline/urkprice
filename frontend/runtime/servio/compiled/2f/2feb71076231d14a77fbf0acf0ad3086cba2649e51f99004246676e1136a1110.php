<?php

/* Site/assets.twig */
class __TwigTemplate_be675fbd4a396c4613a19db2ff7e023e5749894f5b982dcc9e585ad5ba1185db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'assets' => array($this, 'block_assets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "    ";
        $this->displayBlock('assets', $context, $blocks);
    }

    public function block_assets($context, array $blocks = array())
    {
        // line 2
        echo "
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/font-awesome.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/font-notosans.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/bootstrap.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/bootstrap-dialog.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/jquery.datetimepicker.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/loaderAjax.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/css/booking.css\" media=\"all\"/>

        <script type=\"text/javascript\" src=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/jquery.datetimepicker.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/jquery.loaderAjax.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/jquery.cookie.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/processCounter.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/bootstrap.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/bootstrap-dialog.js\"></script>

        <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["staticUrl"]) ? $context["staticUrl"] : null), "html", null, true);
        echo "static/js/minstay.js\"></script>

    ";
    }

    public function getTemplateName()
    {
        return "Site/assets.twig";
    }

    public function getDebugInfo()
    {
        return array (  84 => 18,  79 => 16,  75 => 15,  71 => 14,  67 => 13,  63 => 12,  59 => 11,  54 => 9,  50 => 8,  46 => 7,  42 => 6,  38 => 5,  34 => 4,  30 => 3,  27 => 2,  20 => 1,);
    }

    public function getSource()
    {
        return "    {% block assets %}

        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/font-awesome.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/font-notosans.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/bootstrap.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/bootstrap-dialog.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/jquery.datetimepicker.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/loaderAjax.css\" media=\"all\"/>
        <link rel=\"stylesheet\" type=\"text/css\" href=\"{{staticUrl}}static/css/booking.css\" media=\"all\"/>

        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/jquery.datetimepicker.js\"></script>
        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/jquery.loaderAjax.js\"></script>
        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/jquery.cookie.js\"></script>
        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/processCounter.js\"></script>
        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/bootstrap.js\"></script>
        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/bootstrap-dialog.js\"></script>

        <script type=\"text/javascript\" src=\"{{staticUrl}}static/js/minstay.js\"></script>

    {% endblock %}
";
    }
}

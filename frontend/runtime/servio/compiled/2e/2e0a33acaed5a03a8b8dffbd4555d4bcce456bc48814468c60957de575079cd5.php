<?php

/* Site/Errors/500.twig */
class __TwigTemplate_290d3f7534528b49b7204217d165f93e51a2cee1e3064d8409fcb780597a625a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Site/layout.twig", "Site/Errors/500.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'errorMessages' => array($this, 'block_errorMessages'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Site/layout.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('_')->getCallable(), array("global", "Error_500_title")), "html", null, true);
    }

    // line 5
    public function block_errorMessages($context, array $blocks = array())
    {
        // line 6
        echo "    <h2>";
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('_')->getCallable(), array("global", "Error_500_title")), "html", null, true);
        echo "</h2>
    <ul class=\"warning\">
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["errorMessages"]) ? $context["errorMessages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 9
            echo "            <li>";
            echo nl2br(twig_escape_filter($this->env, $context["error"], "html", null, true));
            echo "</li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    </ul>
";
    }

    public function getTemplateName()
    {
        return "Site/Errors/500.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 11,  48 => 9,  44 => 8,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    public function getSource()
    {
        return "{% extends \"Site/layout.twig\" %}

{% block title %}{{ _('global', 'Error_500_title') }}{% endblock %}

{% block errorMessages %}
    <h2>{{ _('global', 'Error_500_title') }}</h2>
    <ul class=\"warning\">
        {% for error in errorMessages %}
            <li>{{error|nl2br|raw}}</li>
        {% endfor %}
    </ul>
{% endblock %}
";
    }
}

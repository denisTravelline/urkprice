<?php
/**
 * Offers template
 * @var $this yii\web\View
 */

// Title
$title       = $category->descr->title;
$this->title = $title;

// Meta Description
if(!empty($category->descr->meta_description)) {
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $category->descr->meta_description
    ]);
}

// H1
if(empty($h1 = $category->descr->h1)) {
   $h1 = $title;
}

use common\models\Languages;

// Lang
$langCode = Languages::getIsoCode();

// Get servio app
$servio = \SORApp\Components\App::getInstance();

// Set current lang
$servio->lang = $langCode;

$url  = '/';
if (isset($_SERVER['REQUEST_URI'])) {
	$url  = $_SERVER['REQUEST_URI'];
}
$page_main_search  = parse_url($_SERVER['REQUEST_URI']);
$lang_arr = explode('/', $page_main_search ['path']);
?>
<!--<section id="main">
    <?php /*echo $this->render('/banners/carousel', ['banners' => $banners]) */?>
</section>-->
<!-- offers -->
<section id="offers-container"  style="padding-top: 150px;">
    <h1><?php echo $h1 ?></h1>
	<?php if(!empty($offers)) { ?>
	    <div id="offers" class="row">
			<div id="tl-search-form" style="max-width: 1680px; margin-bottom: 20px;">
				<noindex><a rel="nofollow" href="http://travelline.ua/">
						<?php $lang = Languages::getLang(Yii::$app->language)['iso'];
						if ($lang == 'ru') : ?>
							система онлайн-бронирования
						<?php elseif ($lang == 'en') : ?>
							online booking system
						<?php elseif ($lang == 'uk') : ?>
							система онлайн-бронювання
						<?php endif ?>
					</a>
				</noindex>
			</div>

			<!-- start TL search form 2.0 -->
			<script type="text/javascript">
				(function(w) {

					<?php if($lang_arr[1] == 'en') : ?>
					var q = [
						['setContext', 'TL-INT-ukrtour.main-page', 'en'],
						['embed', 'search-form', {
							container: 'tl-search-form'
						}]
					];
					<?php elseif ($lang_arr[1] == 'ru') : ?>
					var q = [
						['setContext', 'TL-INT-ukrtour.main-page', 'ru'],
						['embed', 'search-form', {
							container: 'tl-search-form'
						}]
					];
					<?php else : ?>
					var q = [
						['setContext', 'TL-INT-ukrtour.main-page', 'uk'],
						['embed', 'search-form', {
							container: 'tl-search-form'
						}]
					];
					<?php endif ?>
					var t = w.travelline = (w.travelline || {}),
						ti = t.integration = (t.integration || {});
					ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
					if (!ti.__loader) {
						ti.__loader = true;
						var d = w.document,
							p = d.location.protocol,
							s = d.createElement('script');
						s.type = 'text/javascript';
						s.async = true;
						s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
						(d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
					}
				})(window);
			</script>
			<!-- end TL search form 2.0 -->

	        <?php foreach($offers as $offer) { ?>
	            <div class="offer col s12 m4 l3" onclick="location.href = '<?php echo $offer['url'] ?>'"
	                 style="background-image: url(<?php echo $offer['image'] ?>)">
	                <div class="wrapper">
	                    <div>
	                        <div class="title">
	                            <?php echo $offer['title'] ?>
	                        </div>
	                        <div class="text">
	                            <?php echo $offer['annotation'] ?>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        <?php } ?>
	    </div>
	<?php } ?>
</section>
<!-- proposals -->

<!-- text -->
<div id="text-offers" class="cont text">
    <?php
    if(!empty($category->descr->text)) {
        echo $category->descr->text;
    }
    ?>
</div>
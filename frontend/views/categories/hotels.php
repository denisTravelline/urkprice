<?php
/* @var $this yii\web\View */

use common\models\Languages;

// Lang
$langCode = Languages::getIsoCode();

// Get servio app
$servio = \SORApp\Components\App::getInstance();

// Set current lang
$servio->lang = $langCode;

$url  = '/';
if (isset($_SERVER['REQUEST_URI'])) {
    $url  = $_SERVER['REQUEST_URI'];
}
$page_main_search  = parse_url($_SERVER['REQUEST_URI']);
$lang_arr = explode('/', $page_main_search ['path']);

// Meta title
$metaTitle = $category->descr->meta_title;
if(empty($metaTitle)) {
    $metaTitle = $category->descr->title;
}
$this->title = $metaTitle;

// Meta Description
if(!empty($category->descr->meta_description)) {
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $category->descr->meta_description
    ]);
}
?>

<!-- Hide slider on Hotels Page -->
<?//php echo $this->render('/banners/carousel', ['banners' => $banners]) ?>

<section  style="padding-top: 150px;">
    <h1><?php echo Yii::t('app', 'Оберіть один з найкращих готелів') ?></h1>
    <div id="city-hotels">
        <div id="tl-search-form">
            <noindex><a rel="nofollow" href="http://travelline.ua/">
                    <?php $lang = Languages::getLang(Yii::$app->language)['iso'];
                    if ($lang == 'ru') : ?>
                        система онлайн-бронирования
                    <?php elseif ($lang == 'en') : ?>
                        online booking system
                    <?php elseif ($lang == 'uk') : ?>
                        система онлайн-бронювання
                    <?php endif ?>
                </a>
            </noindex>
        </div>

        <!-- start TL search form 2.0 -->
        <script type="text/javascript">
            (function(w) {

                <?php if($lang_arr[1] == 'en') : ?>
                var q = [
                    ['setContext', 'TL-INT-ukrtour.main-page', 'en'],
                    ['embed', 'search-form', {
                        container: 'tl-search-form'
                    }]
                ];
                <?php elseif ($lang_arr[1] == 'ru') : ?>
                var q = [
                    ['setContext', 'TL-INT-ukrtour.main-page', 'ru'],
                    ['embed', 'search-form', {
                        container: 'tl-search-form'
                    }]
                ];
                <?php else : ?>
                var q = [
                    ['setContext', 'TL-INT-ukrtour.main-page', 'uk'],
                    ['embed', 'search-form', {
                        container: 'tl-search-form'
                    }]
                ];
                <?php endif ?>
                var t = w.travelline = (w.travelline || {}),
                    ti = t.integration = (t.integration || {});
                ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
                if (!ti.__loader) {
                    ti.__loader = true;
                    var d = w.document,
                        p = d.location.protocol,
                        s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
                    (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
                }
            })(window);
        </script>
        <!-- end TL search form 2.0 -->

        <?php echo $this->render('/hotels/list', ['hotels' => $hotels]) ?>
    </div>

    <? /*<div class="center-align hide-on-small-only">
        <button id="hotels-show-all"
                class="button-see-all waves-effect waves-light blue btn">
                    <?php echo Yii::t('app', 'Показати всі міста') ?>
        </button>
    </div> */?>
</section>
<!-- text -->
<div id="text" class="cont text-hotels" style="clear: both;">
    <?//php echo (isset($category->descr->text)) ? $category->descr->text : '' ?>
</div>
<!-- /text -->
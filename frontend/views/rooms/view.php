<?php
    /**
     * Offer page template
     * @var $this yii\web\View
     */
    use common\models\Languages;
    use yii\widgets\Breadcrumbs;
    use mp\bmicrodata\BreadcrumbsUtility;

    // City  name
    if(!empty($room->name)) {
        $this->params['breadcrumbs'][] = [
            'label' => $room->city,
            'url' => null
        ];
    }

    // Breadcrumbs
    $this->params['breadcrumbs'][] = [
        'label' => $room->name,
        'url' => [$room->slug]
    ];
    
    $url  = '/';
    if (isset($_SERVER['REQUEST_URI'])) {
        $url  = $_SERVER['REQUEST_URI'];
    }
    $page_main_search  = parse_url($_SERVER['REQUEST_URI']);
    $lang_arr = explode('/', $page_main_search ['path']);
    
    // title
    $this->title = $room->name;
    $image = '/booking/uploads/rooms/room' . $room->servioId. '/thumb_' . $room->photo . '.jpg';
    
    // Slick
    $slick = '$(".hotel-carousel").slick({
      dots: false,
      infinite: true,
      dragable: true,
      speed: 700,
      slidesToShow: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      appendArrows: "#carousel-nav",
      });';
    $this->registerJs($slick, $this::POS_READY);

    $slickSmall = '$(".hotel-carousel-small").slick({
          dots: false,
          infinite: true,
          dragable: true,
          speed: 700,
          slidesToShow: 1,
          autoplay: true,
          autoplaySpeed: 5000,
          appendArrows: "#carousel-nav-small",
          });';
    $this->registerJs($slickSmall, $this::POS_READY);
    
    // Lightbox
    $this->registerJsFile('/theme/js/lightbox.min.js', [
        'depends' => 'yii\web\YiiAsset'
    ]);
    $lightbox = 'lightbox.option({"showImageNumberLabel": false})';
    $this->registerJs($lightbox, $this::POS_END);
    $this->registerCssFile('/theme/css/lightbox.min.css');
?>

<div id="carousel-hotel">
    <div id="carousel-hotel-images" class="images hotel-carousel">
        <?php
        $roomId = $room->servioId;
        $hotelId = $room->hotelId;
        $dir = '../web/images/rooms/general-' . $hotelId . '';
        $file = scandir($dir);
        $countDir = count($file)-2;

        for ($i = 1; $i <= $countDir; $i++) {
            echo '<div class="banners-item" style="background-image: url(/images/rooms/general-' . $hotelId . '/' . $i . '.jpg)"></div>';
            //echo '<div class="banners-item" style="background-image: url(/images/rooms/general' . $i . '.jpg)"></div>';
        }
        ?>
    </div>
    <div class="wrapper">
        <div class="name">
            <?php echo $room->city . ' ' . '"' . $room->name . '"' ?>
        </div>
        <div class="stars" layout="row" layout-align="center center">
            <?php for($stars = 0; $stars < $room->stars; $stars++) { ?>
                <i class="material-icons">star</i>
            <?php } ?>
        </div>
        <div class="address">
            <span class="hide-on-small-only">
                <?php echo $room->address ?>
            </span>
            <?php if(!empty($room->phone)) { ?>
                <?php foreach(explode(',', $room->phone) as $phone) { ?>
                    <a class="phone" href="tel:<?php echo preg_replace('/[^0-9\+]/i', '', $phone) ?>" title="<?php echo Yii::t('app', 'Зателефонувати') ?>">
                        <?php echo $phone ?>
                    </a>
                <?php } ?>
            <?php } ?>
        </div>
        <div class="gallery">
            <?php
                $roomId = $room->servioId;
                $dir = '../web/images/rooms/general-' . $hotelId . '';
                $file = scandir($dir);
                $countDir = count($file)-2;

                for ($i = 1; $i <= $countDir; $i++) {
                    echo '
                          <span>
                            <a href="/images/rooms/general-' . $hotelId . '/' . $i . '.jpg" data-lightbox="gallery">
                                <img class="img-thumbnail" src="/images/rooms/general-' . $hotelId . '/' . $i . '.jpg"  width="82" height="62">
                            </a>
                          </span>';
                }
            ?>
            <!--<span id="photo-all">
                <a href="/room<?php /*$room->servioId */?>/<?php /*echo $room->id */?>"
                   class="waves-effect waves-light btn-flat hide-on-small-only"
                   title="<?php /*echo Yii::t('app', 'Переглянути всі фото') */?>">
                       <?php /*echo Yii::t('app', 'Всі фото') */?>
                </a>
            </span>-->
        </div>
        <div id="carousel-nav"></div>
    </div>
</div>

<div id="tl-wrapper-mobile">
    <div id="tl-search-form-mobile">
        <noindex><a rel="nofollow" href="http://travelline.ua/">
                <?php $lang = Languages::getLang(Yii::$app->language)['iso'];
                if ($lang == 'ru') : ?>
                    система онлайн-бронирования
                <?php elseif ($lang == 'en') : ?>
                    online booking system
                <?php elseif ($lang == 'uk') : ?>
                    система онлайн-бронювання
                <?php endif ?>
            </a>
        </noindex>
    </div>

    <!-- start TL search form 2.0 -->
    <script type="text/javascript">
        (function(w) {

            <?php if($lang_arr[1] == 'en') : ?>
            var q = [
                ['setContext', 'TL-INT-ukrtour.10260', 'en'],
                ['embed', 'search-form', {
                    container: 'tl-search-form-mobile'
                }]
            ];
            <?php elseif ($lang_arr[1] == 'ru') : ?>
            var q = [
                ['setContext', 'TL-INT-ukrtour.10260', 'ru'],
                ['embed', 'search-form', {
                    container: 'tl-search-form-mobile'
                }]
            ];
            <?php else : ?>
            var q = [
                ['setContext', 'TL-INT-ukrtour.10260', 'uk'],
                ['embed', 'search-form', {
                    container: 'tl-search-form-mobile'
                }]
            ];
            <?php endif ?>
            var t = w.travelline = (w.travelline || {}),
                ti = t.integration = (t.integration || {});
            ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
            if (!ti.__loader) {
                ti.__loader = true;
                var d = w.document,
                    p = d.location.protocol,
                    s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
                (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
            }
        })(window);
    </script>
    <!-- end TL search form 2.0 -->
</div>
    <style>
        #page-room #header {
            position: relative;
        }

        #tl-wrapper-mobile {
            position: relative;
            padding: 15px 5%;
            background-color: #195381;
            z-index: 10;
        }
    </style>

<div class="room-wrap">
    <?
    echo Breadcrumbs::widget([
        'homeLink' => BreadcrumbsUtility::getHome(Yii::t('app', 'Головна'), Yii::$app->getHomeUrl()), // Link home page with microdata
        'links' => isset($this->params['breadcrumbs']) ? BreadcrumbsUtility::UseMicroData($this->params['breadcrumbs']) : [], // Get other links with microdata
        'options' => [ // Set microdata for container BreadcrumbList
            'id' => 'breadcrumbs',
            'itemscope itemtype' => 'http://schema.org/BreadcrumbList'
        ],
    ]);
    ?>

    <section class="room-card" data-room-id="<?php echo $room->id ?>">
        <h1 class="room-head"><?= $room->name?></h1>
        <div class="room-info">
            <div class="room-image">
                <!--<img src="<?/*= $image */?>" alt="<?php /*echo $room->name */?>">-->
                <div id="carousel-hotel-small">
                    <div id="carousel-hotel-images-small" class="images hotel-carousel-small">
                        <?php
                        $roomId = $room->servioId;
                        $dir = '../web/images/rooms/' . $room->servioId;
                        $file = scandir($dir);
                        $countDir = count($file)-2;

                        for ($i = 1; $i <= $countDir; $i++) {
                            echo '<div class="banners-item" style="background-image: url(/images/rooms/' . $roomId . '/' . $i . '.jpg)"></div>';
                        }
                        ?>
                    </div>
                    <div class="wrapper">
                        <div class="gallery">
                            <?php
                            $roomId = $room->servioId;
                            $dir = '../web/images/rooms/' . $room->servioId;
                            $file = scandir($dir);
                            $countDir = count($file)-2;

                            for ($i = 1; $i <= $countDir; $i++) {
                                echo '
                          <span>
                            <a href="/images/rooms/' . $roomId . '/' . $i . '.jpg" data-lightbox="gallery">
                                <img class="img-thumbnail" src="/images/rooms/' . $roomId . '/' . $i . '.jpg"  width="82" height="62">
                            </a>
                          </span>';
                            }
                            ?>
                        </div>
                        <div id="carousel-nav-small"></div>
                    </div>
                </div>
            </div>
            <div class="room-desc">
                <?= $room->costRoom ?>
                <div class="room-desc-content">
                    <div>
                        <?= $room->inRoom ?>
                    </div>
                    <div>
                        <?= $room->roomDesc ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<?php
/* @var $this yii\web\View */

use yii\widgets\Breadcrumbs;
use mp\bmicrodata\BreadcrumbsUtility;
use common\models\Languages;

// Title
$title       = $sanatorium->descr->title;
$this->title = $sanatorium->descr->title;

// Breadcrumbs
if(!empty($category)) {
    $this->params['breadcrumbs'][] = [
        'label' => $category->descr->title,
        'url' => ['categories/view', 'slug' => $category->descr->slug]
    ];
}
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => ['sanatoriums/view', 'slug' => $sanatorium->descr->slug]
];

// Meta Description
if(!empty($sanatorium->descr->meta_description)) {
    $this->registerMetaTag([
        'name' => 'description',
        'content' => $sanatorium->descr->meta_description
    ]);
}

// Slick
$slick = '$(".slider-sanatorium-image").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".slider-sanatorium-small",
});';
$this->registerJs($slick, $this::POS_READY);

$slickSanatorium = '$(".slider-sanatorium-small").slick({
    arrows: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    dots: true,
    focusOnSelect: true,
    asNavFor: ".slider-sanatorium-image",
  });';
$this->registerJs($slickSanatorium, $this::POS_READY);

// Lightbox
$this->registerJsFile('/theme/js/lightbox.min.js', [
    'depends' => 'yii\web\YiiAsset'
]);
$lightbox = 'lightbox.option({"showImageNumberLabel": false})';
$this->registerJs($lightbox, $this::POS_END);
$this->registerCssFile('/theme/css/lightbox.min.css');
?>
<div id="carousel-hotel">
    <div class="carousel-sanatorium-images">
        <div class="banners-item" style="background-image: url('<?php echo $banners[0]['url'] ?>')"></div>
<!--        <?php /*foreach($banners as $banner) { */?>
            <div class="banners-item"  style="background-image: url('<?php /*echo $banner['url'] */?>')">
            </div>
        --><?php /*} */?>
    </div>
</div>
<?php
echo Breadcrumbs::widget([
    'homeLink' => BreadcrumbsUtility::getHome(Yii::t('app', 'Головна'), Yii::$app->getHomeUrl()), // Link home page with microdata
    'links' => isset($this->params['breadcrumbs']) ? BreadcrumbsUtility::UseMicroData($this->params['breadcrumbs']) : [], // Get other links with microdata    
    'options' => [ // Set microdata for container BreadcrumbList     
        'id' => 'breadcrumbs',
        'class' => 'breadcrumb',
        'itemscope itemtype' => 'http://schema.org/BreadcrumbList'
    ],
]);
?>
<section id="main">
    <div class="sanatorium-info">
        <div class="slider-sanatorium">
            <div class="slider-sanatorium-image">
                <?php if(!empty($banners)) { ?>
                    <?php foreach($banners as $banner) { ?>
                        <div>
                            <img src="<?php echo $banner['url'] ?>" class="img-thumbnail" alt="<?php echo $banner['alt'] ?>">
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="slider-sanatorium-small">
                <?php if(!empty($banners)) { ?>
                    <?php foreach($banners as $banner) { ?>
                        <div>
                            <img src="<?php echo $banner['url'] ?>" class="img-thumbnail" width="112" height="90" alt="<?php echo $banner['alt'] ?>">
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <div class="sanatorium-desc">
            <?php
                if(!empty($sanatorium->descr->description)) {
                    echo $sanatorium->descr->description;
                }
            ?>
        </div>
    </div>

    <div class="sanatorium-second-desc">
        <?php
        if(!empty($sanatorium->descr->description_second)) {
            echo $sanatorium->descr->description_second;
        }
        ?>
    </div>

    <div id="confroom-booking">

        <?php if (($sanatorium->id !== 31) && ($sanatorium->id !== 33) && ($sanatorium->id !== 34)) : ?>
        <button onclick="showReserve('sanatorium', '<?php echo $sanatorium->id ?>')"
                class="waves-effect waves-light blue btn-flat button-booking">

            <span id="action-<?php echo $sanatorium->id ?>"
                  data-success="<?php echo Yii::t('app', 'Успішно надіслано') ?>"
                  data-error="<?php echo Yii::t('app', 'Помилка') ?>">
                      <?php echo Yii::t('app', 'Забронювати') ?>
            </span>
        </button>
        <?php endif ?>
    </div>
    <div id="reserve-modal" class="modal"></div>
</section>
<?php if(!empty($confRooms)) { ?>
    <section id="conference-rooms" class="row">
        <hr>
        <h2>
            <?php echo Yii::t('app', 'до Вашої уваги також пропонуєм конференц-зали') ?>
        </h2>
        <div class="row col s12" data-success="<?php echo Yii::t('app', 'Успішно надіслано') ?>">
            <?php foreach($confRooms as $room) { ?>
                <div class="room card col s12 m6">
                    <img src="<?php echo $room['image'] ?>" alt="<?php echo $room['name'] ?>" 
                         class="image" width="275" height="215">
                    <div class="wrapper row right">
                        <div class="name row">
                            <?php echo $room['name'] ?>
                        </div>
                        <div class="features row right-align">
                            <?php foreach($room['features'] as $feature) { ?>
                                <i class="material-icons" title="<?php echo $feature['title'] ?>">
                                    <?php echo $feature['icon'] ?>
                                </i>
                            <?php } ?>
                        </div>
                        <div class="prices row">
                            <?php foreach($room['prices'] as $price) { ?>
                                <div class="col right">
                                    <div class="price">
                                        <?php echo $price['price'] . '&nbsp;' . Yii::t('app', 'грн') ?>
                                    </div>
                                    <div class="term">
                                        <?php echo $price['term'] ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <button onclick="showReserve('room', '<?php echo $room['id'] ?>')" 
                                class="waves-effect waves-light btn button-booking">
                            <span data-success="<?php echo Yii::t('app', 'Успішно надіслано') ?>"
                                  date-error="<?php echo Yii::t('app', 'Помилка') ?>">
                                      <?php echo Yii::t('app', 'Забронювати') ?>
                            </span>
                        </button>

                    </div>
                </div>
            <?php } ?>
        </div>
    </section>
<?php } ?>

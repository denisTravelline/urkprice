<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\models\MenuItems;
use common\models\Languages;

$this->registerJsFile('/booking/static/js/jquery.min.js', [
    'position' => \yii\web\View::POS_HEAD
]);

// modals init
$modalJs = '$(".modal").modal();';
$this->registerJs($modalJs, $this::POS_READY);

AppAsset::register($this);

// Body id
if(!empty($this->context->bodyId)) {
    $bodyId = 'id="' . $this->context->bodyId . '"';
} else {
    $bodyId = '';
}
// Menu items
$menuItems = MenuItems::getMainMenu();

// Datepicker locale
if(Yii::$app->language != Languages::LANG_EN) {
    $this->registerJsFile('/theme/js/pickadate_' . Yii::$app->language . '.js');
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Languages::getIsoCode() ?>">
<head>
    <meta charset="<?php echo Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php echo Html::csrfMetaTags() ?>
    <title><?php echo Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-90808900-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter42310869 = new Ya.Metrika({
                        id:42310869,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/42310869" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <?php
    $url  = '/';
    if (isset($_SERVER['REQUEST_URI'])) {
        $url  = $_SERVER['REQUEST_URI'];
    }
    $page_main_search  = parse_url($_SERVER['REQUEST_URI']);
    $lang_arr = explode('/', $page_main_search ['path']);


    ?>
    <!-- start TL head script --><?
    $if_booking_partner = 0;
    $if_booking_partner = strpos($_SERVER['REQUEST_URI'], "booking-partners");
    ?>

    <script type="text/javascript">
        (function(w) {
            <? if($if_booking_partner) :?>
                <?php if($lang_arr[1] == 'en') : ?>
                var q = [['setContext', 'TL-INT-ukrtour-partner', 'en']];
                <?php elseif ($lang_arr[1] == 'ru') : ?>
                var q = [['setContext', 'TL-INT-ukrtour-partner', 'ru']];
                <?php else : ?>
                var q = [['setContext', 'TL-INT-ukrtour-partner', 'uk']];
                <?php endif ?>

            <?else:?>
                <?php if($lang_arr[1] == 'en') : ?>
                var q = [['setContext', 'TL-INT-ukrtour', 'en']];
                <?php elseif ($lang_arr[1] == 'ru') : ?>
                var q = [['setContext', 'TL-INT-ukrtour', 'ru']];
                <?php else : ?>
                var q = [['setContext', 'TL-INT-ukrtour', 'uk']];
                <?php endif ?>
            <?php endif ?>



            var t = w.travelline = (w.travelline || {}),
                ti = t.integration = (t.integration || {});
            ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
            if (!ti.__loader) {
                ti.__loader = true;
                var d = w.document,
                    p = d.location.protocol,
                    s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
                (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
            }
        })(window);
    </script>
    <!-- end TL head script -->

    <?php if($lang_arr[1] == 'en') : ?>
        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Hotel",
              "name": "Ukrproftour",
              "address": {
                "@type": "PostalAddress",
                "addressCountry": "Ukraine",
                "addressLocality": "Kiev",
                "addressRegion": "Kiev",
                "postalCode": "03134"
              },
              "telephone": "0(800) 211-050",
              "image": "",
              "email": "info@ukrtour.pro"
            }
            </script>
    <?php elseif ($lang_arr[1] == 'ru') : ?>
        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Hotel",
              "name": "Укрпрофтур",
              "address": {
                "@type": "PostalAddress",
                "addressCountry": "Украина",
                "addressLocality": "Киев",
                "addressRegion": "Киев",
                "postalCode": "03134"
              },
              "telephone": "0(800) 211-050",
              "image": "",
              "email": "info@ukrtour.pro"
            }
            </script>
    <?php else : ?>
        <script type="application/ld+json">
            {
              "@context": "http://schema.org",
              "@type": "Hotel",
              "name": "Укрпрофтур",
              "address": {
                "@type": "PostalAddress",
                "addressCountry": "Україна",
                "addressLocality": "Київ",
                "addressRegion": "Київ",
                "postalCode": "03134"
              },
              "telephone": "0(800) 211-050",
              "image": "",
              "email": "info@ukrtour.pro"
            }
            </script>
    <?php endif ?>


</head>
<body <?php echo $bodyId ?>>
<?php $this->beginBody() ?>
<div id="main-wrapper">
    <?php echo $this->render('_header', ['menuItems' => $menuItems]) ?>
    <?php echo $content ?>
</div>
<footer id="footer">
    <?php echo $this->render('_footer', ['menuItems' => $menuItems]) ?>
</footer>
<?php
if($lang_arr[1] == 'en')
    $lang = 'en';
elseif ($lang_arr[1] == 'ru')
    $lang = 'ru';
else
    $lang = 'uk';
?>
<? if($if_booking_partner) :?>
<script type="text/javascript">
    (function(w){
        var q=[
            ['setContext', 'TL-INT-ukrtour-partner', '<?=$lang?>'],
            ['embed', 'booking-form', {container: 'tl-booking-form-partner'}]
        ];
        var t=w.travelline=(w.travelline||{}),ti=t.integration=(t.integration||{});ti.__cq=ti.__cq?ti.__cq.concat(q):q;
        if (!ti.__loader){ti.__loader=true;var d=w.document,p=d.location.protocol,s=d.createElement('script');s.type='text/javascript';s.async=true;s.src=(p=='https:'?p:'http:')+'//eu-ibe.tlintegration.com/integration/loader.js';(d.getElementsByTagName('head')[0]||d.getElementsByTagName('body')[0]).appendChild(s);}
    })(window);
    var select = document.getElementById("tl-hotel-select");
    select.addEventListener('change', function () {
        var hotel_id = "hotel_id";
        var regex = new RegExp(/hotel_id=\d+/g);
        var getParams = window.location.search;
        var params_str = ((getParams == "") ? "?" : "") + hotel_id + "=" + this.value;
        var path = "";
        if (getParams.indexOf(hotel_id) != -1) {
            path = getParams.replace(regex, params_str);
        } else {
            path = getParams + params_str;
        }
        window.history.pushState(false, false, path);
    });
</script>

<!-- end TL Booking form script -->
<style>
    #main {background: none;}
    #tl-block-select {
        color: #3d3d3d; /* Цвет текста*/
        background-color: #e7ebef; /* Цвет фона*/
        border-top: 2px solid #c7c7c7; /* Цвет границы*/
    }
    .tl-container {
        max-width: 1040px;
        margin: 0 auto;
        position: relative;
        display: block;
    }
    #tl-block-select {
        padding: 22px 21px;
        font-family: 'Open Sans',Arial,sans-serif;
        font-size: 17px;
        border: 0;
        width: 350px;
    }
    #tl-hotel-select {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: flex-start;
        width: 100%;
        -webkit-transition: box-shadow .3s,opacity .3s;
        transition: box-shadow .3s,opacity .3s;
        border: none;
        border-radius: 0;
        -webkit-appearance:none;
        -moz-appearance:none;
        appearance: none;
        -ms-appearance:none;
        box-shadow: 0 -1px 0 0 rgba(0,0,0,.27), 0 0 0 0 transparent inset, 0 0 0 1px rgba(114,114,114,.27);
        padding: 9px 20px;
        font-size: 15px;
        font-family: 'Open Sans',Arial,sans-serif;
        color: #3d3d3d;
        background: #fff url("../../images/select.png") no-repeat 280px 20px;
        height: 42px;
        line-height: 24px;
    }
    #tl-hotel-select:hover {
        -webkit-appearance:none;
        -moz-appearance:none;
        appearance: none;
        -ms-appearance:none;
        box-shadow: 0 0 0 1px #30343b, 0 0 0 0 transparent inset, 0 0 0 1px #30343b;
    }
    select::-ms-expand {
        display: none;
    }
    #tl-anchor {font-size: 13px;}
    @media (max-width: 450px) {
        #tl-block-select {
            width: 100%;
        }
        #tl-hotel-select {
            background: #fff url("../../images/select.png") no-repeat 96% 20px;
        }
    }

</style>

<? endif ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


<?php

/**
 * Carousel
 */
use yii\web\View;
use common\models\Languages;

$slick = '$(".banners").slick({
  dots: true,
  infinite: true,
  speed: 700,
  slidesToShow: 1,
  autoplay: true,
  autoplaySpeed: 3000,
  });';
View::registerJs($slick, View::POS_READY);


/**
 *  Booking
 */
// Lang
$langCode = Languages::getIsoCode();

// Get servio app
$servio = \SORApp\Components\App::getInstance();

// Set current lang
$servio->lang = $langCode;

// js
View::registerJsFile('/booking/static/js/jquery.datetimepicker.js');
View::registerJsFile('/booking/static/js/servio.' . $langCode . '.js');
View::registerJsFile('/booking/static/js/servio.js');
?>
<?php
$url  = '/';
if (isset($_SERVER['REQUEST_URI'])) {
    $url  = $_SERVER['REQUEST_URI'];
}
$page_main_search  = parse_url($_SERVER['REQUEST_URI']);
$lang_arr = explode('/', $page_main_search ['path']);
?>
<div id="carousel-wrapper" class="">
    <div id="booking" class="" style="z-index: 2">
        <div id="tl-wrapper">
            <div id="tl-search-form">
                <noindex><a rel="nofollow" href="http://travelline.ua/">
                        <?php $lang = Languages::getLang(Yii::$app->language)['iso'];
                        if ($lang == 'ru') : ?>
                            система онлайн-бронирования
                        <?php elseif ($lang == 'en') : ?>
                            online booking system
                        <?php elseif ($lang == 'uk') : ?>
                            система онлайн-бронювання
                        <?php endif ?>
                    </a>
                </noindex>
            </div>

                <!-- start TL search form 2.0 -->
                <script type="text/javascript">
                    (function(w) {

                        <?php if($lang_arr[1] == 'en') : ?>
                        var q = [
                            ['setContext', 'TL-INT-ukrtour.main-page', 'en'],
                            ['embed', 'search-form', {
                                container: 'tl-search-form'
                            }]
                        ];
                        <?php elseif ($lang_arr[1] == 'ru') : ?>
                        var q = [
                            ['setContext', 'TL-INT-ukrtour.main-page', 'ru'],
                            ['embed', 'search-form', {
                                container: 'tl-search-form'
                            }]
                        ];
                        <?php else : ?>
                        var q = [
                            ['setContext', 'TL-INT-ukrtour.main-page', 'uk'],
                            ['embed', 'search-form', {
                                container: 'tl-search-form'
                            }]
                        ];
                        <?php endif ?>
                        var t = w.travelline = (w.travelline || {}),
                            ti = t.integration = (t.integration || {});
                        ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
                        if (!ti.__loader) {
                            ti.__loader = true;
                            var d = w.document,
                                p = d.location.protocol,
                                s = d.createElement('script');
                            s.type = 'text/javascript';
                            s.async = true;
                            s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
                            (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
                        }
                    })(window);
                </script>
                <!-- end TL search form 2.0 -->
        </div>
<!--        <div id="tl-wrapper" >-->
<!--            <div id="tl-search-form">-->
<!--                <noindex><a rel="nofollow" href="http://travelline.ua/">система онлайн-бронирования</a>-->
<!--                </noindex>-->
<!--            </div>-->
<!--        </div>-->
        <style>
            #tl-wrapper {
                width: 380px;
                background: #f6f6f6;
                padding: 30px;
            }

            #booking {
                width: 382px;
            }
        </style>
    </div>
    <div id="carousel" class="banners" style="z-index: 1">
        <?php foreach($banners as $banner) { ?>
            <div class="image ">
                <div class="banners-item valign-wrapper right-align" 
                     style="background-image: url(<?php echo $banner['img'] ?>);">
                    <div class="valign banner-text right-align">
                        <div class="type">
                            <?php echo $banner['type'] ?>
                        </div>
                        <div class="title">
                            <?php echo $banner['title'] ?>
                        </div>
                        <div class="description">
                            <?php echo $banner['description'] ?>
                        </div>
                        <div class="price">
                            <?php echo $banner['price'] ?>
                        </div>
                        <a href="<?php echo $banner['link'] ?>" 
                           class="waves-effect waves-white blue btn button-more" 
                           title="<?php echo Yii::t('app', 'Детальніше') ?>">
                               <?php echo Yii::t('app', 'Детальніше') ?>
                        </a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <a id="button-skip" class="hide-on-small-only"  href="#carousel-end">
        <i class="material-icons">expand_more</i>
    </a>
</div>
<div id="carousel-end"></div>
<div id="tl-wrapper-mobile">
    <div id="tl-search-form-mobile">
        <noindex><a rel="nofollow" href="http://travelline.ua/">
                <?php $lang = Languages::getLang(Yii::$app->language)['iso'];
                if ($lang == 'ru') : ?>
                    система онлайн-бронирования
                <?php elseif ($lang == 'en') : ?>
                    online booking system
                <?php elseif ($lang == 'uk') : ?>
                    система онлайн-бронювання
                <?php endif ?>
            </a>
        </noindex>
    </div>

    <!-- start TL search form 2.0 -->
    <script type="text/javascript">
        (function(w) {

            <?php if($lang_arr[1] == 'en') : ?>
            var q = [
                ['setContext', 'TL-INT-ukrtour', 'en'],
                ['embed', 'search-form', {
                    container: 'tl-search-form-mobile'
                }]
            ];
            <?php elseif ($lang_arr[1] == 'ru') : ?>
            var q = [
                ['setContext', 'TL-INT-ukrtour', 'ru'],
                ['embed', 'search-form', {
                    container: 'tl-search-form-mobile'
                }]
            ];
            <?php else : ?>
            var q = [
                ['setContext', 'TL-INT-ukrtour', 'uk'],
                ['embed', 'search-form', {
                    container: 'tl-search-form-mobile'
                }]
            ];
            <?php endif ?>
            var t = w.travelline = (w.travelline || {}),
                ti = t.integration = (t.integration || {});
            ti.__cq = ti.__cq ? ti.__cq.concat(q) : q;
            if (!ti.__loader) {
                ti.__loader = true;
                var d = w.document,
                    p = d.location.protocol,
                    s = d.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = (p == 'https:' ? p : 'http:') + '//eu-ibe.tlintegration.com/integration/loader.js';
                (d.getElementsByTagName('head')[0] || d.getElementsByTagName('body')[0]).appendChild(s);
            }
        })(window);
    </script>
    <!-- end TL search form 2.0 -->
</div>

<style>

    #tl-wrapper-mobile {
        display: none;
        background: #195381;
        padding: 15px 5%;
    }

    #tl-search-form-mobile {
        max-width: 1200px;
        margin: 0 auto;
    }

    @media (max-width: 1320px) {
        #booking {
            display: none;
        }

        #tl-wrapper-mobile {
            display: block;
        }
    }
</style>

<?php
/**
 * Hotels list
 */

use yii\helpers\Url;
$i = 0;
?> 
<?php
foreach($hotels as $hotel) { ?>
    <div id="hotel<?php echo $i++ ?>" class="city-hotel">
        <div style="background-image: url(<?php echo $hotel['image'] ?>); background-size: inherit;"
             onclick="location.href = '<?php echo $hotel['url'] ?>'" class="hotel-image">
        <div class="wrapper color-overlay">
            <div>
                <div class="name">
                    <a href="<?php echo $hotel['url'] ?>" title="<?php echo $hotel['name'] ?>">
                        <?php echo $hotel['name'] ?>
                    </a>
                </div>
                <?php for($stars = 0; $stars < $hotel['rating']; $stars++) { ?>
                    <div class="stars">
                        <i class="material-icons">star</i>
                    </div>
                <?php } ?>
                <div class="city">
                    <?php echo $hotel['city'] ?>
                </div>
            </div>
        </div>
    </div >
        <div class="hotel-descr">
            <p class="hotel-text"><?=$hotel["text"];?></p>
            <p>
                <a href="<?php echo $hotel['url'] ?>" title="<?php echo Yii::t('app', 'Детальніше') ?>" class="button-more waves-effect waves-blue btn-flat" style="
    color: #2196f3;
    padding: 11px 0;
    margin: 7px 0;
    height: 55px;
    font-weight: 500;
    font-size: 1.15rem;
">
                    <span><?php echo Yii::t('app', 'Детальніше') ?></span>
                    <i hide-xs="" class="material-icons right">
                        keyboard_arrow_down
                    </i>
                </a>
                <?$tlId = $hotel["tl_id"];?>
                <? if ($tlId):?>
                    <a href="<?php echo Url::to(['/pages/reservation', 'hotel_id' => $tlId]) ?>"
                       class="waves-effect waves-light blue btn"
                       title="<?php echo Yii::t('app', 'Забронювати') ?>"
                       style="padding: 0 5px; margin-top: -5px;">
                        <?php echo Yii::t('app', 'Забронювати') ?>
                    </a>
                <?endif;?>
            </p>
    </div>
    </div>
<?php } ?>

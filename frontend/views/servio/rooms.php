<?php

use common\models\Languages;
use frontend\models\Rooms;

/* @var $this yii\web\View */

$i   = 1;
$all = Yii::$app->params['showRooms'];

// Lang 
$langIso = Languages::getIsoCode();

$features = Rooms::getRoomsFeatures($hotel);
?>
<?php if(!empty($rooms)) { ?>
    <script>
        console.log('<?php echo Languages::getLang(Yii::$app->language)['iso'] ?>');
    </script>
    <div id="rooms-header" class="row">
        <!--<h2 class="col left"><?php /*echo Yii::t('app', 'Наші номери') */?></h2>-->
        <h2><?php echo Yii::t('app', 'Наші номери') ?></h2>
        <div id="sort" class="input-field right col">
            <select id="sort-select">
                <option value="price-asc"><?php echo Yii::t('app', 'зростанням ціни') ?></option>
                <option value="price-desc"><?php echo Yii::t('app', 'спаданням ціни') ?></option>
                <option value="beds-asc"><?php echo Yii::t('app', 'зростанням кількості місць') ?></option>
                <option value="beds-desc"><?php echo Yii::t('app', 'спаданням кількості місць') ?></option>
            </select>
            <label id="sort-title" for="sort-select" class="hide-on-small-only">
                <?php echo Yii::t('app', 'Сортувати за ') ?>
            </label>
        </div>
    </div>
    <div id="rooms" class="row">
        <pre style="display: none;"><?var_dump($rooms);?></pre>
        <?php foreach($rooms as $room) { ?>
            <pre style="display: none;"><?var_dump($room);?></pre>
            <div class="room-card room card col s12 l6<?php echo ($i++ > $all ) ? ' hide' : ''; ?>">
                <div class="room-item">

                    <!-- Start Slider Anons Room -->

                    <?php
                        if(($room['hotelType'] == 10260) || ($room['hotelType'] == 10268) || ($room['hotelType'] == 10273)) {
                            $roomId = $room['id'];

                            $dir = 'images/rooms/' . $roomId . '/';
                            $file = scandir($dir);
                            $countDir = count($file)-2;

                            echo '<div class="main-rooms-slider">';
                                for ($i = 1; $i <= $countDir; $i++) {
                                    echo '
                                         <div>
                                            <img class="image" src="/' . $dir . $i . '.jpg" width="355" height="338">
                                        </div>
                                    ';
                                }
                            echo '</div>';
                        } else {
                            $roomAnonsImage = $room['image'];
                            $roomAnonsName = $room['name'];

                            echo '
                                 <div>
                                    <img class="image" src='. $roomAnonsImage .' width="355" height="338" alt='. $roomAnonsName .'>
                                </div>
                            ';
                        }
                    ?>
                    <!--<img src="<?php /*echo $room['image'] */?>" alt="<?php /*echo $room['name'] */?>"
                         class="image" width="355" height="338">-->

                    <!-- End Slider Anons Room -->

                    <div class="room-content">
                        <div class="room-content__name">
                            <?php echo $room['name'] ?>
                        </div>
                        <div class="room-content__price">
                            <?php echo Yii::t('app', 'від') ?>&nbsp;
                            <?php echo $room['price'] ?>&nbsp;
                            <?php echo Yii::t('app', 'грн') ?>
                        </div>
                        <div class="room-content__term">
                            <?php echo Yii::t('app', 'за період') ?>
                        </div>
                        <!--<div class="features row right">

                            <?php /*if(!empty($features[$room['room_id']])) { */?>
                                <?php /*foreach($features[$room['room_id']] as $feature) { */?>
                                    <i class="material-icons">
                                        <?php /*echo $feature */?>
                                    </i>
                                <?php /*} */?>
                            <?php /*} */?>
                        </div>-->
                        <div class="room-content__button buttons">
                            <?php /* if(!empty($room['slug'])) { */?><!--
                            <a href="/<?php /*echo Languages::getLang(Yii::$app->language)['iso'] */?>/rooms/<?php /*echo $room['slug'] */?>" title="<?php /*echo Yii::t('app', 'Детальніше') */?>"
                            --><?php /*} else { */?>
                            <a title="<?php echo Yii::t('app', 'Детальніше') ?>"
                            <?php /*} */?>
                               class="room-content__detail button-more waves-effect waves-blue btn-flat">
                                <span class="hide-on-small-only"><?php echo Yii::t('app', 'Детальніше') ?></span>
                                <i hide-xs class="material-icons hide-on-small-only right">
                                    keyboard_arrow_down
                                </i>
                                <i class="material-icons hide-on-med-and-up">
                                    arrow_forward
                                </i>
                            </a>
                            <div class="booking-form">
        <!--                        <form action="/booking/search?hashKey=--><?php //echo $room['hashKey'] ?><!--&refStep=1&lang=uk"-->
        <!--                              method="post">-->
        <!--                            <input type="hidden" name="selectedRoom[0]" value="--><?php //echo $room['id'] ?><!--">-->
        <!--                            <button type="submit" class="button-booking waves-effect waves-light btn">-->
        <!--                                --><?php //echo Yii::t('app', 'Забронювати') ?>
        <!--                            </button>-->
        <!--                        </form>-->

                                <a href="/<?php echo Languages::getLang(Yii::$app->language)['iso'] ?>/pages/reservation?room-type=<?php echo $room['roomType'] ?>&hotel_id=<?php echo $room['hotelType'] ?>" class="button-booking"><?php echo Yii::t('app', 'Забронювати') ?></a>
                            </div>
                        </div>
                        <?php /*if(!empty($room['descr'])) { */?><!--
                            <div class="modal room-more" id="descr<?php /*echo $room['room_id'] */?>">
                                <div class="modal-content">
                                    <h4><?php /*echo $room['name'] */?></h4>
                                    <button class="waves-effect waves-blue btn-flat modal-close">
                                        <i class="material-icons">clear</i>
                                    </button>
                                    <p>
                                        <?php /*echo $room['descr'] */?>
                                    </p>
                                </div>
                            </div>
                        --><?php /*} */?>
                    </div>
                </div>
                    <?php if(!empty($room['descr'])) { ?>
                        <div id="descr<?php echo $room['room_id'] ?>" class="room-modal">
                            <?php echo $room['descr'] ?>
                        </div>
                    <?php } ?>
            </div>
        <?php } ?>
    </div>
    <?php if($i > $all) { ?>
        <div class="center-align">
            <button onclick="showAll();" id="button-all-rooms" class="waves-effect waves-white btn-flat">
                <?php echo Yii::t('app', 'Показати всі номери') ?>
            </button>
        </div>
    <?php } ?>

<?php } /*else { ?>
    <div id="rooms-header" class="row">
        <p class="col left">
            <?php echo Yii::t('app', 'Вільних номерів не знайдено') ?>
        </p>
    </div>
<?php } */?>


